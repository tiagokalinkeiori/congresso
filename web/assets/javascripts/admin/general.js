;(function(window,$) {

    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
        
        var quotaUpdate = $(".trigger-quota-update");
        $(quotaUpdate).each(function(key, item) {
            var $item = $(item)
                , button = $item.find("button")
                , text = $item.find("input[type=number]")
                , ajax = $item.data('ajax');
                
            $(button).on("click", function(e) {
                e.preventDefault();
                var ajax = $(this).parents(".trigger-quota-update").data("ajax");
                ajax += "?quotas="+ $(text).val();

                $.ajax(ajax).done(function(response) {
                    window.location.reload();
                });
            });
        });
        
        var dueDateUpdate = $(".trigger-duedate-update");
        $(dueDateUpdate).each(function(key, item) {
            var $item = $(item)
                , button = $item.find("button")
                , text = $item.find("input[type=text]")
                , ajax = $item.data('ajax');
                
            $(button).on("click", function(e) {
                e.preventDefault();
                var ajax = $(this).parents(".trigger-duedate-update").data("ajax");
                ajax += "?dueDate="+ $(text).val();

                $.ajax(ajax).done(function(response) {
                    window.location.reload();
                });
            });
        });
        
        var priceUpdate = $(".trigger-price-update");
        $(priceUpdate).each(function(key, item) {
            var $item = $(item)
                , button = $item.find("button")
                , text = $item.find("input[type=text]")
                , ajax = $item.data('ajax');
                
            $(button).on("click", function(e) {
                e.preventDefault();
                var ajax = $(this).parents(".trigger-price-update").data("ajax");
                ajax += "?price="+ $(text).val();

                $.ajax(ajax).done(function(response) {
                    window.location.reload();
                });
            });
        });

        $(".trigger-wysiwyg").wysihtml5({
            locale: 'pt-BR',
            toolbar: {
                fa: true,
                html: true
            }
        });
        
        mapForm();

    });
    
    function mapForm() {
        $('.trigger-mask-date').mask('99/99/9999');
        $('.trigger-mask-money').maskMoney({thousands: '.', decimal: ',', precision: 2});
            
        $('.trigger-add-new-item').on("click", function(e) {
            e.preventDefault();

            var $ref = $($(this).data('ref'))
                , ajax = $(this).data('ajax')
                , count = $ref.find('.item').length;

            if (ajax.indexOf('?') != -1) {
                ajax += "&posicao="+ count;
            } else {
                ajax += "?posicao="+ count;
            }

            $.ajax(ajax).done(function(resp) {
                $ref.append($.parseHTML(resp));

                mapForm();
            });
        });
        
        $(".trigger-remove-item").on("click", function(e) {
            $(this).parents(".item").remove();
        });
    }

})(window, jQuery);