;(function(window,$) {

    $(function() {
        if ($('.trigger-mask-date').length) {
            $('.trigger-mask-date').mask('99/99/9999');
        }
        if ($('.trigger-mask-datetime').length) {
            $('.trigger-mask-datetime').mask('99/99/9999 99:99');
        }
        if ($('.trigger-mask-cpf').length) {
            $('.trigger-mask-cpf').mask('999.999.999-99');
        }
        if ($('.trigger-mask-money').length) {
            $('.trigger-mask-money').maskMoney({thousands: '.', decimal: ',', precision: 2});
        }
        if ($('.trigger-mask-zipcode').length) {
            $('.trigger-mask-zipcode').mask('99.999-999');
        }
        if ($('.trigger-mask-phone').length) {
            $('.trigger-mask-phone').mask('(99) 9999-9999?9');
        }

        var $triggerTargetLoad = $(".trigger-target-load");
            
        $triggerTargetLoad.on("change", function(e) {
            e.preventDefault();
            
            var $that = $(this)
                , ajax = $that.data('ajax')
                , id = $that.val();
                
            ajax += "?id="+ id;
            
            $.ajax(ajax).done(function(response) {
                var $target = $($that.data("target"));
                $target.find("option").remove();
                for(var i = 0; i < response.result.length; i++) {
                    var item = response.result[i]
                        , $option = $("<option>").val(item.Id).text(item.Name);
                    
                    $target.append($option);
                }
                
                if (typeof $that.data('text') != 'undefined') {
                   $($that.data("text")).html($.parseHTML(response.text));
                }
            });
        });

    });

})(window, jQuery);