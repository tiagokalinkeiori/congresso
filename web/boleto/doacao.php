<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

$loader = require_once __DIR__.'/../../app/bootstrap.php.cache';
require_once __DIR__.'/../../app/AppKernel.php';

mb_internal_encoding('UTF-8');

$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();

$request = Request::createFromGlobals();
$response = $kernel->handle($request);

$payment = \AppBundle\Model\DonationQuery::create()
        ->findOneById((int) $request->query->get('id'));

if (null === $payment) {
    return RedirectResponse("/", 301);
}

// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 5;
$taxa_boleto = 2;
$data_venc = "31/12/2016";
$valor_cobrado = $payment->getPrice();
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

if(!function_exists('formata_numdoc')) {
    function formata_numdoc($num, $tamanho)
    {
        while(strlen($num) < $tamanho) {
            $num = "0".$num; 
        }
        return $num;
    }
}

$IdDoSeuSistemaAutoIncremento = (string) $request->query->get('id'); // Deve informar um numero sequencial a ser passada a função abaixo, Até 6 dígitos
$agencia = "3271"; // Num da agencia, sem digito
$conta = "41994"; // Num da conta, sem digito
$convenio = "0425486"; //Número do convênio indicado no frontend

$NossoNumero = "9" . formata_numdoc($IdDoSeuSistemaAutoIncremento, 6);
$qtde_nosso_numero = strlen($NossoNumero);
$sequencia = formata_numdoc($agencia,4).formata_numdoc(str_replace("-","",$convenio),10).formata_numdoc($NossoNumero,7);
$cont=0;
$calculoDv = '';
for($num = 0; $num <= strlen($sequencia); $num++) {
    $cont++;
    if($cont == 1) {
        // constante fixa Sicoob » 3197 
        $constante = 3;
    }
    if($cont == 2) {
        $constante = 1;
    }
    if($cont == 3) {
        $constante = 9;
    }
    if($cont == 4) {
        $constante = 7;
        $cont = 0;
    }
    $calculoDv = $calculoDv + (substr($sequencia,$num,1) * $constante);
}
$Resto = $calculoDv % 11;
$Dv = 11 - $Resto;
if ($Dv == 0) $Dv = 0;
if ($Dv == 1) $Dv = 0;
if ($Dv > 9) $Dv = 0;
$dadosboleto["nosso_numero"] = $NossoNumero . $Dv;

/*************************************************************************
 * +++
 *************************************************************************/



$dadosboleto["numero_documento"] = $IdDoSeuSistemaAutoIncremento;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $payment->getSubscribe()->getName();
$dadosboleto["endereco1"] = $payment->getSubscribe()->getAddress();
$dadosboleto["endereco2"] = $payment->getSubscribe()->getCity();

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = "45&ordm; Congresso da JELB e 1&ordm; Encontro Latino-Americano";
$dadosboleto["demonstrativo2"] = "";
$dadosboleto["demonstrativo3"] = "";

// INSTRUÇÕES PARA O CAIXA
$dadosboleto["instrucoes1"] = "- Sr. Caixa, n&atilde;o receber ap&oacute;s o vencimento";
$dadosboleto["instrucoes2"] = "";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "1";
$dadosboleto["valor_unitario"] = $payment->getPrice();
$dadosboleto["aceite"] = "N";
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DM";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
// DADOS ESPECIFICOS DO SICOOB
$dadosboleto["modalidade_cobranca"] = "01";
$dadosboleto["numero_parcela"] = "901";


// DADOS DA SUA CONTA - BANCO SICOOB
$dadosboleto["agencia"] = $agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $conta; // Num da conta, sem digito

// DADOS PERSONALIZADOS - SICOOB
$dadosboleto["convenio"] = $convenio; // Num do convênio - REGRA: No máximo 7 dígitos
$dadosboleto["carteira"] = "1";

// SEUS DADOS
$dadosboleto["identificacao"] = "JELB";
$dadosboleto["cpf_cnpj"] = "10.429.854/0001-41";
$dadosboleto["endereco"] = "AV CORONEL LUCAS DE OLIVEIRA, 894, MONT SERRAT, CEP: 90.440010";
$dadosboleto["cidade_uf"] = "PORTO ALEGRE - RS";
$dadosboleto["cedente"] = "JUVENTUDE EVANGELICA LUTERANA DO BRASIL JELB";

// NÃO ALTERAR!
include("include/funcoes_bancoob.php");
include("include/layout_bancoob.php");

$payment->setOwerNumber($dadosboleto['nosso_numero'])
        ->setDigitableLine($dadosboleto['linha_digitavel'])
        ->save();