<?php

namespace AppBundle\Form\SubscribeArea;

use AppBundle\EventListener\SubscribeEditEventListener;
use AppBundle\Model\CountryQuery;
use AppBundle\Model\DisplacementQuery;
use AppBundle\Model\DistrictQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class SubscribeEditForm extends AbstractType
{

    private $locale;

    private $container;

    public function __construct(ContainerInterface $container, $locale)
    {

        $this->container = $container;
        $this->locale = $locale;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        $translator = $this->container->get('translator');

        $builder->add("Country", "model", array(
            "empty_value" => $translator->trans("form.empty.country", array(), null, $this->locale),
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
            "class" => "\AppBundle\Model\Country",
            "property" => "Name",
            "query" => CountryQuery::create()->orderByName(),
        ));
        $builder->add("State", "choice", array(
            "empty_value" => $translator->trans("form.empty.state", array(), null, $this->locale),
            "choices" => array(),
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("District", "model", array(
            "required" => false,
            "constraints" => array(),
            "class" => "\AppBundle\Model\District",
            "property" => "Name",
            "query" => DistrictQuery::create()->orderByName(),
            "empty_value" => $translator->trans("form.empty.district", array(), null, $this->locale),
        ));
        
        $builder->add("DisplacementId", "choice", array(
            "choices" => $this->getDisplacements(),
            "required" => false,
            "constraints" => array(),
            "empty_value" => $translator->trans("form.empty.displacement", array(), null, $this->locale),
        ));
        $builder->add("Email", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
                new Email(),
            ),
            "attr" => array(
                "readonly" => "readonly",
            ),
        ));
        $builder->add("Password", "password", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("Name", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add('Birthday', 'date', array(
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'constraints' => array(
                new NotBlank(),
                new Date(),
            ),
        ));
        $builder->add("Register", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Document", "text", array(
            "required" => false,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Genre", "choice", array(
            "choices" => $this->getGenres(),
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Phone", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Address", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Number", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Complement", "text", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("City", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Region", "text", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("ZipCode", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Member", "checkbox", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("Confirmed", "checkbox", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("Minister", "text", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("UnityName", "text", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("SpecialNeed", "textarea", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("Comment", "textarea", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("Visitor", "checkbox", array(
            "required" => false,
            "constraints" => array(),
        ));
        
        $builder->addEventSubscriber(new SubscribeEditEventListener($this->container, $this->locale));
    }
    
    public function getName()
    {
        return 'form';
    }

    protected function getDisplacements()
    {
        
        $array = array();
        $displacements = DisplacementQuery::create()
                ->useLanguageQuery()
                    ->filterByCode($this->locale)
                ->endUse()
                ->find();
        foreach ($displacements as $item) {
            $array[$item->getId()] = $item->getName();
        }
        
        return $array;
    }

    protected function getGenres()
    {
        
        $translator = $this->container->get('translator');
        return array(
           "M" => $translator->trans("form.label.male", array(), null, $this->locale),
           "F" => $translator->trans("form.label.female", array(), null, $this->locale),
        );
    }
}