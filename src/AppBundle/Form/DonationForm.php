<?php

namespace AppBundle\Form;

use AppBundle\Form\DataTransformer\FloatTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class DonationForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        
        $builder->add("Name", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Email", "text", array(
            "required" => true,
            "constraints" => array(
                new Email(),
                new NotBlank(),
            ),
        ));
        $builder->add("Phone", "text", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("Observation", "textarea", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add($builder->create("Price", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ))->addModelTransformer(new FloatTransformer()));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => '\AppBundle\Model\Donation',
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}
