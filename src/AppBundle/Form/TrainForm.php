<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class TrainForm extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        
        $builder->add("SubscribeId", "hidden", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("ResponsibleName", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("ResponsibleEmail", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
                new Email(),
            ),
        ));
        $builder->add("ResponsiblePhone", "text", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("Space", "integer", array(
            "required" => false,
            "constraints" => array(),
            "empty_data" => 0,
        ));
        $builder->add("Available", "integer", array(
            "required" => false,
            "constraints" => array(),
            "empty_data" => 0,
        ));
        $builder->add("Description", "textarea", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => '\AppBundle\Model\Train',
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        
        return 'form';
    }
}
