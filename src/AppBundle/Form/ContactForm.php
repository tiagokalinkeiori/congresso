<?php

namespace AppBundle\Form;

use AppBundle\EventListener\RegistrationEventListener;
use AppBundle\Model\CountryQuery;
use AppBundle\Model\DisplacementQuery;
use AppBundle\Model\DistrictQuery;
use AppBundle\Model\SubscribeTypeQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactForm extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        
        $builder->add("Email", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
                new Email(),
            ),
        ));
        $builder->add("Name", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Phone", "text", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("Subject", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Message", "textarea", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
    }
    
    public function getName()
    {
        return 'form';
    }
}