<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;

class TransferForm extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        
        $builder->add("SubscribeId", "hidden", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Arrival", "choice", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
            "choices" => array(
                "" => "Selecione",
                "AEROPORTO HERCÍLIO LUZ" => "AEROPORTO HERCÍLIO LUZ",
                "RODOVIÁRIA" => "RODOVIÁRIA",
                "PARTICULAR (na rodoviária)" => "PARTICULAR (na rodoviária)",
            ),
        ));
        $builder->add("ComeBack", "choice", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
            "choices" => array(
                "" => "Selecione",
                "AEROPORTO HERCÍLIO LUZ" => "AEROPORTO HERCÍLIO LUZ",
                "RODOVIÁRIA" => "RODOVIÁRIA",
                "PARTICULAR (na rodoviária)" => "PARTICULAR (na rodoviária)",
            ),
        ));
        $builder->add('ArrivalDate', 'datetime', array(
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy HH:mm',
            'constraints' => array(
                new NotBlank(),
                new Date(),
            ),
            'with_seconds' => false,
        ));
        $builder->add('ComeBackDate', 'datetime', array(
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy HH:mm',
            'constraints' => array(
                new NotBlank(),
                new Date(),
            ),
            'with_seconds' => false,
        ));
        $builder->add('ArrivalInformation', 'textarea', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
        $builder->add('ComeBackInformation', 'textarea', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
        $builder->add("Phone", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => '\AppBundle\Model\Transfer',
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        
        return 'form';
    }
}
