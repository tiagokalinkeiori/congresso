<?php

namespace AppBundle\Form\DataTransformer;

use AppBundle\Util\NumberUtil;
use Symfony\Component\Form\DataTransformerInterface;

class FloatTransformer implements DataTransformerInterface
{

    /**
     * 
     * @param float $number
     * @return string
     */
    public function transform($number)
    {

        return number_format($number, 2, ',', '.');
    }

    /**
     * 
     * @param string $number
     * @return float
     */
    public function reverseTransform($number)
    {

        return NumberUtil::strToFloat($number);
    }

}
