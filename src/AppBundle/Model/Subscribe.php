<?php

namespace AppBundle\Model;

use AppBundle\Model\om\BaseSubscribe;

class Subscribe extends BaseSubscribe
{
    
    public function getPaidPrice()
    {
        
        $paidPrice = SubscribePaymentQuery::create('sp')
                ->withColumn('SUM(sp.Price)', 'Total')
                ->select('Total')
                ->filterBySubscribeId($this->getId())
                ->filterByPaid(1)
                ->findOne();
        
        return $paidPrice;
    }
    
    public function getPaidQuota()
    {
        
        $paidQuota = SubscribePaymentQuery::create('sp')
                ->withColumn('SUM(sp.Price)', 'Total')
                ->select('Total')
                ->filterBySubscribeId($this->getId())
                ->filterByPaid(1)
                ->count();
        
        return $paidQuota;
    }
    
    public function countPaidQuota()
    {
        
        return (int) SubscribePaymentQuery::create('sp')
                ->filterBySubscribeId($this->getId())
                ->filterByPaid(1)
                ->count();
    }
}
