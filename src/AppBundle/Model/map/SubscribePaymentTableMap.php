<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'inscricao_pagamento' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class SubscribePaymentTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.SubscribePaymentTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('inscricao_pagamento');
        $this->setPhpName('SubscribePayment');
        $this->setClassname('AppBundle\\Model\\SubscribePayment');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('inscricao_pagamento_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('inscricao_id', 'SubscribeId', 'INTEGER', 'inscricao', 'inscricao_id', true, null, null);
        $this->addColumn('data_geracao', 'CreateDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_vencimento', 'DueDate', 'DATE', true, null, null);
        $this->addColumn('data_pagamento', 'PaymentDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('valor', 'Price', 'DECIMAL', true, 10, null);
        $this->addColumn('pago', 'Paid', 'BOOLEAN', false, 1, null);
        $this->addColumn('primeira', 'First', 'BOOLEAN', false, 1, null);
        $this->addColumn('cancelada', 'Cancel', 'BOOLEAN', false, 1, null);
        $this->addColumn('url', 'Url', 'VARCHAR', false, 255, null);
        $this->addColumn('nosso_numero', 'OwerNumber', 'VARCHAR', false, 30, null);
        $this->addColumn('linha_digitavel', 'DigitableLine', 'VARCHAR', false, 255, null);
        $this->addColumn('enviado', 'Sent', 'BOOLEAN', false, 1, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::MANY_TO_ONE, array('inscricao_id' => 'inscricao_id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // SubscribePaymentTableMap
