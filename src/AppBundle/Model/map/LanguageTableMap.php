<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'lingua' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class LanguageTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.LanguageTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('lingua');
        $this->setPhpName('Language');
        $this->setClassname('AppBundle\\Model\\Language');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('lingua_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nome', 'Name', 'VARCHAR', true, 40, null);
        $this->addColumn('codigo', 'Code', 'VARCHAR', true, 5, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Displacement', 'AppBundle\\Model\\Displacement', RelationMap::ONE_TO_MANY, array('lingua_id' => 'lingua_id', ), null, null, 'Displacements');
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::ONE_TO_MANY, array('lingua_id' => 'lingua_id', ), 'SET NULL', 'CASCADE', 'Subscribes');
        $this->addRelation('SubscribeTypeName', 'AppBundle\\Model\\SubscribeTypeName', RelationMap::ONE_TO_MANY, array('lingua_id' => 'lingua_id', ), null, null, 'SubscribeTypeNames');
        $this->addRelation('Notice', 'AppBundle\\Model\\Notice', RelationMap::ONE_TO_MANY, array('lingua_id' => 'lingua_id', ), null, null, 'Notices');
        $this->addRelation('Text', 'AppBundle\\Model\\Text', RelationMap::ONE_TO_MANY, array('lingua_id' => 'lingua_id', ), null, null, 'Texts');
    } // buildRelations()

} // LanguageTableMap
