<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'caravana' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class TrainTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.TrainTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('caravana');
        $this->setPhpName('Train');
        $this->setClassname('AppBundle\\Model\\Train');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('caravana_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('inscricao_id', 'SubscribeId', 'INTEGER', 'inscricao', 'inscricao_id', true, null, null);
        $this->addColumn('nome_responsavel', 'ResponsibleName', 'VARCHAR', true, 255, null);
        $this->addColumn('email_responsabel', 'ResponsibleEmail', 'VARCHAR', true, 120, null);
        $this->addColumn('telefone_responsavel', 'ResponsiblePhone', 'VARCHAR', false, 40, null);
        $this->addColumn('vagas', 'Space', 'INTEGER', false, null, 0);
        $this->addColumn('disponiveis', 'Available', 'INTEGER', false, null, 0);
        $this->addColumn('descricao', 'Description', 'CLOB', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::MANY_TO_ONE, array('inscricao_id' => 'inscricao_id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // TrainTableMap
