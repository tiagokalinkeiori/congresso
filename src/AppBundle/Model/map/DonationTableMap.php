<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'doacao' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class DonationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.DonationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('doacao');
        $this->setPhpName('Donation');
        $this->setClassname('AppBundle\\Model\\Donation');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('doacao_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('inscricao_id', 'SubscribeId', 'INTEGER', 'inscricao', 'inscricao_id', false, null, null);
        $this->addColumn('nome', 'Name', 'VARCHAR', false, 180, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 160, null);
        $this->addColumn('telefone', 'Phone', 'VARCHAR', false, 20, null);
        $this->addColumn('valor', 'Price', 'DECIMAL', true, 12, 0);
        $this->addColumn('pago', 'Paid', 'BOOLEAN', false, 1, false);
        $this->addColumn('nosso_numero', 'OwerNumber', 'VARCHAR', false, 40, null);
        $this->addColumn('linha_digitavel', 'DigitableLine', 'VARCHAR', false, 255, null);
        $this->addColumn('observacao', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('data', 'Date', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_pagamento', 'PaymentDate', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::MANY_TO_ONE, array('inscricao_id' => 'inscricao_id', ), 'SET NULL', 'CASCADE');
    } // buildRelations()

} // DonationTableMap
