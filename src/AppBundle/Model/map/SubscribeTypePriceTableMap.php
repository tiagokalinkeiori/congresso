<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'inscricao_tipo_valor' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class SubscribeTypePriceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.SubscribeTypePriceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('inscricao_tipo_valor');
        $this->setPhpName('SubscribeTypePrice');
        $this->setClassname('AppBundle\\Model\\SubscribeTypePrice');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('inscricao_tipo_valor_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('inscricao_tipo_id', 'SubscribeTypeId', 'INTEGER', 'inscricao_tipo', 'inscricao_tipo_id', true, null, 0);
        $this->addColumn('valor', 'Price', 'DECIMAL', true, 10, null);
        $this->addColumn('parcelas', 'Quota', 'INTEGER', false, null, 1);
        $this->addColumn('valor_parcelado', 'InstallmentValue', 'DECIMAL', false, 10, null);
        $this->addColumn('inicio', 'Start', 'DATE', true, null, null);
        $this->addColumn('fim', 'End', 'DATE', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SubscribeType', 'AppBundle\\Model\\SubscribeType', RelationMap::MANY_TO_ONE, array('inscricao_tipo_id' => 'inscricao_tipo_id', ), null, null);
    } // buildRelations()

} // SubscribeTypePriceTableMap
