<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'texto_pagamento' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class PaymentTextTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.PaymentTextTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('texto_pagamento');
        $this->setPhpName('PaymentText');
        $this->setClassname('AppBundle\\Model\\PaymentText');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('texto_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('pais_id', 'CountryId', 'INTEGER', 'pais', 'pais_id', false, null, null);
        $this->addColumn('texto', 'Text', 'CLOB', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Country', 'AppBundle\\Model\\Country', RelationMap::MANY_TO_ONE, array('pais_id' => 'pais_id', ), null, 'CASCADE');
    } // buildRelations()

} // PaymentTextTableMap
