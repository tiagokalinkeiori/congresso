<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'estado' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class StateTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.StateTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('estado');
        $this->setPhpName('State');
        $this->setClassname('AppBundle\\Model\\State');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('estado_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('pais_id', 'CountryId', 'INTEGER', 'pais', 'pais_id', true, null, null);
        $this->addColumn('nome', 'Name', 'VARCHAR', true, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Country', 'AppBundle\\Model\\Country', RelationMap::MANY_TO_ONE, array('pais_id' => 'pais_id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::ONE_TO_MANY, array('estado_id' => 'estado_id', ), null, null, 'Subscribes');
    } // buildRelations()

} // StateTableMap
