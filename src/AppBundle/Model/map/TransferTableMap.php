<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'translado' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class TransferTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.TransferTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('translado');
        $this->setPhpName('Transfer');
        $this->setClassname('AppBundle\\Model\\Transfer');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('inscricao_id', 'SubscribeId', 'INTEGER' , 'inscricao', 'inscricao_id', true, null, null);
        $this->addColumn('telefone', 'Phone', 'VARCHAR', false, 22, null);
        $this->addColumn('chegada_por', 'Arrival', 'VARCHAR', false, 50, null);
        $this->addColumn('volta_por', 'ComeBack', 'VARCHAR', false, 50, null);
        $this->addColumn('data_chegada', 'ArrivalDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_volta', 'ComeBackDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('dados_chegada', 'ArrivalInformation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('dados_volta', 'ComeBackInformation', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::MANY_TO_ONE, array('inscricao_id' => 'inscricao_id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // TransferTableMap
