<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'inscricao' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class SubscribeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.SubscribeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('inscricao');
        $this->setPhpName('Subscribe');
        $this->setClassname('AppBundle\\Model\\Subscribe');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('inscricao_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('usuario_id', 'UserId', 'INTEGER', 'usuario', 'usuario_id', true, null, null);
        $this->addForeignKey('pais_id', 'CountryId', 'INTEGER', 'pais', 'pais_id', true, null, null);
        $this->addForeignKey('estado_id', 'StateId', 'INTEGER', 'estado', 'estado_id', false, null, null);
        $this->addForeignKey('distrito_id', 'DistrictId', 'INTEGER', 'distrito', 'distrito_id', false, null, null);
        $this->addForeignKey('inscricao_tipo_id', 'SubscribeTypeId', 'INTEGER', 'inscricao_tipo', 'inscricao_tipo_id', true, null, null);
        $this->addForeignKey('deslocamento_id', 'DisplacementId', 'INTEGER', 'deslocamento', 'deslocamento_id', false, null, null);
        $this->addForeignKey('lingua_id', 'LanguageId', 'INTEGER', 'lingua', 'lingua_id', false, null, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 120, null);
        $this->addColumn('nome', 'Name', 'VARCHAR', true, 120, null);
        $this->addColumn('data_nascimento', 'Birthday', 'DATE', true, null, null);
        $this->addColumn('nome_responsavel', 'ResponsibleName', 'VARCHAR', false, 120, null);
        $this->addColumn('fone_responsavel', 'ResponsiblePhone', 'VARCHAR', false, 20, null);
        $this->addColumn('registro', 'Register', 'CHAR', false, 20, null);
        $this->addColumn('orgao_expedidor', 'Dispatcher', 'VARCHAR', false, 45, null);
        $this->addColumn('orgao_expedidor_uf', 'DispatcherPlace', 'CHAR', false, 2, null);
        $this->addColumn('documento', 'Document', 'CHAR', false, 20, null);
        $this->addColumn('sexo', 'Genre', 'CHAR', true, null, null);
        $this->addColumn('telefone', 'Phone', 'VARCHAR', true, 20, null);
        $this->addColumn('endereco', 'Address', 'VARCHAR', true, 140, null);
        $this->addColumn('numero', 'Number', 'CHAR', true, 6, null);
        $this->addColumn('complemento', 'Complement', 'VARCHAR', false, 80, null);
        $this->addColumn('cidade', 'City', 'VARCHAR', true, 80, null);
        $this->addColumn('bairro', 'Region', 'VARCHAR', false, 60, null);
        $this->addColumn('cep', 'ZipCode', 'CHAR', true, 10, null);
        $this->addColumn('membro_ielb', 'Member', 'BOOLEAN', false, 1, true);
        $this->addColumn('confirmado', 'Confirmed', 'BOOLEAN', false, 1, null);
        $this->addColumn('delegado_plenaria', 'Delegate', 'BOOLEAN', false, 1, false);
        $this->addColumn('presidente_uj', 'UnityPresident', 'VARCHAR', false, 120, null);
        $this->addColumn('pastor', 'Minister', 'VARCHAR', false, 120, null);
        $this->addColumn('uj', 'UnityName', 'VARCHAR', false, 60, null);
        $this->addColumn('responsavel_caravana', 'TrainResponsible', 'VARCHAR', false, 120, null);
        $this->addColumn('parcelas', 'Quota', 'INTEGER', true, null, 1);
        $this->addColumn('valor_total', 'Price', 'DOUBLE', true, null, 0);
        $this->addColumn('necessidades_especiais', 'SpecialNeed', 'VARCHAR', false, 255, null);
        $this->addColumn('comentarios', 'Comment', 'VARCHAR', true, 255, null);
        $this->addColumn('primeiro_congresso', 'FirstCongress', 'BOOLEAN', true, 1, false);
        $this->addColumn('quantidade_congresso', 'AmountCongress', 'TINYINT', false, 6, null);
        $this->addColumn('isento', 'Free', 'BOOLEAN', false, 1, false);
        $this->addColumn('organizacao', 'Organization', 'BOOLEAN', false, 1, false);
        $this->addColumn('visitante', 'Visitor', 'BOOLEAN', false, 1, false);
        $this->addColumn('cancelado', 'Canceled', 'BOOLEAN', false, 1, false);
        $this->addColumn('data_inscricao', 'Date', 'TIMESTAMP', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('District', 'AppBundle\\Model\\District', RelationMap::MANY_TO_ONE, array('distrito_id' => 'distrito_id', ), null, null);
        $this->addRelation('SubscribeType', 'AppBundle\\Model\\SubscribeType', RelationMap::MANY_TO_ONE, array('inscricao_tipo_id' => 'inscricao_tipo_id', ), null, null);
        $this->addRelation('Language', 'AppBundle\\Model\\Language', RelationMap::MANY_TO_ONE, array('lingua_id' => 'lingua_id', ), 'SET NULL', 'CASCADE');
        $this->addRelation('User', 'AppBundle\\Model\\User', RelationMap::MANY_TO_ONE, array('usuario_id' => 'usuario_id', ), null, null);
        $this->addRelation('Displacement', 'AppBundle\\Model\\Displacement', RelationMap::MANY_TO_ONE, array('deslocamento_id' => 'deslocamento_id', ), null, null);
        $this->addRelation('State', 'AppBundle\\Model\\State', RelationMap::MANY_TO_ONE, array('estado_id' => 'estado_id', ), null, null);
        $this->addRelation('Country', 'AppBundle\\Model\\Country', RelationMap::MANY_TO_ONE, array('pais_id' => 'pais_id', ), null, null);
        $this->addRelation('Train', 'AppBundle\\Model\\Train', RelationMap::ONE_TO_MANY, array('inscricao_id' => 'inscricao_id', ), 'CASCADE', 'CASCADE', 'Trains');
        $this->addRelation('Donation', 'AppBundle\\Model\\Donation', RelationMap::ONE_TO_MANY, array('inscricao_id' => 'inscricao_id', ), 'SET NULL', 'CASCADE', 'Donations');
        $this->addRelation('SubscribePayment', 'AppBundle\\Model\\SubscribePayment', RelationMap::ONE_TO_MANY, array('inscricao_id' => 'inscricao_id', ), 'CASCADE', 'CASCADE', 'SubscribePayments');
        $this->addRelation('WorkshopSubscribe', 'AppBundle\\Model\\WorkshopSubscribe', RelationMap::ONE_TO_MANY, array('inscricao_id' => 'inscricao_id', ), 'CASCADE', 'CASCADE', 'WorkshopSubscribes');
        $this->addRelation('Transfer', 'AppBundle\\Model\\Transfer', RelationMap::ONE_TO_ONE, array('inscricao_id' => 'inscricao_id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // SubscribeTableMap
