<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'inscricao_tipo' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class SubscribeTypeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.SubscribeTypeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('inscricao_tipo');
        $this->setPhpName('SubscribeType');
        $this->setClassname('AppBundle\\Model\\SubscribeType');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('inscricao_tipo_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vaga', 'SpaceAvailable', 'INTEGER', false, null, null);
        $this->addColumn('sexo', 'Genre', 'CHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::ONE_TO_MANY, array('inscricao_tipo_id' => 'inscricao_tipo_id', ), null, null, 'Subscribes');
        $this->addRelation('SubscribeTypeName', 'AppBundle\\Model\\SubscribeTypeName', RelationMap::ONE_TO_MANY, array('inscricao_tipo_id' => 'inscricao_tipo_id', ), null, null, 'SubscribeTypeNames');
        $this->addRelation('SubscribeTypePrice', 'AppBundle\\Model\\SubscribeTypePrice', RelationMap::ONE_TO_MANY, array('inscricao_tipo_id' => 'inscricao_tipo_id', ), null, null, 'SubscribeTypePrices');
    } // buildRelations()

} // SubscribeTypeTableMap
