<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'usuario' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class UserTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.UserTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('usuario');
        $this->setPhpName('User');
        $this->setClassname('AppBundle\\Model\\User');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('usuario_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('grupo_id', 'GroupId', 'INTEGER', 'grupo', 'grupo_id', true, null, 0);
        $this->addColumn('nome', 'Name', 'VARCHAR', true, 120, '');
        $this->addColumn('email', 'Email', 'VARCHAR', true, 255, null);
        $this->addColumn('senha', 'Password', 'VARCHAR', false, 255, null);
        $this->addColumn('salt', 'Salt', 'VARCHAR', false, 32, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Group', 'AppBundle\\Model\\Group', RelationMap::MANY_TO_ONE, array('grupo_id' => 'grupo_id', ), null, null);
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::ONE_TO_MANY, array('usuario_id' => 'usuario_id', ), null, null, 'Subscribes');
    } // buildRelations()

} // UserTableMap
