<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'texto' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class TextTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.TextTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('texto');
        $this->setPhpName('Text');
        $this->setClassname('AppBundle\\Model\\Text');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('texto_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('pagina_id', 'PageId', 'INTEGER', 'pagina', 'pagina_id', true, null, null);
        $this->addForeignKey('lingua_id', 'LanguageId', 'INTEGER', 'lingua', 'lingua_id', true, null, 0);
        $this->addColumn('conteudo', 'Content', 'CLOB', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Language', 'AppBundle\\Model\\Language', RelationMap::MANY_TO_ONE, array('lingua_id' => 'lingua_id', ), null, null);
        $this->addRelation('Page', 'AppBundle\\Model\\Page', RelationMap::MANY_TO_ONE, array('pagina_id' => 'pagina_id', ), null, null);
    } // buildRelations()

} // TextTableMap
