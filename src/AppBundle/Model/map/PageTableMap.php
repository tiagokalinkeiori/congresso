<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'pagina' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class PageTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.PageTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pagina');
        $this->setPhpName('Page');
        $this->setClassname('AppBundle\\Model\\Page');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('pagina_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('rota', 'Route', 'VARCHAR', true, 60, null);
        $this->addColumn('nome', 'Name', 'VARCHAR', true, 60, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Text', 'AppBundle\\Model\\Text', RelationMap::ONE_TO_MANY, array('pagina_id' => 'pagina_id', ), null, null, 'Texts');
    } // buildRelations()

} // PageTableMap
