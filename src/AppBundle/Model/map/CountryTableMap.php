<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'pais' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class CountryTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.CountryTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pais');
        $this->setPhpName('Country');
        $this->setClassname('AppBundle\\Model\\Country');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('pais_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nome', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('codigo', 'Code', 'CHAR', false, 2, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('State', 'AppBundle\\Model\\State', RelationMap::ONE_TO_MANY, array('pais_id' => 'pais_id', ), 'CASCADE', 'CASCADE', 'States');
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::ONE_TO_MANY, array('pais_id' => 'pais_id', ), null, null, 'Subscribes');
        $this->addRelation('PaymentText', 'AppBundle\\Model\\PaymentText', RelationMap::ONE_TO_MANY, array('pais_id' => 'pais_id', ), null, 'CASCADE', 'PaymentTexts');
    } // buildRelations()

} // CountryTableMap
