<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'inscricao_tipo_nome' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class SubscribeTypeNameTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.SubscribeTypeNameTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('inscricao_tipo_nome');
        $this->setPhpName('SubscribeTypeName');
        $this->setClassname('AppBundle\\Model\\SubscribeTypeName');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('inscricao_tipo_id', 'SubscribeTypeId', 'INTEGER' , 'inscricao_tipo', 'inscricao_tipo_id', true, null, null);
        $this->addForeignPrimaryKey('lingua_id', 'LanguageId', 'INTEGER' , 'lingua', 'lingua_id', true, null, null);
        $this->addColumn('nome', 'Name', 'VARCHAR', true, 60, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SubscribeType', 'AppBundle\\Model\\SubscribeType', RelationMap::MANY_TO_ONE, array('inscricao_tipo_id' => 'inscricao_tipo_id', ), null, null);
        $this->addRelation('Language', 'AppBundle\\Model\\Language', RelationMap::MANY_TO_ONE, array('lingua_id' => 'lingua_id', ), null, null);
    } // buildRelations()

} // SubscribeTypeNameTableMap
