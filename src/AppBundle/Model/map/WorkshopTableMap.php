<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'oficina' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class WorkshopTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.WorkshopTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('oficina');
        $this->setPhpName('Workshop');
        $this->setClassname('AppBundle\\Model\\Workshop');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('oficina_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nome', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('descricao', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('modalidade', 'Modality', 'VARCHAR', false, 255, null);
        $this->addColumn('palestrante', 'Speaker', 'VARCHAR', true, 255, null);
        $this->addColumn('data_inicio', 'Start', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_fim', 'End', 'TIMESTAMP', true, null, null);
        $this->addColumn('qtde', 'SpaceAvailable', 'SMALLINT', false, null, null);
        $this->addColumn('aberta', 'Opened', 'BOOLEAN', true, 1, true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('WorkshopSubscribe', 'AppBundle\\Model\\WorkshopSubscribe', RelationMap::ONE_TO_MANY, array('oficina_id' => 'oficina_id', ), 'CASCADE', 'CASCADE', 'WorkshopSubscribes');
    } // buildRelations()

} // WorkshopTableMap
