<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'oficina_inscricao' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class WorkshopSubscribeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.WorkshopSubscribeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('oficina_inscricao');
        $this->setPhpName('WorkshopSubscribe');
        $this->setClassname('AppBundle\\Model\\WorkshopSubscribe');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('oficina_id', 'WorkshopId', 'INTEGER' , 'oficina', 'oficina_id', true, null, null);
        $this->addForeignPrimaryKey('inscricao_id', 'SubscribeId', 'INTEGER' , 'inscricao', 'inscricao_id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Subscribe', 'AppBundle\\Model\\Subscribe', RelationMap::MANY_TO_ONE, array('inscricao_id' => 'inscricao_id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('Workshop', 'AppBundle\\Model\\Workshop', RelationMap::MANY_TO_ONE, array('oficina_id' => 'oficina_id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // WorkshopSubscribeTableMap
