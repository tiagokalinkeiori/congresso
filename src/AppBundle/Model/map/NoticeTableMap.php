<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'noticia' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class NoticeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.NoticeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('noticia');
        $this->setPhpName('Notice');
        $this->setClassname('AppBundle\\Model\\Notice');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('noticia_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('lingua_id', 'LanguageId', 'INTEGER', 'lingua', 'lingua_id', true, null, null);
        $this->addColumn('titulo', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('conteudo', 'Content', 'LONGVARCHAR', false, null, null);
        $this->addColumn('data', 'Date', 'TIMESTAMP', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Language', 'AppBundle\\Model\\Language', RelationMap::MANY_TO_ONE, array('lingua_id' => 'lingua_id', ), null, null);
    } // buildRelations()

} // NoticeTableMap
