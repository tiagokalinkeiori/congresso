<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Country;
use AppBundle\Model\Displacement;
use AppBundle\Model\District;
use AppBundle\Model\Donation;
use AppBundle\Model\Language;
use AppBundle\Model\State;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePeer;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\SubscribeType;
use AppBundle\Model\Train;
use AppBundle\Model\Transfer;
use AppBundle\Model\User;
use AppBundle\Model\WorkshopSubscribe;

/**
 * @method SubscribeQuery orderById($order = Criteria::ASC) Order by the inscricao_id column
 * @method SubscribeQuery orderByUserId($order = Criteria::ASC) Order by the usuario_id column
 * @method SubscribeQuery orderByCountryId($order = Criteria::ASC) Order by the pais_id column
 * @method SubscribeQuery orderByStateId($order = Criteria::ASC) Order by the estado_id column
 * @method SubscribeQuery orderByDistrictId($order = Criteria::ASC) Order by the distrito_id column
 * @method SubscribeQuery orderBySubscribeTypeId($order = Criteria::ASC) Order by the inscricao_tipo_id column
 * @method SubscribeQuery orderByDisplacementId($order = Criteria::ASC) Order by the deslocamento_id column
 * @method SubscribeQuery orderByLanguageId($order = Criteria::ASC) Order by the lingua_id column
 * @method SubscribeQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method SubscribeQuery orderByName($order = Criteria::ASC) Order by the nome column
 * @method SubscribeQuery orderByBirthday($order = Criteria::ASC) Order by the data_nascimento column
 * @method SubscribeQuery orderByResponsibleName($order = Criteria::ASC) Order by the nome_responsavel column
 * @method SubscribeQuery orderByResponsiblePhone($order = Criteria::ASC) Order by the fone_responsavel column
 * @method SubscribeQuery orderByRegister($order = Criteria::ASC) Order by the registro column
 * @method SubscribeQuery orderByDispatcher($order = Criteria::ASC) Order by the orgao_expedidor column
 * @method SubscribeQuery orderByDispatcherPlace($order = Criteria::ASC) Order by the orgao_expedidor_uf column
 * @method SubscribeQuery orderByDocument($order = Criteria::ASC) Order by the documento column
 * @method SubscribeQuery orderByGenre($order = Criteria::ASC) Order by the sexo column
 * @method SubscribeQuery orderByPhone($order = Criteria::ASC) Order by the telefone column
 * @method SubscribeQuery orderByAddress($order = Criteria::ASC) Order by the endereco column
 * @method SubscribeQuery orderByNumber($order = Criteria::ASC) Order by the numero column
 * @method SubscribeQuery orderByComplement($order = Criteria::ASC) Order by the complemento column
 * @method SubscribeQuery orderByCity($order = Criteria::ASC) Order by the cidade column
 * @method SubscribeQuery orderByRegion($order = Criteria::ASC) Order by the bairro column
 * @method SubscribeQuery orderByZipCode($order = Criteria::ASC) Order by the cep column
 * @method SubscribeQuery orderByMember($order = Criteria::ASC) Order by the membro_ielb column
 * @method SubscribeQuery orderByConfirmed($order = Criteria::ASC) Order by the confirmado column
 * @method SubscribeQuery orderByDelegate($order = Criteria::ASC) Order by the delegado_plenaria column
 * @method SubscribeQuery orderByUnityPresident($order = Criteria::ASC) Order by the presidente_uj column
 * @method SubscribeQuery orderByMinister($order = Criteria::ASC) Order by the pastor column
 * @method SubscribeQuery orderByUnityName($order = Criteria::ASC) Order by the uj column
 * @method SubscribeQuery orderByTrainResponsible($order = Criteria::ASC) Order by the responsavel_caravana column
 * @method SubscribeQuery orderByQuota($order = Criteria::ASC) Order by the parcelas column
 * @method SubscribeQuery orderByPrice($order = Criteria::ASC) Order by the valor_total column
 * @method SubscribeQuery orderBySpecialNeed($order = Criteria::ASC) Order by the necessidades_especiais column
 * @method SubscribeQuery orderByComment($order = Criteria::ASC) Order by the comentarios column
 * @method SubscribeQuery orderByFirstCongress($order = Criteria::ASC) Order by the primeiro_congresso column
 * @method SubscribeQuery orderByAmountCongress($order = Criteria::ASC) Order by the quantidade_congresso column
 * @method SubscribeQuery orderByFree($order = Criteria::ASC) Order by the isento column
 * @method SubscribeQuery orderByOrganization($order = Criteria::ASC) Order by the organizacao column
 * @method SubscribeQuery orderByVisitor($order = Criteria::ASC) Order by the visitante column
 * @method SubscribeQuery orderByCanceled($order = Criteria::ASC) Order by the cancelado column
 * @method SubscribeQuery orderByDate($order = Criteria::ASC) Order by the data_inscricao column
 *
 * @method SubscribeQuery groupById() Group by the inscricao_id column
 * @method SubscribeQuery groupByUserId() Group by the usuario_id column
 * @method SubscribeQuery groupByCountryId() Group by the pais_id column
 * @method SubscribeQuery groupByStateId() Group by the estado_id column
 * @method SubscribeQuery groupByDistrictId() Group by the distrito_id column
 * @method SubscribeQuery groupBySubscribeTypeId() Group by the inscricao_tipo_id column
 * @method SubscribeQuery groupByDisplacementId() Group by the deslocamento_id column
 * @method SubscribeQuery groupByLanguageId() Group by the lingua_id column
 * @method SubscribeQuery groupByEmail() Group by the email column
 * @method SubscribeQuery groupByName() Group by the nome column
 * @method SubscribeQuery groupByBirthday() Group by the data_nascimento column
 * @method SubscribeQuery groupByResponsibleName() Group by the nome_responsavel column
 * @method SubscribeQuery groupByResponsiblePhone() Group by the fone_responsavel column
 * @method SubscribeQuery groupByRegister() Group by the registro column
 * @method SubscribeQuery groupByDispatcher() Group by the orgao_expedidor column
 * @method SubscribeQuery groupByDispatcherPlace() Group by the orgao_expedidor_uf column
 * @method SubscribeQuery groupByDocument() Group by the documento column
 * @method SubscribeQuery groupByGenre() Group by the sexo column
 * @method SubscribeQuery groupByPhone() Group by the telefone column
 * @method SubscribeQuery groupByAddress() Group by the endereco column
 * @method SubscribeQuery groupByNumber() Group by the numero column
 * @method SubscribeQuery groupByComplement() Group by the complemento column
 * @method SubscribeQuery groupByCity() Group by the cidade column
 * @method SubscribeQuery groupByRegion() Group by the bairro column
 * @method SubscribeQuery groupByZipCode() Group by the cep column
 * @method SubscribeQuery groupByMember() Group by the membro_ielb column
 * @method SubscribeQuery groupByConfirmed() Group by the confirmado column
 * @method SubscribeQuery groupByDelegate() Group by the delegado_plenaria column
 * @method SubscribeQuery groupByUnityPresident() Group by the presidente_uj column
 * @method SubscribeQuery groupByMinister() Group by the pastor column
 * @method SubscribeQuery groupByUnityName() Group by the uj column
 * @method SubscribeQuery groupByTrainResponsible() Group by the responsavel_caravana column
 * @method SubscribeQuery groupByQuota() Group by the parcelas column
 * @method SubscribeQuery groupByPrice() Group by the valor_total column
 * @method SubscribeQuery groupBySpecialNeed() Group by the necessidades_especiais column
 * @method SubscribeQuery groupByComment() Group by the comentarios column
 * @method SubscribeQuery groupByFirstCongress() Group by the primeiro_congresso column
 * @method SubscribeQuery groupByAmountCongress() Group by the quantidade_congresso column
 * @method SubscribeQuery groupByFree() Group by the isento column
 * @method SubscribeQuery groupByOrganization() Group by the organizacao column
 * @method SubscribeQuery groupByVisitor() Group by the visitante column
 * @method SubscribeQuery groupByCanceled() Group by the cancelado column
 * @method SubscribeQuery groupByDate() Group by the data_inscricao column
 *
 * @method SubscribeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SubscribeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SubscribeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SubscribeQuery leftJoinDistrict($relationAlias = null) Adds a LEFT JOIN clause to the query using the District relation
 * @method SubscribeQuery rightJoinDistrict($relationAlias = null) Adds a RIGHT JOIN clause to the query using the District relation
 * @method SubscribeQuery innerJoinDistrict($relationAlias = null) Adds a INNER JOIN clause to the query using the District relation
 *
 * @method SubscribeQuery leftJoinSubscribeType($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubscribeType relation
 * @method SubscribeQuery rightJoinSubscribeType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubscribeType relation
 * @method SubscribeQuery innerJoinSubscribeType($relationAlias = null) Adds a INNER JOIN clause to the query using the SubscribeType relation
 *
 * @method SubscribeQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method SubscribeQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method SubscribeQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method SubscribeQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method SubscribeQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method SubscribeQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method SubscribeQuery leftJoinDisplacement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Displacement relation
 * @method SubscribeQuery rightJoinDisplacement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Displacement relation
 * @method SubscribeQuery innerJoinDisplacement($relationAlias = null) Adds a INNER JOIN clause to the query using the Displacement relation
 *
 * @method SubscribeQuery leftJoinState($relationAlias = null) Adds a LEFT JOIN clause to the query using the State relation
 * @method SubscribeQuery rightJoinState($relationAlias = null) Adds a RIGHT JOIN clause to the query using the State relation
 * @method SubscribeQuery innerJoinState($relationAlias = null) Adds a INNER JOIN clause to the query using the State relation
 *
 * @method SubscribeQuery leftJoinCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Country relation
 * @method SubscribeQuery rightJoinCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Country relation
 * @method SubscribeQuery innerJoinCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the Country relation
 *
 * @method SubscribeQuery leftJoinTrain($relationAlias = null) Adds a LEFT JOIN clause to the query using the Train relation
 * @method SubscribeQuery rightJoinTrain($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Train relation
 * @method SubscribeQuery innerJoinTrain($relationAlias = null) Adds a INNER JOIN clause to the query using the Train relation
 *
 * @method SubscribeQuery leftJoinDonation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Donation relation
 * @method SubscribeQuery rightJoinDonation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Donation relation
 * @method SubscribeQuery innerJoinDonation($relationAlias = null) Adds a INNER JOIN clause to the query using the Donation relation
 *
 * @method SubscribeQuery leftJoinSubscribePayment($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubscribePayment relation
 * @method SubscribeQuery rightJoinSubscribePayment($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubscribePayment relation
 * @method SubscribeQuery innerJoinSubscribePayment($relationAlias = null) Adds a INNER JOIN clause to the query using the SubscribePayment relation
 *
 * @method SubscribeQuery leftJoinWorkshopSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the WorkshopSubscribe relation
 * @method SubscribeQuery rightJoinWorkshopSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WorkshopSubscribe relation
 * @method SubscribeQuery innerJoinWorkshopSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the WorkshopSubscribe relation
 *
 * @method SubscribeQuery leftJoinTransfer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Transfer relation
 * @method SubscribeQuery rightJoinTransfer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Transfer relation
 * @method SubscribeQuery innerJoinTransfer($relationAlias = null) Adds a INNER JOIN clause to the query using the Transfer relation
 *
 * @method Subscribe findOne(PropelPDO $con = null) Return the first Subscribe matching the query
 * @method Subscribe findOneOrCreate(PropelPDO $con = null) Return the first Subscribe matching the query, or a new Subscribe object populated from the query conditions when no match is found
 *
 * @method Subscribe findOneByUserId(int $usuario_id) Return the first Subscribe filtered by the usuario_id column
 * @method Subscribe findOneByCountryId(int $pais_id) Return the first Subscribe filtered by the pais_id column
 * @method Subscribe findOneByStateId(int $estado_id) Return the first Subscribe filtered by the estado_id column
 * @method Subscribe findOneByDistrictId(int $distrito_id) Return the first Subscribe filtered by the distrito_id column
 * @method Subscribe findOneBySubscribeTypeId(int $inscricao_tipo_id) Return the first Subscribe filtered by the inscricao_tipo_id column
 * @method Subscribe findOneByDisplacementId(int $deslocamento_id) Return the first Subscribe filtered by the deslocamento_id column
 * @method Subscribe findOneByLanguageId(int $lingua_id) Return the first Subscribe filtered by the lingua_id column
 * @method Subscribe findOneByEmail(string $email) Return the first Subscribe filtered by the email column
 * @method Subscribe findOneByName(string $nome) Return the first Subscribe filtered by the nome column
 * @method Subscribe findOneByBirthday(string $data_nascimento) Return the first Subscribe filtered by the data_nascimento column
 * @method Subscribe findOneByResponsibleName(string $nome_responsavel) Return the first Subscribe filtered by the nome_responsavel column
 * @method Subscribe findOneByResponsiblePhone(string $fone_responsavel) Return the first Subscribe filtered by the fone_responsavel column
 * @method Subscribe findOneByRegister(string $registro) Return the first Subscribe filtered by the registro column
 * @method Subscribe findOneByDispatcher(string $orgao_expedidor) Return the first Subscribe filtered by the orgao_expedidor column
 * @method Subscribe findOneByDispatcherPlace(string $orgao_expedidor_uf) Return the first Subscribe filtered by the orgao_expedidor_uf column
 * @method Subscribe findOneByDocument(string $documento) Return the first Subscribe filtered by the documento column
 * @method Subscribe findOneByGenre(string $sexo) Return the first Subscribe filtered by the sexo column
 * @method Subscribe findOneByPhone(string $telefone) Return the first Subscribe filtered by the telefone column
 * @method Subscribe findOneByAddress(string $endereco) Return the first Subscribe filtered by the endereco column
 * @method Subscribe findOneByNumber(string $numero) Return the first Subscribe filtered by the numero column
 * @method Subscribe findOneByComplement(string $complemento) Return the first Subscribe filtered by the complemento column
 * @method Subscribe findOneByCity(string $cidade) Return the first Subscribe filtered by the cidade column
 * @method Subscribe findOneByRegion(string $bairro) Return the first Subscribe filtered by the bairro column
 * @method Subscribe findOneByZipCode(string $cep) Return the first Subscribe filtered by the cep column
 * @method Subscribe findOneByMember(boolean $membro_ielb) Return the first Subscribe filtered by the membro_ielb column
 * @method Subscribe findOneByConfirmed(boolean $confirmado) Return the first Subscribe filtered by the confirmado column
 * @method Subscribe findOneByDelegate(boolean $delegado_plenaria) Return the first Subscribe filtered by the delegado_plenaria column
 * @method Subscribe findOneByUnityPresident(string $presidente_uj) Return the first Subscribe filtered by the presidente_uj column
 * @method Subscribe findOneByMinister(string $pastor) Return the first Subscribe filtered by the pastor column
 * @method Subscribe findOneByUnityName(string $uj) Return the first Subscribe filtered by the uj column
 * @method Subscribe findOneByTrainResponsible(string $responsavel_caravana) Return the first Subscribe filtered by the responsavel_caravana column
 * @method Subscribe findOneByQuota(int $parcelas) Return the first Subscribe filtered by the parcelas column
 * @method Subscribe findOneByPrice(double $valor_total) Return the first Subscribe filtered by the valor_total column
 * @method Subscribe findOneBySpecialNeed(string $necessidades_especiais) Return the first Subscribe filtered by the necessidades_especiais column
 * @method Subscribe findOneByComment(string $comentarios) Return the first Subscribe filtered by the comentarios column
 * @method Subscribe findOneByFirstCongress(boolean $primeiro_congresso) Return the first Subscribe filtered by the primeiro_congresso column
 * @method Subscribe findOneByAmountCongress(int $quantidade_congresso) Return the first Subscribe filtered by the quantidade_congresso column
 * @method Subscribe findOneByFree(boolean $isento) Return the first Subscribe filtered by the isento column
 * @method Subscribe findOneByOrganization(boolean $organizacao) Return the first Subscribe filtered by the organizacao column
 * @method Subscribe findOneByVisitor(boolean $visitante) Return the first Subscribe filtered by the visitante column
 * @method Subscribe findOneByCanceled(boolean $cancelado) Return the first Subscribe filtered by the cancelado column
 * @method Subscribe findOneByDate(string $data_inscricao) Return the first Subscribe filtered by the data_inscricao column
 *
 * @method array findById(int $inscricao_id) Return Subscribe objects filtered by the inscricao_id column
 * @method array findByUserId(int $usuario_id) Return Subscribe objects filtered by the usuario_id column
 * @method array findByCountryId(int $pais_id) Return Subscribe objects filtered by the pais_id column
 * @method array findByStateId(int $estado_id) Return Subscribe objects filtered by the estado_id column
 * @method array findByDistrictId(int $distrito_id) Return Subscribe objects filtered by the distrito_id column
 * @method array findBySubscribeTypeId(int $inscricao_tipo_id) Return Subscribe objects filtered by the inscricao_tipo_id column
 * @method array findByDisplacementId(int $deslocamento_id) Return Subscribe objects filtered by the deslocamento_id column
 * @method array findByLanguageId(int $lingua_id) Return Subscribe objects filtered by the lingua_id column
 * @method array findByEmail(string $email) Return Subscribe objects filtered by the email column
 * @method array findByName(string $nome) Return Subscribe objects filtered by the nome column
 * @method array findByBirthday(string $data_nascimento) Return Subscribe objects filtered by the data_nascimento column
 * @method array findByResponsibleName(string $nome_responsavel) Return Subscribe objects filtered by the nome_responsavel column
 * @method array findByResponsiblePhone(string $fone_responsavel) Return Subscribe objects filtered by the fone_responsavel column
 * @method array findByRegister(string $registro) Return Subscribe objects filtered by the registro column
 * @method array findByDispatcher(string $orgao_expedidor) Return Subscribe objects filtered by the orgao_expedidor column
 * @method array findByDispatcherPlace(string $orgao_expedidor_uf) Return Subscribe objects filtered by the orgao_expedidor_uf column
 * @method array findByDocument(string $documento) Return Subscribe objects filtered by the documento column
 * @method array findByGenre(string $sexo) Return Subscribe objects filtered by the sexo column
 * @method array findByPhone(string $telefone) Return Subscribe objects filtered by the telefone column
 * @method array findByAddress(string $endereco) Return Subscribe objects filtered by the endereco column
 * @method array findByNumber(string $numero) Return Subscribe objects filtered by the numero column
 * @method array findByComplement(string $complemento) Return Subscribe objects filtered by the complemento column
 * @method array findByCity(string $cidade) Return Subscribe objects filtered by the cidade column
 * @method array findByRegion(string $bairro) Return Subscribe objects filtered by the bairro column
 * @method array findByZipCode(string $cep) Return Subscribe objects filtered by the cep column
 * @method array findByMember(boolean $membro_ielb) Return Subscribe objects filtered by the membro_ielb column
 * @method array findByConfirmed(boolean $confirmado) Return Subscribe objects filtered by the confirmado column
 * @method array findByDelegate(boolean $delegado_plenaria) Return Subscribe objects filtered by the delegado_plenaria column
 * @method array findByUnityPresident(string $presidente_uj) Return Subscribe objects filtered by the presidente_uj column
 * @method array findByMinister(string $pastor) Return Subscribe objects filtered by the pastor column
 * @method array findByUnityName(string $uj) Return Subscribe objects filtered by the uj column
 * @method array findByTrainResponsible(string $responsavel_caravana) Return Subscribe objects filtered by the responsavel_caravana column
 * @method array findByQuota(int $parcelas) Return Subscribe objects filtered by the parcelas column
 * @method array findByPrice(double $valor_total) Return Subscribe objects filtered by the valor_total column
 * @method array findBySpecialNeed(string $necessidades_especiais) Return Subscribe objects filtered by the necessidades_especiais column
 * @method array findByComment(string $comentarios) Return Subscribe objects filtered by the comentarios column
 * @method array findByFirstCongress(boolean $primeiro_congresso) Return Subscribe objects filtered by the primeiro_congresso column
 * @method array findByAmountCongress(int $quantidade_congresso) Return Subscribe objects filtered by the quantidade_congresso column
 * @method array findByFree(boolean $isento) Return Subscribe objects filtered by the isento column
 * @method array findByOrganization(boolean $organizacao) Return Subscribe objects filtered by the organizacao column
 * @method array findByVisitor(boolean $visitante) Return Subscribe objects filtered by the visitante column
 * @method array findByCanceled(boolean $cancelado) Return Subscribe objects filtered by the cancelado column
 * @method array findByDate(string $data_inscricao) Return Subscribe objects filtered by the data_inscricao column
 */
abstract class BaseSubscribeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSubscribeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\Subscribe';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SubscribeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SubscribeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SubscribeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SubscribeQuery) {
            return $criteria;
        }
        $query = new SubscribeQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Subscribe|Subscribe[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SubscribePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Subscribe A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Subscribe A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `inscricao_id`, `usuario_id`, `pais_id`, `estado_id`, `distrito_id`, `inscricao_tipo_id`, `deslocamento_id`, `lingua_id`, `email`, `nome`, `data_nascimento`, `nome_responsavel`, `fone_responsavel`, `registro`, `orgao_expedidor`, `orgao_expedidor_uf`, `documento`, `sexo`, `telefone`, `endereco`, `numero`, `complemento`, `cidade`, `bairro`, `cep`, `membro_ielb`, `confirmado`, `delegado_plenaria`, `presidente_uj`, `pastor`, `uj`, `responsavel_caravana`, `parcelas`, `valor_total`, `necessidades_especiais`, `comentarios`, `primeiro_congresso`, `quantidade_congresso`, `isento`, `organizacao`, `visitante`, `cancelado`, `data_inscricao` FROM `inscricao` WHERE `inscricao_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Subscribe();
            $obj->hydrate($row);
            SubscribePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Subscribe|Subscribe[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Subscribe[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SubscribePeer::INSCRICAO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SubscribePeer::INSCRICAO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the inscricao_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE inscricao_id = 1234
     * $query->filterById(array(12, 34)); // WHERE inscricao_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE inscricao_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE inscricao_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SubscribePeer::INSCRICAO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SubscribePeer::INSCRICAO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::INSCRICAO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the usuario_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE usuario_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE usuario_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE usuario_id >= 12
     * $query->filterByUserId(array('max' => 12)); // WHERE usuario_id <= 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(SubscribePeer::USUARIO_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(SubscribePeer::USUARIO_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::USUARIO_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the pais_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCountryId(1234); // WHERE pais_id = 1234
     * $query->filterByCountryId(array(12, 34)); // WHERE pais_id IN (12, 34)
     * $query->filterByCountryId(array('min' => 12)); // WHERE pais_id >= 12
     * $query->filterByCountryId(array('max' => 12)); // WHERE pais_id <= 12
     * </code>
     *
     * @see       filterByCountry()
     *
     * @param     mixed $countryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByCountryId($countryId = null, $comparison = null)
    {
        if (is_array($countryId)) {
            $useMinMax = false;
            if (isset($countryId['min'])) {
                $this->addUsingAlias(SubscribePeer::PAIS_ID, $countryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countryId['max'])) {
                $this->addUsingAlias(SubscribePeer::PAIS_ID, $countryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::PAIS_ID, $countryId, $comparison);
    }

    /**
     * Filter the query on the estado_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStateId(1234); // WHERE estado_id = 1234
     * $query->filterByStateId(array(12, 34)); // WHERE estado_id IN (12, 34)
     * $query->filterByStateId(array('min' => 12)); // WHERE estado_id >= 12
     * $query->filterByStateId(array('max' => 12)); // WHERE estado_id <= 12
     * </code>
     *
     * @see       filterByState()
     *
     * @param     mixed $stateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByStateId($stateId = null, $comparison = null)
    {
        if (is_array($stateId)) {
            $useMinMax = false;
            if (isset($stateId['min'])) {
                $this->addUsingAlias(SubscribePeer::ESTADO_ID, $stateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stateId['max'])) {
                $this->addUsingAlias(SubscribePeer::ESTADO_ID, $stateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::ESTADO_ID, $stateId, $comparison);
    }

    /**
     * Filter the query on the distrito_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDistrictId(1234); // WHERE distrito_id = 1234
     * $query->filterByDistrictId(array(12, 34)); // WHERE distrito_id IN (12, 34)
     * $query->filterByDistrictId(array('min' => 12)); // WHERE distrito_id >= 12
     * $query->filterByDistrictId(array('max' => 12)); // WHERE distrito_id <= 12
     * </code>
     *
     * @see       filterByDistrict()
     *
     * @param     mixed $districtId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByDistrictId($districtId = null, $comparison = null)
    {
        if (is_array($districtId)) {
            $useMinMax = false;
            if (isset($districtId['min'])) {
                $this->addUsingAlias(SubscribePeer::DISTRITO_ID, $districtId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($districtId['max'])) {
                $this->addUsingAlias(SubscribePeer::DISTRITO_ID, $districtId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::DISTRITO_ID, $districtId, $comparison);
    }

    /**
     * Filter the query on the inscricao_tipo_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySubscribeTypeId(1234); // WHERE inscricao_tipo_id = 1234
     * $query->filterBySubscribeTypeId(array(12, 34)); // WHERE inscricao_tipo_id IN (12, 34)
     * $query->filterBySubscribeTypeId(array('min' => 12)); // WHERE inscricao_tipo_id >= 12
     * $query->filterBySubscribeTypeId(array('max' => 12)); // WHERE inscricao_tipo_id <= 12
     * </code>
     *
     * @see       filterBySubscribeType()
     *
     * @param     mixed $subscribeTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterBySubscribeTypeId($subscribeTypeId = null, $comparison = null)
    {
        if (is_array($subscribeTypeId)) {
            $useMinMax = false;
            if (isset($subscribeTypeId['min'])) {
                $this->addUsingAlias(SubscribePeer::INSCRICAO_TIPO_ID, $subscribeTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subscribeTypeId['max'])) {
                $this->addUsingAlias(SubscribePeer::INSCRICAO_TIPO_ID, $subscribeTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::INSCRICAO_TIPO_ID, $subscribeTypeId, $comparison);
    }

    /**
     * Filter the query on the deslocamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDisplacementId(1234); // WHERE deslocamento_id = 1234
     * $query->filterByDisplacementId(array(12, 34)); // WHERE deslocamento_id IN (12, 34)
     * $query->filterByDisplacementId(array('min' => 12)); // WHERE deslocamento_id >= 12
     * $query->filterByDisplacementId(array('max' => 12)); // WHERE deslocamento_id <= 12
     * </code>
     *
     * @see       filterByDisplacement()
     *
     * @param     mixed $displacementId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByDisplacementId($displacementId = null, $comparison = null)
    {
        if (is_array($displacementId)) {
            $useMinMax = false;
            if (isset($displacementId['min'])) {
                $this->addUsingAlias(SubscribePeer::DESLOCAMENTO_ID, $displacementId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($displacementId['max'])) {
                $this->addUsingAlias(SubscribePeer::DESLOCAMENTO_ID, $displacementId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::DESLOCAMENTO_ID, $displacementId, $comparison);
    }

    /**
     * Filter the query on the lingua_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE lingua_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE lingua_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE lingua_id >= 12
     * $query->filterByLanguageId(array('max' => 12)); // WHERE lingua_id <= 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(SubscribePeer::LINGUA_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(SubscribePeer::LINGUA_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::LINGUA_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::NOME, $name, $comparison);
    }

    /**
     * Filter the query on the data_nascimento column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthday('2011-03-14'); // WHERE data_nascimento = '2011-03-14'
     * $query->filterByBirthday('now'); // WHERE data_nascimento = '2011-03-14'
     * $query->filterByBirthday(array('max' => 'yesterday')); // WHERE data_nascimento < '2011-03-13'
     * </code>
     *
     * @param     mixed $birthday The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByBirthday($birthday = null, $comparison = null)
    {
        if (is_array($birthday)) {
            $useMinMax = false;
            if (isset($birthday['min'])) {
                $this->addUsingAlias(SubscribePeer::DATA_NASCIMENTO, $birthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthday['max'])) {
                $this->addUsingAlias(SubscribePeer::DATA_NASCIMENTO, $birthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::DATA_NASCIMENTO, $birthday, $comparison);
    }

    /**
     * Filter the query on the nome_responsavel column
     *
     * Example usage:
     * <code>
     * $query->filterByResponsibleName('fooValue');   // WHERE nome_responsavel = 'fooValue'
     * $query->filterByResponsibleName('%fooValue%'); // WHERE nome_responsavel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $responsibleName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByResponsibleName($responsibleName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($responsibleName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $responsibleName)) {
                $responsibleName = str_replace('*', '%', $responsibleName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::NOME_RESPONSAVEL, $responsibleName, $comparison);
    }

    /**
     * Filter the query on the fone_responsavel column
     *
     * Example usage:
     * <code>
     * $query->filterByResponsiblePhone('fooValue');   // WHERE fone_responsavel = 'fooValue'
     * $query->filterByResponsiblePhone('%fooValue%'); // WHERE fone_responsavel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $responsiblePhone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByResponsiblePhone($responsiblePhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($responsiblePhone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $responsiblePhone)) {
                $responsiblePhone = str_replace('*', '%', $responsiblePhone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::FONE_RESPONSAVEL, $responsiblePhone, $comparison);
    }

    /**
     * Filter the query on the registro column
     *
     * Example usage:
     * <code>
     * $query->filterByRegister('fooValue');   // WHERE registro = 'fooValue'
     * $query->filterByRegister('%fooValue%'); // WHERE registro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $register The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByRegister($register = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($register)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $register)) {
                $register = str_replace('*', '%', $register);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::REGISTRO, $register, $comparison);
    }

    /**
     * Filter the query on the orgao_expedidor column
     *
     * Example usage:
     * <code>
     * $query->filterByDispatcher('fooValue');   // WHERE orgao_expedidor = 'fooValue'
     * $query->filterByDispatcher('%fooValue%'); // WHERE orgao_expedidor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dispatcher The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByDispatcher($dispatcher = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dispatcher)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dispatcher)) {
                $dispatcher = str_replace('*', '%', $dispatcher);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::ORGAO_EXPEDIDOR, $dispatcher, $comparison);
    }

    /**
     * Filter the query on the orgao_expedidor_uf column
     *
     * Example usage:
     * <code>
     * $query->filterByDispatcherPlace('fooValue');   // WHERE orgao_expedidor_uf = 'fooValue'
     * $query->filterByDispatcherPlace('%fooValue%'); // WHERE orgao_expedidor_uf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dispatcherPlace The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByDispatcherPlace($dispatcherPlace = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dispatcherPlace)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dispatcherPlace)) {
                $dispatcherPlace = str_replace('*', '%', $dispatcherPlace);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::ORGAO_EXPEDIDOR_UF, $dispatcherPlace, $comparison);
    }

    /**
     * Filter the query on the documento column
     *
     * Example usage:
     * <code>
     * $query->filterByDocument('fooValue');   // WHERE documento = 'fooValue'
     * $query->filterByDocument('%fooValue%'); // WHERE documento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $document The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByDocument($document = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($document)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $document)) {
                $document = str_replace('*', '%', $document);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::DOCUMENTO, $document, $comparison);
    }

    /**
     * Filter the query on the sexo column
     *
     * Example usage:
     * <code>
     * $query->filterByGenre('fooValue');   // WHERE sexo = 'fooValue'
     * $query->filterByGenre('%fooValue%'); // WHERE sexo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $genre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByGenre($genre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($genre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $genre)) {
                $genre = str_replace('*', '%', $genre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::SEXO, $genre, $comparison);
    }

    /**
     * Filter the query on the telefone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE telefone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE telefone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::TELEFONE, $phone, $comparison);
    }

    /**
     * Filter the query on the endereco column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE endereco = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE endereco LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::ENDERECO, $address, $comparison);
    }

    /**
     * Filter the query on the numero column
     *
     * Example usage:
     * <code>
     * $query->filterByNumber('fooValue');   // WHERE numero = 'fooValue'
     * $query->filterByNumber('%fooValue%'); // WHERE numero LIKE '%fooValue%'
     * </code>
     *
     * @param     string $number The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByNumber($number = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($number)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $number)) {
                $number = str_replace('*', '%', $number);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::NUMERO, $number, $comparison);
    }

    /**
     * Filter the query on the complemento column
     *
     * Example usage:
     * <code>
     * $query->filterByComplement('fooValue');   // WHERE complemento = 'fooValue'
     * $query->filterByComplement('%fooValue%'); // WHERE complemento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $complement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByComplement($complement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($complement)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $complement)) {
                $complement = str_replace('*', '%', $complement);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::COMPLEMENTO, $complement, $comparison);
    }

    /**
     * Filter the query on the cidade column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE cidade = 'fooValue'
     * $query->filterByCity('%fooValue%'); // WHERE cidade LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $city)) {
                $city = str_replace('*', '%', $city);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::CIDADE, $city, $comparison);
    }

    /**
     * Filter the query on the bairro column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion('fooValue');   // WHERE bairro = 'fooValue'
     * $query->filterByRegion('%fooValue%'); // WHERE bairro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $region The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($region)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $region)) {
                $region = str_replace('*', '%', $region);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::BAIRRO, $region, $comparison);
    }

    /**
     * Filter the query on the cep column
     *
     * Example usage:
     * <code>
     * $query->filterByZipCode('fooValue');   // WHERE cep = 'fooValue'
     * $query->filterByZipCode('%fooValue%'); // WHERE cep LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zipCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByZipCode($zipCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zipCode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $zipCode)) {
                $zipCode = str_replace('*', '%', $zipCode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::CEP, $zipCode, $comparison);
    }

    /**
     * Filter the query on the membro_ielb column
     *
     * Example usage:
     * <code>
     * $query->filterByMember(true); // WHERE membro_ielb = true
     * $query->filterByMember('yes'); // WHERE membro_ielb = true
     * </code>
     *
     * @param     boolean|string $member The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByMember($member = null, $comparison = null)
    {
        if (is_string($member)) {
            $member = in_array(strtolower($member), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePeer::MEMBRO_IELB, $member, $comparison);
    }

    /**
     * Filter the query on the confirmado column
     *
     * Example usage:
     * <code>
     * $query->filterByConfirmed(true); // WHERE confirmado = true
     * $query->filterByConfirmed('yes'); // WHERE confirmado = true
     * </code>
     *
     * @param     boolean|string $confirmed The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByConfirmed($confirmed = null, $comparison = null)
    {
        if (is_string($confirmed)) {
            $confirmed = in_array(strtolower($confirmed), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePeer::CONFIRMADO, $confirmed, $comparison);
    }

    /**
     * Filter the query on the delegado_plenaria column
     *
     * Example usage:
     * <code>
     * $query->filterByDelegate(true); // WHERE delegado_plenaria = true
     * $query->filterByDelegate('yes'); // WHERE delegado_plenaria = true
     * </code>
     *
     * @param     boolean|string $delegate The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByDelegate($delegate = null, $comparison = null)
    {
        if (is_string($delegate)) {
            $delegate = in_array(strtolower($delegate), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePeer::DELEGADO_PLENARIA, $delegate, $comparison);
    }

    /**
     * Filter the query on the presidente_uj column
     *
     * Example usage:
     * <code>
     * $query->filterByUnityPresident('fooValue');   // WHERE presidente_uj = 'fooValue'
     * $query->filterByUnityPresident('%fooValue%'); // WHERE presidente_uj LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unityPresident The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByUnityPresident($unityPresident = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unityPresident)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unityPresident)) {
                $unityPresident = str_replace('*', '%', $unityPresident);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::PRESIDENTE_UJ, $unityPresident, $comparison);
    }

    /**
     * Filter the query on the pastor column
     *
     * Example usage:
     * <code>
     * $query->filterByMinister('fooValue');   // WHERE pastor = 'fooValue'
     * $query->filterByMinister('%fooValue%'); // WHERE pastor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $minister The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByMinister($minister = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($minister)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $minister)) {
                $minister = str_replace('*', '%', $minister);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::PASTOR, $minister, $comparison);
    }

    /**
     * Filter the query on the uj column
     *
     * Example usage:
     * <code>
     * $query->filterByUnityName('fooValue');   // WHERE uj = 'fooValue'
     * $query->filterByUnityName('%fooValue%'); // WHERE uj LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unityName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByUnityName($unityName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unityName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unityName)) {
                $unityName = str_replace('*', '%', $unityName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::UJ, $unityName, $comparison);
    }

    /**
     * Filter the query on the responsavel_caravana column
     *
     * Example usage:
     * <code>
     * $query->filterByTrainResponsible('fooValue');   // WHERE responsavel_caravana = 'fooValue'
     * $query->filterByTrainResponsible('%fooValue%'); // WHERE responsavel_caravana LIKE '%fooValue%'
     * </code>
     *
     * @param     string $trainResponsible The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByTrainResponsible($trainResponsible = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($trainResponsible)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $trainResponsible)) {
                $trainResponsible = str_replace('*', '%', $trainResponsible);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::RESPONSAVEL_CARAVANA, $trainResponsible, $comparison);
    }

    /**
     * Filter the query on the parcelas column
     *
     * Example usage:
     * <code>
     * $query->filterByQuota(1234); // WHERE parcelas = 1234
     * $query->filterByQuota(array(12, 34)); // WHERE parcelas IN (12, 34)
     * $query->filterByQuota(array('min' => 12)); // WHERE parcelas >= 12
     * $query->filterByQuota(array('max' => 12)); // WHERE parcelas <= 12
     * </code>
     *
     * @param     mixed $quota The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByQuota($quota = null, $comparison = null)
    {
        if (is_array($quota)) {
            $useMinMax = false;
            if (isset($quota['min'])) {
                $this->addUsingAlias(SubscribePeer::PARCELAS, $quota['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quota['max'])) {
                $this->addUsingAlias(SubscribePeer::PARCELAS, $quota['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::PARCELAS, $quota, $comparison);
    }

    /**
     * Filter the query on the valor_total column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE valor_total = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE valor_total IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE valor_total >= 12
     * $query->filterByPrice(array('max' => 12)); // WHERE valor_total <= 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(SubscribePeer::VALOR_TOTAL, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(SubscribePeer::VALOR_TOTAL, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::VALOR_TOTAL, $price, $comparison);
    }

    /**
     * Filter the query on the necessidades_especiais column
     *
     * Example usage:
     * <code>
     * $query->filterBySpecialNeed('fooValue');   // WHERE necessidades_especiais = 'fooValue'
     * $query->filterBySpecialNeed('%fooValue%'); // WHERE necessidades_especiais LIKE '%fooValue%'
     * </code>
     *
     * @param     string $specialNeed The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterBySpecialNeed($specialNeed = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($specialNeed)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $specialNeed)) {
                $specialNeed = str_replace('*', '%', $specialNeed);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::NECESSIDADES_ESPECIAIS, $specialNeed, $comparison);
    }

    /**
     * Filter the query on the comentarios column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE comentarios = 'fooValue'
     * $query->filterByComment('%fooValue%'); // WHERE comentarios LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comment The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByComment($comment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $comment)) {
                $comment = str_replace('*', '%', $comment);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePeer::COMENTARIOS, $comment, $comparison);
    }

    /**
     * Filter the query on the primeiro_congresso column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstCongress(true); // WHERE primeiro_congresso = true
     * $query->filterByFirstCongress('yes'); // WHERE primeiro_congresso = true
     * </code>
     *
     * @param     boolean|string $firstCongress The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByFirstCongress($firstCongress = null, $comparison = null)
    {
        if (is_string($firstCongress)) {
            $firstCongress = in_array(strtolower($firstCongress), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePeer::PRIMEIRO_CONGRESSO, $firstCongress, $comparison);
    }

    /**
     * Filter the query on the quantidade_congresso column
     *
     * Example usage:
     * <code>
     * $query->filterByAmountCongress(1234); // WHERE quantidade_congresso = 1234
     * $query->filterByAmountCongress(array(12, 34)); // WHERE quantidade_congresso IN (12, 34)
     * $query->filterByAmountCongress(array('min' => 12)); // WHERE quantidade_congresso >= 12
     * $query->filterByAmountCongress(array('max' => 12)); // WHERE quantidade_congresso <= 12
     * </code>
     *
     * @param     mixed $amountCongress The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByAmountCongress($amountCongress = null, $comparison = null)
    {
        if (is_array($amountCongress)) {
            $useMinMax = false;
            if (isset($amountCongress['min'])) {
                $this->addUsingAlias(SubscribePeer::QUANTIDADE_CONGRESSO, $amountCongress['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amountCongress['max'])) {
                $this->addUsingAlias(SubscribePeer::QUANTIDADE_CONGRESSO, $amountCongress['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::QUANTIDADE_CONGRESSO, $amountCongress, $comparison);
    }

    /**
     * Filter the query on the isento column
     *
     * Example usage:
     * <code>
     * $query->filterByFree(true); // WHERE isento = true
     * $query->filterByFree('yes'); // WHERE isento = true
     * </code>
     *
     * @param     boolean|string $free The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByFree($free = null, $comparison = null)
    {
        if (is_string($free)) {
            $free = in_array(strtolower($free), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePeer::ISENTO, $free, $comparison);
    }

    /**
     * Filter the query on the organizacao column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganization(true); // WHERE organizacao = true
     * $query->filterByOrganization('yes'); // WHERE organizacao = true
     * </code>
     *
     * @param     boolean|string $organization The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByOrganization($organization = null, $comparison = null)
    {
        if (is_string($organization)) {
            $organization = in_array(strtolower($organization), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePeer::ORGANIZACAO, $organization, $comparison);
    }

    /**
     * Filter the query on the visitante column
     *
     * Example usage:
     * <code>
     * $query->filterByVisitor(true); // WHERE visitante = true
     * $query->filterByVisitor('yes'); // WHERE visitante = true
     * </code>
     *
     * @param     boolean|string $visitor The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByVisitor($visitor = null, $comparison = null)
    {
        if (is_string($visitor)) {
            $visitor = in_array(strtolower($visitor), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePeer::VISITANTE, $visitor, $comparison);
    }

    /**
     * Filter the query on the cancelado column
     *
     * Example usage:
     * <code>
     * $query->filterByCanceled(true); // WHERE cancelado = true
     * $query->filterByCanceled('yes'); // WHERE cancelado = true
     * </code>
     *
     * @param     boolean|string $canceled The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByCanceled($canceled = null, $comparison = null)
    {
        if (is_string($canceled)) {
            $canceled = in_array(strtolower($canceled), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePeer::CANCELADO, $canceled, $comparison);
    }

    /**
     * Filter the query on the data_inscricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE data_inscricao = '2011-03-14'
     * $query->filterByDate('now'); // WHERE data_inscricao = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE data_inscricao < '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(SubscribePeer::DATA_INSCRICAO, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(SubscribePeer::DATA_INSCRICAO, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePeer::DATA_INSCRICAO, $date, $comparison);
    }

    /**
     * Filter the query by a related District object
     *
     * @param   District|PropelObjectCollection $district The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDistrict($district, $comparison = null)
    {
        if ($district instanceof District) {
            return $this
                ->addUsingAlias(SubscribePeer::DISTRITO_ID, $district->getId(), $comparison);
        } elseif ($district instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribePeer::DISTRITO_ID, $district->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDistrict() only accepts arguments of type District or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the District relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinDistrict($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('District');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'District');
        }

        return $this;
    }

    /**
     * Use the District relation District object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\DistrictQuery A secondary query class using the current class as primary query
     */
    public function useDistrictQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDistrict($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'District', '\AppBundle\Model\DistrictQuery');
    }

    /**
     * Filter the query by a related SubscribeType object
     *
     * @param   SubscribeType|PropelObjectCollection $subscribeType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribeType($subscribeType, $comparison = null)
    {
        if ($subscribeType instanceof SubscribeType) {
            return $this
                ->addUsingAlias(SubscribePeer::INSCRICAO_TIPO_ID, $subscribeType->getId(), $comparison);
        } elseif ($subscribeType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribePeer::INSCRICAO_TIPO_ID, $subscribeType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySubscribeType() only accepts arguments of type SubscribeType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubscribeType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinSubscribeType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubscribeType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubscribeType');
        }

        return $this;
    }

    /**
     * Use the SubscribeType relation SubscribeType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeTypeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribeType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubscribeType', '\AppBundle\Model\SubscribeTypeQuery');
    }

    /**
     * Filter the query by a related Language object
     *
     * @param   Language|PropelObjectCollection $language The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof Language) {
            return $this
                ->addUsingAlias(SubscribePeer::LINGUA_ID, $language->getId(), $comparison);
        } elseif ($language instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribePeer::LINGUA_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type Language or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\AppBundle\Model\LanguageQuery');
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                ->addUsingAlias(SubscribePeer::USUARIO_ID, $user->getId(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribePeer::USUARIO_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\AppBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related Displacement object
     *
     * @param   Displacement|PropelObjectCollection $displacement The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDisplacement($displacement, $comparison = null)
    {
        if ($displacement instanceof Displacement) {
            return $this
                ->addUsingAlias(SubscribePeer::DESLOCAMENTO_ID, $displacement->getId(), $comparison);
        } elseif ($displacement instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribePeer::DESLOCAMENTO_ID, $displacement->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDisplacement() only accepts arguments of type Displacement or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Displacement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinDisplacement($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Displacement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Displacement');
        }

        return $this;
    }

    /**
     * Use the Displacement relation Displacement object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\DisplacementQuery A secondary query class using the current class as primary query
     */
    public function useDisplacementQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDisplacement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Displacement', '\AppBundle\Model\DisplacementQuery');
    }

    /**
     * Filter the query by a related State object
     *
     * @param   State|PropelObjectCollection $state The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByState($state, $comparison = null)
    {
        if ($state instanceof State) {
            return $this
                ->addUsingAlias(SubscribePeer::ESTADO_ID, $state->getId(), $comparison);
        } elseif ($state instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribePeer::ESTADO_ID, $state->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByState() only accepts arguments of type State or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the State relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinState($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('State');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'State');
        }

        return $this;
    }

    /**
     * Use the State relation State object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\StateQuery A secondary query class using the current class as primary query
     */
    public function useStateQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinState($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'State', '\AppBundle\Model\StateQuery');
    }

    /**
     * Filter the query by a related Country object
     *
     * @param   Country|PropelObjectCollection $country The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCountry($country, $comparison = null)
    {
        if ($country instanceof Country) {
            return $this
                ->addUsingAlias(SubscribePeer::PAIS_ID, $country->getId(), $comparison);
        } elseif ($country instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribePeer::PAIS_ID, $country->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCountry() only accepts arguments of type Country or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Country relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinCountry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Country');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Country');
        }

        return $this;
    }

    /**
     * Use the Country relation Country object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\CountryQuery A secondary query class using the current class as primary query
     */
    public function useCountryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Country', '\AppBundle\Model\CountryQuery');
    }

    /**
     * Filter the query by a related Train object
     *
     * @param   Train|PropelObjectCollection $train  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTrain($train, $comparison = null)
    {
        if ($train instanceof Train) {
            return $this
                ->addUsingAlias(SubscribePeer::INSCRICAO_ID, $train->getSubscribeId(), $comparison);
        } elseif ($train instanceof PropelObjectCollection) {
            return $this
                ->useTrainQuery()
                ->filterByPrimaryKeys($train->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTrain() only accepts arguments of type Train or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Train relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinTrain($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Train');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Train');
        }

        return $this;
    }

    /**
     * Use the Train relation Train object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\TrainQuery A secondary query class using the current class as primary query
     */
    public function useTrainQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTrain($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Train', '\AppBundle\Model\TrainQuery');
    }

    /**
     * Filter the query by a related Donation object
     *
     * @param   Donation|PropelObjectCollection $donation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDonation($donation, $comparison = null)
    {
        if ($donation instanceof Donation) {
            return $this
                ->addUsingAlias(SubscribePeer::INSCRICAO_ID, $donation->getSubscribeId(), $comparison);
        } elseif ($donation instanceof PropelObjectCollection) {
            return $this
                ->useDonationQuery()
                ->filterByPrimaryKeys($donation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDonation() only accepts arguments of type Donation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Donation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinDonation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Donation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Donation');
        }

        return $this;
    }

    /**
     * Use the Donation relation Donation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\DonationQuery A secondary query class using the current class as primary query
     */
    public function useDonationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDonation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Donation', '\AppBundle\Model\DonationQuery');
    }

    /**
     * Filter the query by a related SubscribePayment object
     *
     * @param   SubscribePayment|PropelObjectCollection $subscribePayment  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribePayment($subscribePayment, $comparison = null)
    {
        if ($subscribePayment instanceof SubscribePayment) {
            return $this
                ->addUsingAlias(SubscribePeer::INSCRICAO_ID, $subscribePayment->getSubscribeId(), $comparison);
        } elseif ($subscribePayment instanceof PropelObjectCollection) {
            return $this
                ->useSubscribePaymentQuery()
                ->filterByPrimaryKeys($subscribePayment->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubscribePayment() only accepts arguments of type SubscribePayment or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubscribePayment relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinSubscribePayment($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubscribePayment');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubscribePayment');
        }

        return $this;
    }

    /**
     * Use the SubscribePayment relation SubscribePayment object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribePaymentQuery A secondary query class using the current class as primary query
     */
    public function useSubscribePaymentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribePayment($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubscribePayment', '\AppBundle\Model\SubscribePaymentQuery');
    }

    /**
     * Filter the query by a related WorkshopSubscribe object
     *
     * @param   WorkshopSubscribe|PropelObjectCollection $workshopSubscribe  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWorkshopSubscribe($workshopSubscribe, $comparison = null)
    {
        if ($workshopSubscribe instanceof WorkshopSubscribe) {
            return $this
                ->addUsingAlias(SubscribePeer::INSCRICAO_ID, $workshopSubscribe->getSubscribeId(), $comparison);
        } elseif ($workshopSubscribe instanceof PropelObjectCollection) {
            return $this
                ->useWorkshopSubscribeQuery()
                ->filterByPrimaryKeys($workshopSubscribe->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWorkshopSubscribe() only accepts arguments of type WorkshopSubscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WorkshopSubscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinWorkshopSubscribe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WorkshopSubscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WorkshopSubscribe');
        }

        return $this;
    }

    /**
     * Use the WorkshopSubscribe relation WorkshopSubscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\WorkshopSubscribeQuery A secondary query class using the current class as primary query
     */
    public function useWorkshopSubscribeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWorkshopSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WorkshopSubscribe', '\AppBundle\Model\WorkshopSubscribeQuery');
    }

    /**
     * Filter the query by a related Transfer object
     *
     * @param   Transfer|PropelObjectCollection $transfer  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTransfer($transfer, $comparison = null)
    {
        if ($transfer instanceof Transfer) {
            return $this
                ->addUsingAlias(SubscribePeer::INSCRICAO_ID, $transfer->getSubscribeId(), $comparison);
        } elseif ($transfer instanceof PropelObjectCollection) {
            return $this
                ->useTransferQuery()
                ->filterByPrimaryKeys($transfer->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTransfer() only accepts arguments of type Transfer or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Transfer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function joinTransfer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Transfer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Transfer');
        }

        return $this;
    }

    /**
     * Use the Transfer relation Transfer object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\TransferQuery A secondary query class using the current class as primary query
     */
    public function useTransferQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTransfer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Transfer', '\AppBundle\Model\TransferQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Subscribe $subscribe Object to remove from the list of results
     *
     * @return SubscribeQuery The current query, for fluid interface
     */
    public function prune($subscribe = null)
    {
        if ($subscribe) {
            $this->addUsingAlias(SubscribePeer::INSCRICAO_ID, $subscribe->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
