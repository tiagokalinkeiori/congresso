<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePaymentPeer;
use AppBundle\Model\SubscribePaymentQuery;
use AppBundle\Model\SubscribeQuery;

abstract class BaseSubscribePayment extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\SubscribePaymentPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SubscribePaymentPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the inscricao_pagamento_id field.
     * @var        int
     */
    protected $inscricao_pagamento_id;

    /**
     * The value for the inscricao_id field.
     * @var        int
     */
    protected $inscricao_id;

    /**
     * The value for the data_geracao field.
     * @var        string
     */
    protected $data_geracao;

    /**
     * The value for the data_vencimento field.
     * @var        string
     */
    protected $data_vencimento;

    /**
     * The value for the data_pagamento field.
     * @var        string
     */
    protected $data_pagamento;

    /**
     * The value for the valor field.
     * @var        string
     */
    protected $valor;

    /**
     * The value for the pago field.
     * @var        boolean
     */
    protected $pago;

    /**
     * The value for the primeira field.
     * @var        boolean
     */
    protected $primeira;

    /**
     * The value for the cancelada field.
     * @var        boolean
     */
    protected $cancelada;

    /**
     * The value for the url field.
     * @var        string
     */
    protected $url;

    /**
     * The value for the nosso_numero field.
     * @var        string
     */
    protected $nosso_numero;

    /**
     * The value for the linha_digitavel field.
     * @var        string
     */
    protected $linha_digitavel;

    /**
     * The value for the enviado field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $enviado;

    /**
     * @var        Subscribe
     */
    protected $aSubscribe;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->enviado = false;
    }

    /**
     * Initializes internal state of BaseSubscribePayment object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [inscricao_pagamento_id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->inscricao_pagamento_id;
    }

    /**
     * Get the [inscricao_id] column value.
     *
     * @return int
     */
    public function getSubscribeId()
    {

        return $this->inscricao_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_geracao] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = null)
    {
        if ($this->data_geracao === null) {
            return null;
        }

        if ($this->data_geracao === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_geracao);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_geracao, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [data_vencimento] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDueDate($format = null)
    {
        if ($this->data_vencimento === null) {
            return null;
        }

        if ($this->data_vencimento === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_vencimento);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_vencimento, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [data_pagamento] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPaymentDate($format = null)
    {
        if ($this->data_pagamento === null) {
            return null;
        }

        if ($this->data_pagamento === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_pagamento);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_pagamento, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [valor] column value.
     *
     * @return string
     */
    public function getPrice()
    {

        return $this->valor;
    }

    /**
     * Get the [pago] column value.
     *
     * @return boolean
     */
    public function getPaid()
    {

        return $this->pago;
    }

    /**
     * Get the [primeira] column value.
     *
     * @return boolean
     */
    public function getFirst()
    {

        return $this->primeira;
    }

    /**
     * Get the [cancelada] column value.
     *
     * @return boolean
     */
    public function getCancel()
    {

        return $this->cancelada;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {

        return $this->url;
    }

    /**
     * Get the [nosso_numero] column value.
     *
     * @return string
     */
    public function getOwerNumber()
    {

        return $this->nosso_numero;
    }

    /**
     * Get the [linha_digitavel] column value.
     *
     * @return string
     */
    public function getDigitableLine()
    {

        return $this->linha_digitavel;
    }

    /**
     * Get the [enviado] column value.
     *
     * @return boolean
     */
    public function getSent()
    {

        return $this->enviado;
    }

    /**
     * Set the value of [inscricao_pagamento_id] column.
     *
     * @param  int $v new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_pagamento_id !== $v) {
            $this->inscricao_pagamento_id = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [inscricao_id] column.
     *
     * @param  int $v new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setSubscribeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_id !== $v) {
            $this->inscricao_id = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::INSCRICAO_ID;
        }

        if ($this->aSubscribe !== null && $this->aSubscribe->getId() !== $v) {
            $this->aSubscribe = null;
        }


        return $this;
    } // setSubscribeId()

    /**
     * Sets the value of [data_geracao] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_geracao !== null || $dt !== null) {
            $currentDateAsString = ($this->data_geracao !== null && $tmpDt = new DateTime($this->data_geracao)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_geracao = $newDateAsString;
                $this->modifiedColumns[] = SubscribePaymentPeer::DATA_GERACAO;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [data_vencimento] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setDueDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_vencimento !== null || $dt !== null) {
            $currentDateAsString = ($this->data_vencimento !== null && $tmpDt = new DateTime($this->data_vencimento)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_vencimento = $newDateAsString;
                $this->modifiedColumns[] = SubscribePaymentPeer::DATA_VENCIMENTO;
            }
        } // if either are not null


        return $this;
    } // setDueDate()

    /**
     * Sets the value of [data_pagamento] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setPaymentDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_pagamento !== null || $dt !== null) {
            $currentDateAsString = ($this->data_pagamento !== null && $tmpDt = new DateTime($this->data_pagamento)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_pagamento = $newDateAsString;
                $this->modifiedColumns[] = SubscribePaymentPeer::DATA_PAGAMENTO;
            }
        } // if either are not null


        return $this;
    } // setPaymentDate()

    /**
     * Set the value of [valor] column.
     *
     * @param  string $v new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::VALOR;
        }


        return $this;
    } // setPrice()

    /**
     * Sets the value of the [pago] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setPaid($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->pago !== $v) {
            $this->pago = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::PAGO;
        }


        return $this;
    } // setPaid()

    /**
     * Sets the value of the [primeira] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setFirst($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->primeira !== $v) {
            $this->primeira = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::PRIMEIRA;
        }


        return $this;
    } // setFirst()

    /**
     * Sets the value of the [cancelada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setCancel($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cancelada !== $v) {
            $this->cancelada = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::CANCELADA;
        }


        return $this;
    } // setCancel()

    /**
     * Set the value of [url] column.
     *
     * @param  string $v new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::URL;
        }


        return $this;
    } // setUrl()

    /**
     * Set the value of [nosso_numero] column.
     *
     * @param  string $v new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setOwerNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nosso_numero !== $v) {
            $this->nosso_numero = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::NOSSO_NUMERO;
        }


        return $this;
    } // setOwerNumber()

    /**
     * Set the value of [linha_digitavel] column.
     *
     * @param  string $v new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setDigitableLine($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->linha_digitavel !== $v) {
            $this->linha_digitavel = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::LINHA_DIGITAVEL;
        }


        return $this;
    } // setDigitableLine()

    /**
     * Sets the value of the [enviado] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return SubscribePayment The current object (for fluent API support)
     */
    public function setSent($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->enviado !== $v) {
            $this->enviado = $v;
            $this->modifiedColumns[] = SubscribePaymentPeer::ENVIADO;
        }


        return $this;
    } // setSent()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->enviado !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->inscricao_pagamento_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->inscricao_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->data_geracao = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->data_vencimento = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->data_pagamento = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->valor = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->pago = ($row[$startcol + 6] !== null) ? (boolean) $row[$startcol + 6] : null;
            $this->primeira = ($row[$startcol + 7] !== null) ? (boolean) $row[$startcol + 7] : null;
            $this->cancelada = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
            $this->url = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->nosso_numero = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->linha_digitavel = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->enviado = ($row[$startcol + 12] !== null) ? (boolean) $row[$startcol + 12] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 13; // 13 = SubscribePaymentPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SubscribePayment object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSubscribe !== null && $this->inscricao_id !== $this->aSubscribe->getId()) {
            $this->aSubscribe = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SubscribePaymentPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSubscribe = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SubscribePaymentQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SubscribePaymentPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribe !== null) {
                if ($this->aSubscribe->isModified() || $this->aSubscribe->isNew()) {
                    $affectedRows += $this->aSubscribe->save($con);
                }
                $this->setSubscribe($this->aSubscribe);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID;
        if (null !== $this->inscricao_pagamento_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_pagamento_id`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::INSCRICAO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_id`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::DATA_GERACAO)) {
            $modifiedColumns[':p' . $index++]  = '`data_geracao`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::DATA_VENCIMENTO)) {
            $modifiedColumns[':p' . $index++]  = '`data_vencimento`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::DATA_PAGAMENTO)) {
            $modifiedColumns[':p' . $index++]  = '`data_pagamento`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::VALOR)) {
            $modifiedColumns[':p' . $index++]  = '`valor`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::PAGO)) {
            $modifiedColumns[':p' . $index++]  = '`pago`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::PRIMEIRA)) {
            $modifiedColumns[':p' . $index++]  = '`primeira`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::CANCELADA)) {
            $modifiedColumns[':p' . $index++]  = '`cancelada`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::URL)) {
            $modifiedColumns[':p' . $index++]  = '`url`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::NOSSO_NUMERO)) {
            $modifiedColumns[':p' . $index++]  = '`nosso_numero`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::LINHA_DIGITAVEL)) {
            $modifiedColumns[':p' . $index++]  = '`linha_digitavel`';
        }
        if ($this->isColumnModified(SubscribePaymentPeer::ENVIADO)) {
            $modifiedColumns[':p' . $index++]  = '`enviado`';
        }

        $sql = sprintf(
            'INSERT INTO `inscricao_pagamento` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`inscricao_pagamento_id`':
                        $stmt->bindValue($identifier, $this->inscricao_pagamento_id, PDO::PARAM_INT);
                        break;
                    case '`inscricao_id`':
                        $stmt->bindValue($identifier, $this->inscricao_id, PDO::PARAM_INT);
                        break;
                    case '`data_geracao`':
                        $stmt->bindValue($identifier, $this->data_geracao, PDO::PARAM_STR);
                        break;
                    case '`data_vencimento`':
                        $stmt->bindValue($identifier, $this->data_vencimento, PDO::PARAM_STR);
                        break;
                    case '`data_pagamento`':
                        $stmt->bindValue($identifier, $this->data_pagamento, PDO::PARAM_STR);
                        break;
                    case '`valor`':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                    case '`pago`':
                        $stmt->bindValue($identifier, (int) $this->pago, PDO::PARAM_INT);
                        break;
                    case '`primeira`':
                        $stmt->bindValue($identifier, (int) $this->primeira, PDO::PARAM_INT);
                        break;
                    case '`cancelada`':
                        $stmt->bindValue($identifier, (int) $this->cancelada, PDO::PARAM_INT);
                        break;
                    case '`url`':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`nosso_numero`':
                        $stmt->bindValue($identifier, $this->nosso_numero, PDO::PARAM_STR);
                        break;
                    case '`linha_digitavel`':
                        $stmt->bindValue($identifier, $this->linha_digitavel, PDO::PARAM_STR);
                        break;
                    case '`enviado`':
                        $stmt->bindValue($identifier, (int) $this->enviado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribe !== null) {
                if (!$this->aSubscribe->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSubscribe->getValidationFailures());
                }
            }


            if (($retval = SubscribePaymentPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SubscribePaymentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSubscribeId();
                break;
            case 2:
                return $this->getCreateDate();
                break;
            case 3:
                return $this->getDueDate();
                break;
            case 4:
                return $this->getPaymentDate();
                break;
            case 5:
                return $this->getPrice();
                break;
            case 6:
                return $this->getPaid();
                break;
            case 7:
                return $this->getFirst();
                break;
            case 8:
                return $this->getCancel();
                break;
            case 9:
                return $this->getUrl();
                break;
            case 10:
                return $this->getOwerNumber();
                break;
            case 11:
                return $this->getDigitableLine();
                break;
            case 12:
                return $this->getSent();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SubscribePayment'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SubscribePayment'][$this->getPrimaryKey()] = true;
        $keys = SubscribePaymentPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSubscribeId(),
            $keys[2] => $this->getCreateDate(),
            $keys[3] => $this->getDueDate(),
            $keys[4] => $this->getPaymentDate(),
            $keys[5] => $this->getPrice(),
            $keys[6] => $this->getPaid(),
            $keys[7] => $this->getFirst(),
            $keys[8] => $this->getCancel(),
            $keys[9] => $this->getUrl(),
            $keys[10] => $this->getOwerNumber(),
            $keys[11] => $this->getDigitableLine(),
            $keys[12] => $this->getSent(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSubscribe) {
                $result['Subscribe'] = $this->aSubscribe->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SubscribePaymentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSubscribeId($value);
                break;
            case 2:
                $this->setCreateDate($value);
                break;
            case 3:
                $this->setDueDate($value);
                break;
            case 4:
                $this->setPaymentDate($value);
                break;
            case 5:
                $this->setPrice($value);
                break;
            case 6:
                $this->setPaid($value);
                break;
            case 7:
                $this->setFirst($value);
                break;
            case 8:
                $this->setCancel($value);
                break;
            case 9:
                $this->setUrl($value);
                break;
            case 10:
                $this->setOwerNumber($value);
                break;
            case 11:
                $this->setDigitableLine($value);
                break;
            case 12:
                $this->setSent($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SubscribePaymentPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSubscribeId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCreateDate($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDueDate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPaymentDate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPrice($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setPaid($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setFirst($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCancel($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setUrl($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setOwerNumber($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setDigitableLine($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setSent($arr[$keys[12]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SubscribePaymentPeer::DATABASE_NAME);

        if ($this->isColumnModified(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID)) $criteria->add(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $this->inscricao_pagamento_id);
        if ($this->isColumnModified(SubscribePaymentPeer::INSCRICAO_ID)) $criteria->add(SubscribePaymentPeer::INSCRICAO_ID, $this->inscricao_id);
        if ($this->isColumnModified(SubscribePaymentPeer::DATA_GERACAO)) $criteria->add(SubscribePaymentPeer::DATA_GERACAO, $this->data_geracao);
        if ($this->isColumnModified(SubscribePaymentPeer::DATA_VENCIMENTO)) $criteria->add(SubscribePaymentPeer::DATA_VENCIMENTO, $this->data_vencimento);
        if ($this->isColumnModified(SubscribePaymentPeer::DATA_PAGAMENTO)) $criteria->add(SubscribePaymentPeer::DATA_PAGAMENTO, $this->data_pagamento);
        if ($this->isColumnModified(SubscribePaymentPeer::VALOR)) $criteria->add(SubscribePaymentPeer::VALOR, $this->valor);
        if ($this->isColumnModified(SubscribePaymentPeer::PAGO)) $criteria->add(SubscribePaymentPeer::PAGO, $this->pago);
        if ($this->isColumnModified(SubscribePaymentPeer::PRIMEIRA)) $criteria->add(SubscribePaymentPeer::PRIMEIRA, $this->primeira);
        if ($this->isColumnModified(SubscribePaymentPeer::CANCELADA)) $criteria->add(SubscribePaymentPeer::CANCELADA, $this->cancelada);
        if ($this->isColumnModified(SubscribePaymentPeer::URL)) $criteria->add(SubscribePaymentPeer::URL, $this->url);
        if ($this->isColumnModified(SubscribePaymentPeer::NOSSO_NUMERO)) $criteria->add(SubscribePaymentPeer::NOSSO_NUMERO, $this->nosso_numero);
        if ($this->isColumnModified(SubscribePaymentPeer::LINHA_DIGITAVEL)) $criteria->add(SubscribePaymentPeer::LINHA_DIGITAVEL, $this->linha_digitavel);
        if ($this->isColumnModified(SubscribePaymentPeer::ENVIADO)) $criteria->add(SubscribePaymentPeer::ENVIADO, $this->enviado);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SubscribePaymentPeer::DATABASE_NAME);
        $criteria->add(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $this->inscricao_pagamento_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (inscricao_pagamento_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SubscribePayment (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSubscribeId($this->getSubscribeId());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setDueDate($this->getDueDate());
        $copyObj->setPaymentDate($this->getPaymentDate());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setPaid($this->getPaid());
        $copyObj->setFirst($this->getFirst());
        $copyObj->setCancel($this->getCancel());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setOwerNumber($this->getOwerNumber());
        $copyObj->setDigitableLine($this->getDigitableLine());
        $copyObj->setSent($this->getSent());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SubscribePayment Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SubscribePaymentPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SubscribePaymentPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Subscribe object.
     *
     * @param                  Subscribe $v
     * @return SubscribePayment The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSubscribe(Subscribe $v = null)
    {
        if ($v === null) {
            $this->setSubscribeId(NULL);
        } else {
            $this->setSubscribeId($v->getId());
        }

        $this->aSubscribe = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Subscribe object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribePayment($this);
        }


        return $this;
    }


    /**
     * Get the associated Subscribe object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Subscribe The associated Subscribe object.
     * @throws PropelException
     */
    public function getSubscribe(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSubscribe === null && ($this->inscricao_id !== null) && $doQuery) {
            $this->aSubscribe = SubscribeQuery::create()->findPk($this->inscricao_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSubscribe->addSubscribePayments($this);
             */
        }

        return $this->aSubscribe;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->inscricao_pagamento_id = null;
        $this->inscricao_id = null;
        $this->data_geracao = null;
        $this->data_vencimento = null;
        $this->data_pagamento = null;
        $this->valor = null;
        $this->pago = null;
        $this->primeira = null;
        $this->cancelada = null;
        $this->url = null;
        $this->nosso_numero = null;
        $this->linha_digitavel = null;
        $this->enviado = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSubscribe instanceof Persistent) {
              $this->aSubscribe->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSubscribe = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SubscribePaymentPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
