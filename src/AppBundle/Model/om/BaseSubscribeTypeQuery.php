<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribeType;
use AppBundle\Model\SubscribeTypeName;
use AppBundle\Model\SubscribeTypePeer;
use AppBundle\Model\SubscribeTypePrice;
use AppBundle\Model\SubscribeTypeQuery;

/**
 * @method SubscribeTypeQuery orderById($order = Criteria::ASC) Order by the inscricao_tipo_id column
 * @method SubscribeTypeQuery orderBySpaceAvailable($order = Criteria::ASC) Order by the vaga column
 * @method SubscribeTypeQuery orderByGenre($order = Criteria::ASC) Order by the sexo column
 *
 * @method SubscribeTypeQuery groupById() Group by the inscricao_tipo_id column
 * @method SubscribeTypeQuery groupBySpaceAvailable() Group by the vaga column
 * @method SubscribeTypeQuery groupByGenre() Group by the sexo column
 *
 * @method SubscribeTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SubscribeTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SubscribeTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SubscribeTypeQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method SubscribeTypeQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method SubscribeTypeQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method SubscribeTypeQuery leftJoinSubscribeTypeName($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubscribeTypeName relation
 * @method SubscribeTypeQuery rightJoinSubscribeTypeName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubscribeTypeName relation
 * @method SubscribeTypeQuery innerJoinSubscribeTypeName($relationAlias = null) Adds a INNER JOIN clause to the query using the SubscribeTypeName relation
 *
 * @method SubscribeTypeQuery leftJoinSubscribeTypePrice($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubscribeTypePrice relation
 * @method SubscribeTypeQuery rightJoinSubscribeTypePrice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubscribeTypePrice relation
 * @method SubscribeTypeQuery innerJoinSubscribeTypePrice($relationAlias = null) Adds a INNER JOIN clause to the query using the SubscribeTypePrice relation
 *
 * @method SubscribeType findOne(PropelPDO $con = null) Return the first SubscribeType matching the query
 * @method SubscribeType findOneOrCreate(PropelPDO $con = null) Return the first SubscribeType matching the query, or a new SubscribeType object populated from the query conditions when no match is found
 *
 * @method SubscribeType findOneBySpaceAvailable(int $vaga) Return the first SubscribeType filtered by the vaga column
 * @method SubscribeType findOneByGenre(string $sexo) Return the first SubscribeType filtered by the sexo column
 *
 * @method array findById(int $inscricao_tipo_id) Return SubscribeType objects filtered by the inscricao_tipo_id column
 * @method array findBySpaceAvailable(int $vaga) Return SubscribeType objects filtered by the vaga column
 * @method array findByGenre(string $sexo) Return SubscribeType objects filtered by the sexo column
 */
abstract class BaseSubscribeTypeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSubscribeTypeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\SubscribeType';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SubscribeTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SubscribeTypeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SubscribeTypeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SubscribeTypeQuery) {
            return $criteria;
        }
        $query = new SubscribeTypeQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SubscribeType|SubscribeType[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SubscribeTypePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SubscribeType A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SubscribeType A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `inscricao_tipo_id`, `vaga`, `sexo` FROM `inscricao_tipo` WHERE `inscricao_tipo_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SubscribeType();
            $obj->hydrate($row);
            SubscribeTypePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SubscribeType|SubscribeType[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SubscribeType[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the inscricao_tipo_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE inscricao_tipo_id = 1234
     * $query->filterById(array(12, 34)); // WHERE inscricao_tipo_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE inscricao_tipo_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE inscricao_tipo_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vaga column
     *
     * Example usage:
     * <code>
     * $query->filterBySpaceAvailable(1234); // WHERE vaga = 1234
     * $query->filterBySpaceAvailable(array(12, 34)); // WHERE vaga IN (12, 34)
     * $query->filterBySpaceAvailable(array('min' => 12)); // WHERE vaga >= 12
     * $query->filterBySpaceAvailable(array('max' => 12)); // WHERE vaga <= 12
     * </code>
     *
     * @param     mixed $spaceAvailable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function filterBySpaceAvailable($spaceAvailable = null, $comparison = null)
    {
        if (is_array($spaceAvailable)) {
            $useMinMax = false;
            if (isset($spaceAvailable['min'])) {
                $this->addUsingAlias(SubscribeTypePeer::VAGA, $spaceAvailable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($spaceAvailable['max'])) {
                $this->addUsingAlias(SubscribeTypePeer::VAGA, $spaceAvailable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePeer::VAGA, $spaceAvailable, $comparison);
    }

    /**
     * Filter the query on the sexo column
     *
     * Example usage:
     * <code>
     * $query->filterByGenre('fooValue');   // WHERE sexo = 'fooValue'
     * $query->filterByGenre('%fooValue%'); // WHERE sexo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $genre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function filterByGenre($genre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($genre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $genre)) {
                $genre = str_replace('*', '%', $genre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribeTypePeer::SEXO, $genre, $comparison);
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $subscribe->getSubscribeTypeId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            return $this
                ->useSubscribeQuery()
                ->filterByPrimaryKeys($subscribe->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Filter the query by a related SubscribeTypeName object
     *
     * @param   SubscribeTypeName|PropelObjectCollection $subscribeTypeName  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribeTypeName($subscribeTypeName, $comparison = null)
    {
        if ($subscribeTypeName instanceof SubscribeTypeName) {
            return $this
                ->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $subscribeTypeName->getSubscribeTypeId(), $comparison);
        } elseif ($subscribeTypeName instanceof PropelObjectCollection) {
            return $this
                ->useSubscribeTypeNameQuery()
                ->filterByPrimaryKeys($subscribeTypeName->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubscribeTypeName() only accepts arguments of type SubscribeTypeName or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubscribeTypeName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function joinSubscribeTypeName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubscribeTypeName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubscribeTypeName');
        }

        return $this;
    }

    /**
     * Use the SubscribeTypeName relation SubscribeTypeName object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeTypeNameQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeTypeNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribeTypeName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubscribeTypeName', '\AppBundle\Model\SubscribeTypeNameQuery');
    }

    /**
     * Filter the query by a related SubscribeTypePrice object
     *
     * @param   SubscribeTypePrice|PropelObjectCollection $subscribeTypePrice  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribeTypePrice($subscribeTypePrice, $comparison = null)
    {
        if ($subscribeTypePrice instanceof SubscribeTypePrice) {
            return $this
                ->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $subscribeTypePrice->getSubscribeTypeId(), $comparison);
        } elseif ($subscribeTypePrice instanceof PropelObjectCollection) {
            return $this
                ->useSubscribeTypePriceQuery()
                ->filterByPrimaryKeys($subscribeTypePrice->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubscribeTypePrice() only accepts arguments of type SubscribeTypePrice or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubscribeTypePrice relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function joinSubscribeTypePrice($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubscribeTypePrice');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubscribeTypePrice');
        }

        return $this;
    }

    /**
     * Use the SubscribeTypePrice relation SubscribeTypePrice object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeTypePriceQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeTypePriceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribeTypePrice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubscribeTypePrice', '\AppBundle\Model\SubscribeTypePriceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SubscribeType $subscribeType Object to remove from the list of results
     *
     * @return SubscribeTypeQuery The current query, for fluid interface
     */
    public function prune($subscribeType = null)
    {
        if ($subscribeType) {
            $this->addUsingAlias(SubscribeTypePeer::INSCRICAO_TIPO_ID, $subscribeType->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
