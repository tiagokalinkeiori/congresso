<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Country;
use AppBundle\Model\CountryPeer;
use AppBundle\Model\CountryQuery;
use AppBundle\Model\PaymentText;
use AppBundle\Model\PaymentTextQuery;
use AppBundle\Model\State;
use AppBundle\Model\StateQuery;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribeQuery;

abstract class BaseCountry extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\CountryPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CountryPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the pais_id field.
     * @var        int
     */
    protected $pais_id;

    /**
     * The value for the nome field.
     * @var        string
     */
    protected $nome;

    /**
     * The value for the codigo field.
     * @var        string
     */
    protected $codigo;

    /**
     * @var        PropelObjectCollection|State[] Collection to store aggregation of State objects.
     */
    protected $collStates;
    protected $collStatesPartial;

    /**
     * @var        PropelObjectCollection|Subscribe[] Collection to store aggregation of Subscribe objects.
     */
    protected $collSubscribes;
    protected $collSubscribesPartial;

    /**
     * @var        PropelObjectCollection|PaymentText[] Collection to store aggregation of PaymentText objects.
     */
    protected $collPaymentTexts;
    protected $collPaymentTextsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $statesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $subscribesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $paymentTextsScheduledForDeletion = null;

    /**
     * Get the [pais_id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->pais_id;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->nome;
    }

    /**
     * Get the [codigo] column value.
     *
     * @return string
     */
    public function getCode()
    {

        return $this->codigo;
    }

    /**
     * Set the value of [pais_id] column.
     *
     * @param  int $v new value
     * @return Country The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pais_id !== $v) {
            $this->pais_id = $v;
            $this->modifiedColumns[] = CountryPeer::PAIS_ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nome] column.
     *
     * @param  string $v new value
     * @return Country The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[] = CountryPeer::NOME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [codigo] column.
     *
     * @param  string $v new value
     * @return Country The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo !== $v) {
            $this->codigo = $v;
            $this->modifiedColumns[] = CountryPeer::CODIGO;
        }


        return $this;
    } // setCode()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->pais_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nome = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->codigo = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 3; // 3 = CountryPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Country object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CountryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CountryPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collStates = null;

            $this->collSubscribes = null;

            $this->collPaymentTexts = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CountryPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CountryQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CountryPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CountryPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->statesScheduledForDeletion !== null) {
                if (!$this->statesScheduledForDeletion->isEmpty()) {
                    StateQuery::create()
                        ->filterByPrimaryKeys($this->statesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->statesScheduledForDeletion = null;
                }
            }

            if ($this->collStates !== null) {
                foreach ($this->collStates as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->subscribesScheduledForDeletion !== null) {
                if (!$this->subscribesScheduledForDeletion->isEmpty()) {
                    SubscribeQuery::create()
                        ->filterByPrimaryKeys($this->subscribesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->subscribesScheduledForDeletion = null;
                }
            }

            if ($this->collSubscribes !== null) {
                foreach ($this->collSubscribes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->paymentTextsScheduledForDeletion !== null) {
                if (!$this->paymentTextsScheduledForDeletion->isEmpty()) {
                    foreach ($this->paymentTextsScheduledForDeletion as $paymentText) {
                        // need to save related object because we set the relation to null
                        $paymentText->save($con);
                    }
                    $this->paymentTextsScheduledForDeletion = null;
                }
            }

            if ($this->collPaymentTexts !== null) {
                foreach ($this->collPaymentTexts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CountryPeer::PAIS_ID;
        if (null !== $this->pais_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CountryPeer::PAIS_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CountryPeer::PAIS_ID)) {
            $modifiedColumns[':p' . $index++]  = '`pais_id`';
        }
        if ($this->isColumnModified(CountryPeer::NOME)) {
            $modifiedColumns[':p' . $index++]  = '`nome`';
        }
        if ($this->isColumnModified(CountryPeer::CODIGO)) {
            $modifiedColumns[':p' . $index++]  = '`codigo`';
        }

        $sql = sprintf(
            'INSERT INTO `pais` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`pais_id`':
                        $stmt->bindValue($identifier, $this->pais_id, PDO::PARAM_INT);
                        break;
                    case '`nome`':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case '`codigo`':
                        $stmt->bindValue($identifier, $this->codigo, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CountryPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collStates !== null) {
                    foreach ($this->collStates as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSubscribes !== null) {
                    foreach ($this->collSubscribes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPaymentTexts !== null) {
                    foreach ($this->collPaymentTexts as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CountryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getCode();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Country'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Country'][$this->getPrimaryKey()] = true;
        $keys = CountryPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getCode(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collStates) {
                $result['States'] = $this->collStates->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSubscribes) {
                $result['Subscribes'] = $this->collSubscribes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPaymentTexts) {
                $result['PaymentTexts'] = $this->collPaymentTexts->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CountryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setCode($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CountryPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCode($arr[$keys[2]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CountryPeer::DATABASE_NAME);

        if ($this->isColumnModified(CountryPeer::PAIS_ID)) $criteria->add(CountryPeer::PAIS_ID, $this->pais_id);
        if ($this->isColumnModified(CountryPeer::NOME)) $criteria->add(CountryPeer::NOME, $this->nome);
        if ($this->isColumnModified(CountryPeer::CODIGO)) $criteria->add(CountryPeer::CODIGO, $this->codigo);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CountryPeer::DATABASE_NAME);
        $criteria->add(CountryPeer::PAIS_ID, $this->pais_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (pais_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Country (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setCode($this->getCode());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getStates() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addState($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSubscribes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSubscribe($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPaymentTexts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPaymentText($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Country Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CountryPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CountryPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('State' == $relationName) {
            $this->initStates();
        }
        if ('Subscribe' == $relationName) {
            $this->initSubscribes();
        }
        if ('PaymentText' == $relationName) {
            $this->initPaymentTexts();
        }
    }

    /**
     * Clears out the collStates collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Country The current object (for fluent API support)
     * @see        addStates()
     */
    public function clearStates()
    {
        $this->collStates = null; // important to set this to null since that means it is uninitialized
        $this->collStatesPartial = null;

        return $this;
    }

    /**
     * reset is the collStates collection loaded partially
     *
     * @return void
     */
    public function resetPartialStates($v = true)
    {
        $this->collStatesPartial = $v;
    }

    /**
     * Initializes the collStates collection.
     *
     * By default this just sets the collStates collection to an empty array (like clearcollStates());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initStates($overrideExisting = true)
    {
        if (null !== $this->collStates && !$overrideExisting) {
            return;
        }
        $this->collStates = new PropelObjectCollection();
        $this->collStates->setModel('State');
    }

    /**
     * Gets an array of State objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Country is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|State[] List of State objects
     * @throws PropelException
     */
    public function getStates($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collStatesPartial && !$this->isNew();
        if (null === $this->collStates || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collStates) {
                // return empty collection
                $this->initStates();
            } else {
                $collStates = StateQuery::create(null, $criteria)
                    ->filterByCountry($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collStatesPartial && count($collStates)) {
                      $this->initStates(false);

                      foreach ($collStates as $obj) {
                        if (false == $this->collStates->contains($obj)) {
                          $this->collStates->append($obj);
                        }
                      }

                      $this->collStatesPartial = true;
                    }

                    $collStates->getInternalIterator()->rewind();

                    return $collStates;
                }

                if ($partial && $this->collStates) {
                    foreach ($this->collStates as $obj) {
                        if ($obj->isNew()) {
                            $collStates[] = $obj;
                        }
                    }
                }

                $this->collStates = $collStates;
                $this->collStatesPartial = false;
            }
        }

        return $this->collStates;
    }

    /**
     * Sets a collection of State objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $states A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Country The current object (for fluent API support)
     */
    public function setStates(PropelCollection $states, PropelPDO $con = null)
    {
        $statesToDelete = $this->getStates(new Criteria(), $con)->diff($states);


        $this->statesScheduledForDeletion = $statesToDelete;

        foreach ($statesToDelete as $stateRemoved) {
            $stateRemoved->setCountry(null);
        }

        $this->collStates = null;
        foreach ($states as $state) {
            $this->addState($state);
        }

        $this->collStates = $states;
        $this->collStatesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related State objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related State objects.
     * @throws PropelException
     */
    public function countStates(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collStatesPartial && !$this->isNew();
        if (null === $this->collStates || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collStates) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getStates());
            }
            $query = StateQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCountry($this)
                ->count($con);
        }

        return count($this->collStates);
    }

    /**
     * Method called to associate a State object to this object
     * through the State foreign key attribute.
     *
     * @param    State $l State
     * @return Country The current object (for fluent API support)
     */
    public function addState(State $l)
    {
        if ($this->collStates === null) {
            $this->initStates();
            $this->collStatesPartial = true;
        }

        if (!in_array($l, $this->collStates->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddState($l);

            if ($this->statesScheduledForDeletion and $this->statesScheduledForDeletion->contains($l)) {
                $this->statesScheduledForDeletion->remove($this->statesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	State $state The state object to add.
     */
    protected function doAddState($state)
    {
        $this->collStates[]= $state;
        $state->setCountry($this);
    }

    /**
     * @param	State $state The state object to remove.
     * @return Country The current object (for fluent API support)
     */
    public function removeState($state)
    {
        if ($this->getStates()->contains($state)) {
            $this->collStates->remove($this->collStates->search($state));
            if (null === $this->statesScheduledForDeletion) {
                $this->statesScheduledForDeletion = clone $this->collStates;
                $this->statesScheduledForDeletion->clear();
            }
            $this->statesScheduledForDeletion[]= clone $state;
            $state->setCountry(null);
        }

        return $this;
    }

    /**
     * Clears out the collSubscribes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Country The current object (for fluent API support)
     * @see        addSubscribes()
     */
    public function clearSubscribes()
    {
        $this->collSubscribes = null; // important to set this to null since that means it is uninitialized
        $this->collSubscribesPartial = null;

        return $this;
    }

    /**
     * reset is the collSubscribes collection loaded partially
     *
     * @return void
     */
    public function resetPartialSubscribes($v = true)
    {
        $this->collSubscribesPartial = $v;
    }

    /**
     * Initializes the collSubscribes collection.
     *
     * By default this just sets the collSubscribes collection to an empty array (like clearcollSubscribes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSubscribes($overrideExisting = true)
    {
        if (null !== $this->collSubscribes && !$overrideExisting) {
            return;
        }
        $this->collSubscribes = new PropelObjectCollection();
        $this->collSubscribes->setModel('Subscribe');
    }

    /**
     * Gets an array of Subscribe objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Country is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     * @throws PropelException
     */
    public function getSubscribes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSubscribesPartial && !$this->isNew();
        if (null === $this->collSubscribes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSubscribes) {
                // return empty collection
                $this->initSubscribes();
            } else {
                $collSubscribes = SubscribeQuery::create(null, $criteria)
                    ->filterByCountry($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSubscribesPartial && count($collSubscribes)) {
                      $this->initSubscribes(false);

                      foreach ($collSubscribes as $obj) {
                        if (false == $this->collSubscribes->contains($obj)) {
                          $this->collSubscribes->append($obj);
                        }
                      }

                      $this->collSubscribesPartial = true;
                    }

                    $collSubscribes->getInternalIterator()->rewind();

                    return $collSubscribes;
                }

                if ($partial && $this->collSubscribes) {
                    foreach ($this->collSubscribes as $obj) {
                        if ($obj->isNew()) {
                            $collSubscribes[] = $obj;
                        }
                    }
                }

                $this->collSubscribes = $collSubscribes;
                $this->collSubscribesPartial = false;
            }
        }

        return $this->collSubscribes;
    }

    /**
     * Sets a collection of Subscribe objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $subscribes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Country The current object (for fluent API support)
     */
    public function setSubscribes(PropelCollection $subscribes, PropelPDO $con = null)
    {
        $subscribesToDelete = $this->getSubscribes(new Criteria(), $con)->diff($subscribes);


        $this->subscribesScheduledForDeletion = $subscribesToDelete;

        foreach ($subscribesToDelete as $subscribeRemoved) {
            $subscribeRemoved->setCountry(null);
        }

        $this->collSubscribes = null;
        foreach ($subscribes as $subscribe) {
            $this->addSubscribe($subscribe);
        }

        $this->collSubscribes = $subscribes;
        $this->collSubscribesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Subscribe objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Subscribe objects.
     * @throws PropelException
     */
    public function countSubscribes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSubscribesPartial && !$this->isNew();
        if (null === $this->collSubscribes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSubscribes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSubscribes());
            }
            $query = SubscribeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCountry($this)
                ->count($con);
        }

        return count($this->collSubscribes);
    }

    /**
     * Method called to associate a Subscribe object to this object
     * through the Subscribe foreign key attribute.
     *
     * @param    Subscribe $l Subscribe
     * @return Country The current object (for fluent API support)
     */
    public function addSubscribe(Subscribe $l)
    {
        if ($this->collSubscribes === null) {
            $this->initSubscribes();
            $this->collSubscribesPartial = true;
        }

        if (!in_array($l, $this->collSubscribes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSubscribe($l);

            if ($this->subscribesScheduledForDeletion and $this->subscribesScheduledForDeletion->contains($l)) {
                $this->subscribesScheduledForDeletion->remove($this->subscribesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Subscribe $subscribe The subscribe object to add.
     */
    protected function doAddSubscribe($subscribe)
    {
        $this->collSubscribes[]= $subscribe;
        $subscribe->setCountry($this);
    }

    /**
     * @param	Subscribe $subscribe The subscribe object to remove.
     * @return Country The current object (for fluent API support)
     */
    public function removeSubscribe($subscribe)
    {
        if ($this->getSubscribes()->contains($subscribe)) {
            $this->collSubscribes->remove($this->collSubscribes->search($subscribe));
            if (null === $this->subscribesScheduledForDeletion) {
                $this->subscribesScheduledForDeletion = clone $this->collSubscribes;
                $this->subscribesScheduledForDeletion->clear();
            }
            $this->subscribesScheduledForDeletion[]= clone $subscribe;
            $subscribe->setCountry(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinDistrict($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('District', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinSubscribeType($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('SubscribeType', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinLanguage($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('Language', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinDisplacement($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('Displacement', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinState($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('State', $join_behavior);

        return $this->getSubscribes($query, $con);
    }

    /**
     * Clears out the collPaymentTexts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Country The current object (for fluent API support)
     * @see        addPaymentTexts()
     */
    public function clearPaymentTexts()
    {
        $this->collPaymentTexts = null; // important to set this to null since that means it is uninitialized
        $this->collPaymentTextsPartial = null;

        return $this;
    }

    /**
     * reset is the collPaymentTexts collection loaded partially
     *
     * @return void
     */
    public function resetPartialPaymentTexts($v = true)
    {
        $this->collPaymentTextsPartial = $v;
    }

    /**
     * Initializes the collPaymentTexts collection.
     *
     * By default this just sets the collPaymentTexts collection to an empty array (like clearcollPaymentTexts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPaymentTexts($overrideExisting = true)
    {
        if (null !== $this->collPaymentTexts && !$overrideExisting) {
            return;
        }
        $this->collPaymentTexts = new PropelObjectCollection();
        $this->collPaymentTexts->setModel('PaymentText');
    }

    /**
     * Gets an array of PaymentText objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Country is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PaymentText[] List of PaymentText objects
     * @throws PropelException
     */
    public function getPaymentTexts($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPaymentTextsPartial && !$this->isNew();
        if (null === $this->collPaymentTexts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPaymentTexts) {
                // return empty collection
                $this->initPaymentTexts();
            } else {
                $collPaymentTexts = PaymentTextQuery::create(null, $criteria)
                    ->filterByCountry($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPaymentTextsPartial && count($collPaymentTexts)) {
                      $this->initPaymentTexts(false);

                      foreach ($collPaymentTexts as $obj) {
                        if (false == $this->collPaymentTexts->contains($obj)) {
                          $this->collPaymentTexts->append($obj);
                        }
                      }

                      $this->collPaymentTextsPartial = true;
                    }

                    $collPaymentTexts->getInternalIterator()->rewind();

                    return $collPaymentTexts;
                }

                if ($partial && $this->collPaymentTexts) {
                    foreach ($this->collPaymentTexts as $obj) {
                        if ($obj->isNew()) {
                            $collPaymentTexts[] = $obj;
                        }
                    }
                }

                $this->collPaymentTexts = $collPaymentTexts;
                $this->collPaymentTextsPartial = false;
            }
        }

        return $this->collPaymentTexts;
    }

    /**
     * Sets a collection of PaymentText objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $paymentTexts A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Country The current object (for fluent API support)
     */
    public function setPaymentTexts(PropelCollection $paymentTexts, PropelPDO $con = null)
    {
        $paymentTextsToDelete = $this->getPaymentTexts(new Criteria(), $con)->diff($paymentTexts);


        $this->paymentTextsScheduledForDeletion = $paymentTextsToDelete;

        foreach ($paymentTextsToDelete as $paymentTextRemoved) {
            $paymentTextRemoved->setCountry(null);
        }

        $this->collPaymentTexts = null;
        foreach ($paymentTexts as $paymentText) {
            $this->addPaymentText($paymentText);
        }

        $this->collPaymentTexts = $paymentTexts;
        $this->collPaymentTextsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PaymentText objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PaymentText objects.
     * @throws PropelException
     */
    public function countPaymentTexts(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPaymentTextsPartial && !$this->isNew();
        if (null === $this->collPaymentTexts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPaymentTexts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPaymentTexts());
            }
            $query = PaymentTextQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCountry($this)
                ->count($con);
        }

        return count($this->collPaymentTexts);
    }

    /**
     * Method called to associate a PaymentText object to this object
     * through the PaymentText foreign key attribute.
     *
     * @param    PaymentText $l PaymentText
     * @return Country The current object (for fluent API support)
     */
    public function addPaymentText(PaymentText $l)
    {
        if ($this->collPaymentTexts === null) {
            $this->initPaymentTexts();
            $this->collPaymentTextsPartial = true;
        }

        if (!in_array($l, $this->collPaymentTexts->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPaymentText($l);

            if ($this->paymentTextsScheduledForDeletion and $this->paymentTextsScheduledForDeletion->contains($l)) {
                $this->paymentTextsScheduledForDeletion->remove($this->paymentTextsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	PaymentText $paymentText The paymentText object to add.
     */
    protected function doAddPaymentText($paymentText)
    {
        $this->collPaymentTexts[]= $paymentText;
        $paymentText->setCountry($this);
    }

    /**
     * @param	PaymentText $paymentText The paymentText object to remove.
     * @return Country The current object (for fluent API support)
     */
    public function removePaymentText($paymentText)
    {
        if ($this->getPaymentTexts()->contains($paymentText)) {
            $this->collPaymentTexts->remove($this->collPaymentTexts->search($paymentText));
            if (null === $this->paymentTextsScheduledForDeletion) {
                $this->paymentTextsScheduledForDeletion = clone $this->collPaymentTexts;
                $this->paymentTextsScheduledForDeletion->clear();
            }
            $this->paymentTextsScheduledForDeletion[]= $paymentText;
            $paymentText->setCountry(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->pais_id = null;
        $this->nome = null;
        $this->codigo = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collStates) {
                foreach ($this->collStates as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSubscribes) {
                foreach ($this->collSubscribes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPaymentTexts) {
                foreach ($this->collPaymentTexts as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collStates instanceof PropelCollection) {
            $this->collStates->clearIterator();
        }
        $this->collStates = null;
        if ($this->collSubscribes instanceof PropelCollection) {
            $this->collSubscribes->clearIterator();
        }
        $this->collSubscribes = null;
        if ($this->collPaymentTexts instanceof PropelCollection) {
            $this->collPaymentTexts->clearIterator();
        }
        $this->collPaymentTexts = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CountryPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
