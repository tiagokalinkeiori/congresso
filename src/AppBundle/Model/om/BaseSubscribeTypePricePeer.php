<?php

namespace AppBundle\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use AppBundle\Model\SubscribeTypePeer;
use AppBundle\Model\SubscribeTypePrice;
use AppBundle\Model\SubscribeTypePricePeer;
use AppBundle\Model\map\SubscribeTypePriceTableMap;

abstract class BaseSubscribeTypePricePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'default';

    /** the table name for this class */
    const TABLE_NAME = 'inscricao_tipo_valor';

    /** the related Propel class for this table */
    const OM_CLASS = 'AppBundle\\Model\\SubscribeTypePrice';

    /** the related TableMap class for this table */
    const TM_CLASS = 'AppBundle\\Model\\map\\SubscribeTypePriceTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 7;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 7;

    /** the column name for the inscricao_tipo_valor_id field */
    const INSCRICAO_TIPO_VALOR_ID = 'inscricao_tipo_valor.inscricao_tipo_valor_id';

    /** the column name for the inscricao_tipo_id field */
    const INSCRICAO_TIPO_ID = 'inscricao_tipo_valor.inscricao_tipo_id';

    /** the column name for the valor field */
    const VALOR = 'inscricao_tipo_valor.valor';

    /** the column name for the parcelas field */
    const PARCELAS = 'inscricao_tipo_valor.parcelas';

    /** the column name for the valor_parcelado field */
    const VALOR_PARCELADO = 'inscricao_tipo_valor.valor_parcelado';

    /** the column name for the inicio field */
    const INICIO = 'inscricao_tipo_valor.inicio';

    /** the column name for the fim field */
    const FIM = 'inscricao_tipo_valor.fim';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of SubscribeTypePrice objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array SubscribeTypePrice[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. SubscribeTypePricePeer::$fieldNames[SubscribeTypePricePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'SubscribeTypeId', 'Price', 'Quota', 'InstallmentValue', 'Start', 'End', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'subscribeTypeId', 'price', 'quota', 'installmentValue', 'start', 'end', ),
        BasePeer::TYPE_COLNAME => array (SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, SubscribeTypePricePeer::INSCRICAO_TIPO_ID, SubscribeTypePricePeer::VALOR, SubscribeTypePricePeer::PARCELAS, SubscribeTypePricePeer::VALOR_PARCELADO, SubscribeTypePricePeer::INICIO, SubscribeTypePricePeer::FIM, ),
        BasePeer::TYPE_RAW_COLNAME => array ('INSCRICAO_TIPO_VALOR_ID', 'INSCRICAO_TIPO_ID', 'VALOR', 'PARCELAS', 'VALOR_PARCELADO', 'INICIO', 'FIM', ),
        BasePeer::TYPE_FIELDNAME => array ('inscricao_tipo_valor_id', 'inscricao_tipo_id', 'valor', 'parcelas', 'valor_parcelado', 'inicio', 'fim', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. SubscribeTypePricePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'SubscribeTypeId' => 1, 'Price' => 2, 'Quota' => 3, 'InstallmentValue' => 4, 'Start' => 5, 'End' => 6, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'subscribeTypeId' => 1, 'price' => 2, 'quota' => 3, 'installmentValue' => 4, 'start' => 5, 'end' => 6, ),
        BasePeer::TYPE_COLNAME => array (SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID => 0, SubscribeTypePricePeer::INSCRICAO_TIPO_ID => 1, SubscribeTypePricePeer::VALOR => 2, SubscribeTypePricePeer::PARCELAS => 3, SubscribeTypePricePeer::VALOR_PARCELADO => 4, SubscribeTypePricePeer::INICIO => 5, SubscribeTypePricePeer::FIM => 6, ),
        BasePeer::TYPE_RAW_COLNAME => array ('INSCRICAO_TIPO_VALOR_ID' => 0, 'INSCRICAO_TIPO_ID' => 1, 'VALOR' => 2, 'PARCELAS' => 3, 'VALOR_PARCELADO' => 4, 'INICIO' => 5, 'FIM' => 6, ),
        BasePeer::TYPE_FIELDNAME => array ('inscricao_tipo_valor_id' => 0, 'inscricao_tipo_id' => 1, 'valor' => 2, 'parcelas' => 3, 'valor_parcelado' => 4, 'inicio' => 5, 'fim' => 6, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = SubscribeTypePricePeer::getFieldNames($toType);
        $key = isset(SubscribeTypePricePeer::$fieldKeys[$fromType][$name]) ? SubscribeTypePricePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(SubscribeTypePricePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, SubscribeTypePricePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return SubscribeTypePricePeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. SubscribeTypePricePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(SubscribeTypePricePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID);
            $criteria->addSelectColumn(SubscribeTypePricePeer::INSCRICAO_TIPO_ID);
            $criteria->addSelectColumn(SubscribeTypePricePeer::VALOR);
            $criteria->addSelectColumn(SubscribeTypePricePeer::PARCELAS);
            $criteria->addSelectColumn(SubscribeTypePricePeer::VALOR_PARCELADO);
            $criteria->addSelectColumn(SubscribeTypePricePeer::INICIO);
            $criteria->addSelectColumn(SubscribeTypePricePeer::FIM);
        } else {
            $criteria->addSelectColumn($alias . '.inscricao_tipo_valor_id');
            $criteria->addSelectColumn($alias . '.inscricao_tipo_id');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.parcelas');
            $criteria->addSelectColumn($alias . '.valor_parcelado');
            $criteria->addSelectColumn($alias . '.inicio');
            $criteria->addSelectColumn($alias . '.fim');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribeTypePricePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribeTypePricePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return SubscribeTypePrice
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = SubscribeTypePricePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return SubscribeTypePricePeer::populateObjects(SubscribeTypePricePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            SubscribeTypePricePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param SubscribeTypePrice $obj A SubscribeTypePrice object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            SubscribeTypePricePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A SubscribeTypePrice object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof SubscribeTypePrice) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or SubscribeTypePrice object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(SubscribeTypePricePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return SubscribeTypePrice Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(SubscribeTypePricePeer::$instances[$key])) {
                return SubscribeTypePricePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (SubscribeTypePricePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        SubscribeTypePricePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to inscricao_tipo_valor
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = SubscribeTypePricePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = SubscribeTypePricePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = SubscribeTypePricePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SubscribeTypePricePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (SubscribeTypePrice object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = SubscribeTypePricePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = SubscribeTypePricePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + SubscribeTypePricePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SubscribeTypePricePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            SubscribeTypePricePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related SubscribeType table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSubscribeType(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribeTypePricePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribeTypePricePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SubscribeTypePrice objects pre-filled with their SubscribeType objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SubscribeTypePrice objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSubscribeType(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME);
        }

        SubscribeTypePricePeer::addSelectColumns($criteria);
        $startcol = SubscribeTypePricePeer::NUM_HYDRATE_COLUMNS;
        SubscribeTypePeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribeTypePricePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribeTypePricePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribeTypePricePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribeTypePricePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SubscribeTypePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SubscribeTypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SubscribeTypePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SubscribeTypePrice) to $obj2 (SubscribeType)
                $obj2->addSubscribeTypePrice($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribeTypePricePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribeTypePricePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of SubscribeTypePrice objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SubscribeTypePrice objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME);
        }

        SubscribeTypePricePeer::addSelectColumns($criteria);
        $startcol2 = SubscribeTypePricePeer::NUM_HYDRATE_COLUMNS;

        SubscribeTypePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SubscribeTypePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribeTypePricePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribeTypePricePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribeTypePricePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribeTypePricePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined SubscribeType rows

            $key2 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SubscribeTypePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SubscribeTypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SubscribeTypePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (SubscribeTypePrice) to the collection in $obj2 (SubscribeType)
                $obj2->addSubscribeTypePrice($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(SubscribeTypePricePeer::DATABASE_NAME)->getTable(SubscribeTypePricePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseSubscribeTypePricePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseSubscribeTypePricePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \AppBundle\Model\map\SubscribeTypePriceTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return SubscribeTypePricePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a SubscribeTypePrice or Criteria object.
     *
     * @param      mixed $values Criteria or SubscribeTypePrice object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from SubscribeTypePrice object
        }

        if ($criteria->containsKey(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID) && $criteria->keyContainsValue(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a SubscribeTypePrice or Criteria object.
     *
     * @param      mixed $values Criteria or SubscribeTypePrice object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(SubscribeTypePricePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID);
            $value = $criteria->remove(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID);
            if ($value) {
                $selectCriteria->add(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SubscribeTypePricePeer::TABLE_NAME);
            }

        } else { // $values is SubscribeTypePrice object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the inscricao_tipo_valor table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(SubscribeTypePricePeer::TABLE_NAME, $con, SubscribeTypePricePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SubscribeTypePricePeer::clearInstancePool();
            SubscribeTypePricePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a SubscribeTypePrice or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or SubscribeTypePrice object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            SubscribeTypePricePeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof SubscribeTypePrice) { // it's a model object
            // invalidate the cache for this single object
            SubscribeTypePricePeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SubscribeTypePricePeer::DATABASE_NAME);
            $criteria->add(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                SubscribeTypePricePeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(SubscribeTypePricePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            SubscribeTypePricePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given SubscribeTypePrice object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param SubscribeTypePrice $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(SubscribeTypePricePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(SubscribeTypePricePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(SubscribeTypePricePeer::DATABASE_NAME, SubscribeTypePricePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return SubscribeTypePrice
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = SubscribeTypePricePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(SubscribeTypePricePeer::DATABASE_NAME);
        $criteria->add(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $pk);

        $v = SubscribeTypePricePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return SubscribeTypePrice[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(SubscribeTypePricePeer::DATABASE_NAME);
            $criteria->add(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $pks, Criteria::IN);
            $objs = SubscribeTypePricePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseSubscribeTypePricePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseSubscribeTypePricePeer::buildTableMap();

