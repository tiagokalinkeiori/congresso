<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelException;
use \PropelPDO;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\Train;
use AppBundle\Model\TrainPeer;
use AppBundle\Model\TrainQuery;

abstract class BaseTrain extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\TrainPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        TrainPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the caravana_id field.
     * @var        int
     */
    protected $caravana_id;

    /**
     * The value for the inscricao_id field.
     * @var        int
     */
    protected $inscricao_id;

    /**
     * The value for the nome_responsavel field.
     * @var        string
     */
    protected $nome_responsavel;

    /**
     * The value for the email_responsabel field.
     * @var        string
     */
    protected $email_responsabel;

    /**
     * The value for the telefone_responsavel field.
     * @var        string
     */
    protected $telefone_responsavel;

    /**
     * The value for the vagas field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $vagas;

    /**
     * The value for the disponiveis field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $disponiveis;

    /**
     * The value for the descricao field.
     * @var        string
     */
    protected $descricao;

    /**
     * @var        Subscribe
     */
    protected $aSubscribe;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->vagas = 0;
        $this->disponiveis = 0;
    }

    /**
     * Initializes internal state of BaseTrain object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [caravana_id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->caravana_id;
    }

    /**
     * Get the [inscricao_id] column value.
     *
     * @return int
     */
    public function getSubscribeId()
    {

        return $this->inscricao_id;
    }

    /**
     * Get the [nome_responsavel] column value.
     *
     * @return string
     */
    public function getResponsibleName()
    {

        return $this->nome_responsavel;
    }

    /**
     * Get the [email_responsabel] column value.
     *
     * @return string
     */
    public function getResponsibleEmail()
    {

        return $this->email_responsabel;
    }

    /**
     * Get the [telefone_responsavel] column value.
     *
     * @return string
     */
    public function getResponsiblePhone()
    {

        return $this->telefone_responsavel;
    }

    /**
     * Get the [vagas] column value.
     *
     * @return int
     */
    public function getSpace()
    {

        return $this->vagas;
    }

    /**
     * Get the [disponiveis] column value.
     *
     * @return int
     */
    public function getAvailable()
    {

        return $this->disponiveis;
    }

    /**
     * Get the [descricao] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->descricao;
    }

    /**
     * Set the value of [caravana_id] column.
     *
     * @param  int $v new value
     * @return Train The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->caravana_id !== $v) {
            $this->caravana_id = $v;
            $this->modifiedColumns[] = TrainPeer::CARAVANA_ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [inscricao_id] column.
     *
     * @param  int $v new value
     * @return Train The current object (for fluent API support)
     */
    public function setSubscribeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_id !== $v) {
            $this->inscricao_id = $v;
            $this->modifiedColumns[] = TrainPeer::INSCRICAO_ID;
        }

        if ($this->aSubscribe !== null && $this->aSubscribe->getId() !== $v) {
            $this->aSubscribe = null;
        }


        return $this;
    } // setSubscribeId()

    /**
     * Set the value of [nome_responsavel] column.
     *
     * @param  string $v new value
     * @return Train The current object (for fluent API support)
     */
    public function setResponsibleName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome_responsavel !== $v) {
            $this->nome_responsavel = $v;
            $this->modifiedColumns[] = TrainPeer::NOME_RESPONSAVEL;
        }


        return $this;
    } // setResponsibleName()

    /**
     * Set the value of [email_responsabel] column.
     *
     * @param  string $v new value
     * @return Train The current object (for fluent API support)
     */
    public function setResponsibleEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email_responsabel !== $v) {
            $this->email_responsabel = $v;
            $this->modifiedColumns[] = TrainPeer::EMAIL_RESPONSABEL;
        }


        return $this;
    } // setResponsibleEmail()

    /**
     * Set the value of [telefone_responsavel] column.
     *
     * @param  string $v new value
     * @return Train The current object (for fluent API support)
     */
    public function setResponsiblePhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telefone_responsavel !== $v) {
            $this->telefone_responsavel = $v;
            $this->modifiedColumns[] = TrainPeer::TELEFONE_RESPONSAVEL;
        }


        return $this;
    } // setResponsiblePhone()

    /**
     * Set the value of [vagas] column.
     *
     * @param  int $v new value
     * @return Train The current object (for fluent API support)
     */
    public function setSpace($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->vagas !== $v) {
            $this->vagas = $v;
            $this->modifiedColumns[] = TrainPeer::VAGAS;
        }


        return $this;
    } // setSpace()

    /**
     * Set the value of [disponiveis] column.
     *
     * @param  int $v new value
     * @return Train The current object (for fluent API support)
     */
    public function setAvailable($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->disponiveis !== $v) {
            $this->disponiveis = $v;
            $this->modifiedColumns[] = TrainPeer::DISPONIVEIS;
        }


        return $this;
    } // setAvailable()

    /**
     * Set the value of [descricao] column.
     *
     * @param  string $v new value
     * @return Train The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao !== $v) {
            $this->descricao = $v;
            $this->modifiedColumns[] = TrainPeer::DESCRICAO;
        }


        return $this;
    } // setDescription()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->vagas !== 0) {
                return false;
            }

            if ($this->disponiveis !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->caravana_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->inscricao_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->nome_responsavel = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->email_responsabel = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->telefone_responsavel = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->vagas = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->disponiveis = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->descricao = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 8; // 8 = TrainPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Train object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSubscribe !== null && $this->inscricao_id !== $this->aSubscribe->getId()) {
            $this->aSubscribe = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TrainPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = TrainPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSubscribe = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TrainPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = TrainQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TrainPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TrainPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribe !== null) {
                if ($this->aSubscribe->isModified() || $this->aSubscribe->isNew()) {
                    $affectedRows += $this->aSubscribe->save($con);
                }
                $this->setSubscribe($this->aSubscribe);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = TrainPeer::CARAVANA_ID;
        if (null !== $this->caravana_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . TrainPeer::CARAVANA_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(TrainPeer::CARAVANA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`caravana_id`';
        }
        if ($this->isColumnModified(TrainPeer::INSCRICAO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_id`';
        }
        if ($this->isColumnModified(TrainPeer::NOME_RESPONSAVEL)) {
            $modifiedColumns[':p' . $index++]  = '`nome_responsavel`';
        }
        if ($this->isColumnModified(TrainPeer::EMAIL_RESPONSABEL)) {
            $modifiedColumns[':p' . $index++]  = '`email_responsabel`';
        }
        if ($this->isColumnModified(TrainPeer::TELEFONE_RESPONSAVEL)) {
            $modifiedColumns[':p' . $index++]  = '`telefone_responsavel`';
        }
        if ($this->isColumnModified(TrainPeer::VAGAS)) {
            $modifiedColumns[':p' . $index++]  = '`vagas`';
        }
        if ($this->isColumnModified(TrainPeer::DISPONIVEIS)) {
            $modifiedColumns[':p' . $index++]  = '`disponiveis`';
        }
        if ($this->isColumnModified(TrainPeer::DESCRICAO)) {
            $modifiedColumns[':p' . $index++]  = '`descricao`';
        }

        $sql = sprintf(
            'INSERT INTO `caravana` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`caravana_id`':
                        $stmt->bindValue($identifier, $this->caravana_id, PDO::PARAM_INT);
                        break;
                    case '`inscricao_id`':
                        $stmt->bindValue($identifier, $this->inscricao_id, PDO::PARAM_INT);
                        break;
                    case '`nome_responsavel`':
                        $stmt->bindValue($identifier, $this->nome_responsavel, PDO::PARAM_STR);
                        break;
                    case '`email_responsabel`':
                        $stmt->bindValue($identifier, $this->email_responsabel, PDO::PARAM_STR);
                        break;
                    case '`telefone_responsavel`':
                        $stmt->bindValue($identifier, $this->telefone_responsavel, PDO::PARAM_STR);
                        break;
                    case '`vagas`':
                        $stmt->bindValue($identifier, $this->vagas, PDO::PARAM_INT);
                        break;
                    case '`disponiveis`':
                        $stmt->bindValue($identifier, $this->disponiveis, PDO::PARAM_INT);
                        break;
                    case '`descricao`':
                        $stmt->bindValue($identifier, $this->descricao, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribe !== null) {
                if (!$this->aSubscribe->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSubscribe->getValidationFailures());
                }
            }


            if (($retval = TrainPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = TrainPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSubscribeId();
                break;
            case 2:
                return $this->getResponsibleName();
                break;
            case 3:
                return $this->getResponsibleEmail();
                break;
            case 4:
                return $this->getResponsiblePhone();
                break;
            case 5:
                return $this->getSpace();
                break;
            case 6:
                return $this->getAvailable();
                break;
            case 7:
                return $this->getDescription();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Train'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Train'][$this->getPrimaryKey()] = true;
        $keys = TrainPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSubscribeId(),
            $keys[2] => $this->getResponsibleName(),
            $keys[3] => $this->getResponsibleEmail(),
            $keys[4] => $this->getResponsiblePhone(),
            $keys[5] => $this->getSpace(),
            $keys[6] => $this->getAvailable(),
            $keys[7] => $this->getDescription(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSubscribe) {
                $result['Subscribe'] = $this->aSubscribe->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = TrainPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSubscribeId($value);
                break;
            case 2:
                $this->setResponsibleName($value);
                break;
            case 3:
                $this->setResponsibleEmail($value);
                break;
            case 4:
                $this->setResponsiblePhone($value);
                break;
            case 5:
                $this->setSpace($value);
                break;
            case 6:
                $this->setAvailable($value);
                break;
            case 7:
                $this->setDescription($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = TrainPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSubscribeId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setResponsibleName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setResponsibleEmail($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setResponsiblePhone($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSpace($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAvailable($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDescription($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TrainPeer::DATABASE_NAME);

        if ($this->isColumnModified(TrainPeer::CARAVANA_ID)) $criteria->add(TrainPeer::CARAVANA_ID, $this->caravana_id);
        if ($this->isColumnModified(TrainPeer::INSCRICAO_ID)) $criteria->add(TrainPeer::INSCRICAO_ID, $this->inscricao_id);
        if ($this->isColumnModified(TrainPeer::NOME_RESPONSAVEL)) $criteria->add(TrainPeer::NOME_RESPONSAVEL, $this->nome_responsavel);
        if ($this->isColumnModified(TrainPeer::EMAIL_RESPONSABEL)) $criteria->add(TrainPeer::EMAIL_RESPONSABEL, $this->email_responsabel);
        if ($this->isColumnModified(TrainPeer::TELEFONE_RESPONSAVEL)) $criteria->add(TrainPeer::TELEFONE_RESPONSAVEL, $this->telefone_responsavel);
        if ($this->isColumnModified(TrainPeer::VAGAS)) $criteria->add(TrainPeer::VAGAS, $this->vagas);
        if ($this->isColumnModified(TrainPeer::DISPONIVEIS)) $criteria->add(TrainPeer::DISPONIVEIS, $this->disponiveis);
        if ($this->isColumnModified(TrainPeer::DESCRICAO)) $criteria->add(TrainPeer::DESCRICAO, $this->descricao);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(TrainPeer::DATABASE_NAME);
        $criteria->add(TrainPeer::CARAVANA_ID, $this->caravana_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (caravana_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Train (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSubscribeId($this->getSubscribeId());
        $copyObj->setResponsibleName($this->getResponsibleName());
        $copyObj->setResponsibleEmail($this->getResponsibleEmail());
        $copyObj->setResponsiblePhone($this->getResponsiblePhone());
        $copyObj->setSpace($this->getSpace());
        $copyObj->setAvailable($this->getAvailable());
        $copyObj->setDescription($this->getDescription());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Train Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return TrainPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new TrainPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Subscribe object.
     *
     * @param                  Subscribe $v
     * @return Train The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSubscribe(Subscribe $v = null)
    {
        if ($v === null) {
            $this->setSubscribeId(NULL);
        } else {
            $this->setSubscribeId($v->getId());
        }

        $this->aSubscribe = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Subscribe object, it will not be re-added.
        if ($v !== null) {
            $v->addTrain($this);
        }


        return $this;
    }


    /**
     * Get the associated Subscribe object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Subscribe The associated Subscribe object.
     * @throws PropelException
     */
    public function getSubscribe(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSubscribe === null && ($this->inscricao_id !== null) && $doQuery) {
            $this->aSubscribe = SubscribeQuery::create()->findPk($this->inscricao_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSubscribe->addTrains($this);
             */
        }

        return $this->aSubscribe;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->caravana_id = null;
        $this->inscricao_id = null;
        $this->nome_responsavel = null;
        $this->email_responsabel = null;
        $this->telefone_responsavel = null;
        $this->vagas = null;
        $this->disponiveis = null;
        $this->descricao = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSubscribe instanceof Persistent) {
              $this->aSubscribe->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSubscribe = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TrainPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
