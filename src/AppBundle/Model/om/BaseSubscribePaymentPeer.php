<?php

namespace AppBundle\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePaymentPeer;
use AppBundle\Model\SubscribePeer;
use AppBundle\Model\map\SubscribePaymentTableMap;

abstract class BaseSubscribePaymentPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'default';

    /** the table name for this class */
    const TABLE_NAME = 'inscricao_pagamento';

    /** the related Propel class for this table */
    const OM_CLASS = 'AppBundle\\Model\\SubscribePayment';

    /** the related TableMap class for this table */
    const TM_CLASS = 'AppBundle\\Model\\map\\SubscribePaymentTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 13;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 13;

    /** the column name for the inscricao_pagamento_id field */
    const INSCRICAO_PAGAMENTO_ID = 'inscricao_pagamento.inscricao_pagamento_id';

    /** the column name for the inscricao_id field */
    const INSCRICAO_ID = 'inscricao_pagamento.inscricao_id';

    /** the column name for the data_geracao field */
    const DATA_GERACAO = 'inscricao_pagamento.data_geracao';

    /** the column name for the data_vencimento field */
    const DATA_VENCIMENTO = 'inscricao_pagamento.data_vencimento';

    /** the column name for the data_pagamento field */
    const DATA_PAGAMENTO = 'inscricao_pagamento.data_pagamento';

    /** the column name for the valor field */
    const VALOR = 'inscricao_pagamento.valor';

    /** the column name for the pago field */
    const PAGO = 'inscricao_pagamento.pago';

    /** the column name for the primeira field */
    const PRIMEIRA = 'inscricao_pagamento.primeira';

    /** the column name for the cancelada field */
    const CANCELADA = 'inscricao_pagamento.cancelada';

    /** the column name for the url field */
    const URL = 'inscricao_pagamento.url';

    /** the column name for the nosso_numero field */
    const NOSSO_NUMERO = 'inscricao_pagamento.nosso_numero';

    /** the column name for the linha_digitavel field */
    const LINHA_DIGITAVEL = 'inscricao_pagamento.linha_digitavel';

    /** the column name for the enviado field */
    const ENVIADO = 'inscricao_pagamento.enviado';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of SubscribePayment objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array SubscribePayment[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. SubscribePaymentPeer::$fieldNames[SubscribePaymentPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'SubscribeId', 'CreateDate', 'DueDate', 'PaymentDate', 'Price', 'Paid', 'First', 'Cancel', 'Url', 'OwerNumber', 'DigitableLine', 'Sent', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'subscribeId', 'createDate', 'dueDate', 'paymentDate', 'price', 'paid', 'first', 'cancel', 'url', 'owerNumber', 'digitableLine', 'sent', ),
        BasePeer::TYPE_COLNAME => array (SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, SubscribePaymentPeer::INSCRICAO_ID, SubscribePaymentPeer::DATA_GERACAO, SubscribePaymentPeer::DATA_VENCIMENTO, SubscribePaymentPeer::DATA_PAGAMENTO, SubscribePaymentPeer::VALOR, SubscribePaymentPeer::PAGO, SubscribePaymentPeer::PRIMEIRA, SubscribePaymentPeer::CANCELADA, SubscribePaymentPeer::URL, SubscribePaymentPeer::NOSSO_NUMERO, SubscribePaymentPeer::LINHA_DIGITAVEL, SubscribePaymentPeer::ENVIADO, ),
        BasePeer::TYPE_RAW_COLNAME => array ('INSCRICAO_PAGAMENTO_ID', 'INSCRICAO_ID', 'DATA_GERACAO', 'DATA_VENCIMENTO', 'DATA_PAGAMENTO', 'VALOR', 'PAGO', 'PRIMEIRA', 'CANCELADA', 'URL', 'NOSSO_NUMERO', 'LINHA_DIGITAVEL', 'ENVIADO', ),
        BasePeer::TYPE_FIELDNAME => array ('inscricao_pagamento_id', 'inscricao_id', 'data_geracao', 'data_vencimento', 'data_pagamento', 'valor', 'pago', 'primeira', 'cancelada', 'url', 'nosso_numero', 'linha_digitavel', 'enviado', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. SubscribePaymentPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'SubscribeId' => 1, 'CreateDate' => 2, 'DueDate' => 3, 'PaymentDate' => 4, 'Price' => 5, 'Paid' => 6, 'First' => 7, 'Cancel' => 8, 'Url' => 9, 'OwerNumber' => 10, 'DigitableLine' => 11, 'Sent' => 12, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'subscribeId' => 1, 'createDate' => 2, 'dueDate' => 3, 'paymentDate' => 4, 'price' => 5, 'paid' => 6, 'first' => 7, 'cancel' => 8, 'url' => 9, 'owerNumber' => 10, 'digitableLine' => 11, 'sent' => 12, ),
        BasePeer::TYPE_COLNAME => array (SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID => 0, SubscribePaymentPeer::INSCRICAO_ID => 1, SubscribePaymentPeer::DATA_GERACAO => 2, SubscribePaymentPeer::DATA_VENCIMENTO => 3, SubscribePaymentPeer::DATA_PAGAMENTO => 4, SubscribePaymentPeer::VALOR => 5, SubscribePaymentPeer::PAGO => 6, SubscribePaymentPeer::PRIMEIRA => 7, SubscribePaymentPeer::CANCELADA => 8, SubscribePaymentPeer::URL => 9, SubscribePaymentPeer::NOSSO_NUMERO => 10, SubscribePaymentPeer::LINHA_DIGITAVEL => 11, SubscribePaymentPeer::ENVIADO => 12, ),
        BasePeer::TYPE_RAW_COLNAME => array ('INSCRICAO_PAGAMENTO_ID' => 0, 'INSCRICAO_ID' => 1, 'DATA_GERACAO' => 2, 'DATA_VENCIMENTO' => 3, 'DATA_PAGAMENTO' => 4, 'VALOR' => 5, 'PAGO' => 6, 'PRIMEIRA' => 7, 'CANCELADA' => 8, 'URL' => 9, 'NOSSO_NUMERO' => 10, 'LINHA_DIGITAVEL' => 11, 'ENVIADO' => 12, ),
        BasePeer::TYPE_FIELDNAME => array ('inscricao_pagamento_id' => 0, 'inscricao_id' => 1, 'data_geracao' => 2, 'data_vencimento' => 3, 'data_pagamento' => 4, 'valor' => 5, 'pago' => 6, 'primeira' => 7, 'cancelada' => 8, 'url' => 9, 'nosso_numero' => 10, 'linha_digitavel' => 11, 'enviado' => 12, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = SubscribePaymentPeer::getFieldNames($toType);
        $key = isset(SubscribePaymentPeer::$fieldKeys[$fromType][$name]) ? SubscribePaymentPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(SubscribePaymentPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, SubscribePaymentPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return SubscribePaymentPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. SubscribePaymentPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(SubscribePaymentPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID);
            $criteria->addSelectColumn(SubscribePaymentPeer::INSCRICAO_ID);
            $criteria->addSelectColumn(SubscribePaymentPeer::DATA_GERACAO);
            $criteria->addSelectColumn(SubscribePaymentPeer::DATA_VENCIMENTO);
            $criteria->addSelectColumn(SubscribePaymentPeer::DATA_PAGAMENTO);
            $criteria->addSelectColumn(SubscribePaymentPeer::VALOR);
            $criteria->addSelectColumn(SubscribePaymentPeer::PAGO);
            $criteria->addSelectColumn(SubscribePaymentPeer::PRIMEIRA);
            $criteria->addSelectColumn(SubscribePaymentPeer::CANCELADA);
            $criteria->addSelectColumn(SubscribePaymentPeer::URL);
            $criteria->addSelectColumn(SubscribePaymentPeer::NOSSO_NUMERO);
            $criteria->addSelectColumn(SubscribePaymentPeer::LINHA_DIGITAVEL);
            $criteria->addSelectColumn(SubscribePaymentPeer::ENVIADO);
        } else {
            $criteria->addSelectColumn($alias . '.inscricao_pagamento_id');
            $criteria->addSelectColumn($alias . '.inscricao_id');
            $criteria->addSelectColumn($alias . '.data_geracao');
            $criteria->addSelectColumn($alias . '.data_vencimento');
            $criteria->addSelectColumn($alias . '.data_pagamento');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.pago');
            $criteria->addSelectColumn($alias . '.primeira');
            $criteria->addSelectColumn($alias . '.cancelada');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.nosso_numero');
            $criteria->addSelectColumn($alias . '.linha_digitavel');
            $criteria->addSelectColumn($alias . '.enviado');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePaymentPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePaymentPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return SubscribePayment
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = SubscribePaymentPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return SubscribePaymentPeer::populateObjects(SubscribePaymentPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            SubscribePaymentPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param SubscribePayment $obj A SubscribePayment object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            SubscribePaymentPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A SubscribePayment object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof SubscribePayment) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or SubscribePayment object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(SubscribePaymentPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return SubscribePayment Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(SubscribePaymentPeer::$instances[$key])) {
                return SubscribePaymentPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (SubscribePaymentPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        SubscribePaymentPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to inscricao_pagamento
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = SubscribePaymentPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = SubscribePaymentPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = SubscribePaymentPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SubscribePaymentPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (SubscribePayment object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = SubscribePaymentPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = SubscribePaymentPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + SubscribePaymentPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SubscribePaymentPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            SubscribePaymentPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related Subscribe table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSubscribe(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePaymentPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePaymentPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePaymentPeer::INSCRICAO_ID, SubscribePeer::INSCRICAO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SubscribePayment objects pre-filled with their Subscribe objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SubscribePayment objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSubscribe(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME);
        }

        SubscribePaymentPeer::addSelectColumns($criteria);
        $startcol = SubscribePaymentPeer::NUM_HYDRATE_COLUMNS;
        SubscribePeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribePaymentPeer::INSCRICAO_ID, SubscribePeer::INSCRICAO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePaymentPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePaymentPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribePaymentPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePaymentPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SubscribePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SubscribePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SubscribePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SubscribePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SubscribePayment) to $obj2 (Subscribe)
                $obj2->addSubscribePayment($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePaymentPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePaymentPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePaymentPeer::INSCRICAO_ID, SubscribePeer::INSCRICAO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of SubscribePayment objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SubscribePayment objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME);
        }

        SubscribePaymentPeer::addSelectColumns($criteria);
        $startcol2 = SubscribePaymentPeer::NUM_HYDRATE_COLUMNS;

        SubscribePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SubscribePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePaymentPeer::INSCRICAO_ID, SubscribePeer::INSCRICAO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePaymentPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePaymentPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePaymentPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePaymentPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Subscribe rows

            $key2 = SubscribePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SubscribePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SubscribePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SubscribePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (SubscribePayment) to the collection in $obj2 (Subscribe)
                $obj2->addSubscribePayment($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(SubscribePaymentPeer::DATABASE_NAME)->getTable(SubscribePaymentPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseSubscribePaymentPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseSubscribePaymentPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \AppBundle\Model\map\SubscribePaymentTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return SubscribePaymentPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a SubscribePayment or Criteria object.
     *
     * @param      mixed $values Criteria or SubscribePayment object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from SubscribePayment object
        }

        if ($criteria->containsKey(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID) && $criteria->keyContainsValue(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a SubscribePayment or Criteria object.
     *
     * @param      mixed $values Criteria or SubscribePayment object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(SubscribePaymentPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID);
            $value = $criteria->remove(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID);
            if ($value) {
                $selectCriteria->add(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SubscribePaymentPeer::TABLE_NAME);
            }

        } else { // $values is SubscribePayment object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the inscricao_pagamento table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(SubscribePaymentPeer::TABLE_NAME, $con, SubscribePaymentPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SubscribePaymentPeer::clearInstancePool();
            SubscribePaymentPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a SubscribePayment or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or SubscribePayment object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            SubscribePaymentPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof SubscribePayment) { // it's a model object
            // invalidate the cache for this single object
            SubscribePaymentPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SubscribePaymentPeer::DATABASE_NAME);
            $criteria->add(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                SubscribePaymentPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(SubscribePaymentPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            SubscribePaymentPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given SubscribePayment object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param SubscribePayment $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(SubscribePaymentPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(SubscribePaymentPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(SubscribePaymentPeer::DATABASE_NAME, SubscribePaymentPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return SubscribePayment
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = SubscribePaymentPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(SubscribePaymentPeer::DATABASE_NAME);
        $criteria->add(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $pk);

        $v = SubscribePaymentPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return SubscribePayment[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(SubscribePaymentPeer::DATABASE_NAME);
            $criteria->add(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $pks, Criteria::IN);
            $objs = SubscribePaymentPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseSubscribePaymentPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseSubscribePaymentPeer::buildTableMap();

