<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Donation;
use AppBundle\Model\DonationPeer;
use AppBundle\Model\DonationQuery;
use AppBundle\Model\Subscribe;

/**
 * @method DonationQuery orderById($order = Criteria::ASC) Order by the doacao_id column
 * @method DonationQuery orderBySubscribeId($order = Criteria::ASC) Order by the inscricao_id column
 * @method DonationQuery orderByName($order = Criteria::ASC) Order by the nome column
 * @method DonationQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method DonationQuery orderByPhone($order = Criteria::ASC) Order by the telefone column
 * @method DonationQuery orderByPrice($order = Criteria::ASC) Order by the valor column
 * @method DonationQuery orderByPaid($order = Criteria::ASC) Order by the pago column
 * @method DonationQuery orderByOwerNumber($order = Criteria::ASC) Order by the nosso_numero column
 * @method DonationQuery orderByDigitableLine($order = Criteria::ASC) Order by the linha_digitavel column
 * @method DonationQuery orderByObservation($order = Criteria::ASC) Order by the observacao column
 * @method DonationQuery orderByDate($order = Criteria::ASC) Order by the data column
 * @method DonationQuery orderByPaymentDate($order = Criteria::ASC) Order by the data_pagamento column
 *
 * @method DonationQuery groupById() Group by the doacao_id column
 * @method DonationQuery groupBySubscribeId() Group by the inscricao_id column
 * @method DonationQuery groupByName() Group by the nome column
 * @method DonationQuery groupByEmail() Group by the email column
 * @method DonationQuery groupByPhone() Group by the telefone column
 * @method DonationQuery groupByPrice() Group by the valor column
 * @method DonationQuery groupByPaid() Group by the pago column
 * @method DonationQuery groupByOwerNumber() Group by the nosso_numero column
 * @method DonationQuery groupByDigitableLine() Group by the linha_digitavel column
 * @method DonationQuery groupByObservation() Group by the observacao column
 * @method DonationQuery groupByDate() Group by the data column
 * @method DonationQuery groupByPaymentDate() Group by the data_pagamento column
 *
 * @method DonationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DonationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DonationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DonationQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method DonationQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method DonationQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method Donation findOne(PropelPDO $con = null) Return the first Donation matching the query
 * @method Donation findOneOrCreate(PropelPDO $con = null) Return the first Donation matching the query, or a new Donation object populated from the query conditions when no match is found
 *
 * @method Donation findOneBySubscribeId(int $inscricao_id) Return the first Donation filtered by the inscricao_id column
 * @method Donation findOneByName(string $nome) Return the first Donation filtered by the nome column
 * @method Donation findOneByEmail(string $email) Return the first Donation filtered by the email column
 * @method Donation findOneByPhone(string $telefone) Return the first Donation filtered by the telefone column
 * @method Donation findOneByPrice(string $valor) Return the first Donation filtered by the valor column
 * @method Donation findOneByPaid(boolean $pago) Return the first Donation filtered by the pago column
 * @method Donation findOneByOwerNumber(string $nosso_numero) Return the first Donation filtered by the nosso_numero column
 * @method Donation findOneByDigitableLine(string $linha_digitavel) Return the first Donation filtered by the linha_digitavel column
 * @method Donation findOneByObservation(string $observacao) Return the first Donation filtered by the observacao column
 * @method Donation findOneByDate(string $data) Return the first Donation filtered by the data column
 * @method Donation findOneByPaymentDate(string $data_pagamento) Return the first Donation filtered by the data_pagamento column
 *
 * @method array findById(int $doacao_id) Return Donation objects filtered by the doacao_id column
 * @method array findBySubscribeId(int $inscricao_id) Return Donation objects filtered by the inscricao_id column
 * @method array findByName(string $nome) Return Donation objects filtered by the nome column
 * @method array findByEmail(string $email) Return Donation objects filtered by the email column
 * @method array findByPhone(string $telefone) Return Donation objects filtered by the telefone column
 * @method array findByPrice(string $valor) Return Donation objects filtered by the valor column
 * @method array findByPaid(boolean $pago) Return Donation objects filtered by the pago column
 * @method array findByOwerNumber(string $nosso_numero) Return Donation objects filtered by the nosso_numero column
 * @method array findByDigitableLine(string $linha_digitavel) Return Donation objects filtered by the linha_digitavel column
 * @method array findByObservation(string $observacao) Return Donation objects filtered by the observacao column
 * @method array findByDate(string $data) Return Donation objects filtered by the data column
 * @method array findByPaymentDate(string $data_pagamento) Return Donation objects filtered by the data_pagamento column
 */
abstract class BaseDonationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDonationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\Donation';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DonationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DonationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DonationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DonationQuery) {
            return $criteria;
        }
        $query = new DonationQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Donation|Donation[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DonationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DonationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Donation A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Donation A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `doacao_id`, `inscricao_id`, `nome`, `email`, `telefone`, `valor`, `pago`, `nosso_numero`, `linha_digitavel`, `observacao`, `data`, `data_pagamento` FROM `doacao` WHERE `doacao_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Donation();
            $obj->hydrate($row);
            DonationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Donation|Donation[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Donation[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DonationPeer::DOACAO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DonationPeer::DOACAO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the doacao_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE doacao_id = 1234
     * $query->filterById(array(12, 34)); // WHERE doacao_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE doacao_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE doacao_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DonationPeer::DOACAO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DonationPeer::DOACAO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DonationPeer::DOACAO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the inscricao_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySubscribeId(1234); // WHERE inscricao_id = 1234
     * $query->filterBySubscribeId(array(12, 34)); // WHERE inscricao_id IN (12, 34)
     * $query->filterBySubscribeId(array('min' => 12)); // WHERE inscricao_id >= 12
     * $query->filterBySubscribeId(array('max' => 12)); // WHERE inscricao_id <= 12
     * </code>
     *
     * @see       filterBySubscribe()
     *
     * @param     mixed $subscribeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterBySubscribeId($subscribeId = null, $comparison = null)
    {
        if (is_array($subscribeId)) {
            $useMinMax = false;
            if (isset($subscribeId['min'])) {
                $this->addUsingAlias(DonationPeer::INSCRICAO_ID, $subscribeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subscribeId['max'])) {
                $this->addUsingAlias(DonationPeer::INSCRICAO_ID, $subscribeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DonationPeer::INSCRICAO_ID, $subscribeId, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DonationPeer::NOME, $name, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DonationPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the telefone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE telefone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE telefone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DonationPeer::TELEFONE, $phone, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE valor = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE valor >= 12
     * $query->filterByPrice(array('max' => 12)); // WHERE valor <= 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(DonationPeer::VALOR, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(DonationPeer::VALOR, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DonationPeer::VALOR, $price, $comparison);
    }

    /**
     * Filter the query on the pago column
     *
     * Example usage:
     * <code>
     * $query->filterByPaid(true); // WHERE pago = true
     * $query->filterByPaid('yes'); // WHERE pago = true
     * </code>
     *
     * @param     boolean|string $paid The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByPaid($paid = null, $comparison = null)
    {
        if (is_string($paid)) {
            $paid = in_array(strtolower($paid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DonationPeer::PAGO, $paid, $comparison);
    }

    /**
     * Filter the query on the nosso_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByOwerNumber('fooValue');   // WHERE nosso_numero = 'fooValue'
     * $query->filterByOwerNumber('%fooValue%'); // WHERE nosso_numero LIKE '%fooValue%'
     * </code>
     *
     * @param     string $owerNumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByOwerNumber($owerNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($owerNumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $owerNumber)) {
                $owerNumber = str_replace('*', '%', $owerNumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DonationPeer::NOSSO_NUMERO, $owerNumber, $comparison);
    }

    /**
     * Filter the query on the linha_digitavel column
     *
     * Example usage:
     * <code>
     * $query->filterByDigitableLine('fooValue');   // WHERE linha_digitavel = 'fooValue'
     * $query->filterByDigitableLine('%fooValue%'); // WHERE linha_digitavel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $digitableLine The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByDigitableLine($digitableLine = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($digitableLine)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $digitableLine)) {
                $digitableLine = str_replace('*', '%', $digitableLine);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DonationPeer::LINHA_DIGITAVEL, $digitableLine, $comparison);
    }

    /**
     * Filter the query on the observacao column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE observacao = 'fooValue'
     * $query->filterByObservation('%fooValue%'); // WHERE observacao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observation)) {
                $observation = str_replace('*', '%', $observation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DonationPeer::OBSERVACAO, $observation, $comparison);
    }

    /**
     * Filter the query on the data column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE data = '2011-03-14'
     * $query->filterByDate('now'); // WHERE data = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE data < '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(DonationPeer::DATA, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(DonationPeer::DATA, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DonationPeer::DATA, $date, $comparison);
    }

    /**
     * Filter the query on the data_pagamento column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentDate('2011-03-14'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByPaymentDate('now'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByPaymentDate(array('max' => 'yesterday')); // WHERE data_pagamento < '2011-03-13'
     * </code>
     *
     * @param     mixed $paymentDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function filterByPaymentDate($paymentDate = null, $comparison = null)
    {
        if (is_array($paymentDate)) {
            $useMinMax = false;
            if (isset($paymentDate['min'])) {
                $this->addUsingAlias(DonationPeer::DATA_PAGAMENTO, $paymentDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymentDate['max'])) {
                $this->addUsingAlias(DonationPeer::DATA_PAGAMENTO, $paymentDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DonationPeer::DATA_PAGAMENTO, $paymentDate, $comparison);
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DonationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(DonationPeer::INSCRICAO_ID, $subscribe->getId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DonationPeer::INSCRICAO_ID, $subscribe->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Donation $donation Object to remove from the list of results
     *
     * @return DonationQuery The current query, for fluid interface
     */
    public function prune($donation = null)
    {
        if ($donation) {
            $this->addUsingAlias(DonationPeer::DOACAO_ID, $donation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
