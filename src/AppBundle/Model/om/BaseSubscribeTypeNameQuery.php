<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Language;
use AppBundle\Model\SubscribeType;
use AppBundle\Model\SubscribeTypeName;
use AppBundle\Model\SubscribeTypeNamePeer;
use AppBundle\Model\SubscribeTypeNameQuery;

/**
 * @method SubscribeTypeNameQuery orderBySubscribeTypeId($order = Criteria::ASC) Order by the inscricao_tipo_id column
 * @method SubscribeTypeNameQuery orderByLanguageId($order = Criteria::ASC) Order by the lingua_id column
 * @method SubscribeTypeNameQuery orderByName($order = Criteria::ASC) Order by the nome column
 *
 * @method SubscribeTypeNameQuery groupBySubscribeTypeId() Group by the inscricao_tipo_id column
 * @method SubscribeTypeNameQuery groupByLanguageId() Group by the lingua_id column
 * @method SubscribeTypeNameQuery groupByName() Group by the nome column
 *
 * @method SubscribeTypeNameQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SubscribeTypeNameQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SubscribeTypeNameQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SubscribeTypeNameQuery leftJoinSubscribeType($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubscribeType relation
 * @method SubscribeTypeNameQuery rightJoinSubscribeType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubscribeType relation
 * @method SubscribeTypeNameQuery innerJoinSubscribeType($relationAlias = null) Adds a INNER JOIN clause to the query using the SubscribeType relation
 *
 * @method SubscribeTypeNameQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method SubscribeTypeNameQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method SubscribeTypeNameQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method SubscribeTypeName findOne(PropelPDO $con = null) Return the first SubscribeTypeName matching the query
 * @method SubscribeTypeName findOneOrCreate(PropelPDO $con = null) Return the first SubscribeTypeName matching the query, or a new SubscribeTypeName object populated from the query conditions when no match is found
 *
 * @method SubscribeTypeName findOneBySubscribeTypeId(int $inscricao_tipo_id) Return the first SubscribeTypeName filtered by the inscricao_tipo_id column
 * @method SubscribeTypeName findOneByLanguageId(int $lingua_id) Return the first SubscribeTypeName filtered by the lingua_id column
 * @method SubscribeTypeName findOneByName(string $nome) Return the first SubscribeTypeName filtered by the nome column
 *
 * @method array findBySubscribeTypeId(int $inscricao_tipo_id) Return SubscribeTypeName objects filtered by the inscricao_tipo_id column
 * @method array findByLanguageId(int $lingua_id) Return SubscribeTypeName objects filtered by the lingua_id column
 * @method array findByName(string $nome) Return SubscribeTypeName objects filtered by the nome column
 */
abstract class BaseSubscribeTypeNameQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSubscribeTypeNameQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\SubscribeTypeName';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SubscribeTypeNameQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SubscribeTypeNameQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SubscribeTypeNameQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SubscribeTypeNameQuery) {
            return $criteria;
        }
        $query = new SubscribeTypeNameQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$inscricao_tipo_id, $lingua_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SubscribeTypeName|SubscribeTypeName[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SubscribeTypeNamePeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypeNamePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SubscribeTypeName A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `inscricao_tipo_id`, `lingua_id`, `nome` FROM `inscricao_tipo_nome` WHERE `inscricao_tipo_id` = :p0 AND `lingua_id` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SubscribeTypeName();
            $obj->hydrate($row);
            SubscribeTypeNamePeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SubscribeTypeName|SubscribeTypeName[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SubscribeTypeName[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SubscribeTypeNameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(SubscribeTypeNamePeer::INSCRICAO_TIPO_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(SubscribeTypeNamePeer::LINGUA_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SubscribeTypeNameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(SubscribeTypeNamePeer::INSCRICAO_TIPO_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(SubscribeTypeNamePeer::LINGUA_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the inscricao_tipo_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySubscribeTypeId(1234); // WHERE inscricao_tipo_id = 1234
     * $query->filterBySubscribeTypeId(array(12, 34)); // WHERE inscricao_tipo_id IN (12, 34)
     * $query->filterBySubscribeTypeId(array('min' => 12)); // WHERE inscricao_tipo_id >= 12
     * $query->filterBySubscribeTypeId(array('max' => 12)); // WHERE inscricao_tipo_id <= 12
     * </code>
     *
     * @see       filterBySubscribeType()
     *
     * @param     mixed $subscribeTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypeNameQuery The current query, for fluid interface
     */
    public function filterBySubscribeTypeId($subscribeTypeId = null, $comparison = null)
    {
        if (is_array($subscribeTypeId)) {
            $useMinMax = false;
            if (isset($subscribeTypeId['min'])) {
                $this->addUsingAlias(SubscribeTypeNamePeer::INSCRICAO_TIPO_ID, $subscribeTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subscribeTypeId['max'])) {
                $this->addUsingAlias(SubscribeTypeNamePeer::INSCRICAO_TIPO_ID, $subscribeTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypeNamePeer::INSCRICAO_TIPO_ID, $subscribeTypeId, $comparison);
    }

    /**
     * Filter the query on the lingua_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE lingua_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE lingua_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE lingua_id >= 12
     * $query->filterByLanguageId(array('max' => 12)); // WHERE lingua_id <= 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypeNameQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(SubscribeTypeNamePeer::LINGUA_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(SubscribeTypeNamePeer::LINGUA_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypeNamePeer::LINGUA_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypeNameQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribeTypeNamePeer::NOME, $name, $comparison);
    }

    /**
     * Filter the query by a related SubscribeType object
     *
     * @param   SubscribeType|PropelObjectCollection $subscribeType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeTypeNameQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribeType($subscribeType, $comparison = null)
    {
        if ($subscribeType instanceof SubscribeType) {
            return $this
                ->addUsingAlias(SubscribeTypeNamePeer::INSCRICAO_TIPO_ID, $subscribeType->getId(), $comparison);
        } elseif ($subscribeType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribeTypeNamePeer::INSCRICAO_TIPO_ID, $subscribeType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySubscribeType() only accepts arguments of type SubscribeType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubscribeType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeTypeNameQuery The current query, for fluid interface
     */
    public function joinSubscribeType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubscribeType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubscribeType');
        }

        return $this;
    }

    /**
     * Use the SubscribeType relation SubscribeType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeTypeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribeType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubscribeType', '\AppBundle\Model\SubscribeTypeQuery');
    }

    /**
     * Filter the query by a related Language object
     *
     * @param   Language|PropelObjectCollection $language The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeTypeNameQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof Language) {
            return $this
                ->addUsingAlias(SubscribeTypeNamePeer::LINGUA_ID, $language->getId(), $comparison);
        } elseif ($language instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribeTypeNamePeer::LINGUA_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type Language or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeTypeNameQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\AppBundle\Model\LanguageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SubscribeTypeName $subscribeTypeName Object to remove from the list of results
     *
     * @return SubscribeTypeNameQuery The current query, for fluid interface
     */
    public function prune($subscribeTypeName = null)
    {
        if ($subscribeTypeName) {
            $this->addCond('pruneCond0', $this->getAliasedColName(SubscribeTypeNamePeer::INSCRICAO_TIPO_ID), $subscribeTypeName->getSubscribeTypeId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(SubscribeTypeNamePeer::LINGUA_ID), $subscribeTypeName->getLanguageId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
