<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Displacement;
use AppBundle\Model\DisplacementPeer;
use AppBundle\Model\DisplacementQuery;
use AppBundle\Model\Language;
use AppBundle\Model\Subscribe;

/**
 * @method DisplacementQuery orderById($order = Criteria::ASC) Order by the deslocamento_id column
 * @method DisplacementQuery orderByLanguageId($order = Criteria::ASC) Order by the lingua_id column
 * @method DisplacementQuery orderByName($order = Criteria::ASC) Order by the nome column
 *
 * @method DisplacementQuery groupById() Group by the deslocamento_id column
 * @method DisplacementQuery groupByLanguageId() Group by the lingua_id column
 * @method DisplacementQuery groupByName() Group by the nome column
 *
 * @method DisplacementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DisplacementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DisplacementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DisplacementQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method DisplacementQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method DisplacementQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method DisplacementQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method DisplacementQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method DisplacementQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method Displacement findOne(PropelPDO $con = null) Return the first Displacement matching the query
 * @method Displacement findOneOrCreate(PropelPDO $con = null) Return the first Displacement matching the query, or a new Displacement object populated from the query conditions when no match is found
 *
 * @method Displacement findOneByLanguageId(int $lingua_id) Return the first Displacement filtered by the lingua_id column
 * @method Displacement findOneByName(string $nome) Return the first Displacement filtered by the nome column
 *
 * @method array findById(int $deslocamento_id) Return Displacement objects filtered by the deslocamento_id column
 * @method array findByLanguageId(int $lingua_id) Return Displacement objects filtered by the lingua_id column
 * @method array findByName(string $nome) Return Displacement objects filtered by the nome column
 */
abstract class BaseDisplacementQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDisplacementQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\Displacement';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DisplacementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DisplacementQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DisplacementQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DisplacementQuery) {
            return $criteria;
        }
        $query = new DisplacementQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Displacement|Displacement[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DisplacementPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DisplacementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Displacement A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Displacement A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `deslocamento_id`, `lingua_id`, `nome` FROM `deslocamento` WHERE `deslocamento_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Displacement();
            $obj->hydrate($row);
            DisplacementPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Displacement|Displacement[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Displacement[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DisplacementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DisplacementPeer::DESLOCAMENTO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DisplacementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DisplacementPeer::DESLOCAMENTO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the deslocamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE deslocamento_id = 1234
     * $query->filterById(array(12, 34)); // WHERE deslocamento_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE deslocamento_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE deslocamento_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DisplacementQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DisplacementPeer::DESLOCAMENTO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DisplacementPeer::DESLOCAMENTO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DisplacementPeer::DESLOCAMENTO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the lingua_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE lingua_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE lingua_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE lingua_id >= 12
     * $query->filterByLanguageId(array('max' => 12)); // WHERE lingua_id <= 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DisplacementQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(DisplacementPeer::LINGUA_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(DisplacementPeer::LINGUA_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DisplacementPeer::LINGUA_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DisplacementQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DisplacementPeer::NOME, $name, $comparison);
    }

    /**
     * Filter the query by a related Language object
     *
     * @param   Language|PropelObjectCollection $language The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DisplacementQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof Language) {
            return $this
                ->addUsingAlias(DisplacementPeer::LINGUA_ID, $language->getId(), $comparison);
        } elseif ($language instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DisplacementPeer::LINGUA_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type Language or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DisplacementQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\AppBundle\Model\LanguageQuery');
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DisplacementQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(DisplacementPeer::DESLOCAMENTO_ID, $subscribe->getDisplacementId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            return $this
                ->useSubscribeQuery()
                ->filterByPrimaryKeys($subscribe->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DisplacementQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Displacement $displacement Object to remove from the list of results
     *
     * @return DisplacementQuery The current query, for fluid interface
     */
    public function prune($displacement = null)
    {
        if ($displacement) {
            $this->addUsingAlias(DisplacementPeer::DESLOCAMENTO_ID, $displacement->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
