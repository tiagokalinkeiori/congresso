<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use AppBundle\Model\Donation;
use AppBundle\Model\DonationPeer;
use AppBundle\Model\DonationQuery;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribeQuery;

abstract class BaseDonation extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\DonationPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        DonationPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the doacao_id field.
     * @var        int
     */
    protected $doacao_id;

    /**
     * The value for the inscricao_id field.
     * @var        int
     */
    protected $inscricao_id;

    /**
     * The value for the nome field.
     * @var        string
     */
    protected $nome;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the telefone field.
     * @var        string
     */
    protected $telefone;

    /**
     * The value for the valor field.
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor;

    /**
     * The value for the pago field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $pago;

    /**
     * The value for the nosso_numero field.
     * @var        string
     */
    protected $nosso_numero;

    /**
     * The value for the linha_digitavel field.
     * @var        string
     */
    protected $linha_digitavel;

    /**
     * The value for the observacao field.
     * @var        string
     */
    protected $observacao;

    /**
     * The value for the data field.
     * @var        string
     */
    protected $data;

    /**
     * The value for the data_pagamento field.
     * @var        string
     */
    protected $data_pagamento;

    /**
     * @var        Subscribe
     */
    protected $aSubscribe;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->valor = '0.00';
        $this->pago = false;
    }

    /**
     * Initializes internal state of BaseDonation object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [doacao_id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->doacao_id;
    }

    /**
     * Get the [inscricao_id] column value.
     *
     * @return int
     */
    public function getSubscribeId()
    {

        return $this->inscricao_id;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->nome;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [telefone] column value.
     *
     * @return string
     */
    public function getPhone()
    {

        return $this->telefone;
    }

    /**
     * Get the [valor] column value.
     *
     * @return string
     */
    public function getPrice()
    {

        return $this->valor;
    }

    /**
     * Get the [pago] column value.
     *
     * @return boolean
     */
    public function getPaid()
    {

        return $this->pago;
    }

    /**
     * Get the [nosso_numero] column value.
     *
     * @return string
     */
    public function getOwerNumber()
    {

        return $this->nosso_numero;
    }

    /**
     * Get the [linha_digitavel] column value.
     *
     * @return string
     */
    public function getDigitableLine()
    {

        return $this->linha_digitavel;
    }

    /**
     * Get the [observacao] column value.
     *
     * @return string
     */
    public function getObservation()
    {

        return $this->observacao;
    }

    /**
     * Get the [optionally formatted] temporal [data] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDate($format = null)
    {
        if ($this->data === null) {
            return null;
        }

        if ($this->data === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [data_pagamento] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPaymentDate($format = null)
    {
        if ($this->data_pagamento === null) {
            return null;
        }

        if ($this->data_pagamento === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_pagamento);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_pagamento, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [doacao_id] column.
     *
     * @param  int $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->doacao_id !== $v) {
            $this->doacao_id = $v;
            $this->modifiedColumns[] = DonationPeer::DOACAO_ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [inscricao_id] column.
     *
     * @param  int $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setSubscribeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_id !== $v) {
            $this->inscricao_id = $v;
            $this->modifiedColumns[] = DonationPeer::INSCRICAO_ID;
        }

        if ($this->aSubscribe !== null && $this->aSubscribe->getId() !== $v) {
            $this->aSubscribe = null;
        }


        return $this;
    } // setSubscribeId()

    /**
     * Set the value of [nome] column.
     *
     * @param  string $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[] = DonationPeer::NOME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = DonationPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [telefone] column.
     *
     * @param  string $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telefone !== $v) {
            $this->telefone = $v;
            $this->modifiedColumns[] = DonationPeer::TELEFONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [valor] column.
     *
     * @param  string $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[] = DonationPeer::VALOR;
        }


        return $this;
    } // setPrice()

    /**
     * Sets the value of the [pago] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Donation The current object (for fluent API support)
     */
    public function setPaid($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->pago !== $v) {
            $this->pago = $v;
            $this->modifiedColumns[] = DonationPeer::PAGO;
        }


        return $this;
    } // setPaid()

    /**
     * Set the value of [nosso_numero] column.
     *
     * @param  string $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setOwerNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nosso_numero !== $v) {
            $this->nosso_numero = $v;
            $this->modifiedColumns[] = DonationPeer::NOSSO_NUMERO;
        }


        return $this;
    } // setOwerNumber()

    /**
     * Set the value of [linha_digitavel] column.
     *
     * @param  string $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setDigitableLine($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->linha_digitavel !== $v) {
            $this->linha_digitavel = $v;
            $this->modifiedColumns[] = DonationPeer::LINHA_DIGITAVEL;
        }


        return $this;
    } // setDigitableLine()

    /**
     * Set the value of [observacao] column.
     *
     * @param  string $v new value
     * @return Donation The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observacao !== $v) {
            $this->observacao = $v;
            $this->modifiedColumns[] = DonationPeer::OBSERVACAO;
        }


        return $this;
    } // setObservation()

    /**
     * Sets the value of [data] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Donation The current object (for fluent API support)
     */
    public function setDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data !== null || $dt !== null) {
            $currentDateAsString = ($this->data !== null && $tmpDt = new DateTime($this->data)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data = $newDateAsString;
                $this->modifiedColumns[] = DonationPeer::DATA;
            }
        } // if either are not null


        return $this;
    } // setDate()

    /**
     * Sets the value of [data_pagamento] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Donation The current object (for fluent API support)
     */
    public function setPaymentDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_pagamento !== null || $dt !== null) {
            $currentDateAsString = ($this->data_pagamento !== null && $tmpDt = new DateTime($this->data_pagamento)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_pagamento = $newDateAsString;
                $this->modifiedColumns[] = DonationPeer::DATA_PAGAMENTO;
            }
        } // if either are not null


        return $this;
    } // setPaymentDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->valor !== '0.00') {
                return false;
            }

            if ($this->pago !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->doacao_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->inscricao_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->nome = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->email = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->telefone = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->valor = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->pago = ($row[$startcol + 6] !== null) ? (boolean) $row[$startcol + 6] : null;
            $this->nosso_numero = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->linha_digitavel = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->observacao = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->data = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->data_pagamento = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 12; // 12 = DonationPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Donation object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSubscribe !== null && $this->inscricao_id !== $this->aSubscribe->getId()) {
            $this->aSubscribe = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DonationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = DonationPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSubscribe = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DonationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = DonationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DonationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DonationPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribe !== null) {
                if ($this->aSubscribe->isModified() || $this->aSubscribe->isNew()) {
                    $affectedRows += $this->aSubscribe->save($con);
                }
                $this->setSubscribe($this->aSubscribe);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = DonationPeer::DOACAO_ID;
        if (null !== $this->doacao_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . DonationPeer::DOACAO_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(DonationPeer::DOACAO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`doacao_id`';
        }
        if ($this->isColumnModified(DonationPeer::INSCRICAO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_id`';
        }
        if ($this->isColumnModified(DonationPeer::NOME)) {
            $modifiedColumns[':p' . $index++]  = '`nome`';
        }
        if ($this->isColumnModified(DonationPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(DonationPeer::TELEFONE)) {
            $modifiedColumns[':p' . $index++]  = '`telefone`';
        }
        if ($this->isColumnModified(DonationPeer::VALOR)) {
            $modifiedColumns[':p' . $index++]  = '`valor`';
        }
        if ($this->isColumnModified(DonationPeer::PAGO)) {
            $modifiedColumns[':p' . $index++]  = '`pago`';
        }
        if ($this->isColumnModified(DonationPeer::NOSSO_NUMERO)) {
            $modifiedColumns[':p' . $index++]  = '`nosso_numero`';
        }
        if ($this->isColumnModified(DonationPeer::LINHA_DIGITAVEL)) {
            $modifiedColumns[':p' . $index++]  = '`linha_digitavel`';
        }
        if ($this->isColumnModified(DonationPeer::OBSERVACAO)) {
            $modifiedColumns[':p' . $index++]  = '`observacao`';
        }
        if ($this->isColumnModified(DonationPeer::DATA)) {
            $modifiedColumns[':p' . $index++]  = '`data`';
        }
        if ($this->isColumnModified(DonationPeer::DATA_PAGAMENTO)) {
            $modifiedColumns[':p' . $index++]  = '`data_pagamento`';
        }

        $sql = sprintf(
            'INSERT INTO `doacao` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`doacao_id`':
                        $stmt->bindValue($identifier, $this->doacao_id, PDO::PARAM_INT);
                        break;
                    case '`inscricao_id`':
                        $stmt->bindValue($identifier, $this->inscricao_id, PDO::PARAM_INT);
                        break;
                    case '`nome`':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`telefone`':
                        $stmt->bindValue($identifier, $this->telefone, PDO::PARAM_STR);
                        break;
                    case '`valor`':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                    case '`pago`':
                        $stmt->bindValue($identifier, (int) $this->pago, PDO::PARAM_INT);
                        break;
                    case '`nosso_numero`':
                        $stmt->bindValue($identifier, $this->nosso_numero, PDO::PARAM_STR);
                        break;
                    case '`linha_digitavel`':
                        $stmt->bindValue($identifier, $this->linha_digitavel, PDO::PARAM_STR);
                        break;
                    case '`observacao`':
                        $stmt->bindValue($identifier, $this->observacao, PDO::PARAM_STR);
                        break;
                    case '`data`':
                        $stmt->bindValue($identifier, $this->data, PDO::PARAM_STR);
                        break;
                    case '`data_pagamento`':
                        $stmt->bindValue($identifier, $this->data_pagamento, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribe !== null) {
                if (!$this->aSubscribe->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSubscribe->getValidationFailures());
                }
            }


            if (($retval = DonationPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = DonationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSubscribeId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getEmail();
                break;
            case 4:
                return $this->getPhone();
                break;
            case 5:
                return $this->getPrice();
                break;
            case 6:
                return $this->getPaid();
                break;
            case 7:
                return $this->getOwerNumber();
                break;
            case 8:
                return $this->getDigitableLine();
                break;
            case 9:
                return $this->getObservation();
                break;
            case 10:
                return $this->getDate();
                break;
            case 11:
                return $this->getPaymentDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Donation'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Donation'][$this->getPrimaryKey()] = true;
        $keys = DonationPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSubscribeId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getEmail(),
            $keys[4] => $this->getPhone(),
            $keys[5] => $this->getPrice(),
            $keys[6] => $this->getPaid(),
            $keys[7] => $this->getOwerNumber(),
            $keys[8] => $this->getDigitableLine(),
            $keys[9] => $this->getObservation(),
            $keys[10] => $this->getDate(),
            $keys[11] => $this->getPaymentDate(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSubscribe) {
                $result['Subscribe'] = $this->aSubscribe->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = DonationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSubscribeId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setEmail($value);
                break;
            case 4:
                $this->setPhone($value);
                break;
            case 5:
                $this->setPrice($value);
                break;
            case 6:
                $this->setPaid($value);
                break;
            case 7:
                $this->setOwerNumber($value);
                break;
            case 8:
                $this->setDigitableLine($value);
                break;
            case 9:
                $this->setObservation($value);
                break;
            case 10:
                $this->setDate($value);
                break;
            case 11:
                $this->setPaymentDate($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = DonationPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSubscribeId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setEmail($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPhone($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPrice($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setPaid($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setOwerNumber($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setDigitableLine($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setObservation($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setDate($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setPaymentDate($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DonationPeer::DATABASE_NAME);

        if ($this->isColumnModified(DonationPeer::DOACAO_ID)) $criteria->add(DonationPeer::DOACAO_ID, $this->doacao_id);
        if ($this->isColumnModified(DonationPeer::INSCRICAO_ID)) $criteria->add(DonationPeer::INSCRICAO_ID, $this->inscricao_id);
        if ($this->isColumnModified(DonationPeer::NOME)) $criteria->add(DonationPeer::NOME, $this->nome);
        if ($this->isColumnModified(DonationPeer::EMAIL)) $criteria->add(DonationPeer::EMAIL, $this->email);
        if ($this->isColumnModified(DonationPeer::TELEFONE)) $criteria->add(DonationPeer::TELEFONE, $this->telefone);
        if ($this->isColumnModified(DonationPeer::VALOR)) $criteria->add(DonationPeer::VALOR, $this->valor);
        if ($this->isColumnModified(DonationPeer::PAGO)) $criteria->add(DonationPeer::PAGO, $this->pago);
        if ($this->isColumnModified(DonationPeer::NOSSO_NUMERO)) $criteria->add(DonationPeer::NOSSO_NUMERO, $this->nosso_numero);
        if ($this->isColumnModified(DonationPeer::LINHA_DIGITAVEL)) $criteria->add(DonationPeer::LINHA_DIGITAVEL, $this->linha_digitavel);
        if ($this->isColumnModified(DonationPeer::OBSERVACAO)) $criteria->add(DonationPeer::OBSERVACAO, $this->observacao);
        if ($this->isColumnModified(DonationPeer::DATA)) $criteria->add(DonationPeer::DATA, $this->data);
        if ($this->isColumnModified(DonationPeer::DATA_PAGAMENTO)) $criteria->add(DonationPeer::DATA_PAGAMENTO, $this->data_pagamento);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(DonationPeer::DATABASE_NAME);
        $criteria->add(DonationPeer::DOACAO_ID, $this->doacao_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (doacao_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Donation (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSubscribeId($this->getSubscribeId());
        $copyObj->setName($this->getName());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setPaid($this->getPaid());
        $copyObj->setOwerNumber($this->getOwerNumber());
        $copyObj->setDigitableLine($this->getDigitableLine());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setDate($this->getDate());
        $copyObj->setPaymentDate($this->getPaymentDate());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Donation Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return DonationPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new DonationPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Subscribe object.
     *
     * @param                  Subscribe $v
     * @return Donation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSubscribe(Subscribe $v = null)
    {
        if ($v === null) {
            $this->setSubscribeId(NULL);
        } else {
            $this->setSubscribeId($v->getId());
        }

        $this->aSubscribe = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Subscribe object, it will not be re-added.
        if ($v !== null) {
            $v->addDonation($this);
        }


        return $this;
    }


    /**
     * Get the associated Subscribe object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Subscribe The associated Subscribe object.
     * @throws PropelException
     */
    public function getSubscribe(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSubscribe === null && ($this->inscricao_id !== null) && $doQuery) {
            $this->aSubscribe = SubscribeQuery::create()->findPk($this->inscricao_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSubscribe->addDonations($this);
             */
        }

        return $this->aSubscribe;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->doacao_id = null;
        $this->inscricao_id = null;
        $this->nome = null;
        $this->email = null;
        $this->telefone = null;
        $this->valor = null;
        $this->pago = null;
        $this->nosso_numero = null;
        $this->linha_digitavel = null;
        $this->observacao = null;
        $this->data = null;
        $this->data_pagamento = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSubscribe instanceof Persistent) {
              $this->aSubscribe->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSubscribe = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DonationPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
