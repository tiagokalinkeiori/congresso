<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Country;
use AppBundle\Model\CountryQuery;
use AppBundle\Model\Displacement;
use AppBundle\Model\DisplacementQuery;
use AppBundle\Model\District;
use AppBundle\Model\DistrictQuery;
use AppBundle\Model\Donation;
use AppBundle\Model\DonationQuery;
use AppBundle\Model\Language;
use AppBundle\Model\LanguageQuery;
use AppBundle\Model\State;
use AppBundle\Model\StateQuery;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePaymentQuery;
use AppBundle\Model\SubscribePeer;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\SubscribeType;
use AppBundle\Model\SubscribeTypeQuery;
use AppBundle\Model\Train;
use AppBundle\Model\TrainQuery;
use AppBundle\Model\Transfer;
use AppBundle\Model\TransferQuery;
use AppBundle\Model\User;
use AppBundle\Model\UserQuery;
use AppBundle\Model\WorkshopSubscribe;
use AppBundle\Model\WorkshopSubscribeQuery;

abstract class BaseSubscribe extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\SubscribePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SubscribePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the inscricao_id field.
     * @var        int
     */
    protected $inscricao_id;

    /**
     * The value for the usuario_id field.
     * @var        int
     */
    protected $usuario_id;

    /**
     * The value for the pais_id field.
     * @var        int
     */
    protected $pais_id;

    /**
     * The value for the estado_id field.
     * @var        int
     */
    protected $estado_id;

    /**
     * The value for the distrito_id field.
     * @var        int
     */
    protected $distrito_id;

    /**
     * The value for the inscricao_tipo_id field.
     * @var        int
     */
    protected $inscricao_tipo_id;

    /**
     * The value for the deslocamento_id field.
     * @var        int
     */
    protected $deslocamento_id;

    /**
     * The value for the lingua_id field.
     * @var        int
     */
    protected $lingua_id;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the nome field.
     * @var        string
     */
    protected $nome;

    /**
     * The value for the data_nascimento field.
     * @var        string
     */
    protected $data_nascimento;

    /**
     * The value for the nome_responsavel field.
     * @var        string
     */
    protected $nome_responsavel;

    /**
     * The value for the fone_responsavel field.
     * @var        string
     */
    protected $fone_responsavel;

    /**
     * The value for the registro field.
     * @var        string
     */
    protected $registro;

    /**
     * The value for the orgao_expedidor field.
     * @var        string
     */
    protected $orgao_expedidor;

    /**
     * The value for the orgao_expedidor_uf field.
     * @var        string
     */
    protected $orgao_expedidor_uf;

    /**
     * The value for the documento field.
     * @var        string
     */
    protected $documento;

    /**
     * The value for the sexo field.
     * @var        string
     */
    protected $sexo;

    /**
     * The value for the telefone field.
     * @var        string
     */
    protected $telefone;

    /**
     * The value for the endereco field.
     * @var        string
     */
    protected $endereco;

    /**
     * The value for the numero field.
     * @var        string
     */
    protected $numero;

    /**
     * The value for the complemento field.
     * @var        string
     */
    protected $complemento;

    /**
     * The value for the cidade field.
     * @var        string
     */
    protected $cidade;

    /**
     * The value for the bairro field.
     * @var        string
     */
    protected $bairro;

    /**
     * The value for the cep field.
     * @var        string
     */
    protected $cep;

    /**
     * The value for the membro_ielb field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $membro_ielb;

    /**
     * The value for the confirmado field.
     * @var        boolean
     */
    protected $confirmado;

    /**
     * The value for the delegado_plenaria field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $delegado_plenaria;

    /**
     * The value for the presidente_uj field.
     * @var        string
     */
    protected $presidente_uj;

    /**
     * The value for the pastor field.
     * @var        string
     */
    protected $pastor;

    /**
     * The value for the uj field.
     * @var        string
     */
    protected $uj;

    /**
     * The value for the responsavel_caravana field.
     * @var        string
     */
    protected $responsavel_caravana;

    /**
     * The value for the parcelas field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $parcelas;

    /**
     * The value for the valor_total field.
     * Note: this column has a database default value of: 0.0
     * @var        double
     */
    protected $valor_total;

    /**
     * The value for the necessidades_especiais field.
     * @var        string
     */
    protected $necessidades_especiais;

    /**
     * The value for the comentarios field.
     * @var        string
     */
    protected $comentarios;

    /**
     * The value for the primeiro_congresso field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $primeiro_congresso;

    /**
     * The value for the quantidade_congresso field.
     * @var        int
     */
    protected $quantidade_congresso;

    /**
     * The value for the isento field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $isento;

    /**
     * The value for the organizacao field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $organizacao;

    /**
     * The value for the visitante field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $visitante;

    /**
     * The value for the cancelado field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cancelado;

    /**
     * The value for the data_inscricao field.
     * @var        string
     */
    protected $data_inscricao;

    /**
     * @var        District
     */
    protected $aDistrict;

    /**
     * @var        SubscribeType
     */
    protected $aSubscribeType;

    /**
     * @var        Language
     */
    protected $aLanguage;

    /**
     * @var        User
     */
    protected $aUser;

    /**
     * @var        Displacement
     */
    protected $aDisplacement;

    /**
     * @var        State
     */
    protected $aState;

    /**
     * @var        Country
     */
    protected $aCountry;

    /**
     * @var        PropelObjectCollection|Train[] Collection to store aggregation of Train objects.
     */
    protected $collTrains;
    protected $collTrainsPartial;

    /**
     * @var        PropelObjectCollection|Donation[] Collection to store aggregation of Donation objects.
     */
    protected $collDonations;
    protected $collDonationsPartial;

    /**
     * @var        PropelObjectCollection|SubscribePayment[] Collection to store aggregation of SubscribePayment objects.
     */
    protected $collSubscribePayments;
    protected $collSubscribePaymentsPartial;

    /**
     * @var        PropelObjectCollection|WorkshopSubscribe[] Collection to store aggregation of WorkshopSubscribe objects.
     */
    protected $collWorkshopSubscribes;
    protected $collWorkshopSubscribesPartial;

    /**
     * @var        Transfer one-to-one related Transfer object
     */
    protected $singleTransfer;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $trainsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $donationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $subscribePaymentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $workshopSubscribesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->membro_ielb = true;
        $this->delegado_plenaria = false;
        $this->parcelas = 1;
        $this->valor_total = 0.0;
        $this->primeiro_congresso = false;
        $this->isento = false;
        $this->organizacao = false;
        $this->visitante = false;
        $this->cancelado = false;
    }

    /**
     * Initializes internal state of BaseSubscribe object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [inscricao_id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->inscricao_id;
    }

    /**
     * Get the [usuario_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {

        return $this->usuario_id;
    }

    /**
     * Get the [pais_id] column value.
     *
     * @return int
     */
    public function getCountryId()
    {

        return $this->pais_id;
    }

    /**
     * Get the [estado_id] column value.
     *
     * @return int
     */
    public function getStateId()
    {

        return $this->estado_id;
    }

    /**
     * Get the [distrito_id] column value.
     *
     * @return int
     */
    public function getDistrictId()
    {

        return $this->distrito_id;
    }

    /**
     * Get the [inscricao_tipo_id] column value.
     *
     * @return int
     */
    public function getSubscribeTypeId()
    {

        return $this->inscricao_tipo_id;
    }

    /**
     * Get the [deslocamento_id] column value.
     *
     * @return int
     */
    public function getDisplacementId()
    {

        return $this->deslocamento_id;
    }

    /**
     * Get the [lingua_id] column value.
     *
     * @return int
     */
    public function getLanguageId()
    {

        return $this->lingua_id;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->nome;
    }

    /**
     * Get the [optionally formatted] temporal [data_nascimento] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBirthday($format = null)
    {
        if ($this->data_nascimento === null) {
            return null;
        }

        if ($this->data_nascimento === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_nascimento);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_nascimento, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [nome_responsavel] column value.
     *
     * @return string
     */
    public function getResponsibleName()
    {

        return $this->nome_responsavel;
    }

    /**
     * Get the [fone_responsavel] column value.
     *
     * @return string
     */
    public function getResponsiblePhone()
    {

        return $this->fone_responsavel;
    }

    /**
     * Get the [registro] column value.
     *
     * @return string
     */
    public function getRegister()
    {

        return $this->registro;
    }

    /**
     * Get the [orgao_expedidor] column value.
     *
     * @return string
     */
    public function getDispatcher()
    {

        return $this->orgao_expedidor;
    }

    /**
     * Get the [orgao_expedidor_uf] column value.
     *
     * @return string
     */
    public function getDispatcherPlace()
    {

        return $this->orgao_expedidor_uf;
    }

    /**
     * Get the [documento] column value.
     *
     * @return string
     */
    public function getDocument()
    {

        return $this->documento;
    }

    /**
     * Get the [sexo] column value.
     *
     * @return string
     */
    public function getGenre()
    {

        return $this->sexo;
    }

    /**
     * Get the [telefone] column value.
     *
     * @return string
     */
    public function getPhone()
    {

        return $this->telefone;
    }

    /**
     * Get the [endereco] column value.
     *
     * @return string
     */
    public function getAddress()
    {

        return $this->endereco;
    }

    /**
     * Get the [numero] column value.
     *
     * @return string
     */
    public function getNumber()
    {

        return $this->numero;
    }

    /**
     * Get the [complemento] column value.
     *
     * @return string
     */
    public function getComplement()
    {

        return $this->complemento;
    }

    /**
     * Get the [cidade] column value.
     *
     * @return string
     */
    public function getCity()
    {

        return $this->cidade;
    }

    /**
     * Get the [bairro] column value.
     *
     * @return string
     */
    public function getRegion()
    {

        return $this->bairro;
    }

    /**
     * Get the [cep] column value.
     *
     * @return string
     */
    public function getZipCode()
    {

        return $this->cep;
    }

    /**
     * Get the [membro_ielb] column value.
     *
     * @return boolean
     */
    public function getMember()
    {

        return $this->membro_ielb;
    }

    /**
     * Get the [confirmado] column value.
     *
     * @return boolean
     */
    public function getConfirmed()
    {

        return $this->confirmado;
    }

    /**
     * Get the [delegado_plenaria] column value.
     *
     * @return boolean
     */
    public function getDelegate()
    {

        return $this->delegado_plenaria;
    }

    /**
     * Get the [presidente_uj] column value.
     *
     * @return string
     */
    public function getUnityPresident()
    {

        return $this->presidente_uj;
    }

    /**
     * Get the [pastor] column value.
     *
     * @return string
     */
    public function getMinister()
    {

        return $this->pastor;
    }

    /**
     * Get the [uj] column value.
     *
     * @return string
     */
    public function getUnityName()
    {

        return $this->uj;
    }

    /**
     * Get the [responsavel_caravana] column value.
     *
     * @return string
     */
    public function getTrainResponsible()
    {

        return $this->responsavel_caravana;
    }

    /**
     * Get the [parcelas] column value.
     *
     * @return int
     */
    public function getQuota()
    {

        return $this->parcelas;
    }

    /**
     * Get the [valor_total] column value.
     *
     * @return double
     */
    public function getPrice()
    {

        return $this->valor_total;
    }

    /**
     * Get the [necessidades_especiais] column value.
     *
     * @return string
     */
    public function getSpecialNeed()
    {

        return $this->necessidades_especiais;
    }

    /**
     * Get the [comentarios] column value.
     *
     * @return string
     */
    public function getComment()
    {

        return $this->comentarios;
    }

    /**
     * Get the [primeiro_congresso] column value.
     *
     * @return boolean
     */
    public function getFirstCongress()
    {

        return $this->primeiro_congresso;
    }

    /**
     * Get the [quantidade_congresso] column value.
     *
     * @return int
     */
    public function getAmountCongress()
    {

        return $this->quantidade_congresso;
    }

    /**
     * Get the [isento] column value.
     *
     * @return boolean
     */
    public function getFree()
    {

        return $this->isento;
    }

    /**
     * Get the [organizacao] column value.
     *
     * @return boolean
     */
    public function getOrganization()
    {

        return $this->organizacao;
    }

    /**
     * Get the [visitante] column value.
     *
     * @return boolean
     */
    public function getVisitor()
    {

        return $this->visitante;
    }

    /**
     * Get the [cancelado] column value.
     *
     * @return boolean
     */
    public function getCanceled()
    {

        return $this->cancelado;
    }

    /**
     * Get the [optionally formatted] temporal [data_inscricao] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDate($format = null)
    {
        if ($this->data_inscricao === null) {
            return null;
        }

        if ($this->data_inscricao === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_inscricao);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_inscricao, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [inscricao_id] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_id !== $v) {
            $this->inscricao_id = $v;
            $this->modifiedColumns[] = SubscribePeer::INSCRICAO_ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [usuario_id] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->usuario_id !== $v) {
            $this->usuario_id = $v;
            $this->modifiedColumns[] = SubscribePeer::USUARIO_ID;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }


        return $this;
    } // setUserId()

    /**
     * Set the value of [pais_id] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setCountryId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pais_id !== $v) {
            $this->pais_id = $v;
            $this->modifiedColumns[] = SubscribePeer::PAIS_ID;
        }

        if ($this->aCountry !== null && $this->aCountry->getId() !== $v) {
            $this->aCountry = null;
        }


        return $this;
    } // setCountryId()

    /**
     * Set the value of [estado_id] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setStateId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->estado_id !== $v) {
            $this->estado_id = $v;
            $this->modifiedColumns[] = SubscribePeer::ESTADO_ID;
        }

        if ($this->aState !== null && $this->aState->getId() !== $v) {
            $this->aState = null;
        }


        return $this;
    } // setStateId()

    /**
     * Set the value of [distrito_id] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setDistrictId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->distrito_id !== $v) {
            $this->distrito_id = $v;
            $this->modifiedColumns[] = SubscribePeer::DISTRITO_ID;
        }

        if ($this->aDistrict !== null && $this->aDistrict->getId() !== $v) {
            $this->aDistrict = null;
        }


        return $this;
    } // setDistrictId()

    /**
     * Set the value of [inscricao_tipo_id] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setSubscribeTypeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_tipo_id !== $v) {
            $this->inscricao_tipo_id = $v;
            $this->modifiedColumns[] = SubscribePeer::INSCRICAO_TIPO_ID;
        }

        if ($this->aSubscribeType !== null && $this->aSubscribeType->getId() !== $v) {
            $this->aSubscribeType = null;
        }


        return $this;
    } // setSubscribeTypeId()

    /**
     * Set the value of [deslocamento_id] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setDisplacementId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->deslocamento_id !== $v) {
            $this->deslocamento_id = $v;
            $this->modifiedColumns[] = SubscribePeer::DESLOCAMENTO_ID;
        }

        if ($this->aDisplacement !== null && $this->aDisplacement->getId() !== $v) {
            $this->aDisplacement = null;
        }


        return $this;
    } // setDisplacementId()

    /**
     * Set the value of [lingua_id] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setLanguageId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->lingua_id !== $v) {
            $this->lingua_id = $v;
            $this->modifiedColumns[] = SubscribePeer::LINGUA_ID;
        }

        if ($this->aLanguage !== null && $this->aLanguage->getId() !== $v) {
            $this->aLanguage = null;
        }


        return $this;
    } // setLanguageId()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = SubscribePeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [nome] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[] = SubscribePeer::NOME;
        }


        return $this;
    } // setName()

    /**
     * Sets the value of [data_nascimento] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Subscribe The current object (for fluent API support)
     */
    public function setBirthday($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_nascimento !== null || $dt !== null) {
            $currentDateAsString = ($this->data_nascimento !== null && $tmpDt = new DateTime($this->data_nascimento)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_nascimento = $newDateAsString;
                $this->modifiedColumns[] = SubscribePeer::DATA_NASCIMENTO;
            }
        } // if either are not null


        return $this;
    } // setBirthday()

    /**
     * Set the value of [nome_responsavel] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setResponsibleName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome_responsavel !== $v) {
            $this->nome_responsavel = $v;
            $this->modifiedColumns[] = SubscribePeer::NOME_RESPONSAVEL;
        }


        return $this;
    } // setResponsibleName()

    /**
     * Set the value of [fone_responsavel] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setResponsiblePhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fone_responsavel !== $v) {
            $this->fone_responsavel = $v;
            $this->modifiedColumns[] = SubscribePeer::FONE_RESPONSAVEL;
        }


        return $this;
    } // setResponsiblePhone()

    /**
     * Set the value of [registro] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setRegister($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->registro !== $v) {
            $this->registro = $v;
            $this->modifiedColumns[] = SubscribePeer::REGISTRO;
        }


        return $this;
    } // setRegister()

    /**
     * Set the value of [orgao_expedidor] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setDispatcher($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->orgao_expedidor !== $v) {
            $this->orgao_expedidor = $v;
            $this->modifiedColumns[] = SubscribePeer::ORGAO_EXPEDIDOR;
        }


        return $this;
    } // setDispatcher()

    /**
     * Set the value of [orgao_expedidor_uf] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setDispatcherPlace($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->orgao_expedidor_uf !== $v) {
            $this->orgao_expedidor_uf = $v;
            $this->modifiedColumns[] = SubscribePeer::ORGAO_EXPEDIDOR_UF;
        }


        return $this;
    } // setDispatcherPlace()

    /**
     * Set the value of [documento] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setDocument($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->documento !== $v) {
            $this->documento = $v;
            $this->modifiedColumns[] = SubscribePeer::DOCUMENTO;
        }


        return $this;
    } // setDocument()

    /**
     * Set the value of [sexo] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setGenre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sexo !== $v) {
            $this->sexo = $v;
            $this->modifiedColumns[] = SubscribePeer::SEXO;
        }


        return $this;
    } // setGenre()

    /**
     * Set the value of [telefone] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telefone !== $v) {
            $this->telefone = $v;
            $this->modifiedColumns[] = SubscribePeer::TELEFONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [endereco] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->endereco !== $v) {
            $this->endereco = $v;
            $this->modifiedColumns[] = SubscribePeer::ENDERECO;
        }


        return $this;
    } // setAddress()

    /**
     * Set the value of [numero] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->numero !== $v) {
            $this->numero = $v;
            $this->modifiedColumns[] = SubscribePeer::NUMERO;
        }


        return $this;
    } // setNumber()

    /**
     * Set the value of [complemento] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setComplement($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->complemento !== $v) {
            $this->complemento = $v;
            $this->modifiedColumns[] = SubscribePeer::COMPLEMENTO;
        }


        return $this;
    } // setComplement()

    /**
     * Set the value of [cidade] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cidade !== $v) {
            $this->cidade = $v;
            $this->modifiedColumns[] = SubscribePeer::CIDADE;
        }


        return $this;
    } // setCity()

    /**
     * Set the value of [bairro] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setRegion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bairro !== $v) {
            $this->bairro = $v;
            $this->modifiedColumns[] = SubscribePeer::BAIRRO;
        }


        return $this;
    } // setRegion()

    /**
     * Set the value of [cep] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setZipCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cep !== $v) {
            $this->cep = $v;
            $this->modifiedColumns[] = SubscribePeer::CEP;
        }


        return $this;
    } // setZipCode()

    /**
     * Sets the value of the [membro_ielb] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setMember($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->membro_ielb !== $v) {
            $this->membro_ielb = $v;
            $this->modifiedColumns[] = SubscribePeer::MEMBRO_IELB;
        }


        return $this;
    } // setMember()

    /**
     * Sets the value of the [confirmado] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setConfirmed($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->confirmado !== $v) {
            $this->confirmado = $v;
            $this->modifiedColumns[] = SubscribePeer::CONFIRMADO;
        }


        return $this;
    } // setConfirmed()

    /**
     * Sets the value of the [delegado_plenaria] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setDelegate($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->delegado_plenaria !== $v) {
            $this->delegado_plenaria = $v;
            $this->modifiedColumns[] = SubscribePeer::DELEGADO_PLENARIA;
        }


        return $this;
    } // setDelegate()

    /**
     * Set the value of [presidente_uj] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setUnityPresident($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->presidente_uj !== $v) {
            $this->presidente_uj = $v;
            $this->modifiedColumns[] = SubscribePeer::PRESIDENTE_UJ;
        }


        return $this;
    } // setUnityPresident()

    /**
     * Set the value of [pastor] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setMinister($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pastor !== $v) {
            $this->pastor = $v;
            $this->modifiedColumns[] = SubscribePeer::PASTOR;
        }


        return $this;
    } // setMinister()

    /**
     * Set the value of [uj] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setUnityName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uj !== $v) {
            $this->uj = $v;
            $this->modifiedColumns[] = SubscribePeer::UJ;
        }


        return $this;
    } // setUnityName()

    /**
     * Set the value of [responsavel_caravana] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setTrainResponsible($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->responsavel_caravana !== $v) {
            $this->responsavel_caravana = $v;
            $this->modifiedColumns[] = SubscribePeer::RESPONSAVEL_CARAVANA;
        }


        return $this;
    } // setTrainResponsible()

    /**
     * Set the value of [parcelas] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setQuota($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->parcelas !== $v) {
            $this->parcelas = $v;
            $this->modifiedColumns[] = SubscribePeer::PARCELAS;
        }


        return $this;
    } // setQuota()

    /**
     * Set the value of [valor_total] column.
     *
     * @param  double $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->valor_total !== $v) {
            $this->valor_total = $v;
            $this->modifiedColumns[] = SubscribePeer::VALOR_TOTAL;
        }


        return $this;
    } // setPrice()

    /**
     * Set the value of [necessidades_especiais] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setSpecialNeed($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->necessidades_especiais !== $v) {
            $this->necessidades_especiais = $v;
            $this->modifiedColumns[] = SubscribePeer::NECESSIDADES_ESPECIAIS;
        }


        return $this;
    } // setSpecialNeed()

    /**
     * Set the value of [comentarios] column.
     *
     * @param  string $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setComment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->comentarios !== $v) {
            $this->comentarios = $v;
            $this->modifiedColumns[] = SubscribePeer::COMENTARIOS;
        }


        return $this;
    } // setComment()

    /**
     * Sets the value of the [primeiro_congresso] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setFirstCongress($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->primeiro_congresso !== $v) {
            $this->primeiro_congresso = $v;
            $this->modifiedColumns[] = SubscribePeer::PRIMEIRO_CONGRESSO;
        }


        return $this;
    } // setFirstCongress()

    /**
     * Set the value of [quantidade_congresso] column.
     *
     * @param  int $v new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setAmountCongress($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->quantidade_congresso !== $v) {
            $this->quantidade_congresso = $v;
            $this->modifiedColumns[] = SubscribePeer::QUANTIDADE_CONGRESSO;
        }


        return $this;
    } // setAmountCongress()

    /**
     * Sets the value of the [isento] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setFree($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->isento !== $v) {
            $this->isento = $v;
            $this->modifiedColumns[] = SubscribePeer::ISENTO;
        }


        return $this;
    } // setFree()

    /**
     * Sets the value of the [organizacao] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setOrganization($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->organizacao !== $v) {
            $this->organizacao = $v;
            $this->modifiedColumns[] = SubscribePeer::ORGANIZACAO;
        }


        return $this;
    } // setOrganization()

    /**
     * Sets the value of the [visitante] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setVisitor($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->visitante !== $v) {
            $this->visitante = $v;
            $this->modifiedColumns[] = SubscribePeer::VISITANTE;
        }


        return $this;
    } // setVisitor()

    /**
     * Sets the value of the [cancelado] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Subscribe The current object (for fluent API support)
     */
    public function setCanceled($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cancelado !== $v) {
            $this->cancelado = $v;
            $this->modifiedColumns[] = SubscribePeer::CANCELADO;
        }


        return $this;
    } // setCanceled()

    /**
     * Sets the value of [data_inscricao] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Subscribe The current object (for fluent API support)
     */
    public function setDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_inscricao !== null || $dt !== null) {
            $currentDateAsString = ($this->data_inscricao !== null && $tmpDt = new DateTime($this->data_inscricao)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_inscricao = $newDateAsString;
                $this->modifiedColumns[] = SubscribePeer::DATA_INSCRICAO;
            }
        } // if either are not null


        return $this;
    } // setDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->membro_ielb !== true) {
                return false;
            }

            if ($this->delegado_plenaria !== false) {
                return false;
            }

            if ($this->parcelas !== 1) {
                return false;
            }

            if ($this->valor_total !== 0.0) {
                return false;
            }

            if ($this->primeiro_congresso !== false) {
                return false;
            }

            if ($this->isento !== false) {
                return false;
            }

            if ($this->organizacao !== false) {
                return false;
            }

            if ($this->visitante !== false) {
                return false;
            }

            if ($this->cancelado !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->inscricao_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->usuario_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->pais_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->estado_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->distrito_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->inscricao_tipo_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->deslocamento_id = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->lingua_id = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->email = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->nome = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->data_nascimento = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->nome_responsavel = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->fone_responsavel = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->registro = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->orgao_expedidor = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->orgao_expedidor_uf = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->documento = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->sexo = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->telefone = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->endereco = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->numero = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->complemento = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->cidade = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->bairro = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->cep = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->membro_ielb = ($row[$startcol + 25] !== null) ? (boolean) $row[$startcol + 25] : null;
            $this->confirmado = ($row[$startcol + 26] !== null) ? (boolean) $row[$startcol + 26] : null;
            $this->delegado_plenaria = ($row[$startcol + 27] !== null) ? (boolean) $row[$startcol + 27] : null;
            $this->presidente_uj = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->pastor = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->uj = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->responsavel_caravana = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->parcelas = ($row[$startcol + 32] !== null) ? (int) $row[$startcol + 32] : null;
            $this->valor_total = ($row[$startcol + 33] !== null) ? (double) $row[$startcol + 33] : null;
            $this->necessidades_especiais = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->comentarios = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->primeiro_congresso = ($row[$startcol + 36] !== null) ? (boolean) $row[$startcol + 36] : null;
            $this->quantidade_congresso = ($row[$startcol + 37] !== null) ? (int) $row[$startcol + 37] : null;
            $this->isento = ($row[$startcol + 38] !== null) ? (boolean) $row[$startcol + 38] : null;
            $this->organizacao = ($row[$startcol + 39] !== null) ? (boolean) $row[$startcol + 39] : null;
            $this->visitante = ($row[$startcol + 40] !== null) ? (boolean) $row[$startcol + 40] : null;
            $this->cancelado = ($row[$startcol + 41] !== null) ? (boolean) $row[$startcol + 41] : null;
            $this->data_inscricao = ($row[$startcol + 42] !== null) ? (string) $row[$startcol + 42] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 43; // 43 = SubscribePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Subscribe object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aUser !== null && $this->usuario_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
        if ($this->aCountry !== null && $this->pais_id !== $this->aCountry->getId()) {
            $this->aCountry = null;
        }
        if ($this->aState !== null && $this->estado_id !== $this->aState->getId()) {
            $this->aState = null;
        }
        if ($this->aDistrict !== null && $this->distrito_id !== $this->aDistrict->getId()) {
            $this->aDistrict = null;
        }
        if ($this->aSubscribeType !== null && $this->inscricao_tipo_id !== $this->aSubscribeType->getId()) {
            $this->aSubscribeType = null;
        }
        if ($this->aDisplacement !== null && $this->deslocamento_id !== $this->aDisplacement->getId()) {
            $this->aDisplacement = null;
        }
        if ($this->aLanguage !== null && $this->lingua_id !== $this->aLanguage->getId()) {
            $this->aLanguage = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SubscribePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aDistrict = null;
            $this->aSubscribeType = null;
            $this->aLanguage = null;
            $this->aUser = null;
            $this->aDisplacement = null;
            $this->aState = null;
            $this->aCountry = null;
            $this->collTrains = null;

            $this->collDonations = null;

            $this->collSubscribePayments = null;

            $this->collWorkshopSubscribes = null;

            $this->singleTransfer = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SubscribeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SubscribePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDistrict !== null) {
                if ($this->aDistrict->isModified() || $this->aDistrict->isNew()) {
                    $affectedRows += $this->aDistrict->save($con);
                }
                $this->setDistrict($this->aDistrict);
            }

            if ($this->aSubscribeType !== null) {
                if ($this->aSubscribeType->isModified() || $this->aSubscribeType->isNew()) {
                    $affectedRows += $this->aSubscribeType->save($con);
                }
                $this->setSubscribeType($this->aSubscribeType);
            }

            if ($this->aLanguage !== null) {
                if ($this->aLanguage->isModified() || $this->aLanguage->isNew()) {
                    $affectedRows += $this->aLanguage->save($con);
                }
                $this->setLanguage($this->aLanguage);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aDisplacement !== null) {
                if ($this->aDisplacement->isModified() || $this->aDisplacement->isNew()) {
                    $affectedRows += $this->aDisplacement->save($con);
                }
                $this->setDisplacement($this->aDisplacement);
            }

            if ($this->aState !== null) {
                if ($this->aState->isModified() || $this->aState->isNew()) {
                    $affectedRows += $this->aState->save($con);
                }
                $this->setState($this->aState);
            }

            if ($this->aCountry !== null) {
                if ($this->aCountry->isModified() || $this->aCountry->isNew()) {
                    $affectedRows += $this->aCountry->save($con);
                }
                $this->setCountry($this->aCountry);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->trainsScheduledForDeletion !== null) {
                if (!$this->trainsScheduledForDeletion->isEmpty()) {
                    TrainQuery::create()
                        ->filterByPrimaryKeys($this->trainsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->trainsScheduledForDeletion = null;
                }
            }

            if ($this->collTrains !== null) {
                foreach ($this->collTrains as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->donationsScheduledForDeletion !== null) {
                if (!$this->donationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->donationsScheduledForDeletion as $donation) {
                        // need to save related object because we set the relation to null
                        $donation->save($con);
                    }
                    $this->donationsScheduledForDeletion = null;
                }
            }

            if ($this->collDonations !== null) {
                foreach ($this->collDonations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->subscribePaymentsScheduledForDeletion !== null) {
                if (!$this->subscribePaymentsScheduledForDeletion->isEmpty()) {
                    SubscribePaymentQuery::create()
                        ->filterByPrimaryKeys($this->subscribePaymentsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->subscribePaymentsScheduledForDeletion = null;
                }
            }

            if ($this->collSubscribePayments !== null) {
                foreach ($this->collSubscribePayments as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->workshopSubscribesScheduledForDeletion !== null) {
                if (!$this->workshopSubscribesScheduledForDeletion->isEmpty()) {
                    WorkshopSubscribeQuery::create()
                        ->filterByPrimaryKeys($this->workshopSubscribesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->workshopSubscribesScheduledForDeletion = null;
                }
            }

            if ($this->collWorkshopSubscribes !== null) {
                foreach ($this->collWorkshopSubscribes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->singleTransfer !== null) {
                if (!$this->singleTransfer->isDeleted() && ($this->singleTransfer->isNew() || $this->singleTransfer->isModified())) {
                        $affectedRows += $this->singleTransfer->save($con);
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SubscribePeer::INSCRICAO_ID;
        if (null !== $this->inscricao_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SubscribePeer::INSCRICAO_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SubscribePeer::INSCRICAO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_id`';
        }
        if ($this->isColumnModified(SubscribePeer::USUARIO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`usuario_id`';
        }
        if ($this->isColumnModified(SubscribePeer::PAIS_ID)) {
            $modifiedColumns[':p' . $index++]  = '`pais_id`';
        }
        if ($this->isColumnModified(SubscribePeer::ESTADO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`estado_id`';
        }
        if ($this->isColumnModified(SubscribePeer::DISTRITO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`distrito_id`';
        }
        if ($this->isColumnModified(SubscribePeer::INSCRICAO_TIPO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_tipo_id`';
        }
        if ($this->isColumnModified(SubscribePeer::DESLOCAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`deslocamento_id`';
        }
        if ($this->isColumnModified(SubscribePeer::LINGUA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`lingua_id`';
        }
        if ($this->isColumnModified(SubscribePeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(SubscribePeer::NOME)) {
            $modifiedColumns[':p' . $index++]  = '`nome`';
        }
        if ($this->isColumnModified(SubscribePeer::DATA_NASCIMENTO)) {
            $modifiedColumns[':p' . $index++]  = '`data_nascimento`';
        }
        if ($this->isColumnModified(SubscribePeer::NOME_RESPONSAVEL)) {
            $modifiedColumns[':p' . $index++]  = '`nome_responsavel`';
        }
        if ($this->isColumnModified(SubscribePeer::FONE_RESPONSAVEL)) {
            $modifiedColumns[':p' . $index++]  = '`fone_responsavel`';
        }
        if ($this->isColumnModified(SubscribePeer::REGISTRO)) {
            $modifiedColumns[':p' . $index++]  = '`registro`';
        }
        if ($this->isColumnModified(SubscribePeer::ORGAO_EXPEDIDOR)) {
            $modifiedColumns[':p' . $index++]  = '`orgao_expedidor`';
        }
        if ($this->isColumnModified(SubscribePeer::ORGAO_EXPEDIDOR_UF)) {
            $modifiedColumns[':p' . $index++]  = '`orgao_expedidor_uf`';
        }
        if ($this->isColumnModified(SubscribePeer::DOCUMENTO)) {
            $modifiedColumns[':p' . $index++]  = '`documento`';
        }
        if ($this->isColumnModified(SubscribePeer::SEXO)) {
            $modifiedColumns[':p' . $index++]  = '`sexo`';
        }
        if ($this->isColumnModified(SubscribePeer::TELEFONE)) {
            $modifiedColumns[':p' . $index++]  = '`telefone`';
        }
        if ($this->isColumnModified(SubscribePeer::ENDERECO)) {
            $modifiedColumns[':p' . $index++]  = '`endereco`';
        }
        if ($this->isColumnModified(SubscribePeer::NUMERO)) {
            $modifiedColumns[':p' . $index++]  = '`numero`';
        }
        if ($this->isColumnModified(SubscribePeer::COMPLEMENTO)) {
            $modifiedColumns[':p' . $index++]  = '`complemento`';
        }
        if ($this->isColumnModified(SubscribePeer::CIDADE)) {
            $modifiedColumns[':p' . $index++]  = '`cidade`';
        }
        if ($this->isColumnModified(SubscribePeer::BAIRRO)) {
            $modifiedColumns[':p' . $index++]  = '`bairro`';
        }
        if ($this->isColumnModified(SubscribePeer::CEP)) {
            $modifiedColumns[':p' . $index++]  = '`cep`';
        }
        if ($this->isColumnModified(SubscribePeer::MEMBRO_IELB)) {
            $modifiedColumns[':p' . $index++]  = '`membro_ielb`';
        }
        if ($this->isColumnModified(SubscribePeer::CONFIRMADO)) {
            $modifiedColumns[':p' . $index++]  = '`confirmado`';
        }
        if ($this->isColumnModified(SubscribePeer::DELEGADO_PLENARIA)) {
            $modifiedColumns[':p' . $index++]  = '`delegado_plenaria`';
        }
        if ($this->isColumnModified(SubscribePeer::PRESIDENTE_UJ)) {
            $modifiedColumns[':p' . $index++]  = '`presidente_uj`';
        }
        if ($this->isColumnModified(SubscribePeer::PASTOR)) {
            $modifiedColumns[':p' . $index++]  = '`pastor`';
        }
        if ($this->isColumnModified(SubscribePeer::UJ)) {
            $modifiedColumns[':p' . $index++]  = '`uj`';
        }
        if ($this->isColumnModified(SubscribePeer::RESPONSAVEL_CARAVANA)) {
            $modifiedColumns[':p' . $index++]  = '`responsavel_caravana`';
        }
        if ($this->isColumnModified(SubscribePeer::PARCELAS)) {
            $modifiedColumns[':p' . $index++]  = '`parcelas`';
        }
        if ($this->isColumnModified(SubscribePeer::VALOR_TOTAL)) {
            $modifiedColumns[':p' . $index++]  = '`valor_total`';
        }
        if ($this->isColumnModified(SubscribePeer::NECESSIDADES_ESPECIAIS)) {
            $modifiedColumns[':p' . $index++]  = '`necessidades_especiais`';
        }
        if ($this->isColumnModified(SubscribePeer::COMENTARIOS)) {
            $modifiedColumns[':p' . $index++]  = '`comentarios`';
        }
        if ($this->isColumnModified(SubscribePeer::PRIMEIRO_CONGRESSO)) {
            $modifiedColumns[':p' . $index++]  = '`primeiro_congresso`';
        }
        if ($this->isColumnModified(SubscribePeer::QUANTIDADE_CONGRESSO)) {
            $modifiedColumns[':p' . $index++]  = '`quantidade_congresso`';
        }
        if ($this->isColumnModified(SubscribePeer::ISENTO)) {
            $modifiedColumns[':p' . $index++]  = '`isento`';
        }
        if ($this->isColumnModified(SubscribePeer::ORGANIZACAO)) {
            $modifiedColumns[':p' . $index++]  = '`organizacao`';
        }
        if ($this->isColumnModified(SubscribePeer::VISITANTE)) {
            $modifiedColumns[':p' . $index++]  = '`visitante`';
        }
        if ($this->isColumnModified(SubscribePeer::CANCELADO)) {
            $modifiedColumns[':p' . $index++]  = '`cancelado`';
        }
        if ($this->isColumnModified(SubscribePeer::DATA_INSCRICAO)) {
            $modifiedColumns[':p' . $index++]  = '`data_inscricao`';
        }

        $sql = sprintf(
            'INSERT INTO `inscricao` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`inscricao_id`':
                        $stmt->bindValue($identifier, $this->inscricao_id, PDO::PARAM_INT);
                        break;
                    case '`usuario_id`':
                        $stmt->bindValue($identifier, $this->usuario_id, PDO::PARAM_INT);
                        break;
                    case '`pais_id`':
                        $stmt->bindValue($identifier, $this->pais_id, PDO::PARAM_INT);
                        break;
                    case '`estado_id`':
                        $stmt->bindValue($identifier, $this->estado_id, PDO::PARAM_INT);
                        break;
                    case '`distrito_id`':
                        $stmt->bindValue($identifier, $this->distrito_id, PDO::PARAM_INT);
                        break;
                    case '`inscricao_tipo_id`':
                        $stmt->bindValue($identifier, $this->inscricao_tipo_id, PDO::PARAM_INT);
                        break;
                    case '`deslocamento_id`':
                        $stmt->bindValue($identifier, $this->deslocamento_id, PDO::PARAM_INT);
                        break;
                    case '`lingua_id`':
                        $stmt->bindValue($identifier, $this->lingua_id, PDO::PARAM_INT);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`nome`':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case '`data_nascimento`':
                        $stmt->bindValue($identifier, $this->data_nascimento, PDO::PARAM_STR);
                        break;
                    case '`nome_responsavel`':
                        $stmt->bindValue($identifier, $this->nome_responsavel, PDO::PARAM_STR);
                        break;
                    case '`fone_responsavel`':
                        $stmt->bindValue($identifier, $this->fone_responsavel, PDO::PARAM_STR);
                        break;
                    case '`registro`':
                        $stmt->bindValue($identifier, $this->registro, PDO::PARAM_STR);
                        break;
                    case '`orgao_expedidor`':
                        $stmt->bindValue($identifier, $this->orgao_expedidor, PDO::PARAM_STR);
                        break;
                    case '`orgao_expedidor_uf`':
                        $stmt->bindValue($identifier, $this->orgao_expedidor_uf, PDO::PARAM_STR);
                        break;
                    case '`documento`':
                        $stmt->bindValue($identifier, $this->documento, PDO::PARAM_STR);
                        break;
                    case '`sexo`':
                        $stmt->bindValue($identifier, $this->sexo, PDO::PARAM_STR);
                        break;
                    case '`telefone`':
                        $stmt->bindValue($identifier, $this->telefone, PDO::PARAM_STR);
                        break;
                    case '`endereco`':
                        $stmt->bindValue($identifier, $this->endereco, PDO::PARAM_STR);
                        break;
                    case '`numero`':
                        $stmt->bindValue($identifier, $this->numero, PDO::PARAM_STR);
                        break;
                    case '`complemento`':
                        $stmt->bindValue($identifier, $this->complemento, PDO::PARAM_STR);
                        break;
                    case '`cidade`':
                        $stmt->bindValue($identifier, $this->cidade, PDO::PARAM_STR);
                        break;
                    case '`bairro`':
                        $stmt->bindValue($identifier, $this->bairro, PDO::PARAM_STR);
                        break;
                    case '`cep`':
                        $stmt->bindValue($identifier, $this->cep, PDO::PARAM_STR);
                        break;
                    case '`membro_ielb`':
                        $stmt->bindValue($identifier, (int) $this->membro_ielb, PDO::PARAM_INT);
                        break;
                    case '`confirmado`':
                        $stmt->bindValue($identifier, (int) $this->confirmado, PDO::PARAM_INT);
                        break;
                    case '`delegado_plenaria`':
                        $stmt->bindValue($identifier, (int) $this->delegado_plenaria, PDO::PARAM_INT);
                        break;
                    case '`presidente_uj`':
                        $stmt->bindValue($identifier, $this->presidente_uj, PDO::PARAM_STR);
                        break;
                    case '`pastor`':
                        $stmt->bindValue($identifier, $this->pastor, PDO::PARAM_STR);
                        break;
                    case '`uj`':
                        $stmt->bindValue($identifier, $this->uj, PDO::PARAM_STR);
                        break;
                    case '`responsavel_caravana`':
                        $stmt->bindValue($identifier, $this->responsavel_caravana, PDO::PARAM_STR);
                        break;
                    case '`parcelas`':
                        $stmt->bindValue($identifier, $this->parcelas, PDO::PARAM_INT);
                        break;
                    case '`valor_total`':
                        $stmt->bindValue($identifier, $this->valor_total, PDO::PARAM_STR);
                        break;
                    case '`necessidades_especiais`':
                        $stmt->bindValue($identifier, $this->necessidades_especiais, PDO::PARAM_STR);
                        break;
                    case '`comentarios`':
                        $stmt->bindValue($identifier, $this->comentarios, PDO::PARAM_STR);
                        break;
                    case '`primeiro_congresso`':
                        $stmt->bindValue($identifier, (int) $this->primeiro_congresso, PDO::PARAM_INT);
                        break;
                    case '`quantidade_congresso`':
                        $stmt->bindValue($identifier, $this->quantidade_congresso, PDO::PARAM_INT);
                        break;
                    case '`isento`':
                        $stmt->bindValue($identifier, (int) $this->isento, PDO::PARAM_INT);
                        break;
                    case '`organizacao`':
                        $stmt->bindValue($identifier, (int) $this->organizacao, PDO::PARAM_INT);
                        break;
                    case '`visitante`':
                        $stmt->bindValue($identifier, (int) $this->visitante, PDO::PARAM_INT);
                        break;
                    case '`cancelado`':
                        $stmt->bindValue($identifier, (int) $this->cancelado, PDO::PARAM_INT);
                        break;
                    case '`data_inscricao`':
                        $stmt->bindValue($identifier, $this->data_inscricao, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDistrict !== null) {
                if (!$this->aDistrict->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aDistrict->getValidationFailures());
                }
            }

            if ($this->aSubscribeType !== null) {
                if (!$this->aSubscribeType->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSubscribeType->getValidationFailures());
                }
            }

            if ($this->aLanguage !== null) {
                if (!$this->aLanguage->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aLanguage->getValidationFailures());
                }
            }

            if ($this->aUser !== null) {
                if (!$this->aUser->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aUser->getValidationFailures());
                }
            }

            if ($this->aDisplacement !== null) {
                if (!$this->aDisplacement->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aDisplacement->getValidationFailures());
                }
            }

            if ($this->aState !== null) {
                if (!$this->aState->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aState->getValidationFailures());
                }
            }

            if ($this->aCountry !== null) {
                if (!$this->aCountry->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
                }
            }


            if (($retval = SubscribePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collTrains !== null) {
                    foreach ($this->collTrains as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDonations !== null) {
                    foreach ($this->collDonations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSubscribePayments !== null) {
                    foreach ($this->collSubscribePayments as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collWorkshopSubscribes !== null) {
                    foreach ($this->collWorkshopSubscribes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->singleTransfer !== null) {
                    if (!$this->singleTransfer->validate($columns)) {
                        $failureMap = array_merge($failureMap, $this->singleTransfer->getValidationFailures());
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SubscribePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUserId();
                break;
            case 2:
                return $this->getCountryId();
                break;
            case 3:
                return $this->getStateId();
                break;
            case 4:
                return $this->getDistrictId();
                break;
            case 5:
                return $this->getSubscribeTypeId();
                break;
            case 6:
                return $this->getDisplacementId();
                break;
            case 7:
                return $this->getLanguageId();
                break;
            case 8:
                return $this->getEmail();
                break;
            case 9:
                return $this->getName();
                break;
            case 10:
                return $this->getBirthday();
                break;
            case 11:
                return $this->getResponsibleName();
                break;
            case 12:
                return $this->getResponsiblePhone();
                break;
            case 13:
                return $this->getRegister();
                break;
            case 14:
                return $this->getDispatcher();
                break;
            case 15:
                return $this->getDispatcherPlace();
                break;
            case 16:
                return $this->getDocument();
                break;
            case 17:
                return $this->getGenre();
                break;
            case 18:
                return $this->getPhone();
                break;
            case 19:
                return $this->getAddress();
                break;
            case 20:
                return $this->getNumber();
                break;
            case 21:
                return $this->getComplement();
                break;
            case 22:
                return $this->getCity();
                break;
            case 23:
                return $this->getRegion();
                break;
            case 24:
                return $this->getZipCode();
                break;
            case 25:
                return $this->getMember();
                break;
            case 26:
                return $this->getConfirmed();
                break;
            case 27:
                return $this->getDelegate();
                break;
            case 28:
                return $this->getUnityPresident();
                break;
            case 29:
                return $this->getMinister();
                break;
            case 30:
                return $this->getUnityName();
                break;
            case 31:
                return $this->getTrainResponsible();
                break;
            case 32:
                return $this->getQuota();
                break;
            case 33:
                return $this->getPrice();
                break;
            case 34:
                return $this->getSpecialNeed();
                break;
            case 35:
                return $this->getComment();
                break;
            case 36:
                return $this->getFirstCongress();
                break;
            case 37:
                return $this->getAmountCongress();
                break;
            case 38:
                return $this->getFree();
                break;
            case 39:
                return $this->getOrganization();
                break;
            case 40:
                return $this->getVisitor();
                break;
            case 41:
                return $this->getCanceled();
                break;
            case 42:
                return $this->getDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Subscribe'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Subscribe'][$this->getPrimaryKey()] = true;
        $keys = SubscribePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUserId(),
            $keys[2] => $this->getCountryId(),
            $keys[3] => $this->getStateId(),
            $keys[4] => $this->getDistrictId(),
            $keys[5] => $this->getSubscribeTypeId(),
            $keys[6] => $this->getDisplacementId(),
            $keys[7] => $this->getLanguageId(),
            $keys[8] => $this->getEmail(),
            $keys[9] => $this->getName(),
            $keys[10] => $this->getBirthday(),
            $keys[11] => $this->getResponsibleName(),
            $keys[12] => $this->getResponsiblePhone(),
            $keys[13] => $this->getRegister(),
            $keys[14] => $this->getDispatcher(),
            $keys[15] => $this->getDispatcherPlace(),
            $keys[16] => $this->getDocument(),
            $keys[17] => $this->getGenre(),
            $keys[18] => $this->getPhone(),
            $keys[19] => $this->getAddress(),
            $keys[20] => $this->getNumber(),
            $keys[21] => $this->getComplement(),
            $keys[22] => $this->getCity(),
            $keys[23] => $this->getRegion(),
            $keys[24] => $this->getZipCode(),
            $keys[25] => $this->getMember(),
            $keys[26] => $this->getConfirmed(),
            $keys[27] => $this->getDelegate(),
            $keys[28] => $this->getUnityPresident(),
            $keys[29] => $this->getMinister(),
            $keys[30] => $this->getUnityName(),
            $keys[31] => $this->getTrainResponsible(),
            $keys[32] => $this->getQuota(),
            $keys[33] => $this->getPrice(),
            $keys[34] => $this->getSpecialNeed(),
            $keys[35] => $this->getComment(),
            $keys[36] => $this->getFirstCongress(),
            $keys[37] => $this->getAmountCongress(),
            $keys[38] => $this->getFree(),
            $keys[39] => $this->getOrganization(),
            $keys[40] => $this->getVisitor(),
            $keys[41] => $this->getCanceled(),
            $keys[42] => $this->getDate(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aDistrict) {
                $result['District'] = $this->aDistrict->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSubscribeType) {
                $result['SubscribeType'] = $this->aSubscribeType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLanguage) {
                $result['Language'] = $this->aLanguage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {
                $result['User'] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aDisplacement) {
                $result['Displacement'] = $this->aDisplacement->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aState) {
                $result['State'] = $this->aState->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCountry) {
                $result['Country'] = $this->aCountry->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collTrains) {
                $result['Trains'] = $this->collTrains->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDonations) {
                $result['Donations'] = $this->collDonations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSubscribePayments) {
                $result['SubscribePayments'] = $this->collSubscribePayments->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWorkshopSubscribes) {
                $result['WorkshopSubscribes'] = $this->collWorkshopSubscribes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->singleTransfer) {
                $result['Transfer'] = $this->singleTransfer->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SubscribePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUserId($value);
                break;
            case 2:
                $this->setCountryId($value);
                break;
            case 3:
                $this->setStateId($value);
                break;
            case 4:
                $this->setDistrictId($value);
                break;
            case 5:
                $this->setSubscribeTypeId($value);
                break;
            case 6:
                $this->setDisplacementId($value);
                break;
            case 7:
                $this->setLanguageId($value);
                break;
            case 8:
                $this->setEmail($value);
                break;
            case 9:
                $this->setName($value);
                break;
            case 10:
                $this->setBirthday($value);
                break;
            case 11:
                $this->setResponsibleName($value);
                break;
            case 12:
                $this->setResponsiblePhone($value);
                break;
            case 13:
                $this->setRegister($value);
                break;
            case 14:
                $this->setDispatcher($value);
                break;
            case 15:
                $this->setDispatcherPlace($value);
                break;
            case 16:
                $this->setDocument($value);
                break;
            case 17:
                $this->setGenre($value);
                break;
            case 18:
                $this->setPhone($value);
                break;
            case 19:
                $this->setAddress($value);
                break;
            case 20:
                $this->setNumber($value);
                break;
            case 21:
                $this->setComplement($value);
                break;
            case 22:
                $this->setCity($value);
                break;
            case 23:
                $this->setRegion($value);
                break;
            case 24:
                $this->setZipCode($value);
                break;
            case 25:
                $this->setMember($value);
                break;
            case 26:
                $this->setConfirmed($value);
                break;
            case 27:
                $this->setDelegate($value);
                break;
            case 28:
                $this->setUnityPresident($value);
                break;
            case 29:
                $this->setMinister($value);
                break;
            case 30:
                $this->setUnityName($value);
                break;
            case 31:
                $this->setTrainResponsible($value);
                break;
            case 32:
                $this->setQuota($value);
                break;
            case 33:
                $this->setPrice($value);
                break;
            case 34:
                $this->setSpecialNeed($value);
                break;
            case 35:
                $this->setComment($value);
                break;
            case 36:
                $this->setFirstCongress($value);
                break;
            case 37:
                $this->setAmountCongress($value);
                break;
            case 38:
                $this->setFree($value);
                break;
            case 39:
                $this->setOrganization($value);
                break;
            case 40:
                $this->setVisitor($value);
                break;
            case 41:
                $this->setCanceled($value);
                break;
            case 42:
                $this->setDate($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SubscribePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setUserId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCountryId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setStateId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDistrictId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSubscribeTypeId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setDisplacementId($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLanguageId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setEmail($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setName($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setBirthday($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setResponsibleName($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setResponsiblePhone($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setRegister($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setDispatcher($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setDispatcherPlace($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setDocument($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setGenre($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setPhone($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setAddress($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setNumber($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setComplement($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setCity($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setRegion($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setZipCode($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setMember($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setConfirmed($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setDelegate($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setUnityPresident($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setMinister($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setUnityName($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setTrainResponsible($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setQuota($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setPrice($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setSpecialNeed($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setComment($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setFirstCongress($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setAmountCongress($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setFree($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setOrganization($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setVisitor($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setCanceled($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setDate($arr[$keys[42]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SubscribePeer::DATABASE_NAME);

        if ($this->isColumnModified(SubscribePeer::INSCRICAO_ID)) $criteria->add(SubscribePeer::INSCRICAO_ID, $this->inscricao_id);
        if ($this->isColumnModified(SubscribePeer::USUARIO_ID)) $criteria->add(SubscribePeer::USUARIO_ID, $this->usuario_id);
        if ($this->isColumnModified(SubscribePeer::PAIS_ID)) $criteria->add(SubscribePeer::PAIS_ID, $this->pais_id);
        if ($this->isColumnModified(SubscribePeer::ESTADO_ID)) $criteria->add(SubscribePeer::ESTADO_ID, $this->estado_id);
        if ($this->isColumnModified(SubscribePeer::DISTRITO_ID)) $criteria->add(SubscribePeer::DISTRITO_ID, $this->distrito_id);
        if ($this->isColumnModified(SubscribePeer::INSCRICAO_TIPO_ID)) $criteria->add(SubscribePeer::INSCRICAO_TIPO_ID, $this->inscricao_tipo_id);
        if ($this->isColumnModified(SubscribePeer::DESLOCAMENTO_ID)) $criteria->add(SubscribePeer::DESLOCAMENTO_ID, $this->deslocamento_id);
        if ($this->isColumnModified(SubscribePeer::LINGUA_ID)) $criteria->add(SubscribePeer::LINGUA_ID, $this->lingua_id);
        if ($this->isColumnModified(SubscribePeer::EMAIL)) $criteria->add(SubscribePeer::EMAIL, $this->email);
        if ($this->isColumnModified(SubscribePeer::NOME)) $criteria->add(SubscribePeer::NOME, $this->nome);
        if ($this->isColumnModified(SubscribePeer::DATA_NASCIMENTO)) $criteria->add(SubscribePeer::DATA_NASCIMENTO, $this->data_nascimento);
        if ($this->isColumnModified(SubscribePeer::NOME_RESPONSAVEL)) $criteria->add(SubscribePeer::NOME_RESPONSAVEL, $this->nome_responsavel);
        if ($this->isColumnModified(SubscribePeer::FONE_RESPONSAVEL)) $criteria->add(SubscribePeer::FONE_RESPONSAVEL, $this->fone_responsavel);
        if ($this->isColumnModified(SubscribePeer::REGISTRO)) $criteria->add(SubscribePeer::REGISTRO, $this->registro);
        if ($this->isColumnModified(SubscribePeer::ORGAO_EXPEDIDOR)) $criteria->add(SubscribePeer::ORGAO_EXPEDIDOR, $this->orgao_expedidor);
        if ($this->isColumnModified(SubscribePeer::ORGAO_EXPEDIDOR_UF)) $criteria->add(SubscribePeer::ORGAO_EXPEDIDOR_UF, $this->orgao_expedidor_uf);
        if ($this->isColumnModified(SubscribePeer::DOCUMENTO)) $criteria->add(SubscribePeer::DOCUMENTO, $this->documento);
        if ($this->isColumnModified(SubscribePeer::SEXO)) $criteria->add(SubscribePeer::SEXO, $this->sexo);
        if ($this->isColumnModified(SubscribePeer::TELEFONE)) $criteria->add(SubscribePeer::TELEFONE, $this->telefone);
        if ($this->isColumnModified(SubscribePeer::ENDERECO)) $criteria->add(SubscribePeer::ENDERECO, $this->endereco);
        if ($this->isColumnModified(SubscribePeer::NUMERO)) $criteria->add(SubscribePeer::NUMERO, $this->numero);
        if ($this->isColumnModified(SubscribePeer::COMPLEMENTO)) $criteria->add(SubscribePeer::COMPLEMENTO, $this->complemento);
        if ($this->isColumnModified(SubscribePeer::CIDADE)) $criteria->add(SubscribePeer::CIDADE, $this->cidade);
        if ($this->isColumnModified(SubscribePeer::BAIRRO)) $criteria->add(SubscribePeer::BAIRRO, $this->bairro);
        if ($this->isColumnModified(SubscribePeer::CEP)) $criteria->add(SubscribePeer::CEP, $this->cep);
        if ($this->isColumnModified(SubscribePeer::MEMBRO_IELB)) $criteria->add(SubscribePeer::MEMBRO_IELB, $this->membro_ielb);
        if ($this->isColumnModified(SubscribePeer::CONFIRMADO)) $criteria->add(SubscribePeer::CONFIRMADO, $this->confirmado);
        if ($this->isColumnModified(SubscribePeer::DELEGADO_PLENARIA)) $criteria->add(SubscribePeer::DELEGADO_PLENARIA, $this->delegado_plenaria);
        if ($this->isColumnModified(SubscribePeer::PRESIDENTE_UJ)) $criteria->add(SubscribePeer::PRESIDENTE_UJ, $this->presidente_uj);
        if ($this->isColumnModified(SubscribePeer::PASTOR)) $criteria->add(SubscribePeer::PASTOR, $this->pastor);
        if ($this->isColumnModified(SubscribePeer::UJ)) $criteria->add(SubscribePeer::UJ, $this->uj);
        if ($this->isColumnModified(SubscribePeer::RESPONSAVEL_CARAVANA)) $criteria->add(SubscribePeer::RESPONSAVEL_CARAVANA, $this->responsavel_caravana);
        if ($this->isColumnModified(SubscribePeer::PARCELAS)) $criteria->add(SubscribePeer::PARCELAS, $this->parcelas);
        if ($this->isColumnModified(SubscribePeer::VALOR_TOTAL)) $criteria->add(SubscribePeer::VALOR_TOTAL, $this->valor_total);
        if ($this->isColumnModified(SubscribePeer::NECESSIDADES_ESPECIAIS)) $criteria->add(SubscribePeer::NECESSIDADES_ESPECIAIS, $this->necessidades_especiais);
        if ($this->isColumnModified(SubscribePeer::COMENTARIOS)) $criteria->add(SubscribePeer::COMENTARIOS, $this->comentarios);
        if ($this->isColumnModified(SubscribePeer::PRIMEIRO_CONGRESSO)) $criteria->add(SubscribePeer::PRIMEIRO_CONGRESSO, $this->primeiro_congresso);
        if ($this->isColumnModified(SubscribePeer::QUANTIDADE_CONGRESSO)) $criteria->add(SubscribePeer::QUANTIDADE_CONGRESSO, $this->quantidade_congresso);
        if ($this->isColumnModified(SubscribePeer::ISENTO)) $criteria->add(SubscribePeer::ISENTO, $this->isento);
        if ($this->isColumnModified(SubscribePeer::ORGANIZACAO)) $criteria->add(SubscribePeer::ORGANIZACAO, $this->organizacao);
        if ($this->isColumnModified(SubscribePeer::VISITANTE)) $criteria->add(SubscribePeer::VISITANTE, $this->visitante);
        if ($this->isColumnModified(SubscribePeer::CANCELADO)) $criteria->add(SubscribePeer::CANCELADO, $this->cancelado);
        if ($this->isColumnModified(SubscribePeer::DATA_INSCRICAO)) $criteria->add(SubscribePeer::DATA_INSCRICAO, $this->data_inscricao);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SubscribePeer::DATABASE_NAME);
        $criteria->add(SubscribePeer::INSCRICAO_ID, $this->inscricao_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (inscricao_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Subscribe (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUserId($this->getUserId());
        $copyObj->setCountryId($this->getCountryId());
        $copyObj->setStateId($this->getStateId());
        $copyObj->setDistrictId($this->getDistrictId());
        $copyObj->setSubscribeTypeId($this->getSubscribeTypeId());
        $copyObj->setDisplacementId($this->getDisplacementId());
        $copyObj->setLanguageId($this->getLanguageId());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setName($this->getName());
        $copyObj->setBirthday($this->getBirthday());
        $copyObj->setResponsibleName($this->getResponsibleName());
        $copyObj->setResponsiblePhone($this->getResponsiblePhone());
        $copyObj->setRegister($this->getRegister());
        $copyObj->setDispatcher($this->getDispatcher());
        $copyObj->setDispatcherPlace($this->getDispatcherPlace());
        $copyObj->setDocument($this->getDocument());
        $copyObj->setGenre($this->getGenre());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setNumber($this->getNumber());
        $copyObj->setComplement($this->getComplement());
        $copyObj->setCity($this->getCity());
        $copyObj->setRegion($this->getRegion());
        $copyObj->setZipCode($this->getZipCode());
        $copyObj->setMember($this->getMember());
        $copyObj->setConfirmed($this->getConfirmed());
        $copyObj->setDelegate($this->getDelegate());
        $copyObj->setUnityPresident($this->getUnityPresident());
        $copyObj->setMinister($this->getMinister());
        $copyObj->setUnityName($this->getUnityName());
        $copyObj->setTrainResponsible($this->getTrainResponsible());
        $copyObj->setQuota($this->getQuota());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setSpecialNeed($this->getSpecialNeed());
        $copyObj->setComment($this->getComment());
        $copyObj->setFirstCongress($this->getFirstCongress());
        $copyObj->setAmountCongress($this->getAmountCongress());
        $copyObj->setFree($this->getFree());
        $copyObj->setOrganization($this->getOrganization());
        $copyObj->setVisitor($this->getVisitor());
        $copyObj->setCanceled($this->getCanceled());
        $copyObj->setDate($this->getDate());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getTrains() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTrain($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDonations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDonation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSubscribePayments() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSubscribePayment($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWorkshopSubscribes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWorkshopSubscribe($relObj->copy($deepCopy));
                }
            }

            $relObj = $this->getTransfer();
            if ($relObj) {
                $copyObj->setTransfer($relObj->copy($deepCopy));
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Subscribe Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SubscribePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SubscribePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a District object.
     *
     * @param                  District $v
     * @return Subscribe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDistrict(District $v = null)
    {
        if ($v === null) {
            $this->setDistrictId(NULL);
        } else {
            $this->setDistrictId($v->getId());
        }

        $this->aDistrict = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the District object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribe($this);
        }


        return $this;
    }


    /**
     * Get the associated District object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return District The associated District object.
     * @throws PropelException
     */
    public function getDistrict(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aDistrict === null && ($this->distrito_id !== null) && $doQuery) {
            $this->aDistrict = DistrictQuery::create()->findPk($this->distrito_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDistrict->addSubscribes($this);
             */
        }

        return $this->aDistrict;
    }

    /**
     * Declares an association between this object and a SubscribeType object.
     *
     * @param                  SubscribeType $v
     * @return Subscribe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSubscribeType(SubscribeType $v = null)
    {
        if ($v === null) {
            $this->setSubscribeTypeId(NULL);
        } else {
            $this->setSubscribeTypeId($v->getId());
        }

        $this->aSubscribeType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SubscribeType object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribe($this);
        }


        return $this;
    }


    /**
     * Get the associated SubscribeType object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SubscribeType The associated SubscribeType object.
     * @throws PropelException
     */
    public function getSubscribeType(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSubscribeType === null && ($this->inscricao_tipo_id !== null) && $doQuery) {
            $this->aSubscribeType = SubscribeTypeQuery::create()->findPk($this->inscricao_tipo_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSubscribeType->addSubscribes($this);
             */
        }

        return $this->aSubscribeType;
    }

    /**
     * Declares an association between this object and a Language object.
     *
     * @param                  Language $v
     * @return Subscribe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLanguage(Language $v = null)
    {
        if ($v === null) {
            $this->setLanguageId(NULL);
        } else {
            $this->setLanguageId($v->getId());
        }

        $this->aLanguage = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Language object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribe($this);
        }


        return $this;
    }


    /**
     * Get the associated Language object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Language The associated Language object.
     * @throws PropelException
     */
    public function getLanguage(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aLanguage === null && ($this->lingua_id !== null) && $doQuery) {
            $this->aLanguage = LanguageQuery::create()->findPk($this->lingua_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLanguage->addSubscribes($this);
             */
        }

        return $this->aLanguage;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param                  User $v
     * @return Subscribe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(User $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribe($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUser(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aUser === null && ($this->usuario_id !== null) && $doQuery) {
            $this->aUser = UserQuery::create()->findPk($this->usuario_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addSubscribes($this);
             */
        }

        return $this->aUser;
    }

    /**
     * Declares an association between this object and a Displacement object.
     *
     * @param                  Displacement $v
     * @return Subscribe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDisplacement(Displacement $v = null)
    {
        if ($v === null) {
            $this->setDisplacementId(NULL);
        } else {
            $this->setDisplacementId($v->getId());
        }

        $this->aDisplacement = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Displacement object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribe($this);
        }


        return $this;
    }


    /**
     * Get the associated Displacement object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Displacement The associated Displacement object.
     * @throws PropelException
     */
    public function getDisplacement(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aDisplacement === null && ($this->deslocamento_id !== null) && $doQuery) {
            $this->aDisplacement = DisplacementQuery::create()->findPk($this->deslocamento_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDisplacement->addSubscribes($this);
             */
        }

        return $this->aDisplacement;
    }

    /**
     * Declares an association between this object and a State object.
     *
     * @param                  State $v
     * @return Subscribe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setState(State $v = null)
    {
        if ($v === null) {
            $this->setStateId(NULL);
        } else {
            $this->setStateId($v->getId());
        }

        $this->aState = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the State object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribe($this);
        }


        return $this;
    }


    /**
     * Get the associated State object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return State The associated State object.
     * @throws PropelException
     */
    public function getState(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aState === null && ($this->estado_id !== null) && $doQuery) {
            $this->aState = StateQuery::create()->findPk($this->estado_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aState->addSubscribes($this);
             */
        }

        return $this->aState;
    }

    /**
     * Declares an association between this object and a Country object.
     *
     * @param                  Country $v
     * @return Subscribe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCountry(Country $v = null)
    {
        if ($v === null) {
            $this->setCountryId(NULL);
        } else {
            $this->setCountryId($v->getId());
        }

        $this->aCountry = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Country object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribe($this);
        }


        return $this;
    }


    /**
     * Get the associated Country object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Country The associated Country object.
     * @throws PropelException
     */
    public function getCountry(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCountry === null && ($this->pais_id !== null) && $doQuery) {
            $this->aCountry = CountryQuery::create()->findPk($this->pais_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCountry->addSubscribes($this);
             */
        }

        return $this->aCountry;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Train' == $relationName) {
            $this->initTrains();
        }
        if ('Donation' == $relationName) {
            $this->initDonations();
        }
        if ('SubscribePayment' == $relationName) {
            $this->initSubscribePayments();
        }
        if ('WorkshopSubscribe' == $relationName) {
            $this->initWorkshopSubscribes();
        }
    }

    /**
     * Clears out the collTrains collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Subscribe The current object (for fluent API support)
     * @see        addTrains()
     */
    public function clearTrains()
    {
        $this->collTrains = null; // important to set this to null since that means it is uninitialized
        $this->collTrainsPartial = null;

        return $this;
    }

    /**
     * reset is the collTrains collection loaded partially
     *
     * @return void
     */
    public function resetPartialTrains($v = true)
    {
        $this->collTrainsPartial = $v;
    }

    /**
     * Initializes the collTrains collection.
     *
     * By default this just sets the collTrains collection to an empty array (like clearcollTrains());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTrains($overrideExisting = true)
    {
        if (null !== $this->collTrains && !$overrideExisting) {
            return;
        }
        $this->collTrains = new PropelObjectCollection();
        $this->collTrains->setModel('Train');
    }

    /**
     * Gets an array of Train objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Subscribe is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Train[] List of Train objects
     * @throws PropelException
     */
    public function getTrains($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTrainsPartial && !$this->isNew();
        if (null === $this->collTrains || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTrains) {
                // return empty collection
                $this->initTrains();
            } else {
                $collTrains = TrainQuery::create(null, $criteria)
                    ->filterBySubscribe($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTrainsPartial && count($collTrains)) {
                      $this->initTrains(false);

                      foreach ($collTrains as $obj) {
                        if (false == $this->collTrains->contains($obj)) {
                          $this->collTrains->append($obj);
                        }
                      }

                      $this->collTrainsPartial = true;
                    }

                    $collTrains->getInternalIterator()->rewind();

                    return $collTrains;
                }

                if ($partial && $this->collTrains) {
                    foreach ($this->collTrains as $obj) {
                        if ($obj->isNew()) {
                            $collTrains[] = $obj;
                        }
                    }
                }

                $this->collTrains = $collTrains;
                $this->collTrainsPartial = false;
            }
        }

        return $this->collTrains;
    }

    /**
     * Sets a collection of Train objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $trains A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Subscribe The current object (for fluent API support)
     */
    public function setTrains(PropelCollection $trains, PropelPDO $con = null)
    {
        $trainsToDelete = $this->getTrains(new Criteria(), $con)->diff($trains);


        $this->trainsScheduledForDeletion = $trainsToDelete;

        foreach ($trainsToDelete as $trainRemoved) {
            $trainRemoved->setSubscribe(null);
        }

        $this->collTrains = null;
        foreach ($trains as $train) {
            $this->addTrain($train);
        }

        $this->collTrains = $trains;
        $this->collTrainsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Train objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Train objects.
     * @throws PropelException
     */
    public function countTrains(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTrainsPartial && !$this->isNew();
        if (null === $this->collTrains || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTrains) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTrains());
            }
            $query = TrainQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySubscribe($this)
                ->count($con);
        }

        return count($this->collTrains);
    }

    /**
     * Method called to associate a Train object to this object
     * through the Train foreign key attribute.
     *
     * @param    Train $l Train
     * @return Subscribe The current object (for fluent API support)
     */
    public function addTrain(Train $l)
    {
        if ($this->collTrains === null) {
            $this->initTrains();
            $this->collTrainsPartial = true;
        }

        if (!in_array($l, $this->collTrains->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTrain($l);

            if ($this->trainsScheduledForDeletion and $this->trainsScheduledForDeletion->contains($l)) {
                $this->trainsScheduledForDeletion->remove($this->trainsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Train $train The train object to add.
     */
    protected function doAddTrain($train)
    {
        $this->collTrains[]= $train;
        $train->setSubscribe($this);
    }

    /**
     * @param	Train $train The train object to remove.
     * @return Subscribe The current object (for fluent API support)
     */
    public function removeTrain($train)
    {
        if ($this->getTrains()->contains($train)) {
            $this->collTrains->remove($this->collTrains->search($train));
            if (null === $this->trainsScheduledForDeletion) {
                $this->trainsScheduledForDeletion = clone $this->collTrains;
                $this->trainsScheduledForDeletion->clear();
            }
            $this->trainsScheduledForDeletion[]= clone $train;
            $train->setSubscribe(null);
        }

        return $this;
    }

    /**
     * Clears out the collDonations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Subscribe The current object (for fluent API support)
     * @see        addDonations()
     */
    public function clearDonations()
    {
        $this->collDonations = null; // important to set this to null since that means it is uninitialized
        $this->collDonationsPartial = null;

        return $this;
    }

    /**
     * reset is the collDonations collection loaded partially
     *
     * @return void
     */
    public function resetPartialDonations($v = true)
    {
        $this->collDonationsPartial = $v;
    }

    /**
     * Initializes the collDonations collection.
     *
     * By default this just sets the collDonations collection to an empty array (like clearcollDonations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDonations($overrideExisting = true)
    {
        if (null !== $this->collDonations && !$overrideExisting) {
            return;
        }
        $this->collDonations = new PropelObjectCollection();
        $this->collDonations->setModel('Donation');
    }

    /**
     * Gets an array of Donation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Subscribe is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Donation[] List of Donation objects
     * @throws PropelException
     */
    public function getDonations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDonationsPartial && !$this->isNew();
        if (null === $this->collDonations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDonations) {
                // return empty collection
                $this->initDonations();
            } else {
                $collDonations = DonationQuery::create(null, $criteria)
                    ->filterBySubscribe($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDonationsPartial && count($collDonations)) {
                      $this->initDonations(false);

                      foreach ($collDonations as $obj) {
                        if (false == $this->collDonations->contains($obj)) {
                          $this->collDonations->append($obj);
                        }
                      }

                      $this->collDonationsPartial = true;
                    }

                    $collDonations->getInternalIterator()->rewind();

                    return $collDonations;
                }

                if ($partial && $this->collDonations) {
                    foreach ($this->collDonations as $obj) {
                        if ($obj->isNew()) {
                            $collDonations[] = $obj;
                        }
                    }
                }

                $this->collDonations = $collDonations;
                $this->collDonationsPartial = false;
            }
        }

        return $this->collDonations;
    }

    /**
     * Sets a collection of Donation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $donations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Subscribe The current object (for fluent API support)
     */
    public function setDonations(PropelCollection $donations, PropelPDO $con = null)
    {
        $donationsToDelete = $this->getDonations(new Criteria(), $con)->diff($donations);


        $this->donationsScheduledForDeletion = $donationsToDelete;

        foreach ($donationsToDelete as $donationRemoved) {
            $donationRemoved->setSubscribe(null);
        }

        $this->collDonations = null;
        foreach ($donations as $donation) {
            $this->addDonation($donation);
        }

        $this->collDonations = $donations;
        $this->collDonationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Donation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Donation objects.
     * @throws PropelException
     */
    public function countDonations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDonationsPartial && !$this->isNew();
        if (null === $this->collDonations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDonations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDonations());
            }
            $query = DonationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySubscribe($this)
                ->count($con);
        }

        return count($this->collDonations);
    }

    /**
     * Method called to associate a Donation object to this object
     * through the Donation foreign key attribute.
     *
     * @param    Donation $l Donation
     * @return Subscribe The current object (for fluent API support)
     */
    public function addDonation(Donation $l)
    {
        if ($this->collDonations === null) {
            $this->initDonations();
            $this->collDonationsPartial = true;
        }

        if (!in_array($l, $this->collDonations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDonation($l);

            if ($this->donationsScheduledForDeletion and $this->donationsScheduledForDeletion->contains($l)) {
                $this->donationsScheduledForDeletion->remove($this->donationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Donation $donation The donation object to add.
     */
    protected function doAddDonation($donation)
    {
        $this->collDonations[]= $donation;
        $donation->setSubscribe($this);
    }

    /**
     * @param	Donation $donation The donation object to remove.
     * @return Subscribe The current object (for fluent API support)
     */
    public function removeDonation($donation)
    {
        if ($this->getDonations()->contains($donation)) {
            $this->collDonations->remove($this->collDonations->search($donation));
            if (null === $this->donationsScheduledForDeletion) {
                $this->donationsScheduledForDeletion = clone $this->collDonations;
                $this->donationsScheduledForDeletion->clear();
            }
            $this->donationsScheduledForDeletion[]= $donation;
            $donation->setSubscribe(null);
        }

        return $this;
    }

    /**
     * Clears out the collSubscribePayments collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Subscribe The current object (for fluent API support)
     * @see        addSubscribePayments()
     */
    public function clearSubscribePayments()
    {
        $this->collSubscribePayments = null; // important to set this to null since that means it is uninitialized
        $this->collSubscribePaymentsPartial = null;

        return $this;
    }

    /**
     * reset is the collSubscribePayments collection loaded partially
     *
     * @return void
     */
    public function resetPartialSubscribePayments($v = true)
    {
        $this->collSubscribePaymentsPartial = $v;
    }

    /**
     * Initializes the collSubscribePayments collection.
     *
     * By default this just sets the collSubscribePayments collection to an empty array (like clearcollSubscribePayments());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSubscribePayments($overrideExisting = true)
    {
        if (null !== $this->collSubscribePayments && !$overrideExisting) {
            return;
        }
        $this->collSubscribePayments = new PropelObjectCollection();
        $this->collSubscribePayments->setModel('SubscribePayment');
    }

    /**
     * Gets an array of SubscribePayment objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Subscribe is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SubscribePayment[] List of SubscribePayment objects
     * @throws PropelException
     */
    public function getSubscribePayments($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSubscribePaymentsPartial && !$this->isNew();
        if (null === $this->collSubscribePayments || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSubscribePayments) {
                // return empty collection
                $this->initSubscribePayments();
            } else {
                $collSubscribePayments = SubscribePaymentQuery::create(null, $criteria)
                    ->filterBySubscribe($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSubscribePaymentsPartial && count($collSubscribePayments)) {
                      $this->initSubscribePayments(false);

                      foreach ($collSubscribePayments as $obj) {
                        if (false == $this->collSubscribePayments->contains($obj)) {
                          $this->collSubscribePayments->append($obj);
                        }
                      }

                      $this->collSubscribePaymentsPartial = true;
                    }

                    $collSubscribePayments->getInternalIterator()->rewind();

                    return $collSubscribePayments;
                }

                if ($partial && $this->collSubscribePayments) {
                    foreach ($this->collSubscribePayments as $obj) {
                        if ($obj->isNew()) {
                            $collSubscribePayments[] = $obj;
                        }
                    }
                }

                $this->collSubscribePayments = $collSubscribePayments;
                $this->collSubscribePaymentsPartial = false;
            }
        }

        return $this->collSubscribePayments;
    }

    /**
     * Sets a collection of SubscribePayment objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $subscribePayments A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Subscribe The current object (for fluent API support)
     */
    public function setSubscribePayments(PropelCollection $subscribePayments, PropelPDO $con = null)
    {
        $subscribePaymentsToDelete = $this->getSubscribePayments(new Criteria(), $con)->diff($subscribePayments);


        $this->subscribePaymentsScheduledForDeletion = $subscribePaymentsToDelete;

        foreach ($subscribePaymentsToDelete as $subscribePaymentRemoved) {
            $subscribePaymentRemoved->setSubscribe(null);
        }

        $this->collSubscribePayments = null;
        foreach ($subscribePayments as $subscribePayment) {
            $this->addSubscribePayment($subscribePayment);
        }

        $this->collSubscribePayments = $subscribePayments;
        $this->collSubscribePaymentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SubscribePayment objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SubscribePayment objects.
     * @throws PropelException
     */
    public function countSubscribePayments(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSubscribePaymentsPartial && !$this->isNew();
        if (null === $this->collSubscribePayments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSubscribePayments) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSubscribePayments());
            }
            $query = SubscribePaymentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySubscribe($this)
                ->count($con);
        }

        return count($this->collSubscribePayments);
    }

    /**
     * Method called to associate a SubscribePayment object to this object
     * through the SubscribePayment foreign key attribute.
     *
     * @param    SubscribePayment $l SubscribePayment
     * @return Subscribe The current object (for fluent API support)
     */
    public function addSubscribePayment(SubscribePayment $l)
    {
        if ($this->collSubscribePayments === null) {
            $this->initSubscribePayments();
            $this->collSubscribePaymentsPartial = true;
        }

        if (!in_array($l, $this->collSubscribePayments->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSubscribePayment($l);

            if ($this->subscribePaymentsScheduledForDeletion and $this->subscribePaymentsScheduledForDeletion->contains($l)) {
                $this->subscribePaymentsScheduledForDeletion->remove($this->subscribePaymentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SubscribePayment $subscribePayment The subscribePayment object to add.
     */
    protected function doAddSubscribePayment($subscribePayment)
    {
        $this->collSubscribePayments[]= $subscribePayment;
        $subscribePayment->setSubscribe($this);
    }

    /**
     * @param	SubscribePayment $subscribePayment The subscribePayment object to remove.
     * @return Subscribe The current object (for fluent API support)
     */
    public function removeSubscribePayment($subscribePayment)
    {
        if ($this->getSubscribePayments()->contains($subscribePayment)) {
            $this->collSubscribePayments->remove($this->collSubscribePayments->search($subscribePayment));
            if (null === $this->subscribePaymentsScheduledForDeletion) {
                $this->subscribePaymentsScheduledForDeletion = clone $this->collSubscribePayments;
                $this->subscribePaymentsScheduledForDeletion->clear();
            }
            $this->subscribePaymentsScheduledForDeletion[]= clone $subscribePayment;
            $subscribePayment->setSubscribe(null);
        }

        return $this;
    }

    /**
     * Clears out the collWorkshopSubscribes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Subscribe The current object (for fluent API support)
     * @see        addWorkshopSubscribes()
     */
    public function clearWorkshopSubscribes()
    {
        $this->collWorkshopSubscribes = null; // important to set this to null since that means it is uninitialized
        $this->collWorkshopSubscribesPartial = null;

        return $this;
    }

    /**
     * reset is the collWorkshopSubscribes collection loaded partially
     *
     * @return void
     */
    public function resetPartialWorkshopSubscribes($v = true)
    {
        $this->collWorkshopSubscribesPartial = $v;
    }

    /**
     * Initializes the collWorkshopSubscribes collection.
     *
     * By default this just sets the collWorkshopSubscribes collection to an empty array (like clearcollWorkshopSubscribes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWorkshopSubscribes($overrideExisting = true)
    {
        if (null !== $this->collWorkshopSubscribes && !$overrideExisting) {
            return;
        }
        $this->collWorkshopSubscribes = new PropelObjectCollection();
        $this->collWorkshopSubscribes->setModel('WorkshopSubscribe');
    }

    /**
     * Gets an array of WorkshopSubscribe objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Subscribe is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WorkshopSubscribe[] List of WorkshopSubscribe objects
     * @throws PropelException
     */
    public function getWorkshopSubscribes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWorkshopSubscribesPartial && !$this->isNew();
        if (null === $this->collWorkshopSubscribes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWorkshopSubscribes) {
                // return empty collection
                $this->initWorkshopSubscribes();
            } else {
                $collWorkshopSubscribes = WorkshopSubscribeQuery::create(null, $criteria)
                    ->filterBySubscribe($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWorkshopSubscribesPartial && count($collWorkshopSubscribes)) {
                      $this->initWorkshopSubscribes(false);

                      foreach ($collWorkshopSubscribes as $obj) {
                        if (false == $this->collWorkshopSubscribes->contains($obj)) {
                          $this->collWorkshopSubscribes->append($obj);
                        }
                      }

                      $this->collWorkshopSubscribesPartial = true;
                    }

                    $collWorkshopSubscribes->getInternalIterator()->rewind();

                    return $collWorkshopSubscribes;
                }

                if ($partial && $this->collWorkshopSubscribes) {
                    foreach ($this->collWorkshopSubscribes as $obj) {
                        if ($obj->isNew()) {
                            $collWorkshopSubscribes[] = $obj;
                        }
                    }
                }

                $this->collWorkshopSubscribes = $collWorkshopSubscribes;
                $this->collWorkshopSubscribesPartial = false;
            }
        }

        return $this->collWorkshopSubscribes;
    }

    /**
     * Sets a collection of WorkshopSubscribe objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $workshopSubscribes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Subscribe The current object (for fluent API support)
     */
    public function setWorkshopSubscribes(PropelCollection $workshopSubscribes, PropelPDO $con = null)
    {
        $workshopSubscribesToDelete = $this->getWorkshopSubscribes(new Criteria(), $con)->diff($workshopSubscribes);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->workshopSubscribesScheduledForDeletion = clone $workshopSubscribesToDelete;

        foreach ($workshopSubscribesToDelete as $workshopSubscribeRemoved) {
            $workshopSubscribeRemoved->setSubscribe(null);
        }

        $this->collWorkshopSubscribes = null;
        foreach ($workshopSubscribes as $workshopSubscribe) {
            $this->addWorkshopSubscribe($workshopSubscribe);
        }

        $this->collWorkshopSubscribes = $workshopSubscribes;
        $this->collWorkshopSubscribesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WorkshopSubscribe objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WorkshopSubscribe objects.
     * @throws PropelException
     */
    public function countWorkshopSubscribes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWorkshopSubscribesPartial && !$this->isNew();
        if (null === $this->collWorkshopSubscribes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWorkshopSubscribes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWorkshopSubscribes());
            }
            $query = WorkshopSubscribeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySubscribe($this)
                ->count($con);
        }

        return count($this->collWorkshopSubscribes);
    }

    /**
     * Method called to associate a WorkshopSubscribe object to this object
     * through the WorkshopSubscribe foreign key attribute.
     *
     * @param    WorkshopSubscribe $l WorkshopSubscribe
     * @return Subscribe The current object (for fluent API support)
     */
    public function addWorkshopSubscribe(WorkshopSubscribe $l)
    {
        if ($this->collWorkshopSubscribes === null) {
            $this->initWorkshopSubscribes();
            $this->collWorkshopSubscribesPartial = true;
        }

        if (!in_array($l, $this->collWorkshopSubscribes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWorkshopSubscribe($l);

            if ($this->workshopSubscribesScheduledForDeletion and $this->workshopSubscribesScheduledForDeletion->contains($l)) {
                $this->workshopSubscribesScheduledForDeletion->remove($this->workshopSubscribesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	WorkshopSubscribe $workshopSubscribe The workshopSubscribe object to add.
     */
    protected function doAddWorkshopSubscribe($workshopSubscribe)
    {
        $this->collWorkshopSubscribes[]= $workshopSubscribe;
        $workshopSubscribe->setSubscribe($this);
    }

    /**
     * @param	WorkshopSubscribe $workshopSubscribe The workshopSubscribe object to remove.
     * @return Subscribe The current object (for fluent API support)
     */
    public function removeWorkshopSubscribe($workshopSubscribe)
    {
        if ($this->getWorkshopSubscribes()->contains($workshopSubscribe)) {
            $this->collWorkshopSubscribes->remove($this->collWorkshopSubscribes->search($workshopSubscribe));
            if (null === $this->workshopSubscribesScheduledForDeletion) {
                $this->workshopSubscribesScheduledForDeletion = clone $this->collWorkshopSubscribes;
                $this->workshopSubscribesScheduledForDeletion->clear();
            }
            $this->workshopSubscribesScheduledForDeletion[]= clone $workshopSubscribe;
            $workshopSubscribe->setSubscribe(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Subscribe is new, it will return
     * an empty collection; or if this Subscribe has previously
     * been saved, it will retrieve related WorkshopSubscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Subscribe.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WorkshopSubscribe[] List of WorkshopSubscribe objects
     */
    public function getWorkshopSubscribesJoinWorkshop($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WorkshopSubscribeQuery::create(null, $criteria);
        $query->joinWith('Workshop', $join_behavior);

        return $this->getWorkshopSubscribes($query, $con);
    }

    /**
     * Gets a single Transfer object, which is related to this object by a one-to-one relationship.
     *
     * @param PropelPDO $con optional connection object
     * @return Transfer
     * @throws PropelException
     */
    public function getTransfer(PropelPDO $con = null)
    {

        if ($this->singleTransfer === null && !$this->isNew()) {
            $this->singleTransfer = TransferQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleTransfer;
    }

    /**
     * Sets a single Transfer object as related to this object by a one-to-one relationship.
     *
     * @param                  Transfer $v Transfer
     * @return Subscribe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTransfer(Transfer $v = null)
    {
        $this->singleTransfer = $v;

        // Make sure that that the passed-in Transfer isn't already associated with this object
        if ($v !== null && $v->getSubscribe(null, false) === null) {
            $v->setSubscribe($this);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->inscricao_id = null;
        $this->usuario_id = null;
        $this->pais_id = null;
        $this->estado_id = null;
        $this->distrito_id = null;
        $this->inscricao_tipo_id = null;
        $this->deslocamento_id = null;
        $this->lingua_id = null;
        $this->email = null;
        $this->nome = null;
        $this->data_nascimento = null;
        $this->nome_responsavel = null;
        $this->fone_responsavel = null;
        $this->registro = null;
        $this->orgao_expedidor = null;
        $this->orgao_expedidor_uf = null;
        $this->documento = null;
        $this->sexo = null;
        $this->telefone = null;
        $this->endereco = null;
        $this->numero = null;
        $this->complemento = null;
        $this->cidade = null;
        $this->bairro = null;
        $this->cep = null;
        $this->membro_ielb = null;
        $this->confirmado = null;
        $this->delegado_plenaria = null;
        $this->presidente_uj = null;
        $this->pastor = null;
        $this->uj = null;
        $this->responsavel_caravana = null;
        $this->parcelas = null;
        $this->valor_total = null;
        $this->necessidades_especiais = null;
        $this->comentarios = null;
        $this->primeiro_congresso = null;
        $this->quantidade_congresso = null;
        $this->isento = null;
        $this->organizacao = null;
        $this->visitante = null;
        $this->cancelado = null;
        $this->data_inscricao = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collTrains) {
                foreach ($this->collTrains as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDonations) {
                foreach ($this->collDonations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSubscribePayments) {
                foreach ($this->collSubscribePayments as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWorkshopSubscribes) {
                foreach ($this->collWorkshopSubscribes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->singleTransfer) {
                $this->singleTransfer->clearAllReferences($deep);
            }
            if ($this->aDistrict instanceof Persistent) {
              $this->aDistrict->clearAllReferences($deep);
            }
            if ($this->aSubscribeType instanceof Persistent) {
              $this->aSubscribeType->clearAllReferences($deep);
            }
            if ($this->aLanguage instanceof Persistent) {
              $this->aLanguage->clearAllReferences($deep);
            }
            if ($this->aUser instanceof Persistent) {
              $this->aUser->clearAllReferences($deep);
            }
            if ($this->aDisplacement instanceof Persistent) {
              $this->aDisplacement->clearAllReferences($deep);
            }
            if ($this->aState instanceof Persistent) {
              $this->aState->clearAllReferences($deep);
            }
            if ($this->aCountry instanceof Persistent) {
              $this->aCountry->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collTrains instanceof PropelCollection) {
            $this->collTrains->clearIterator();
        }
        $this->collTrains = null;
        if ($this->collDonations instanceof PropelCollection) {
            $this->collDonations->clearIterator();
        }
        $this->collDonations = null;
        if ($this->collSubscribePayments instanceof PropelCollection) {
            $this->collSubscribePayments->clearIterator();
        }
        $this->collSubscribePayments = null;
        if ($this->collWorkshopSubscribes instanceof PropelCollection) {
            $this->collWorkshopSubscribes->clearIterator();
        }
        $this->collWorkshopSubscribes = null;
        if ($this->singleTransfer instanceof PropelCollection) {
            $this->singleTransfer->clearIterator();
        }
        $this->singleTransfer = null;
        $this->aDistrict = null;
        $this->aSubscribeType = null;
        $this->aLanguage = null;
        $this->aUser = null;
        $this->aDisplacement = null;
        $this->aState = null;
        $this->aCountry = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SubscribePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
