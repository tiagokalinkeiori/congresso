<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use AppBundle\Model\SubscribeType;
use AppBundle\Model\SubscribeTypePrice;
use AppBundle\Model\SubscribeTypePricePeer;
use AppBundle\Model\SubscribeTypePriceQuery;
use AppBundle\Model\SubscribeTypeQuery;

abstract class BaseSubscribeTypePrice extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\SubscribeTypePricePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SubscribeTypePricePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the inscricao_tipo_valor_id field.
     * @var        int
     */
    protected $inscricao_tipo_valor_id;

    /**
     * The value for the inscricao_tipo_id field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $inscricao_tipo_id;

    /**
     * The value for the valor field.
     * @var        string
     */
    protected $valor;

    /**
     * The value for the parcelas field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $parcelas;

    /**
     * The value for the valor_parcelado field.
     * @var        string
     */
    protected $valor_parcelado;

    /**
     * The value for the inicio field.
     * @var        string
     */
    protected $inicio;

    /**
     * The value for the fim field.
     * @var        string
     */
    protected $fim;

    /**
     * @var        SubscribeType
     */
    protected $aSubscribeType;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->inscricao_tipo_id = 0;
        $this->parcelas = 1;
    }

    /**
     * Initializes internal state of BaseSubscribeTypePrice object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [inscricao_tipo_valor_id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->inscricao_tipo_valor_id;
    }

    /**
     * Get the [inscricao_tipo_id] column value.
     *
     * @return int
     */
    public function getSubscribeTypeId()
    {

        return $this->inscricao_tipo_id;
    }

    /**
     * Get the [valor] column value.
     *
     * @return string
     */
    public function getPrice()
    {

        return $this->valor;
    }

    /**
     * Get the [parcelas] column value.
     *
     * @return int
     */
    public function getQuota()
    {

        return $this->parcelas;
    }

    /**
     * Get the [valor_parcelado] column value.
     *
     * @return string
     */
    public function getInstallmentValue()
    {

        return $this->valor_parcelado;
    }

    /**
     * Get the [optionally formatted] temporal [inicio] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStart($format = null)
    {
        if ($this->inicio === null) {
            return null;
        }

        if ($this->inicio === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->inicio);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->inicio, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [fim] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEnd($format = null)
    {
        if ($this->fim === null) {
            return null;
        }

        if ($this->fim === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fim);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fim, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [inscricao_tipo_valor_id] column.
     *
     * @param  int $v new value
     * @return SubscribeTypePrice The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_tipo_valor_id !== $v) {
            $this->inscricao_tipo_valor_id = $v;
            $this->modifiedColumns[] = SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [inscricao_tipo_id] column.
     *
     * @param  int $v new value
     * @return SubscribeTypePrice The current object (for fluent API support)
     */
    public function setSubscribeTypeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_tipo_id !== $v) {
            $this->inscricao_tipo_id = $v;
            $this->modifiedColumns[] = SubscribeTypePricePeer::INSCRICAO_TIPO_ID;
        }

        if ($this->aSubscribeType !== null && $this->aSubscribeType->getId() !== $v) {
            $this->aSubscribeType = null;
        }


        return $this;
    } // setSubscribeTypeId()

    /**
     * Set the value of [valor] column.
     *
     * @param  string $v new value
     * @return SubscribeTypePrice The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[] = SubscribeTypePricePeer::VALOR;
        }


        return $this;
    } // setPrice()

    /**
     * Set the value of [parcelas] column.
     *
     * @param  int $v new value
     * @return SubscribeTypePrice The current object (for fluent API support)
     */
    public function setQuota($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->parcelas !== $v) {
            $this->parcelas = $v;
            $this->modifiedColumns[] = SubscribeTypePricePeer::PARCELAS;
        }


        return $this;
    } // setQuota()

    /**
     * Set the value of [valor_parcelado] column.
     *
     * @param  string $v new value
     * @return SubscribeTypePrice The current object (for fluent API support)
     */
    public function setInstallmentValue($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->valor_parcelado !== $v) {
            $this->valor_parcelado = $v;
            $this->modifiedColumns[] = SubscribeTypePricePeer::VALOR_PARCELADO;
        }


        return $this;
    } // setInstallmentValue()

    /**
     * Sets the value of [inicio] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SubscribeTypePrice The current object (for fluent API support)
     */
    public function setStart($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->inicio !== null || $dt !== null) {
            $currentDateAsString = ($this->inicio !== null && $tmpDt = new DateTime($this->inicio)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->inicio = $newDateAsString;
                $this->modifiedColumns[] = SubscribeTypePricePeer::INICIO;
            }
        } // if either are not null


        return $this;
    } // setStart()

    /**
     * Sets the value of [fim] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SubscribeTypePrice The current object (for fluent API support)
     */
    public function setEnd($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fim !== null || $dt !== null) {
            $currentDateAsString = ($this->fim !== null && $tmpDt = new DateTime($this->fim)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fim = $newDateAsString;
                $this->modifiedColumns[] = SubscribeTypePricePeer::FIM;
            }
        } // if either are not null


        return $this;
    } // setEnd()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->inscricao_tipo_id !== 0) {
                return false;
            }

            if ($this->parcelas !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->inscricao_tipo_valor_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->inscricao_tipo_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->valor = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->parcelas = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->valor_parcelado = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->inicio = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->fim = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 7; // 7 = SubscribeTypePricePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SubscribeTypePrice object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSubscribeType !== null && $this->inscricao_tipo_id !== $this->aSubscribeType->getId()) {
            $this->aSubscribeType = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SubscribeTypePricePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSubscribeType = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SubscribeTypePriceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SubscribeTypePricePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribeType !== null) {
                if ($this->aSubscribeType->isModified() || $this->aSubscribeType->isNew()) {
                    $affectedRows += $this->aSubscribeType->save($con);
                }
                $this->setSubscribeType($this->aSubscribeType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID;
        if (null !== $this->inscricao_tipo_valor_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_tipo_valor_id`';
        }
        if ($this->isColumnModified(SubscribeTypePricePeer::INSCRICAO_TIPO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_tipo_id`';
        }
        if ($this->isColumnModified(SubscribeTypePricePeer::VALOR)) {
            $modifiedColumns[':p' . $index++]  = '`valor`';
        }
        if ($this->isColumnModified(SubscribeTypePricePeer::PARCELAS)) {
            $modifiedColumns[':p' . $index++]  = '`parcelas`';
        }
        if ($this->isColumnModified(SubscribeTypePricePeer::VALOR_PARCELADO)) {
            $modifiedColumns[':p' . $index++]  = '`valor_parcelado`';
        }
        if ($this->isColumnModified(SubscribeTypePricePeer::INICIO)) {
            $modifiedColumns[':p' . $index++]  = '`inicio`';
        }
        if ($this->isColumnModified(SubscribeTypePricePeer::FIM)) {
            $modifiedColumns[':p' . $index++]  = '`fim`';
        }

        $sql = sprintf(
            'INSERT INTO `inscricao_tipo_valor` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`inscricao_tipo_valor_id`':
                        $stmt->bindValue($identifier, $this->inscricao_tipo_valor_id, PDO::PARAM_INT);
                        break;
                    case '`inscricao_tipo_id`':
                        $stmt->bindValue($identifier, $this->inscricao_tipo_id, PDO::PARAM_INT);
                        break;
                    case '`valor`':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                    case '`parcelas`':
                        $stmt->bindValue($identifier, $this->parcelas, PDO::PARAM_INT);
                        break;
                    case '`valor_parcelado`':
                        $stmt->bindValue($identifier, $this->valor_parcelado, PDO::PARAM_STR);
                        break;
                    case '`inicio`':
                        $stmt->bindValue($identifier, $this->inicio, PDO::PARAM_STR);
                        break;
                    case '`fim`':
                        $stmt->bindValue($identifier, $this->fim, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribeType !== null) {
                if (!$this->aSubscribeType->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSubscribeType->getValidationFailures());
                }
            }


            if (($retval = SubscribeTypePricePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SubscribeTypePricePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSubscribeTypeId();
                break;
            case 2:
                return $this->getPrice();
                break;
            case 3:
                return $this->getQuota();
                break;
            case 4:
                return $this->getInstallmentValue();
                break;
            case 5:
                return $this->getStart();
                break;
            case 6:
                return $this->getEnd();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SubscribeTypePrice'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SubscribeTypePrice'][$this->getPrimaryKey()] = true;
        $keys = SubscribeTypePricePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSubscribeTypeId(),
            $keys[2] => $this->getPrice(),
            $keys[3] => $this->getQuota(),
            $keys[4] => $this->getInstallmentValue(),
            $keys[5] => $this->getStart(),
            $keys[6] => $this->getEnd(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSubscribeType) {
                $result['SubscribeType'] = $this->aSubscribeType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SubscribeTypePricePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSubscribeTypeId($value);
                break;
            case 2:
                $this->setPrice($value);
                break;
            case 3:
                $this->setQuota($value);
                break;
            case 4:
                $this->setInstallmentValue($value);
                break;
            case 5:
                $this->setStart($value);
                break;
            case 6:
                $this->setEnd($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SubscribeTypePricePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSubscribeTypeId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPrice($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setQuota($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setInstallmentValue($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setStart($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setEnd($arr[$keys[6]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SubscribeTypePricePeer::DATABASE_NAME);

        if ($this->isColumnModified(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID)) $criteria->add(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $this->inscricao_tipo_valor_id);
        if ($this->isColumnModified(SubscribeTypePricePeer::INSCRICAO_TIPO_ID)) $criteria->add(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, $this->inscricao_tipo_id);
        if ($this->isColumnModified(SubscribeTypePricePeer::VALOR)) $criteria->add(SubscribeTypePricePeer::VALOR, $this->valor);
        if ($this->isColumnModified(SubscribeTypePricePeer::PARCELAS)) $criteria->add(SubscribeTypePricePeer::PARCELAS, $this->parcelas);
        if ($this->isColumnModified(SubscribeTypePricePeer::VALOR_PARCELADO)) $criteria->add(SubscribeTypePricePeer::VALOR_PARCELADO, $this->valor_parcelado);
        if ($this->isColumnModified(SubscribeTypePricePeer::INICIO)) $criteria->add(SubscribeTypePricePeer::INICIO, $this->inicio);
        if ($this->isColumnModified(SubscribeTypePricePeer::FIM)) $criteria->add(SubscribeTypePricePeer::FIM, $this->fim);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SubscribeTypePricePeer::DATABASE_NAME);
        $criteria->add(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $this->inscricao_tipo_valor_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (inscricao_tipo_valor_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SubscribeTypePrice (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSubscribeTypeId($this->getSubscribeTypeId());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setQuota($this->getQuota());
        $copyObj->setInstallmentValue($this->getInstallmentValue());
        $copyObj->setStart($this->getStart());
        $copyObj->setEnd($this->getEnd());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SubscribeTypePrice Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SubscribeTypePricePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SubscribeTypePricePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SubscribeType object.
     *
     * @param                  SubscribeType $v
     * @return SubscribeTypePrice The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSubscribeType(SubscribeType $v = null)
    {
        if ($v === null) {
            $this->setSubscribeTypeId(0);
        } else {
            $this->setSubscribeTypeId($v->getId());
        }

        $this->aSubscribeType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SubscribeType object, it will not be re-added.
        if ($v !== null) {
            $v->addSubscribeTypePrice($this);
        }


        return $this;
    }


    /**
     * Get the associated SubscribeType object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SubscribeType The associated SubscribeType object.
     * @throws PropelException
     */
    public function getSubscribeType(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSubscribeType === null && ($this->inscricao_tipo_id !== null) && $doQuery) {
            $this->aSubscribeType = SubscribeTypeQuery::create()->findPk($this->inscricao_tipo_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSubscribeType->addSubscribeTypePrices($this);
             */
        }

        return $this->aSubscribeType;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->inscricao_tipo_valor_id = null;
        $this->inscricao_tipo_id = null;
        $this->valor = null;
        $this->parcelas = null;
        $this->valor_parcelado = null;
        $this->inicio = null;
        $this->fim = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSubscribeType instanceof Persistent) {
              $this->aSubscribeType->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSubscribeType = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SubscribeTypePricePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
