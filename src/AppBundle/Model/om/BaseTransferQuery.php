<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Subscribe;
use AppBundle\Model\Transfer;
use AppBundle\Model\TransferPeer;
use AppBundle\Model\TransferQuery;

/**
 * @method TransferQuery orderBySubscribeId($order = Criteria::ASC) Order by the inscricao_id column
 * @method TransferQuery orderByPhone($order = Criteria::ASC) Order by the telefone column
 * @method TransferQuery orderByArrival($order = Criteria::ASC) Order by the chegada_por column
 * @method TransferQuery orderByComeBack($order = Criteria::ASC) Order by the volta_por column
 * @method TransferQuery orderByArrivalDate($order = Criteria::ASC) Order by the data_chegada column
 * @method TransferQuery orderByComeBackDate($order = Criteria::ASC) Order by the data_volta column
 * @method TransferQuery orderByArrivalInformation($order = Criteria::ASC) Order by the dados_chegada column
 * @method TransferQuery orderByComeBackInformation($order = Criteria::ASC) Order by the dados_volta column
 *
 * @method TransferQuery groupBySubscribeId() Group by the inscricao_id column
 * @method TransferQuery groupByPhone() Group by the telefone column
 * @method TransferQuery groupByArrival() Group by the chegada_por column
 * @method TransferQuery groupByComeBack() Group by the volta_por column
 * @method TransferQuery groupByArrivalDate() Group by the data_chegada column
 * @method TransferQuery groupByComeBackDate() Group by the data_volta column
 * @method TransferQuery groupByArrivalInformation() Group by the dados_chegada column
 * @method TransferQuery groupByComeBackInformation() Group by the dados_volta column
 *
 * @method TransferQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method TransferQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method TransferQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method TransferQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method TransferQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method TransferQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method Transfer findOne(PropelPDO $con = null) Return the first Transfer matching the query
 * @method Transfer findOneOrCreate(PropelPDO $con = null) Return the first Transfer matching the query, or a new Transfer object populated from the query conditions when no match is found
 *
 * @method Transfer findOneByPhone(string $telefone) Return the first Transfer filtered by the telefone column
 * @method Transfer findOneByArrival(string $chegada_por) Return the first Transfer filtered by the chegada_por column
 * @method Transfer findOneByComeBack(string $volta_por) Return the first Transfer filtered by the volta_por column
 * @method Transfer findOneByArrivalDate(string $data_chegada) Return the first Transfer filtered by the data_chegada column
 * @method Transfer findOneByComeBackDate(string $data_volta) Return the first Transfer filtered by the data_volta column
 * @method Transfer findOneByArrivalInformation(string $dados_chegada) Return the first Transfer filtered by the dados_chegada column
 * @method Transfer findOneByComeBackInformation(string $dados_volta) Return the first Transfer filtered by the dados_volta column
 *
 * @method array findBySubscribeId(int $inscricao_id) Return Transfer objects filtered by the inscricao_id column
 * @method array findByPhone(string $telefone) Return Transfer objects filtered by the telefone column
 * @method array findByArrival(string $chegada_por) Return Transfer objects filtered by the chegada_por column
 * @method array findByComeBack(string $volta_por) Return Transfer objects filtered by the volta_por column
 * @method array findByArrivalDate(string $data_chegada) Return Transfer objects filtered by the data_chegada column
 * @method array findByComeBackDate(string $data_volta) Return Transfer objects filtered by the data_volta column
 * @method array findByArrivalInformation(string $dados_chegada) Return Transfer objects filtered by the dados_chegada column
 * @method array findByComeBackInformation(string $dados_volta) Return Transfer objects filtered by the dados_volta column
 */
abstract class BaseTransferQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseTransferQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\Transfer';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new TransferQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   TransferQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return TransferQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof TransferQuery) {
            return $criteria;
        }
        $query = new TransferQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Transfer|Transfer[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TransferPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(TransferPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Transfer A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneBySubscribeId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Transfer A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `inscricao_id`, `telefone`, `chegada_por`, `volta_por`, `data_chegada`, `data_volta`, `dados_chegada`, `dados_volta` FROM `translado` WHERE `inscricao_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Transfer();
            $obj->hydrate($row);
            TransferPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Transfer|Transfer[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Transfer[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TransferPeer::INSCRICAO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TransferPeer::INSCRICAO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the inscricao_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySubscribeId(1234); // WHERE inscricao_id = 1234
     * $query->filterBySubscribeId(array(12, 34)); // WHERE inscricao_id IN (12, 34)
     * $query->filterBySubscribeId(array('min' => 12)); // WHERE inscricao_id >= 12
     * $query->filterBySubscribeId(array('max' => 12)); // WHERE inscricao_id <= 12
     * </code>
     *
     * @see       filterBySubscribe()
     *
     * @param     mixed $subscribeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterBySubscribeId($subscribeId = null, $comparison = null)
    {
        if (is_array($subscribeId)) {
            $useMinMax = false;
            if (isset($subscribeId['min'])) {
                $this->addUsingAlias(TransferPeer::INSCRICAO_ID, $subscribeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subscribeId['max'])) {
                $this->addUsingAlias(TransferPeer::INSCRICAO_ID, $subscribeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransferPeer::INSCRICAO_ID, $subscribeId, $comparison);
    }

    /**
     * Filter the query on the telefone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE telefone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE telefone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TransferPeer::TELEFONE, $phone, $comparison);
    }

    /**
     * Filter the query on the chegada_por column
     *
     * Example usage:
     * <code>
     * $query->filterByArrival('fooValue');   // WHERE chegada_por = 'fooValue'
     * $query->filterByArrival('%fooValue%'); // WHERE chegada_por LIKE '%fooValue%'
     * </code>
     *
     * @param     string $arrival The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByArrival($arrival = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($arrival)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $arrival)) {
                $arrival = str_replace('*', '%', $arrival);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TransferPeer::CHEGADA_POR, $arrival, $comparison);
    }

    /**
     * Filter the query on the volta_por column
     *
     * Example usage:
     * <code>
     * $query->filterByComeBack('fooValue');   // WHERE volta_por = 'fooValue'
     * $query->filterByComeBack('%fooValue%'); // WHERE volta_por LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comeBack The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByComeBack($comeBack = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comeBack)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $comeBack)) {
                $comeBack = str_replace('*', '%', $comeBack);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TransferPeer::VOLTA_POR, $comeBack, $comparison);
    }

    /**
     * Filter the query on the data_chegada column
     *
     * Example usage:
     * <code>
     * $query->filterByArrivalDate('2011-03-14'); // WHERE data_chegada = '2011-03-14'
     * $query->filterByArrivalDate('now'); // WHERE data_chegada = '2011-03-14'
     * $query->filterByArrivalDate(array('max' => 'yesterday')); // WHERE data_chegada < '2011-03-13'
     * </code>
     *
     * @param     mixed $arrivalDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByArrivalDate($arrivalDate = null, $comparison = null)
    {
        if (is_array($arrivalDate)) {
            $useMinMax = false;
            if (isset($arrivalDate['min'])) {
                $this->addUsingAlias(TransferPeer::DATA_CHEGADA, $arrivalDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($arrivalDate['max'])) {
                $this->addUsingAlias(TransferPeer::DATA_CHEGADA, $arrivalDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransferPeer::DATA_CHEGADA, $arrivalDate, $comparison);
    }

    /**
     * Filter the query on the data_volta column
     *
     * Example usage:
     * <code>
     * $query->filterByComeBackDate('2011-03-14'); // WHERE data_volta = '2011-03-14'
     * $query->filterByComeBackDate('now'); // WHERE data_volta = '2011-03-14'
     * $query->filterByComeBackDate(array('max' => 'yesterday')); // WHERE data_volta < '2011-03-13'
     * </code>
     *
     * @param     mixed $comeBackDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByComeBackDate($comeBackDate = null, $comparison = null)
    {
        if (is_array($comeBackDate)) {
            $useMinMax = false;
            if (isset($comeBackDate['min'])) {
                $this->addUsingAlias(TransferPeer::DATA_VOLTA, $comeBackDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comeBackDate['max'])) {
                $this->addUsingAlias(TransferPeer::DATA_VOLTA, $comeBackDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TransferPeer::DATA_VOLTA, $comeBackDate, $comparison);
    }

    /**
     * Filter the query on the dados_chegada column
     *
     * Example usage:
     * <code>
     * $query->filterByArrivalInformation('fooValue');   // WHERE dados_chegada = 'fooValue'
     * $query->filterByArrivalInformation('%fooValue%'); // WHERE dados_chegada LIKE '%fooValue%'
     * </code>
     *
     * @param     string $arrivalInformation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByArrivalInformation($arrivalInformation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($arrivalInformation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $arrivalInformation)) {
                $arrivalInformation = str_replace('*', '%', $arrivalInformation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TransferPeer::DADOS_CHEGADA, $arrivalInformation, $comparison);
    }

    /**
     * Filter the query on the dados_volta column
     *
     * Example usage:
     * <code>
     * $query->filterByComeBackInformation('fooValue');   // WHERE dados_volta = 'fooValue'
     * $query->filterByComeBackInformation('%fooValue%'); // WHERE dados_volta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comeBackInformation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function filterByComeBackInformation($comeBackInformation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comeBackInformation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $comeBackInformation)) {
                $comeBackInformation = str_replace('*', '%', $comeBackInformation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TransferPeer::DADOS_VOLTA, $comeBackInformation, $comparison);
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TransferQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(TransferPeer::INSCRICAO_ID, $subscribe->getId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TransferPeer::INSCRICAO_ID, $subscribe->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Transfer $transfer Object to remove from the list of results
     *
     * @return TransferQuery The current query, for fluid interface
     */
    public function prune($transfer = null)
    {
        if ($transfer) {
            $this->addUsingAlias(TransferPeer::INSCRICAO_ID, $transfer->getSubscribeId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
