<?php

namespace AppBundle\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use AppBundle\Model\CountryPeer;
use AppBundle\Model\DisplacementPeer;
use AppBundle\Model\DistrictPeer;
use AppBundle\Model\DonationPeer;
use AppBundle\Model\LanguagePeer;
use AppBundle\Model\StatePeer;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribePaymentPeer;
use AppBundle\Model\SubscribePeer;
use AppBundle\Model\SubscribeTypePeer;
use AppBundle\Model\TrainPeer;
use AppBundle\Model\TransferPeer;
use AppBundle\Model\UserPeer;
use AppBundle\Model\WorkshopSubscribePeer;
use AppBundle\Model\map\SubscribeTableMap;

abstract class BaseSubscribePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'default';

    /** the table name for this class */
    const TABLE_NAME = 'inscricao';

    /** the related Propel class for this table */
    const OM_CLASS = 'AppBundle\\Model\\Subscribe';

    /** the related TableMap class for this table */
    const TM_CLASS = 'AppBundle\\Model\\map\\SubscribeTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 43;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 43;

    /** the column name for the inscricao_id field */
    const INSCRICAO_ID = 'inscricao.inscricao_id';

    /** the column name for the usuario_id field */
    const USUARIO_ID = 'inscricao.usuario_id';

    /** the column name for the pais_id field */
    const PAIS_ID = 'inscricao.pais_id';

    /** the column name for the estado_id field */
    const ESTADO_ID = 'inscricao.estado_id';

    /** the column name for the distrito_id field */
    const DISTRITO_ID = 'inscricao.distrito_id';

    /** the column name for the inscricao_tipo_id field */
    const INSCRICAO_TIPO_ID = 'inscricao.inscricao_tipo_id';

    /** the column name for the deslocamento_id field */
    const DESLOCAMENTO_ID = 'inscricao.deslocamento_id';

    /** the column name for the lingua_id field */
    const LINGUA_ID = 'inscricao.lingua_id';

    /** the column name for the email field */
    const EMAIL = 'inscricao.email';

    /** the column name for the nome field */
    const NOME = 'inscricao.nome';

    /** the column name for the data_nascimento field */
    const DATA_NASCIMENTO = 'inscricao.data_nascimento';

    /** the column name for the nome_responsavel field */
    const NOME_RESPONSAVEL = 'inscricao.nome_responsavel';

    /** the column name for the fone_responsavel field */
    const FONE_RESPONSAVEL = 'inscricao.fone_responsavel';

    /** the column name for the registro field */
    const REGISTRO = 'inscricao.registro';

    /** the column name for the orgao_expedidor field */
    const ORGAO_EXPEDIDOR = 'inscricao.orgao_expedidor';

    /** the column name for the orgao_expedidor_uf field */
    const ORGAO_EXPEDIDOR_UF = 'inscricao.orgao_expedidor_uf';

    /** the column name for the documento field */
    const DOCUMENTO = 'inscricao.documento';

    /** the column name for the sexo field */
    const SEXO = 'inscricao.sexo';

    /** the column name for the telefone field */
    const TELEFONE = 'inscricao.telefone';

    /** the column name for the endereco field */
    const ENDERECO = 'inscricao.endereco';

    /** the column name for the numero field */
    const NUMERO = 'inscricao.numero';

    /** the column name for the complemento field */
    const COMPLEMENTO = 'inscricao.complemento';

    /** the column name for the cidade field */
    const CIDADE = 'inscricao.cidade';

    /** the column name for the bairro field */
    const BAIRRO = 'inscricao.bairro';

    /** the column name for the cep field */
    const CEP = 'inscricao.cep';

    /** the column name for the membro_ielb field */
    const MEMBRO_IELB = 'inscricao.membro_ielb';

    /** the column name for the confirmado field */
    const CONFIRMADO = 'inscricao.confirmado';

    /** the column name for the delegado_plenaria field */
    const DELEGADO_PLENARIA = 'inscricao.delegado_plenaria';

    /** the column name for the presidente_uj field */
    const PRESIDENTE_UJ = 'inscricao.presidente_uj';

    /** the column name for the pastor field */
    const PASTOR = 'inscricao.pastor';

    /** the column name for the uj field */
    const UJ = 'inscricao.uj';

    /** the column name for the responsavel_caravana field */
    const RESPONSAVEL_CARAVANA = 'inscricao.responsavel_caravana';

    /** the column name for the parcelas field */
    const PARCELAS = 'inscricao.parcelas';

    /** the column name for the valor_total field */
    const VALOR_TOTAL = 'inscricao.valor_total';

    /** the column name for the necessidades_especiais field */
    const NECESSIDADES_ESPECIAIS = 'inscricao.necessidades_especiais';

    /** the column name for the comentarios field */
    const COMENTARIOS = 'inscricao.comentarios';

    /** the column name for the primeiro_congresso field */
    const PRIMEIRO_CONGRESSO = 'inscricao.primeiro_congresso';

    /** the column name for the quantidade_congresso field */
    const QUANTIDADE_CONGRESSO = 'inscricao.quantidade_congresso';

    /** the column name for the isento field */
    const ISENTO = 'inscricao.isento';

    /** the column name for the organizacao field */
    const ORGANIZACAO = 'inscricao.organizacao';

    /** the column name for the visitante field */
    const VISITANTE = 'inscricao.visitante';

    /** the column name for the cancelado field */
    const CANCELADO = 'inscricao.cancelado';

    /** the column name for the data_inscricao field */
    const DATA_INSCRICAO = 'inscricao.data_inscricao';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of Subscribe objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Subscribe[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. SubscribePeer::$fieldNames[SubscribePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'UserId', 'CountryId', 'StateId', 'DistrictId', 'SubscribeTypeId', 'DisplacementId', 'LanguageId', 'Email', 'Name', 'Birthday', 'ResponsibleName', 'ResponsiblePhone', 'Register', 'Dispatcher', 'DispatcherPlace', 'Document', 'Genre', 'Phone', 'Address', 'Number', 'Complement', 'City', 'Region', 'ZipCode', 'Member', 'Confirmed', 'Delegate', 'UnityPresident', 'Minister', 'UnityName', 'TrainResponsible', 'Quota', 'Price', 'SpecialNeed', 'Comment', 'FirstCongress', 'AmountCongress', 'Free', 'Organization', 'Visitor', 'Canceled', 'Date', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'userId', 'countryId', 'stateId', 'districtId', 'subscribeTypeId', 'displacementId', 'languageId', 'email', 'name', 'birthday', 'responsibleName', 'responsiblePhone', 'register', 'dispatcher', 'dispatcherPlace', 'document', 'genre', 'phone', 'address', 'number', 'complement', 'city', 'region', 'zipCode', 'member', 'confirmed', 'delegate', 'unityPresident', 'minister', 'unityName', 'trainResponsible', 'quota', 'price', 'specialNeed', 'comment', 'firstCongress', 'amountCongress', 'free', 'organization', 'visitor', 'canceled', 'date', ),
        BasePeer::TYPE_COLNAME => array (SubscribePeer::INSCRICAO_ID, SubscribePeer::USUARIO_ID, SubscribePeer::PAIS_ID, SubscribePeer::ESTADO_ID, SubscribePeer::DISTRITO_ID, SubscribePeer::INSCRICAO_TIPO_ID, SubscribePeer::DESLOCAMENTO_ID, SubscribePeer::LINGUA_ID, SubscribePeer::EMAIL, SubscribePeer::NOME, SubscribePeer::DATA_NASCIMENTO, SubscribePeer::NOME_RESPONSAVEL, SubscribePeer::FONE_RESPONSAVEL, SubscribePeer::REGISTRO, SubscribePeer::ORGAO_EXPEDIDOR, SubscribePeer::ORGAO_EXPEDIDOR_UF, SubscribePeer::DOCUMENTO, SubscribePeer::SEXO, SubscribePeer::TELEFONE, SubscribePeer::ENDERECO, SubscribePeer::NUMERO, SubscribePeer::COMPLEMENTO, SubscribePeer::CIDADE, SubscribePeer::BAIRRO, SubscribePeer::CEP, SubscribePeer::MEMBRO_IELB, SubscribePeer::CONFIRMADO, SubscribePeer::DELEGADO_PLENARIA, SubscribePeer::PRESIDENTE_UJ, SubscribePeer::PASTOR, SubscribePeer::UJ, SubscribePeer::RESPONSAVEL_CARAVANA, SubscribePeer::PARCELAS, SubscribePeer::VALOR_TOTAL, SubscribePeer::NECESSIDADES_ESPECIAIS, SubscribePeer::COMENTARIOS, SubscribePeer::PRIMEIRO_CONGRESSO, SubscribePeer::QUANTIDADE_CONGRESSO, SubscribePeer::ISENTO, SubscribePeer::ORGANIZACAO, SubscribePeer::VISITANTE, SubscribePeer::CANCELADO, SubscribePeer::DATA_INSCRICAO, ),
        BasePeer::TYPE_RAW_COLNAME => array ('INSCRICAO_ID', 'USUARIO_ID', 'PAIS_ID', 'ESTADO_ID', 'DISTRITO_ID', 'INSCRICAO_TIPO_ID', 'DESLOCAMENTO_ID', 'LINGUA_ID', 'EMAIL', 'NOME', 'DATA_NASCIMENTO', 'NOME_RESPONSAVEL', 'FONE_RESPONSAVEL', 'REGISTRO', 'ORGAO_EXPEDIDOR', 'ORGAO_EXPEDIDOR_UF', 'DOCUMENTO', 'SEXO', 'TELEFONE', 'ENDERECO', 'NUMERO', 'COMPLEMENTO', 'CIDADE', 'BAIRRO', 'CEP', 'MEMBRO_IELB', 'CONFIRMADO', 'DELEGADO_PLENARIA', 'PRESIDENTE_UJ', 'PASTOR', 'UJ', 'RESPONSAVEL_CARAVANA', 'PARCELAS', 'VALOR_TOTAL', 'NECESSIDADES_ESPECIAIS', 'COMENTARIOS', 'PRIMEIRO_CONGRESSO', 'QUANTIDADE_CONGRESSO', 'ISENTO', 'ORGANIZACAO', 'VISITANTE', 'CANCELADO', 'DATA_INSCRICAO', ),
        BasePeer::TYPE_FIELDNAME => array ('inscricao_id', 'usuario_id', 'pais_id', 'estado_id', 'distrito_id', 'inscricao_tipo_id', 'deslocamento_id', 'lingua_id', 'email', 'nome', 'data_nascimento', 'nome_responsavel', 'fone_responsavel', 'registro', 'orgao_expedidor', 'orgao_expedidor_uf', 'documento', 'sexo', 'telefone', 'endereco', 'numero', 'complemento', 'cidade', 'bairro', 'cep', 'membro_ielb', 'confirmado', 'delegado_plenaria', 'presidente_uj', 'pastor', 'uj', 'responsavel_caravana', 'parcelas', 'valor_total', 'necessidades_especiais', 'comentarios', 'primeiro_congresso', 'quantidade_congresso', 'isento', 'organizacao', 'visitante', 'cancelado', 'data_inscricao', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. SubscribePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'UserId' => 1, 'CountryId' => 2, 'StateId' => 3, 'DistrictId' => 4, 'SubscribeTypeId' => 5, 'DisplacementId' => 6, 'LanguageId' => 7, 'Email' => 8, 'Name' => 9, 'Birthday' => 10, 'ResponsibleName' => 11, 'ResponsiblePhone' => 12, 'Register' => 13, 'Dispatcher' => 14, 'DispatcherPlace' => 15, 'Document' => 16, 'Genre' => 17, 'Phone' => 18, 'Address' => 19, 'Number' => 20, 'Complement' => 21, 'City' => 22, 'Region' => 23, 'ZipCode' => 24, 'Member' => 25, 'Confirmed' => 26, 'Delegate' => 27, 'UnityPresident' => 28, 'Minister' => 29, 'UnityName' => 30, 'TrainResponsible' => 31, 'Quota' => 32, 'Price' => 33, 'SpecialNeed' => 34, 'Comment' => 35, 'FirstCongress' => 36, 'AmountCongress' => 37, 'Free' => 38, 'Organization' => 39, 'Visitor' => 40, 'Canceled' => 41, 'Date' => 42, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'userId' => 1, 'countryId' => 2, 'stateId' => 3, 'districtId' => 4, 'subscribeTypeId' => 5, 'displacementId' => 6, 'languageId' => 7, 'email' => 8, 'name' => 9, 'birthday' => 10, 'responsibleName' => 11, 'responsiblePhone' => 12, 'register' => 13, 'dispatcher' => 14, 'dispatcherPlace' => 15, 'document' => 16, 'genre' => 17, 'phone' => 18, 'address' => 19, 'number' => 20, 'complement' => 21, 'city' => 22, 'region' => 23, 'zipCode' => 24, 'member' => 25, 'confirmed' => 26, 'delegate' => 27, 'unityPresident' => 28, 'minister' => 29, 'unityName' => 30, 'trainResponsible' => 31, 'quota' => 32, 'price' => 33, 'specialNeed' => 34, 'comment' => 35, 'firstCongress' => 36, 'amountCongress' => 37, 'free' => 38, 'organization' => 39, 'visitor' => 40, 'canceled' => 41, 'date' => 42, ),
        BasePeer::TYPE_COLNAME => array (SubscribePeer::INSCRICAO_ID => 0, SubscribePeer::USUARIO_ID => 1, SubscribePeer::PAIS_ID => 2, SubscribePeer::ESTADO_ID => 3, SubscribePeer::DISTRITO_ID => 4, SubscribePeer::INSCRICAO_TIPO_ID => 5, SubscribePeer::DESLOCAMENTO_ID => 6, SubscribePeer::LINGUA_ID => 7, SubscribePeer::EMAIL => 8, SubscribePeer::NOME => 9, SubscribePeer::DATA_NASCIMENTO => 10, SubscribePeer::NOME_RESPONSAVEL => 11, SubscribePeer::FONE_RESPONSAVEL => 12, SubscribePeer::REGISTRO => 13, SubscribePeer::ORGAO_EXPEDIDOR => 14, SubscribePeer::ORGAO_EXPEDIDOR_UF => 15, SubscribePeer::DOCUMENTO => 16, SubscribePeer::SEXO => 17, SubscribePeer::TELEFONE => 18, SubscribePeer::ENDERECO => 19, SubscribePeer::NUMERO => 20, SubscribePeer::COMPLEMENTO => 21, SubscribePeer::CIDADE => 22, SubscribePeer::BAIRRO => 23, SubscribePeer::CEP => 24, SubscribePeer::MEMBRO_IELB => 25, SubscribePeer::CONFIRMADO => 26, SubscribePeer::DELEGADO_PLENARIA => 27, SubscribePeer::PRESIDENTE_UJ => 28, SubscribePeer::PASTOR => 29, SubscribePeer::UJ => 30, SubscribePeer::RESPONSAVEL_CARAVANA => 31, SubscribePeer::PARCELAS => 32, SubscribePeer::VALOR_TOTAL => 33, SubscribePeer::NECESSIDADES_ESPECIAIS => 34, SubscribePeer::COMENTARIOS => 35, SubscribePeer::PRIMEIRO_CONGRESSO => 36, SubscribePeer::QUANTIDADE_CONGRESSO => 37, SubscribePeer::ISENTO => 38, SubscribePeer::ORGANIZACAO => 39, SubscribePeer::VISITANTE => 40, SubscribePeer::CANCELADO => 41, SubscribePeer::DATA_INSCRICAO => 42, ),
        BasePeer::TYPE_RAW_COLNAME => array ('INSCRICAO_ID' => 0, 'USUARIO_ID' => 1, 'PAIS_ID' => 2, 'ESTADO_ID' => 3, 'DISTRITO_ID' => 4, 'INSCRICAO_TIPO_ID' => 5, 'DESLOCAMENTO_ID' => 6, 'LINGUA_ID' => 7, 'EMAIL' => 8, 'NOME' => 9, 'DATA_NASCIMENTO' => 10, 'NOME_RESPONSAVEL' => 11, 'FONE_RESPONSAVEL' => 12, 'REGISTRO' => 13, 'ORGAO_EXPEDIDOR' => 14, 'ORGAO_EXPEDIDOR_UF' => 15, 'DOCUMENTO' => 16, 'SEXO' => 17, 'TELEFONE' => 18, 'ENDERECO' => 19, 'NUMERO' => 20, 'COMPLEMENTO' => 21, 'CIDADE' => 22, 'BAIRRO' => 23, 'CEP' => 24, 'MEMBRO_IELB' => 25, 'CONFIRMADO' => 26, 'DELEGADO_PLENARIA' => 27, 'PRESIDENTE_UJ' => 28, 'PASTOR' => 29, 'UJ' => 30, 'RESPONSAVEL_CARAVANA' => 31, 'PARCELAS' => 32, 'VALOR_TOTAL' => 33, 'NECESSIDADES_ESPECIAIS' => 34, 'COMENTARIOS' => 35, 'PRIMEIRO_CONGRESSO' => 36, 'QUANTIDADE_CONGRESSO' => 37, 'ISENTO' => 38, 'ORGANIZACAO' => 39, 'VISITANTE' => 40, 'CANCELADO' => 41, 'DATA_INSCRICAO' => 42, ),
        BasePeer::TYPE_FIELDNAME => array ('inscricao_id' => 0, 'usuario_id' => 1, 'pais_id' => 2, 'estado_id' => 3, 'distrito_id' => 4, 'inscricao_tipo_id' => 5, 'deslocamento_id' => 6, 'lingua_id' => 7, 'email' => 8, 'nome' => 9, 'data_nascimento' => 10, 'nome_responsavel' => 11, 'fone_responsavel' => 12, 'registro' => 13, 'orgao_expedidor' => 14, 'orgao_expedidor_uf' => 15, 'documento' => 16, 'sexo' => 17, 'telefone' => 18, 'endereco' => 19, 'numero' => 20, 'complemento' => 21, 'cidade' => 22, 'bairro' => 23, 'cep' => 24, 'membro_ielb' => 25, 'confirmado' => 26, 'delegado_plenaria' => 27, 'presidente_uj' => 28, 'pastor' => 29, 'uj' => 30, 'responsavel_caravana' => 31, 'parcelas' => 32, 'valor_total' => 33, 'necessidades_especiais' => 34, 'comentarios' => 35, 'primeiro_congresso' => 36, 'quantidade_congresso' => 37, 'isento' => 38, 'organizacao' => 39, 'visitante' => 40, 'cancelado' => 41, 'data_inscricao' => 42, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = SubscribePeer::getFieldNames($toType);
        $key = isset(SubscribePeer::$fieldKeys[$fromType][$name]) ? SubscribePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(SubscribePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, SubscribePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return SubscribePeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. SubscribePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(SubscribePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SubscribePeer::INSCRICAO_ID);
            $criteria->addSelectColumn(SubscribePeer::USUARIO_ID);
            $criteria->addSelectColumn(SubscribePeer::PAIS_ID);
            $criteria->addSelectColumn(SubscribePeer::ESTADO_ID);
            $criteria->addSelectColumn(SubscribePeer::DISTRITO_ID);
            $criteria->addSelectColumn(SubscribePeer::INSCRICAO_TIPO_ID);
            $criteria->addSelectColumn(SubscribePeer::DESLOCAMENTO_ID);
            $criteria->addSelectColumn(SubscribePeer::LINGUA_ID);
            $criteria->addSelectColumn(SubscribePeer::EMAIL);
            $criteria->addSelectColumn(SubscribePeer::NOME);
            $criteria->addSelectColumn(SubscribePeer::DATA_NASCIMENTO);
            $criteria->addSelectColumn(SubscribePeer::NOME_RESPONSAVEL);
            $criteria->addSelectColumn(SubscribePeer::FONE_RESPONSAVEL);
            $criteria->addSelectColumn(SubscribePeer::REGISTRO);
            $criteria->addSelectColumn(SubscribePeer::ORGAO_EXPEDIDOR);
            $criteria->addSelectColumn(SubscribePeer::ORGAO_EXPEDIDOR_UF);
            $criteria->addSelectColumn(SubscribePeer::DOCUMENTO);
            $criteria->addSelectColumn(SubscribePeer::SEXO);
            $criteria->addSelectColumn(SubscribePeer::TELEFONE);
            $criteria->addSelectColumn(SubscribePeer::ENDERECO);
            $criteria->addSelectColumn(SubscribePeer::NUMERO);
            $criteria->addSelectColumn(SubscribePeer::COMPLEMENTO);
            $criteria->addSelectColumn(SubscribePeer::CIDADE);
            $criteria->addSelectColumn(SubscribePeer::BAIRRO);
            $criteria->addSelectColumn(SubscribePeer::CEP);
            $criteria->addSelectColumn(SubscribePeer::MEMBRO_IELB);
            $criteria->addSelectColumn(SubscribePeer::CONFIRMADO);
            $criteria->addSelectColumn(SubscribePeer::DELEGADO_PLENARIA);
            $criteria->addSelectColumn(SubscribePeer::PRESIDENTE_UJ);
            $criteria->addSelectColumn(SubscribePeer::PASTOR);
            $criteria->addSelectColumn(SubscribePeer::UJ);
            $criteria->addSelectColumn(SubscribePeer::RESPONSAVEL_CARAVANA);
            $criteria->addSelectColumn(SubscribePeer::PARCELAS);
            $criteria->addSelectColumn(SubscribePeer::VALOR_TOTAL);
            $criteria->addSelectColumn(SubscribePeer::NECESSIDADES_ESPECIAIS);
            $criteria->addSelectColumn(SubscribePeer::COMENTARIOS);
            $criteria->addSelectColumn(SubscribePeer::PRIMEIRO_CONGRESSO);
            $criteria->addSelectColumn(SubscribePeer::QUANTIDADE_CONGRESSO);
            $criteria->addSelectColumn(SubscribePeer::ISENTO);
            $criteria->addSelectColumn(SubscribePeer::ORGANIZACAO);
            $criteria->addSelectColumn(SubscribePeer::VISITANTE);
            $criteria->addSelectColumn(SubscribePeer::CANCELADO);
            $criteria->addSelectColumn(SubscribePeer::DATA_INSCRICAO);
        } else {
            $criteria->addSelectColumn($alias . '.inscricao_id');
            $criteria->addSelectColumn($alias . '.usuario_id');
            $criteria->addSelectColumn($alias . '.pais_id');
            $criteria->addSelectColumn($alias . '.estado_id');
            $criteria->addSelectColumn($alias . '.distrito_id');
            $criteria->addSelectColumn($alias . '.inscricao_tipo_id');
            $criteria->addSelectColumn($alias . '.deslocamento_id');
            $criteria->addSelectColumn($alias . '.lingua_id');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.nome');
            $criteria->addSelectColumn($alias . '.data_nascimento');
            $criteria->addSelectColumn($alias . '.nome_responsavel');
            $criteria->addSelectColumn($alias . '.fone_responsavel');
            $criteria->addSelectColumn($alias . '.registro');
            $criteria->addSelectColumn($alias . '.orgao_expedidor');
            $criteria->addSelectColumn($alias . '.orgao_expedidor_uf');
            $criteria->addSelectColumn($alias . '.documento');
            $criteria->addSelectColumn($alias . '.sexo');
            $criteria->addSelectColumn($alias . '.telefone');
            $criteria->addSelectColumn($alias . '.endereco');
            $criteria->addSelectColumn($alias . '.numero');
            $criteria->addSelectColumn($alias . '.complemento');
            $criteria->addSelectColumn($alias . '.cidade');
            $criteria->addSelectColumn($alias . '.bairro');
            $criteria->addSelectColumn($alias . '.cep');
            $criteria->addSelectColumn($alias . '.membro_ielb');
            $criteria->addSelectColumn($alias . '.confirmado');
            $criteria->addSelectColumn($alias . '.delegado_plenaria');
            $criteria->addSelectColumn($alias . '.presidente_uj');
            $criteria->addSelectColumn($alias . '.pastor');
            $criteria->addSelectColumn($alias . '.uj');
            $criteria->addSelectColumn($alias . '.responsavel_caravana');
            $criteria->addSelectColumn($alias . '.parcelas');
            $criteria->addSelectColumn($alias . '.valor_total');
            $criteria->addSelectColumn($alias . '.necessidades_especiais');
            $criteria->addSelectColumn($alias . '.comentarios');
            $criteria->addSelectColumn($alias . '.primeiro_congresso');
            $criteria->addSelectColumn($alias . '.quantidade_congresso');
            $criteria->addSelectColumn($alias . '.isento');
            $criteria->addSelectColumn($alias . '.organizacao');
            $criteria->addSelectColumn($alias . '.visitante');
            $criteria->addSelectColumn($alias . '.cancelado');
            $criteria->addSelectColumn($alias . '.data_inscricao');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(SubscribePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return Subscribe
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = SubscribePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return SubscribePeer::populateObjects(SubscribePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            SubscribePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param Subscribe $obj A Subscribe object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            SubscribePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Subscribe object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Subscribe) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Subscribe object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(SubscribePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return Subscribe Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(SubscribePeer::$instances[$key])) {
                return SubscribePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (SubscribePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        SubscribePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to inscricao
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in TrainPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        TrainPeer::clearInstancePool();
        // Invalidate objects in DonationPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        DonationPeer::clearInstancePool();
        // Invalidate objects in SubscribePaymentPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SubscribePaymentPeer::clearInstancePool();
        // Invalidate objects in WorkshopSubscribePeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        WorkshopSubscribePeer::clearInstancePool();
        // Invalidate objects in TransferPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        TransferPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = SubscribePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = SubscribePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SubscribePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Subscribe object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = SubscribePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = SubscribePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + SubscribePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SubscribePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            SubscribePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related District table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinDistrict(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SubscribeType table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSubscribeType(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Language table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinLanguage(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related User table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinUser(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Displacement table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinDisplacement(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related State table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinState(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Country table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCountry(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with their District objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinDistrict(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol = SubscribePeer::NUM_HYDRATE_COLUMNS;
        DistrictPeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = DistrictPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = DistrictPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = DistrictPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    DistrictPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Subscribe) to $obj2 (District)
                $obj2->addSubscribe($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with their SubscribeType objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSubscribeType(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol = SubscribePeer::NUM_HYDRATE_COLUMNS;
        SubscribeTypePeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SubscribeTypePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SubscribeTypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SubscribeTypePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Subscribe) to $obj2 (SubscribeType)
                $obj2->addSubscribe($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with their Language objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinLanguage(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol = SubscribePeer::NUM_HYDRATE_COLUMNS;
        LanguagePeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = LanguagePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = LanguagePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = LanguagePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    LanguagePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Subscribe) to $obj2 (Language)
                $obj2->addSubscribe($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with their User objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinUser(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol = SubscribePeer::NUM_HYDRATE_COLUMNS;
        UserPeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = UserPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = UserPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    UserPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Subscribe) to $obj2 (User)
                $obj2->addSubscribe($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with their Displacement objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinDisplacement(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol = SubscribePeer::NUM_HYDRATE_COLUMNS;
        DisplacementPeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = DisplacementPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = DisplacementPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = DisplacementPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    DisplacementPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Subscribe) to $obj2 (Displacement)
                $obj2->addSubscribe($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with their State objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinState(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol = SubscribePeer::NUM_HYDRATE_COLUMNS;
        StatePeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = StatePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = StatePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = StatePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    StatePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Subscribe) to $obj2 (State)
                $obj2->addSubscribe($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with their Country objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCountry(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol = SubscribePeer::NUM_HYDRATE_COLUMNS;
        CountryPeer::addSelectColumns($criteria);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CountryPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CountryPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CountryPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CountryPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Subscribe) to $obj2 (Country)
                $obj2->addSubscribe($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of Subscribe objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol2 = SubscribePeer::NUM_HYDRATE_COLUMNS;

        DistrictPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DistrictPeer::NUM_HYDRATE_COLUMNS;

        SubscribeTypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SubscribeTypePeer::NUM_HYDRATE_COLUMNS;

        LanguagePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + LanguagePeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + UserPeer::NUM_HYDRATE_COLUMNS;

        DisplacementPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + DisplacementPeer::NUM_HYDRATE_COLUMNS;

        StatePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + StatePeer::NUM_HYDRATE_COLUMNS;

        CountryPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CountryPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined District rows

            $key2 = DistrictPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = DistrictPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = DistrictPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DistrictPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (Subscribe) to the collection in $obj2 (District)
                $obj2->addSubscribe($obj1);
            } // if joined row not null

            // Add objects for joined SubscribeType rows

            $key3 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = SubscribeTypePeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = SubscribeTypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SubscribeTypePeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (Subscribe) to the collection in $obj3 (SubscribeType)
                $obj3->addSubscribe($obj1);
            } // if joined row not null

            // Add objects for joined Language rows

            $key4 = LanguagePeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = LanguagePeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = LanguagePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    LanguagePeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (Subscribe) to the collection in $obj4 (Language)
                $obj4->addSubscribe($obj1);
            } // if joined row not null

            // Add objects for joined User rows

            $key5 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = UserPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = UserPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    UserPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (Subscribe) to the collection in $obj5 (User)
                $obj5->addSubscribe($obj1);
            } // if joined row not null

            // Add objects for joined Displacement rows

            $key6 = DisplacementPeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = DisplacementPeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = DisplacementPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    DisplacementPeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (Subscribe) to the collection in $obj6 (Displacement)
                $obj6->addSubscribe($obj1);
            } // if joined row not null

            // Add objects for joined State rows

            $key7 = StatePeer::getPrimaryKeyHashFromRow($row, $startcol7);
            if ($key7 !== null) {
                $obj7 = StatePeer::getInstanceFromPool($key7);
                if (!$obj7) {

                    $cls = StatePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    StatePeer::addInstanceToPool($obj7, $key7);
                } // if obj7 loaded

                // Add the $obj1 (Subscribe) to the collection in $obj7 (State)
                $obj7->addSubscribe($obj1);
            } // if joined row not null

            // Add objects for joined Country rows

            $key8 = CountryPeer::getPrimaryKeyHashFromRow($row, $startcol8);
            if ($key8 !== null) {
                $obj8 = CountryPeer::getInstanceFromPool($key8);
                if (!$obj8) {

                    $cls = CountryPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CountryPeer::addInstanceToPool($obj8, $key8);
                } // if obj8 loaded

                // Add the $obj1 (Subscribe) to the collection in $obj8 (Country)
                $obj8->addSubscribe($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related District table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptDistrict(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SubscribeType table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSubscribeType(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Language table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptLanguage(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related User table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptUser(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Displacement table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptDisplacement(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related State table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptState(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Country table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCountry(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SubscribePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with all related objects except District.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptDistrict(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol2 = SubscribePeer::NUM_HYDRATE_COLUMNS;

        SubscribeTypePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SubscribeTypePeer::NUM_HYDRATE_COLUMNS;

        LanguagePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + LanguagePeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + UserPeer::NUM_HYDRATE_COLUMNS;

        DisplacementPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + DisplacementPeer::NUM_HYDRATE_COLUMNS;

        StatePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + StatePeer::NUM_HYDRATE_COLUMNS;

        CountryPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CountryPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SubscribeType rows

                $key2 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SubscribeTypePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SubscribeTypePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SubscribeTypePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj2 (SubscribeType)
                $obj2->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Language rows

                $key3 = LanguagePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = LanguagePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = LanguagePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    LanguagePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj3 (Language)
                $obj3->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined User rows

                $key4 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = UserPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = UserPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    UserPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj4 (User)
                $obj4->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Displacement rows

                $key5 = DisplacementPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = DisplacementPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = DisplacementPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    DisplacementPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj5 (Displacement)
                $obj5->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined State rows

                $key6 = StatePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = StatePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = StatePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    StatePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj6 (State)
                $obj6->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Country rows

                $key7 = CountryPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CountryPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CountryPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CountryPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj7 (Country)
                $obj7->addSubscribe($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with all related objects except SubscribeType.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSubscribeType(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol2 = SubscribePeer::NUM_HYDRATE_COLUMNS;

        DistrictPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DistrictPeer::NUM_HYDRATE_COLUMNS;

        LanguagePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + LanguagePeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + UserPeer::NUM_HYDRATE_COLUMNS;

        DisplacementPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + DisplacementPeer::NUM_HYDRATE_COLUMNS;

        StatePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + StatePeer::NUM_HYDRATE_COLUMNS;

        CountryPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CountryPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined District rows

                $key2 = DistrictPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DistrictPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DistrictPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DistrictPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj2 (District)
                $obj2->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Language rows

                $key3 = LanguagePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = LanguagePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = LanguagePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    LanguagePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj3 (Language)
                $obj3->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined User rows

                $key4 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = UserPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = UserPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    UserPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj4 (User)
                $obj4->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Displacement rows

                $key5 = DisplacementPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = DisplacementPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = DisplacementPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    DisplacementPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj5 (Displacement)
                $obj5->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined State rows

                $key6 = StatePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = StatePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = StatePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    StatePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj6 (State)
                $obj6->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Country rows

                $key7 = CountryPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CountryPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CountryPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CountryPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj7 (Country)
                $obj7->addSubscribe($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with all related objects except Language.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptLanguage(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol2 = SubscribePeer::NUM_HYDRATE_COLUMNS;

        DistrictPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DistrictPeer::NUM_HYDRATE_COLUMNS;

        SubscribeTypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SubscribeTypePeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + UserPeer::NUM_HYDRATE_COLUMNS;

        DisplacementPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + DisplacementPeer::NUM_HYDRATE_COLUMNS;

        StatePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + StatePeer::NUM_HYDRATE_COLUMNS;

        CountryPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CountryPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined District rows

                $key2 = DistrictPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DistrictPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DistrictPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DistrictPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj2 (District)
                $obj2->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined SubscribeType rows

                $key3 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SubscribeTypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SubscribeTypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SubscribeTypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj3 (SubscribeType)
                $obj3->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined User rows

                $key4 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = UserPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = UserPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    UserPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj4 (User)
                $obj4->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Displacement rows

                $key5 = DisplacementPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = DisplacementPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = DisplacementPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    DisplacementPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj5 (Displacement)
                $obj5->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined State rows

                $key6 = StatePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = StatePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = StatePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    StatePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj6 (State)
                $obj6->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Country rows

                $key7 = CountryPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CountryPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CountryPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CountryPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj7 (Country)
                $obj7->addSubscribe($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with all related objects except User.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptUser(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol2 = SubscribePeer::NUM_HYDRATE_COLUMNS;

        DistrictPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DistrictPeer::NUM_HYDRATE_COLUMNS;

        SubscribeTypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SubscribeTypePeer::NUM_HYDRATE_COLUMNS;

        LanguagePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + LanguagePeer::NUM_HYDRATE_COLUMNS;

        DisplacementPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + DisplacementPeer::NUM_HYDRATE_COLUMNS;

        StatePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + StatePeer::NUM_HYDRATE_COLUMNS;

        CountryPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CountryPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined District rows

                $key2 = DistrictPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DistrictPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DistrictPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DistrictPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj2 (District)
                $obj2->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined SubscribeType rows

                $key3 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SubscribeTypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SubscribeTypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SubscribeTypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj3 (SubscribeType)
                $obj3->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Language rows

                $key4 = LanguagePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = LanguagePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = LanguagePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    LanguagePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj4 (Language)
                $obj4->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Displacement rows

                $key5 = DisplacementPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = DisplacementPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = DisplacementPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    DisplacementPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj5 (Displacement)
                $obj5->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined State rows

                $key6 = StatePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = StatePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = StatePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    StatePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj6 (State)
                $obj6->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Country rows

                $key7 = CountryPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CountryPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CountryPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CountryPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj7 (Country)
                $obj7->addSubscribe($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with all related objects except Displacement.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptDisplacement(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol2 = SubscribePeer::NUM_HYDRATE_COLUMNS;

        DistrictPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DistrictPeer::NUM_HYDRATE_COLUMNS;

        SubscribeTypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SubscribeTypePeer::NUM_HYDRATE_COLUMNS;

        LanguagePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + LanguagePeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + UserPeer::NUM_HYDRATE_COLUMNS;

        StatePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + StatePeer::NUM_HYDRATE_COLUMNS;

        CountryPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CountryPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined District rows

                $key2 = DistrictPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DistrictPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DistrictPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DistrictPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj2 (District)
                $obj2->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined SubscribeType rows

                $key3 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SubscribeTypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SubscribeTypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SubscribeTypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj3 (SubscribeType)
                $obj3->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Language rows

                $key4 = LanguagePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = LanguagePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = LanguagePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    LanguagePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj4 (Language)
                $obj4->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined User rows

                $key5 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = UserPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = UserPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    UserPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj5 (User)
                $obj5->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined State rows

                $key6 = StatePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = StatePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = StatePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    StatePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj6 (State)
                $obj6->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Country rows

                $key7 = CountryPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CountryPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CountryPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CountryPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj7 (Country)
                $obj7->addSubscribe($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with all related objects except State.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptState(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol2 = SubscribePeer::NUM_HYDRATE_COLUMNS;

        DistrictPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DistrictPeer::NUM_HYDRATE_COLUMNS;

        SubscribeTypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SubscribeTypePeer::NUM_HYDRATE_COLUMNS;

        LanguagePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + LanguagePeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + UserPeer::NUM_HYDRATE_COLUMNS;

        DisplacementPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + DisplacementPeer::NUM_HYDRATE_COLUMNS;

        CountryPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CountryPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::PAIS_ID, CountryPeer::PAIS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined District rows

                $key2 = DistrictPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DistrictPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DistrictPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DistrictPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj2 (District)
                $obj2->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined SubscribeType rows

                $key3 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SubscribeTypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SubscribeTypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SubscribeTypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj3 (SubscribeType)
                $obj3->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Language rows

                $key4 = LanguagePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = LanguagePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = LanguagePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    LanguagePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj4 (Language)
                $obj4->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined User rows

                $key5 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = UserPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = UserPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    UserPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj5 (User)
                $obj5->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Displacement rows

                $key6 = DisplacementPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = DisplacementPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = DisplacementPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    DisplacementPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj6 (Displacement)
                $obj6->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Country rows

                $key7 = CountryPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CountryPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CountryPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CountryPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj7 (Country)
                $obj7->addSubscribe($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Subscribe objects pre-filled with all related objects except Country.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Subscribe objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCountry(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SubscribePeer::DATABASE_NAME);
        }

        SubscribePeer::addSelectColumns($criteria);
        $startcol2 = SubscribePeer::NUM_HYDRATE_COLUMNS;

        DistrictPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DistrictPeer::NUM_HYDRATE_COLUMNS;

        SubscribeTypePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SubscribeTypePeer::NUM_HYDRATE_COLUMNS;

        LanguagePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + LanguagePeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + UserPeer::NUM_HYDRATE_COLUMNS;

        DisplacementPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + DisplacementPeer::NUM_HYDRATE_COLUMNS;

        StatePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + StatePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SubscribePeer::DISTRITO_ID, DistrictPeer::DISTRITO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::INSCRICAO_TIPO_ID, SubscribeTypePeer::INSCRICAO_TIPO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::LINGUA_ID, LanguagePeer::LINGUA_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::USUARIO_ID, UserPeer::USUARIO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::DESLOCAMENTO_ID, DisplacementPeer::DESLOCAMENTO_ID, $join_behavior);

        $criteria->addJoin(SubscribePeer::ESTADO_ID, StatePeer::ESTADO_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SubscribePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SubscribePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SubscribePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SubscribePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined District rows

                $key2 = DistrictPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DistrictPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DistrictPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DistrictPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj2 (District)
                $obj2->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined SubscribeType rows

                $key3 = SubscribeTypePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SubscribeTypePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SubscribeTypePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SubscribeTypePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj3 (SubscribeType)
                $obj3->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Language rows

                $key4 = LanguagePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = LanguagePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = LanguagePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    LanguagePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj4 (Language)
                $obj4->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined User rows

                $key5 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = UserPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = UserPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    UserPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj5 (User)
                $obj5->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined Displacement rows

                $key6 = DisplacementPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = DisplacementPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = DisplacementPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    DisplacementPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj6 (Displacement)
                $obj6->addSubscribe($obj1);

            } // if joined row is not null

                // Add objects for joined State rows

                $key7 = StatePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = StatePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = StatePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    StatePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (Subscribe) to the collection in $obj7 (State)
                $obj7->addSubscribe($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(SubscribePeer::DATABASE_NAME)->getTable(SubscribePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseSubscribePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseSubscribePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \AppBundle\Model\map\SubscribeTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return SubscribePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Subscribe or Criteria object.
     *
     * @param      mixed $values Criteria or Subscribe object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Subscribe object
        }

        if ($criteria->containsKey(SubscribePeer::INSCRICAO_ID) && $criteria->keyContainsValue(SubscribePeer::INSCRICAO_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SubscribePeer::INSCRICAO_ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Subscribe or Criteria object.
     *
     * @param      mixed $values Criteria or Subscribe object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(SubscribePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(SubscribePeer::INSCRICAO_ID);
            $value = $criteria->remove(SubscribePeer::INSCRICAO_ID);
            if ($value) {
                $selectCriteria->add(SubscribePeer::INSCRICAO_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SubscribePeer::TABLE_NAME);
            }

        } else { // $values is Subscribe object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the inscricao table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += SubscribePeer::doOnDeleteCascade(new Criteria(SubscribePeer::DATABASE_NAME), $con);
            SubscribePeer::doOnDeleteSetNull(new Criteria(SubscribePeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(SubscribePeer::TABLE_NAME, $con, SubscribePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SubscribePeer::clearInstancePool();
            SubscribePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Subscribe or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Subscribe object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Subscribe) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SubscribePeer::DATABASE_NAME);
            $criteria->add(SubscribePeer::INSCRICAO_ID, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(SubscribePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += SubscribePeer::doOnDeleteCascade($c, $con);

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            SubscribePeer::doOnDeleteSetNull($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                SubscribePeer::clearInstancePool();
            } elseif ($values instanceof Subscribe) { // it's a model object
                SubscribePeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    SubscribePeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            SubscribePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = SubscribePeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related Train objects
            $criteria = new Criteria(TrainPeer::DATABASE_NAME);

            $criteria->add(TrainPeer::INSCRICAO_ID, $obj->getId());
            $affectedRows += TrainPeer::doDelete($criteria, $con);

            // delete related SubscribePayment objects
            $criteria = new Criteria(SubscribePaymentPeer::DATABASE_NAME);

            $criteria->add(SubscribePaymentPeer::INSCRICAO_ID, $obj->getId());
            $affectedRows += SubscribePaymentPeer::doDelete($criteria, $con);

            // delete related WorkshopSubscribe objects
            $criteria = new Criteria(WorkshopSubscribePeer::DATABASE_NAME);

            $criteria->add(WorkshopSubscribePeer::INSCRICAO_ID, $obj->getId());
            $affectedRows += WorkshopSubscribePeer::doDelete($criteria, $con);

            // delete related Transfer objects
            $criteria = new Criteria(TransferPeer::DATABASE_NAME);

            $criteria->add(TransferPeer::INSCRICAO_ID, $obj->getId());
            $affectedRows += TransferPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * This is a method for emulating ON DELETE SET NULL DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return void
     */
    protected static function doOnDeleteSetNull(Criteria $criteria, PropelPDO $con)
    {

        // first find the objects that are implicated by the $criteria
        $objects = SubscribePeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {

            // set fkey col in related Donation rows to null
            $selectCriteria = new Criteria(SubscribePeer::DATABASE_NAME);
            $updateValues = new Criteria(SubscribePeer::DATABASE_NAME);
            $selectCriteria->add(DonationPeer::INSCRICAO_ID, $obj->getId());
            $updateValues->add(DonationPeer::INSCRICAO_ID, null);

            BasePeer::doUpdate($selectCriteria, $updateValues, $con); // use BasePeer because generated Peer doUpdate() methods only update using pkey

        }
    }

    /**
     * Validates all modified columns of given Subscribe object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param Subscribe $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(SubscribePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(SubscribePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(SubscribePeer::DATABASE_NAME, SubscribePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Subscribe
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = SubscribePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(SubscribePeer::DATABASE_NAME);
        $criteria->add(SubscribePeer::INSCRICAO_ID, $pk);

        $v = SubscribePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Subscribe[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(SubscribePeer::DATABASE_NAME);
            $criteria->add(SubscribePeer::INSCRICAO_ID, $pks, Criteria::IN);
            $objs = SubscribePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseSubscribePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseSubscribePeer::buildTableMap();

