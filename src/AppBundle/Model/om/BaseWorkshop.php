<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Workshop;
use AppBundle\Model\WorkshopPeer;
use AppBundle\Model\WorkshopQuery;
use AppBundle\Model\WorkshopSubscribe;
use AppBundle\Model\WorkshopSubscribeQuery;

abstract class BaseWorkshop extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\WorkshopPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        WorkshopPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the oficina_id field.
     * @var        int
     */
    protected $oficina_id;

    /**
     * The value for the nome field.
     * @var        string
     */
    protected $nome;

    /**
     * The value for the descricao field.
     * @var        string
     */
    protected $descricao;

    /**
     * The value for the modalidade field.
     * @var        string
     */
    protected $modalidade;

    /**
     * The value for the palestrante field.
     * @var        string
     */
    protected $palestrante;

    /**
     * The value for the data_inicio field.
     * @var        string
     */
    protected $data_inicio;

    /**
     * The value for the data_fim field.
     * @var        string
     */
    protected $data_fim;

    /**
     * The value for the qtde field.
     * @var        int
     */
    protected $qtde;

    /**
     * The value for the aberta field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $aberta;

    /**
     * @var        PropelObjectCollection|WorkshopSubscribe[] Collection to store aggregation of WorkshopSubscribe objects.
     */
    protected $collWorkshopSubscribes;
    protected $collWorkshopSubscribesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $workshopSubscribesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->aberta = true;
    }

    /**
     * Initializes internal state of BaseWorkshop object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [oficina_id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->oficina_id;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->nome;
    }

    /**
     * Get the [descricao] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->descricao;
    }

    /**
     * Get the [modalidade] column value.
     *
     * @return string
     */
    public function getModality()
    {

        return $this->modalidade;
    }

    /**
     * Get the [palestrante] column value.
     *
     * @return string
     */
    public function getSpeaker()
    {

        return $this->palestrante;
    }

    /**
     * Get the [optionally formatted] temporal [data_inicio] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStart($format = null)
    {
        if ($this->data_inicio === null) {
            return null;
        }

        if ($this->data_inicio === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_inicio);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_inicio, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [data_fim] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEnd($format = null)
    {
        if ($this->data_fim === null) {
            return null;
        }

        if ($this->data_fim === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_fim);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_fim, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [qtde] column value.
     *
     * @return int
     */
    public function getSpaceAvailable()
    {

        return $this->qtde;
    }

    /**
     * Get the [aberta] column value.
     *
     * @return boolean
     */
    public function getOpened()
    {

        return $this->aberta;
    }

    /**
     * Set the value of [oficina_id] column.
     *
     * @param  int $v new value
     * @return Workshop The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->oficina_id !== $v) {
            $this->oficina_id = $v;
            $this->modifiedColumns[] = WorkshopPeer::OFICINA_ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nome] column.
     *
     * @param  string $v new value
     * @return Workshop The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[] = WorkshopPeer::NOME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [descricao] column.
     *
     * @param  string $v new value
     * @return Workshop The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao !== $v) {
            $this->descricao = $v;
            $this->modifiedColumns[] = WorkshopPeer::DESCRICAO;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [modalidade] column.
     *
     * @param  string $v new value
     * @return Workshop The current object (for fluent API support)
     */
    public function setModality($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modalidade !== $v) {
            $this->modalidade = $v;
            $this->modifiedColumns[] = WorkshopPeer::MODALIDADE;
        }


        return $this;
    } // setModality()

    /**
     * Set the value of [palestrante] column.
     *
     * @param  string $v new value
     * @return Workshop The current object (for fluent API support)
     */
    public function setSpeaker($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->palestrante !== $v) {
            $this->palestrante = $v;
            $this->modifiedColumns[] = WorkshopPeer::PALESTRANTE;
        }


        return $this;
    } // setSpeaker()

    /**
     * Sets the value of [data_inicio] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Workshop The current object (for fluent API support)
     */
    public function setStart($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_inicio !== null || $dt !== null) {
            $currentDateAsString = ($this->data_inicio !== null && $tmpDt = new DateTime($this->data_inicio)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_inicio = $newDateAsString;
                $this->modifiedColumns[] = WorkshopPeer::DATA_INICIO;
            }
        } // if either are not null


        return $this;
    } // setStart()

    /**
     * Sets the value of [data_fim] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Workshop The current object (for fluent API support)
     */
    public function setEnd($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_fim !== null || $dt !== null) {
            $currentDateAsString = ($this->data_fim !== null && $tmpDt = new DateTime($this->data_fim)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_fim = $newDateAsString;
                $this->modifiedColumns[] = WorkshopPeer::DATA_FIM;
            }
        } // if either are not null


        return $this;
    } // setEnd()

    /**
     * Set the value of [qtde] column.
     *
     * @param  int $v new value
     * @return Workshop The current object (for fluent API support)
     */
    public function setSpaceAvailable($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->qtde !== $v) {
            $this->qtde = $v;
            $this->modifiedColumns[] = WorkshopPeer::QTDE;
        }


        return $this;
    } // setSpaceAvailable()

    /**
     * Sets the value of the [aberta] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Workshop The current object (for fluent API support)
     */
    public function setOpened($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->aberta !== $v) {
            $this->aberta = $v;
            $this->modifiedColumns[] = WorkshopPeer::ABERTA;
        }


        return $this;
    } // setOpened()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->aberta !== true) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->oficina_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nome = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->descricao = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->modalidade = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->palestrante = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->data_inicio = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->data_fim = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->qtde = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->aberta = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 9; // 9 = WorkshopPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Workshop object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(WorkshopPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = WorkshopPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collWorkshopSubscribes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(WorkshopPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = WorkshopQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(WorkshopPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                WorkshopPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->workshopSubscribesScheduledForDeletion !== null) {
                if (!$this->workshopSubscribesScheduledForDeletion->isEmpty()) {
                    WorkshopSubscribeQuery::create()
                        ->filterByPrimaryKeys($this->workshopSubscribesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->workshopSubscribesScheduledForDeletion = null;
                }
            }

            if ($this->collWorkshopSubscribes !== null) {
                foreach ($this->collWorkshopSubscribes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = WorkshopPeer::OFICINA_ID;
        if (null !== $this->oficina_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . WorkshopPeer::OFICINA_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(WorkshopPeer::OFICINA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`oficina_id`';
        }
        if ($this->isColumnModified(WorkshopPeer::NOME)) {
            $modifiedColumns[':p' . $index++]  = '`nome`';
        }
        if ($this->isColumnModified(WorkshopPeer::DESCRICAO)) {
            $modifiedColumns[':p' . $index++]  = '`descricao`';
        }
        if ($this->isColumnModified(WorkshopPeer::MODALIDADE)) {
            $modifiedColumns[':p' . $index++]  = '`modalidade`';
        }
        if ($this->isColumnModified(WorkshopPeer::PALESTRANTE)) {
            $modifiedColumns[':p' . $index++]  = '`palestrante`';
        }
        if ($this->isColumnModified(WorkshopPeer::DATA_INICIO)) {
            $modifiedColumns[':p' . $index++]  = '`data_inicio`';
        }
        if ($this->isColumnModified(WorkshopPeer::DATA_FIM)) {
            $modifiedColumns[':p' . $index++]  = '`data_fim`';
        }
        if ($this->isColumnModified(WorkshopPeer::QTDE)) {
            $modifiedColumns[':p' . $index++]  = '`qtde`';
        }
        if ($this->isColumnModified(WorkshopPeer::ABERTA)) {
            $modifiedColumns[':p' . $index++]  = '`aberta`';
        }

        $sql = sprintf(
            'INSERT INTO `oficina` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`oficina_id`':
                        $stmt->bindValue($identifier, $this->oficina_id, PDO::PARAM_INT);
                        break;
                    case '`nome`':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case '`descricao`':
                        $stmt->bindValue($identifier, $this->descricao, PDO::PARAM_STR);
                        break;
                    case '`modalidade`':
                        $stmt->bindValue($identifier, $this->modalidade, PDO::PARAM_STR);
                        break;
                    case '`palestrante`':
                        $stmt->bindValue($identifier, $this->palestrante, PDO::PARAM_STR);
                        break;
                    case '`data_inicio`':
                        $stmt->bindValue($identifier, $this->data_inicio, PDO::PARAM_STR);
                        break;
                    case '`data_fim`':
                        $stmt->bindValue($identifier, $this->data_fim, PDO::PARAM_STR);
                        break;
                    case '`qtde`':
                        $stmt->bindValue($identifier, $this->qtde, PDO::PARAM_INT);
                        break;
                    case '`aberta`':
                        $stmt->bindValue($identifier, (int) $this->aberta, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = WorkshopPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collWorkshopSubscribes !== null) {
                    foreach ($this->collWorkshopSubscribes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = WorkshopPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getDescription();
                break;
            case 3:
                return $this->getModality();
                break;
            case 4:
                return $this->getSpeaker();
                break;
            case 5:
                return $this->getStart();
                break;
            case 6:
                return $this->getEnd();
                break;
            case 7:
                return $this->getSpaceAvailable();
                break;
            case 8:
                return $this->getOpened();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Workshop'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Workshop'][$this->getPrimaryKey()] = true;
        $keys = WorkshopPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getDescription(),
            $keys[3] => $this->getModality(),
            $keys[4] => $this->getSpeaker(),
            $keys[5] => $this->getStart(),
            $keys[6] => $this->getEnd(),
            $keys[7] => $this->getSpaceAvailable(),
            $keys[8] => $this->getOpened(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collWorkshopSubscribes) {
                $result['WorkshopSubscribes'] = $this->collWorkshopSubscribes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = WorkshopPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setDescription($value);
                break;
            case 3:
                $this->setModality($value);
                break;
            case 4:
                $this->setSpeaker($value);
                break;
            case 5:
                $this->setStart($value);
                break;
            case 6:
                $this->setEnd($value);
                break;
            case 7:
                $this->setSpaceAvailable($value);
                break;
            case 8:
                $this->setOpened($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = WorkshopPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setDescription($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setModality($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setSpeaker($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setStart($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setEnd($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setSpaceAvailable($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setOpened($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(WorkshopPeer::DATABASE_NAME);

        if ($this->isColumnModified(WorkshopPeer::OFICINA_ID)) $criteria->add(WorkshopPeer::OFICINA_ID, $this->oficina_id);
        if ($this->isColumnModified(WorkshopPeer::NOME)) $criteria->add(WorkshopPeer::NOME, $this->nome);
        if ($this->isColumnModified(WorkshopPeer::DESCRICAO)) $criteria->add(WorkshopPeer::DESCRICAO, $this->descricao);
        if ($this->isColumnModified(WorkshopPeer::MODALIDADE)) $criteria->add(WorkshopPeer::MODALIDADE, $this->modalidade);
        if ($this->isColumnModified(WorkshopPeer::PALESTRANTE)) $criteria->add(WorkshopPeer::PALESTRANTE, $this->palestrante);
        if ($this->isColumnModified(WorkshopPeer::DATA_INICIO)) $criteria->add(WorkshopPeer::DATA_INICIO, $this->data_inicio);
        if ($this->isColumnModified(WorkshopPeer::DATA_FIM)) $criteria->add(WorkshopPeer::DATA_FIM, $this->data_fim);
        if ($this->isColumnModified(WorkshopPeer::QTDE)) $criteria->add(WorkshopPeer::QTDE, $this->qtde);
        if ($this->isColumnModified(WorkshopPeer::ABERTA)) $criteria->add(WorkshopPeer::ABERTA, $this->aberta);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(WorkshopPeer::DATABASE_NAME);
        $criteria->add(WorkshopPeer::OFICINA_ID, $this->oficina_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (oficina_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Workshop (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setModality($this->getModality());
        $copyObj->setSpeaker($this->getSpeaker());
        $copyObj->setStart($this->getStart());
        $copyObj->setEnd($this->getEnd());
        $copyObj->setSpaceAvailable($this->getSpaceAvailable());
        $copyObj->setOpened($this->getOpened());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getWorkshopSubscribes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWorkshopSubscribe($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Workshop Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return WorkshopPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new WorkshopPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('WorkshopSubscribe' == $relationName) {
            $this->initWorkshopSubscribes();
        }
    }

    /**
     * Clears out the collWorkshopSubscribes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Workshop The current object (for fluent API support)
     * @see        addWorkshopSubscribes()
     */
    public function clearWorkshopSubscribes()
    {
        $this->collWorkshopSubscribes = null; // important to set this to null since that means it is uninitialized
        $this->collWorkshopSubscribesPartial = null;

        return $this;
    }

    /**
     * reset is the collWorkshopSubscribes collection loaded partially
     *
     * @return void
     */
    public function resetPartialWorkshopSubscribes($v = true)
    {
        $this->collWorkshopSubscribesPartial = $v;
    }

    /**
     * Initializes the collWorkshopSubscribes collection.
     *
     * By default this just sets the collWorkshopSubscribes collection to an empty array (like clearcollWorkshopSubscribes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWorkshopSubscribes($overrideExisting = true)
    {
        if (null !== $this->collWorkshopSubscribes && !$overrideExisting) {
            return;
        }
        $this->collWorkshopSubscribes = new PropelObjectCollection();
        $this->collWorkshopSubscribes->setModel('WorkshopSubscribe');
    }

    /**
     * Gets an array of WorkshopSubscribe objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Workshop is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|WorkshopSubscribe[] List of WorkshopSubscribe objects
     * @throws PropelException
     */
    public function getWorkshopSubscribes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collWorkshopSubscribesPartial && !$this->isNew();
        if (null === $this->collWorkshopSubscribes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWorkshopSubscribes) {
                // return empty collection
                $this->initWorkshopSubscribes();
            } else {
                $collWorkshopSubscribes = WorkshopSubscribeQuery::create(null, $criteria)
                    ->filterByWorkshop($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collWorkshopSubscribesPartial && count($collWorkshopSubscribes)) {
                      $this->initWorkshopSubscribes(false);

                      foreach ($collWorkshopSubscribes as $obj) {
                        if (false == $this->collWorkshopSubscribes->contains($obj)) {
                          $this->collWorkshopSubscribes->append($obj);
                        }
                      }

                      $this->collWorkshopSubscribesPartial = true;
                    }

                    $collWorkshopSubscribes->getInternalIterator()->rewind();

                    return $collWorkshopSubscribes;
                }

                if ($partial && $this->collWorkshopSubscribes) {
                    foreach ($this->collWorkshopSubscribes as $obj) {
                        if ($obj->isNew()) {
                            $collWorkshopSubscribes[] = $obj;
                        }
                    }
                }

                $this->collWorkshopSubscribes = $collWorkshopSubscribes;
                $this->collWorkshopSubscribesPartial = false;
            }
        }

        return $this->collWorkshopSubscribes;
    }

    /**
     * Sets a collection of WorkshopSubscribe objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $workshopSubscribes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Workshop The current object (for fluent API support)
     */
    public function setWorkshopSubscribes(PropelCollection $workshopSubscribes, PropelPDO $con = null)
    {
        $workshopSubscribesToDelete = $this->getWorkshopSubscribes(new Criteria(), $con)->diff($workshopSubscribes);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->workshopSubscribesScheduledForDeletion = clone $workshopSubscribesToDelete;

        foreach ($workshopSubscribesToDelete as $workshopSubscribeRemoved) {
            $workshopSubscribeRemoved->setWorkshop(null);
        }

        $this->collWorkshopSubscribes = null;
        foreach ($workshopSubscribes as $workshopSubscribe) {
            $this->addWorkshopSubscribe($workshopSubscribe);
        }

        $this->collWorkshopSubscribes = $workshopSubscribes;
        $this->collWorkshopSubscribesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WorkshopSubscribe objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related WorkshopSubscribe objects.
     * @throws PropelException
     */
    public function countWorkshopSubscribes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collWorkshopSubscribesPartial && !$this->isNew();
        if (null === $this->collWorkshopSubscribes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWorkshopSubscribes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWorkshopSubscribes());
            }
            $query = WorkshopSubscribeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByWorkshop($this)
                ->count($con);
        }

        return count($this->collWorkshopSubscribes);
    }

    /**
     * Method called to associate a WorkshopSubscribe object to this object
     * through the WorkshopSubscribe foreign key attribute.
     *
     * @param    WorkshopSubscribe $l WorkshopSubscribe
     * @return Workshop The current object (for fluent API support)
     */
    public function addWorkshopSubscribe(WorkshopSubscribe $l)
    {
        if ($this->collWorkshopSubscribes === null) {
            $this->initWorkshopSubscribes();
            $this->collWorkshopSubscribesPartial = true;
        }

        if (!in_array($l, $this->collWorkshopSubscribes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddWorkshopSubscribe($l);

            if ($this->workshopSubscribesScheduledForDeletion and $this->workshopSubscribesScheduledForDeletion->contains($l)) {
                $this->workshopSubscribesScheduledForDeletion->remove($this->workshopSubscribesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	WorkshopSubscribe $workshopSubscribe The workshopSubscribe object to add.
     */
    protected function doAddWorkshopSubscribe($workshopSubscribe)
    {
        $this->collWorkshopSubscribes[]= $workshopSubscribe;
        $workshopSubscribe->setWorkshop($this);
    }

    /**
     * @param	WorkshopSubscribe $workshopSubscribe The workshopSubscribe object to remove.
     * @return Workshop The current object (for fluent API support)
     */
    public function removeWorkshopSubscribe($workshopSubscribe)
    {
        if ($this->getWorkshopSubscribes()->contains($workshopSubscribe)) {
            $this->collWorkshopSubscribes->remove($this->collWorkshopSubscribes->search($workshopSubscribe));
            if (null === $this->workshopSubscribesScheduledForDeletion) {
                $this->workshopSubscribesScheduledForDeletion = clone $this->collWorkshopSubscribes;
                $this->workshopSubscribesScheduledForDeletion->clear();
            }
            $this->workshopSubscribesScheduledForDeletion[]= clone $workshopSubscribe;
            $workshopSubscribe->setWorkshop(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Workshop is new, it will return
     * an empty collection; or if this Workshop has previously
     * been saved, it will retrieve related WorkshopSubscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Workshop.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|WorkshopSubscribe[] List of WorkshopSubscribe objects
     */
    public function getWorkshopSubscribesJoinSubscribe($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = WorkshopSubscribeQuery::create(null, $criteria);
        $query->joinWith('Subscribe', $join_behavior);

        return $this->getWorkshopSubscribes($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->oficina_id = null;
        $this->nome = null;
        $this->descricao = null;
        $this->modalidade = null;
        $this->palestrante = null;
        $this->data_inicio = null;
        $this->data_fim = null;
        $this->qtde = null;
        $this->aberta = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collWorkshopSubscribes) {
                foreach ($this->collWorkshopSubscribes as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collWorkshopSubscribes instanceof PropelCollection) {
            $this->collWorkshopSubscribes->clearIterator();
        }
        $this->collWorkshopSubscribes = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(WorkshopPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
