<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Sessions;
use AppBundle\Model\SessionsPeer;
use AppBundle\Model\SessionsQuery;

/**
 * @method SessionsQuery orderById($order = Criteria::ASC) Order by the sess_id column
 * @method SessionsQuery orderByDate($order = Criteria::ASC) Order by the sess_data column
 * @method SessionsQuery orderByTime($order = Criteria::ASC) Order by the sess_time column
 * @method SessionsQuery orderByLifetime($order = Criteria::ASC) Order by the sess_lifetime column
 *
 * @method SessionsQuery groupById() Group by the sess_id column
 * @method SessionsQuery groupByDate() Group by the sess_data column
 * @method SessionsQuery groupByTime() Group by the sess_time column
 * @method SessionsQuery groupByLifetime() Group by the sess_lifetime column
 *
 * @method SessionsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SessionsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SessionsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Sessions findOne(PropelPDO $con = null) Return the first Sessions matching the query
 * @method Sessions findOneOrCreate(PropelPDO $con = null) Return the first Sessions matching the query, or a new Sessions object populated from the query conditions when no match is found
 *
 * @method Sessions findOneByDate(resource $sess_data) Return the first Sessions filtered by the sess_data column
 * @method Sessions findOneByTime(int $sess_time) Return the first Sessions filtered by the sess_time column
 * @method Sessions findOneByLifetime(int $sess_lifetime) Return the first Sessions filtered by the sess_lifetime column
 *
 * @method array findById(string $sess_id) Return Sessions objects filtered by the sess_id column
 * @method array findByDate(resource $sess_data) Return Sessions objects filtered by the sess_data column
 * @method array findByTime(int $sess_time) Return Sessions objects filtered by the sess_time column
 * @method array findByLifetime(int $sess_lifetime) Return Sessions objects filtered by the sess_lifetime column
 */
abstract class BaseSessionsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSessionsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\Sessions';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SessionsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SessionsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SessionsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SessionsQuery) {
            return $criteria;
        }
        $query = new SessionsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Sessions|Sessions[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SessionsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SessionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sessions A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sessions A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `sess_id`, `sess_data`, `sess_time`, `sess_lifetime` FROM `sessions` WHERE `sess_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Sessions();
            $obj->hydrate($row);
            SessionsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Sessions|Sessions[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Sessions[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SessionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SessionsPeer::SESS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SessionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SessionsPeer::SESS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the sess_id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE sess_id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE sess_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SessionsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SessionsPeer::SESS_ID, $id, $comparison);
    }

    /**
     * Filter the query on the sess_data column
     *
     * @param     mixed $date The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SessionsQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {

        return $this->addUsingAlias(SessionsPeer::SESS_DATA, $date, $comparison);
    }

    /**
     * Filter the query on the sess_time column
     *
     * Example usage:
     * <code>
     * $query->filterByTime(1234); // WHERE sess_time = 1234
     * $query->filterByTime(array(12, 34)); // WHERE sess_time IN (12, 34)
     * $query->filterByTime(array('min' => 12)); // WHERE sess_time >= 12
     * $query->filterByTime(array('max' => 12)); // WHERE sess_time <= 12
     * </code>
     *
     * @param     mixed $time The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SessionsQuery The current query, for fluid interface
     */
    public function filterByTime($time = null, $comparison = null)
    {
        if (is_array($time)) {
            $useMinMax = false;
            if (isset($time['min'])) {
                $this->addUsingAlias(SessionsPeer::SESS_TIME, $time['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($time['max'])) {
                $this->addUsingAlias(SessionsPeer::SESS_TIME, $time['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SessionsPeer::SESS_TIME, $time, $comparison);
    }

    /**
     * Filter the query on the sess_lifetime column
     *
     * Example usage:
     * <code>
     * $query->filterByLifetime(1234); // WHERE sess_lifetime = 1234
     * $query->filterByLifetime(array(12, 34)); // WHERE sess_lifetime IN (12, 34)
     * $query->filterByLifetime(array('min' => 12)); // WHERE sess_lifetime >= 12
     * $query->filterByLifetime(array('max' => 12)); // WHERE sess_lifetime <= 12
     * </code>
     *
     * @param     mixed $lifetime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SessionsQuery The current query, for fluid interface
     */
    public function filterByLifetime($lifetime = null, $comparison = null)
    {
        if (is_array($lifetime)) {
            $useMinMax = false;
            if (isset($lifetime['min'])) {
                $this->addUsingAlias(SessionsPeer::SESS_LIFETIME, $lifetime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lifetime['max'])) {
                $this->addUsingAlias(SessionsPeer::SESS_LIFETIME, $lifetime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SessionsPeer::SESS_LIFETIME, $lifetime, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Sessions $sessions Object to remove from the list of results
     *
     * @return SessionsQuery The current query, for fluid interface
     */
    public function prune($sessions = null)
    {
        if ($sessions) {
            $this->addUsingAlias(SessionsPeer::SESS_ID, $sessions->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
