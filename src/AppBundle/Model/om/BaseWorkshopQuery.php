<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Workshop;
use AppBundle\Model\WorkshopPeer;
use AppBundle\Model\WorkshopQuery;
use AppBundle\Model\WorkshopSubscribe;

/**
 * @method WorkshopQuery orderById($order = Criteria::ASC) Order by the oficina_id column
 * @method WorkshopQuery orderByName($order = Criteria::ASC) Order by the nome column
 * @method WorkshopQuery orderByDescription($order = Criteria::ASC) Order by the descricao column
 * @method WorkshopQuery orderByModality($order = Criteria::ASC) Order by the modalidade column
 * @method WorkshopQuery orderBySpeaker($order = Criteria::ASC) Order by the palestrante column
 * @method WorkshopQuery orderByStart($order = Criteria::ASC) Order by the data_inicio column
 * @method WorkshopQuery orderByEnd($order = Criteria::ASC) Order by the data_fim column
 * @method WorkshopQuery orderBySpaceAvailable($order = Criteria::ASC) Order by the qtde column
 * @method WorkshopQuery orderByOpened($order = Criteria::ASC) Order by the aberta column
 *
 * @method WorkshopQuery groupById() Group by the oficina_id column
 * @method WorkshopQuery groupByName() Group by the nome column
 * @method WorkshopQuery groupByDescription() Group by the descricao column
 * @method WorkshopQuery groupByModality() Group by the modalidade column
 * @method WorkshopQuery groupBySpeaker() Group by the palestrante column
 * @method WorkshopQuery groupByStart() Group by the data_inicio column
 * @method WorkshopQuery groupByEnd() Group by the data_fim column
 * @method WorkshopQuery groupBySpaceAvailable() Group by the qtde column
 * @method WorkshopQuery groupByOpened() Group by the aberta column
 *
 * @method WorkshopQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method WorkshopQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method WorkshopQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method WorkshopQuery leftJoinWorkshopSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the WorkshopSubscribe relation
 * @method WorkshopQuery rightJoinWorkshopSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WorkshopSubscribe relation
 * @method WorkshopQuery innerJoinWorkshopSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the WorkshopSubscribe relation
 *
 * @method Workshop findOne(PropelPDO $con = null) Return the first Workshop matching the query
 * @method Workshop findOneOrCreate(PropelPDO $con = null) Return the first Workshop matching the query, or a new Workshop object populated from the query conditions when no match is found
 *
 * @method Workshop findOneByName(string $nome) Return the first Workshop filtered by the nome column
 * @method Workshop findOneByDescription(string $descricao) Return the first Workshop filtered by the descricao column
 * @method Workshop findOneByModality(string $modalidade) Return the first Workshop filtered by the modalidade column
 * @method Workshop findOneBySpeaker(string $palestrante) Return the first Workshop filtered by the palestrante column
 * @method Workshop findOneByStart(string $data_inicio) Return the first Workshop filtered by the data_inicio column
 * @method Workshop findOneByEnd(string $data_fim) Return the first Workshop filtered by the data_fim column
 * @method Workshop findOneBySpaceAvailable(int $qtde) Return the first Workshop filtered by the qtde column
 * @method Workshop findOneByOpened(boolean $aberta) Return the first Workshop filtered by the aberta column
 *
 * @method array findById(int $oficina_id) Return Workshop objects filtered by the oficina_id column
 * @method array findByName(string $nome) Return Workshop objects filtered by the nome column
 * @method array findByDescription(string $descricao) Return Workshop objects filtered by the descricao column
 * @method array findByModality(string $modalidade) Return Workshop objects filtered by the modalidade column
 * @method array findBySpeaker(string $palestrante) Return Workshop objects filtered by the palestrante column
 * @method array findByStart(string $data_inicio) Return Workshop objects filtered by the data_inicio column
 * @method array findByEnd(string $data_fim) Return Workshop objects filtered by the data_fim column
 * @method array findBySpaceAvailable(int $qtde) Return Workshop objects filtered by the qtde column
 * @method array findByOpened(boolean $aberta) Return Workshop objects filtered by the aberta column
 */
abstract class BaseWorkshopQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseWorkshopQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\Workshop';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new WorkshopQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   WorkshopQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return WorkshopQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof WorkshopQuery) {
            return $criteria;
        }
        $query = new WorkshopQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Workshop|Workshop[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = WorkshopPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(WorkshopPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Workshop A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Workshop A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `oficina_id`, `nome`, `descricao`, `modalidade`, `palestrante`, `data_inicio`, `data_fim`, `qtde`, `aberta` FROM `oficina` WHERE `oficina_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Workshop();
            $obj->hydrate($row);
            WorkshopPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Workshop|Workshop[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Workshop[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(WorkshopPeer::OFICINA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(WorkshopPeer::OFICINA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the oficina_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE oficina_id = 1234
     * $query->filterById(array(12, 34)); // WHERE oficina_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE oficina_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE oficina_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(WorkshopPeer::OFICINA_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(WorkshopPeer::OFICINA_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WorkshopPeer::OFICINA_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WorkshopPeer::NOME, $name, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WorkshopPeer::DESCRICAO, $description, $comparison);
    }

    /**
     * Filter the query on the modalidade column
     *
     * Example usage:
     * <code>
     * $query->filterByModality('fooValue');   // WHERE modalidade = 'fooValue'
     * $query->filterByModality('%fooValue%'); // WHERE modalidade LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modality The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterByModality($modality = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modality)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modality)) {
                $modality = str_replace('*', '%', $modality);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WorkshopPeer::MODALIDADE, $modality, $comparison);
    }

    /**
     * Filter the query on the palestrante column
     *
     * Example usage:
     * <code>
     * $query->filterBySpeaker('fooValue');   // WHERE palestrante = 'fooValue'
     * $query->filterBySpeaker('%fooValue%'); // WHERE palestrante LIKE '%fooValue%'
     * </code>
     *
     * @param     string $speaker The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterBySpeaker($speaker = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($speaker)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $speaker)) {
                $speaker = str_replace('*', '%', $speaker);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WorkshopPeer::PALESTRANTE, $speaker, $comparison);
    }

    /**
     * Filter the query on the data_inicio column
     *
     * Example usage:
     * <code>
     * $query->filterByStart('2011-03-14'); // WHERE data_inicio = '2011-03-14'
     * $query->filterByStart('now'); // WHERE data_inicio = '2011-03-14'
     * $query->filterByStart(array('max' => 'yesterday')); // WHERE data_inicio < '2011-03-13'
     * </code>
     *
     * @param     mixed $start The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterByStart($start = null, $comparison = null)
    {
        if (is_array($start)) {
            $useMinMax = false;
            if (isset($start['min'])) {
                $this->addUsingAlias(WorkshopPeer::DATA_INICIO, $start['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($start['max'])) {
                $this->addUsingAlias(WorkshopPeer::DATA_INICIO, $start['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WorkshopPeer::DATA_INICIO, $start, $comparison);
    }

    /**
     * Filter the query on the data_fim column
     *
     * Example usage:
     * <code>
     * $query->filterByEnd('2011-03-14'); // WHERE data_fim = '2011-03-14'
     * $query->filterByEnd('now'); // WHERE data_fim = '2011-03-14'
     * $query->filterByEnd(array('max' => 'yesterday')); // WHERE data_fim < '2011-03-13'
     * </code>
     *
     * @param     mixed $end The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterByEnd($end = null, $comparison = null)
    {
        if (is_array($end)) {
            $useMinMax = false;
            if (isset($end['min'])) {
                $this->addUsingAlias(WorkshopPeer::DATA_FIM, $end['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($end['max'])) {
                $this->addUsingAlias(WorkshopPeer::DATA_FIM, $end['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WorkshopPeer::DATA_FIM, $end, $comparison);
    }

    /**
     * Filter the query on the qtde column
     *
     * Example usage:
     * <code>
     * $query->filterBySpaceAvailable(1234); // WHERE qtde = 1234
     * $query->filterBySpaceAvailable(array(12, 34)); // WHERE qtde IN (12, 34)
     * $query->filterBySpaceAvailable(array('min' => 12)); // WHERE qtde >= 12
     * $query->filterBySpaceAvailable(array('max' => 12)); // WHERE qtde <= 12
     * </code>
     *
     * @param     mixed $spaceAvailable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterBySpaceAvailable($spaceAvailable = null, $comparison = null)
    {
        if (is_array($spaceAvailable)) {
            $useMinMax = false;
            if (isset($spaceAvailable['min'])) {
                $this->addUsingAlias(WorkshopPeer::QTDE, $spaceAvailable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($spaceAvailable['max'])) {
                $this->addUsingAlias(WorkshopPeer::QTDE, $spaceAvailable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WorkshopPeer::QTDE, $spaceAvailable, $comparison);
    }

    /**
     * Filter the query on the aberta column
     *
     * Example usage:
     * <code>
     * $query->filterByOpened(true); // WHERE aberta = true
     * $query->filterByOpened('yes'); // WHERE aberta = true
     * </code>
     *
     * @param     boolean|string $opened The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function filterByOpened($opened = null, $comparison = null)
    {
        if (is_string($opened)) {
            $opened = in_array(strtolower($opened), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(WorkshopPeer::ABERTA, $opened, $comparison);
    }

    /**
     * Filter the query by a related WorkshopSubscribe object
     *
     * @param   WorkshopSubscribe|PropelObjectCollection $workshopSubscribe  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 WorkshopQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWorkshopSubscribe($workshopSubscribe, $comparison = null)
    {
        if ($workshopSubscribe instanceof WorkshopSubscribe) {
            return $this
                ->addUsingAlias(WorkshopPeer::OFICINA_ID, $workshopSubscribe->getWorkshopId(), $comparison);
        } elseif ($workshopSubscribe instanceof PropelObjectCollection) {
            return $this
                ->useWorkshopSubscribeQuery()
                ->filterByPrimaryKeys($workshopSubscribe->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWorkshopSubscribe() only accepts arguments of type WorkshopSubscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WorkshopSubscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function joinWorkshopSubscribe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WorkshopSubscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WorkshopSubscribe');
        }

        return $this;
    }

    /**
     * Use the WorkshopSubscribe relation WorkshopSubscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\WorkshopSubscribeQuery A secondary query class using the current class as primary query
     */
    public function useWorkshopSubscribeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWorkshopSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WorkshopSubscribe', '\AppBundle\Model\WorkshopSubscribeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Workshop $workshop Object to remove from the list of results
     *
     * @return WorkshopQuery The current query, for fluid interface
     */
    public function prune($workshop = null)
    {
        if ($workshop) {
            $this->addUsingAlias(WorkshopPeer::OFICINA_ID, $workshop->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
