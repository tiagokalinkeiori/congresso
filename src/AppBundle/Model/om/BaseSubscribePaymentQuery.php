<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePaymentPeer;
use AppBundle\Model\SubscribePaymentQuery;

/**
 * @method SubscribePaymentQuery orderById($order = Criteria::ASC) Order by the inscricao_pagamento_id column
 * @method SubscribePaymentQuery orderBySubscribeId($order = Criteria::ASC) Order by the inscricao_id column
 * @method SubscribePaymentQuery orderByCreateDate($order = Criteria::ASC) Order by the data_geracao column
 * @method SubscribePaymentQuery orderByDueDate($order = Criteria::ASC) Order by the data_vencimento column
 * @method SubscribePaymentQuery orderByPaymentDate($order = Criteria::ASC) Order by the data_pagamento column
 * @method SubscribePaymentQuery orderByPrice($order = Criteria::ASC) Order by the valor column
 * @method SubscribePaymentQuery orderByPaid($order = Criteria::ASC) Order by the pago column
 * @method SubscribePaymentQuery orderByFirst($order = Criteria::ASC) Order by the primeira column
 * @method SubscribePaymentQuery orderByCancel($order = Criteria::ASC) Order by the cancelada column
 * @method SubscribePaymentQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method SubscribePaymentQuery orderByOwerNumber($order = Criteria::ASC) Order by the nosso_numero column
 * @method SubscribePaymentQuery orderByDigitableLine($order = Criteria::ASC) Order by the linha_digitavel column
 * @method SubscribePaymentQuery orderBySent($order = Criteria::ASC) Order by the enviado column
 *
 * @method SubscribePaymentQuery groupById() Group by the inscricao_pagamento_id column
 * @method SubscribePaymentQuery groupBySubscribeId() Group by the inscricao_id column
 * @method SubscribePaymentQuery groupByCreateDate() Group by the data_geracao column
 * @method SubscribePaymentQuery groupByDueDate() Group by the data_vencimento column
 * @method SubscribePaymentQuery groupByPaymentDate() Group by the data_pagamento column
 * @method SubscribePaymentQuery groupByPrice() Group by the valor column
 * @method SubscribePaymentQuery groupByPaid() Group by the pago column
 * @method SubscribePaymentQuery groupByFirst() Group by the primeira column
 * @method SubscribePaymentQuery groupByCancel() Group by the cancelada column
 * @method SubscribePaymentQuery groupByUrl() Group by the url column
 * @method SubscribePaymentQuery groupByOwerNumber() Group by the nosso_numero column
 * @method SubscribePaymentQuery groupByDigitableLine() Group by the linha_digitavel column
 * @method SubscribePaymentQuery groupBySent() Group by the enviado column
 *
 * @method SubscribePaymentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SubscribePaymentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SubscribePaymentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SubscribePaymentQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method SubscribePaymentQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method SubscribePaymentQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method SubscribePayment findOne(PropelPDO $con = null) Return the first SubscribePayment matching the query
 * @method SubscribePayment findOneOrCreate(PropelPDO $con = null) Return the first SubscribePayment matching the query, or a new SubscribePayment object populated from the query conditions when no match is found
 *
 * @method SubscribePayment findOneBySubscribeId(int $inscricao_id) Return the first SubscribePayment filtered by the inscricao_id column
 * @method SubscribePayment findOneByCreateDate(string $data_geracao) Return the first SubscribePayment filtered by the data_geracao column
 * @method SubscribePayment findOneByDueDate(string $data_vencimento) Return the first SubscribePayment filtered by the data_vencimento column
 * @method SubscribePayment findOneByPaymentDate(string $data_pagamento) Return the first SubscribePayment filtered by the data_pagamento column
 * @method SubscribePayment findOneByPrice(string $valor) Return the first SubscribePayment filtered by the valor column
 * @method SubscribePayment findOneByPaid(boolean $pago) Return the first SubscribePayment filtered by the pago column
 * @method SubscribePayment findOneByFirst(boolean $primeira) Return the first SubscribePayment filtered by the primeira column
 * @method SubscribePayment findOneByCancel(boolean $cancelada) Return the first SubscribePayment filtered by the cancelada column
 * @method SubscribePayment findOneByUrl(string $url) Return the first SubscribePayment filtered by the url column
 * @method SubscribePayment findOneByOwerNumber(string $nosso_numero) Return the first SubscribePayment filtered by the nosso_numero column
 * @method SubscribePayment findOneByDigitableLine(string $linha_digitavel) Return the first SubscribePayment filtered by the linha_digitavel column
 * @method SubscribePayment findOneBySent(boolean $enviado) Return the first SubscribePayment filtered by the enviado column
 *
 * @method array findById(int $inscricao_pagamento_id) Return SubscribePayment objects filtered by the inscricao_pagamento_id column
 * @method array findBySubscribeId(int $inscricao_id) Return SubscribePayment objects filtered by the inscricao_id column
 * @method array findByCreateDate(string $data_geracao) Return SubscribePayment objects filtered by the data_geracao column
 * @method array findByDueDate(string $data_vencimento) Return SubscribePayment objects filtered by the data_vencimento column
 * @method array findByPaymentDate(string $data_pagamento) Return SubscribePayment objects filtered by the data_pagamento column
 * @method array findByPrice(string $valor) Return SubscribePayment objects filtered by the valor column
 * @method array findByPaid(boolean $pago) Return SubscribePayment objects filtered by the pago column
 * @method array findByFirst(boolean $primeira) Return SubscribePayment objects filtered by the primeira column
 * @method array findByCancel(boolean $cancelada) Return SubscribePayment objects filtered by the cancelada column
 * @method array findByUrl(string $url) Return SubscribePayment objects filtered by the url column
 * @method array findByOwerNumber(string $nosso_numero) Return SubscribePayment objects filtered by the nosso_numero column
 * @method array findByDigitableLine(string $linha_digitavel) Return SubscribePayment objects filtered by the linha_digitavel column
 * @method array findBySent(boolean $enviado) Return SubscribePayment objects filtered by the enviado column
 */
abstract class BaseSubscribePaymentQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSubscribePaymentQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\SubscribePayment';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SubscribePaymentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SubscribePaymentQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SubscribePaymentQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SubscribePaymentQuery) {
            return $criteria;
        }
        $query = new SubscribePaymentQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SubscribePayment|SubscribePayment[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SubscribePaymentPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SubscribePaymentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SubscribePayment A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SubscribePayment A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `inscricao_pagamento_id`, `inscricao_id`, `data_geracao`, `data_vencimento`, `data_pagamento`, `valor`, `pago`, `primeira`, `cancelada`, `url`, `nosso_numero`, `linha_digitavel`, `enviado` FROM `inscricao_pagamento` WHERE `inscricao_pagamento_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SubscribePayment();
            $obj->hydrate($row);
            SubscribePaymentPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SubscribePayment|SubscribePayment[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SubscribePayment[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the inscricao_pagamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE inscricao_pagamento_id = 1234
     * $query->filterById(array(12, 34)); // WHERE inscricao_pagamento_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE inscricao_pagamento_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE inscricao_pagamento_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the inscricao_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySubscribeId(1234); // WHERE inscricao_id = 1234
     * $query->filterBySubscribeId(array(12, 34)); // WHERE inscricao_id IN (12, 34)
     * $query->filterBySubscribeId(array('min' => 12)); // WHERE inscricao_id >= 12
     * $query->filterBySubscribeId(array('max' => 12)); // WHERE inscricao_id <= 12
     * </code>
     *
     * @see       filterBySubscribe()
     *
     * @param     mixed $subscribeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterBySubscribeId($subscribeId = null, $comparison = null)
    {
        if (is_array($subscribeId)) {
            $useMinMax = false;
            if (isset($subscribeId['min'])) {
                $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_ID, $subscribeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subscribeId['max'])) {
                $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_ID, $subscribeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_ID, $subscribeId, $comparison);
    }

    /**
     * Filter the query on the data_geracao column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE data_geracao = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE data_geracao = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE data_geracao < '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(SubscribePaymentPeer::DATA_GERACAO, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(SubscribePaymentPeer::DATA_GERACAO, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::DATA_GERACAO, $createDate, $comparison);
    }

    /**
     * Filter the query on the data_vencimento column
     *
     * Example usage:
     * <code>
     * $query->filterByDueDate('2011-03-14'); // WHERE data_vencimento = '2011-03-14'
     * $query->filterByDueDate('now'); // WHERE data_vencimento = '2011-03-14'
     * $query->filterByDueDate(array('max' => 'yesterday')); // WHERE data_vencimento < '2011-03-13'
     * </code>
     *
     * @param     mixed $dueDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByDueDate($dueDate = null, $comparison = null)
    {
        if (is_array($dueDate)) {
            $useMinMax = false;
            if (isset($dueDate['min'])) {
                $this->addUsingAlias(SubscribePaymentPeer::DATA_VENCIMENTO, $dueDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dueDate['max'])) {
                $this->addUsingAlias(SubscribePaymentPeer::DATA_VENCIMENTO, $dueDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::DATA_VENCIMENTO, $dueDate, $comparison);
    }

    /**
     * Filter the query on the data_pagamento column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentDate('2011-03-14'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByPaymentDate('now'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByPaymentDate(array('max' => 'yesterday')); // WHERE data_pagamento < '2011-03-13'
     * </code>
     *
     * @param     mixed $paymentDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByPaymentDate($paymentDate = null, $comparison = null)
    {
        if (is_array($paymentDate)) {
            $useMinMax = false;
            if (isset($paymentDate['min'])) {
                $this->addUsingAlias(SubscribePaymentPeer::DATA_PAGAMENTO, $paymentDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymentDate['max'])) {
                $this->addUsingAlias(SubscribePaymentPeer::DATA_PAGAMENTO, $paymentDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::DATA_PAGAMENTO, $paymentDate, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE valor = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE valor >= 12
     * $query->filterByPrice(array('max' => 12)); // WHERE valor <= 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(SubscribePaymentPeer::VALOR, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(SubscribePaymentPeer::VALOR, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::VALOR, $price, $comparison);
    }

    /**
     * Filter the query on the pago column
     *
     * Example usage:
     * <code>
     * $query->filterByPaid(true); // WHERE pago = true
     * $query->filterByPaid('yes'); // WHERE pago = true
     * </code>
     *
     * @param     boolean|string $paid The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByPaid($paid = null, $comparison = null)
    {
        if (is_string($paid)) {
            $paid = in_array(strtolower($paid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePaymentPeer::PAGO, $paid, $comparison);
    }

    /**
     * Filter the query on the primeira column
     *
     * Example usage:
     * <code>
     * $query->filterByFirst(true); // WHERE primeira = true
     * $query->filterByFirst('yes'); // WHERE primeira = true
     * </code>
     *
     * @param     boolean|string $first The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByFirst($first = null, $comparison = null)
    {
        if (is_string($first)) {
            $first = in_array(strtolower($first), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePaymentPeer::PRIMEIRA, $first, $comparison);
    }

    /**
     * Filter the query on the cancelada column
     *
     * Example usage:
     * <code>
     * $query->filterByCancel(true); // WHERE cancelada = true
     * $query->filterByCancel('yes'); // WHERE cancelada = true
     * </code>
     *
     * @param     boolean|string $cancel The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByCancel($cancel = null, $comparison = null)
    {
        if (is_string($cancel)) {
            $cancel = in_array(strtolower($cancel), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePaymentPeer::CANCELADA, $cancel, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%'); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $url)) {
                $url = str_replace('*', '%', $url);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::URL, $url, $comparison);
    }

    /**
     * Filter the query on the nosso_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByOwerNumber('fooValue');   // WHERE nosso_numero = 'fooValue'
     * $query->filterByOwerNumber('%fooValue%'); // WHERE nosso_numero LIKE '%fooValue%'
     * </code>
     *
     * @param     string $owerNumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByOwerNumber($owerNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($owerNumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $owerNumber)) {
                $owerNumber = str_replace('*', '%', $owerNumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::NOSSO_NUMERO, $owerNumber, $comparison);
    }

    /**
     * Filter the query on the linha_digitavel column
     *
     * Example usage:
     * <code>
     * $query->filterByDigitableLine('fooValue');   // WHERE linha_digitavel = 'fooValue'
     * $query->filterByDigitableLine('%fooValue%'); // WHERE linha_digitavel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $digitableLine The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterByDigitableLine($digitableLine = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($digitableLine)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $digitableLine)) {
                $digitableLine = str_replace('*', '%', $digitableLine);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SubscribePaymentPeer::LINHA_DIGITAVEL, $digitableLine, $comparison);
    }

    /**
     * Filter the query on the enviado column
     *
     * Example usage:
     * <code>
     * $query->filterBySent(true); // WHERE enviado = true
     * $query->filterBySent('yes'); // WHERE enviado = true
     * </code>
     *
     * @param     boolean|string $sent The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function filterBySent($sent = null, $comparison = null)
    {
        if (is_string($sent)) {
            $sent = in_array(strtolower($sent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SubscribePaymentPeer::ENVIADO, $sent, $comparison);
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribePaymentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(SubscribePaymentPeer::INSCRICAO_ID, $subscribe->getId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribePaymentPeer::INSCRICAO_ID, $subscribe->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SubscribePayment $subscribePayment Object to remove from the list of results
     *
     * @return SubscribePaymentQuery The current query, for fluid interface
     */
    public function prune($subscribePayment = null)
    {
        if ($subscribePayment) {
            $this->addUsingAlias(SubscribePaymentPeer::INSCRICAO_PAGAMENTO_ID, $subscribePayment->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
