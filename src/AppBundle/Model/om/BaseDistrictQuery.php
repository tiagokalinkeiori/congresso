<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\District;
use AppBundle\Model\DistrictPeer;
use AppBundle\Model\DistrictQuery;
use AppBundle\Model\Subscribe;

/**
 * @method DistrictQuery orderById($order = Criteria::ASC) Order by the distrito_id column
 * @method DistrictQuery orderByName($order = Criteria::ASC) Order by the nome column
 *
 * @method DistrictQuery groupById() Group by the distrito_id column
 * @method DistrictQuery groupByName() Group by the nome column
 *
 * @method DistrictQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DistrictQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DistrictQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DistrictQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method DistrictQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method DistrictQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method District findOne(PropelPDO $con = null) Return the first District matching the query
 * @method District findOneOrCreate(PropelPDO $con = null) Return the first District matching the query, or a new District object populated from the query conditions when no match is found
 *
 * @method District findOneByName(string $nome) Return the first District filtered by the nome column
 *
 * @method array findById(int $distrito_id) Return District objects filtered by the distrito_id column
 * @method array findByName(string $nome) Return District objects filtered by the nome column
 */
abstract class BaseDistrictQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDistrictQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\District';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DistrictQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DistrictQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DistrictQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DistrictQuery) {
            return $criteria;
        }
        $query = new DistrictQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   District|District[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DistrictPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DistrictPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 District A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 District A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `distrito_id`, `nome` FROM `distrito` WHERE `distrito_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new District();
            $obj->hydrate($row);
            DistrictPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return District|District[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|District[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DistrictQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DistrictPeer::DISTRITO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DistrictQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DistrictPeer::DISTRITO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the distrito_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE distrito_id = 1234
     * $query->filterById(array(12, 34)); // WHERE distrito_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE distrito_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE distrito_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DistrictQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DistrictPeer::DISTRITO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DistrictPeer::DISTRITO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DistrictPeer::DISTRITO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DistrictQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DistrictPeer::NOME, $name, $comparison);
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DistrictQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(DistrictPeer::DISTRITO_ID, $subscribe->getDistrictId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            return $this
                ->useSubscribeQuery()
                ->filterByPrimaryKeys($subscribe->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DistrictQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   District $district Object to remove from the list of results
     *
     * @return DistrictQuery The current query, for fluid interface
     */
    public function prune($district = null)
    {
        if ($district) {
            $this->addUsingAlias(DistrictPeer::DISTRITO_ID, $district->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
