<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\SubscribeType;
use AppBundle\Model\SubscribeTypePrice;
use AppBundle\Model\SubscribeTypePricePeer;
use AppBundle\Model\SubscribeTypePriceQuery;

/**
 * @method SubscribeTypePriceQuery orderById($order = Criteria::ASC) Order by the inscricao_tipo_valor_id column
 * @method SubscribeTypePriceQuery orderBySubscribeTypeId($order = Criteria::ASC) Order by the inscricao_tipo_id column
 * @method SubscribeTypePriceQuery orderByPrice($order = Criteria::ASC) Order by the valor column
 * @method SubscribeTypePriceQuery orderByQuota($order = Criteria::ASC) Order by the parcelas column
 * @method SubscribeTypePriceQuery orderByInstallmentValue($order = Criteria::ASC) Order by the valor_parcelado column
 * @method SubscribeTypePriceQuery orderByStart($order = Criteria::ASC) Order by the inicio column
 * @method SubscribeTypePriceQuery orderByEnd($order = Criteria::ASC) Order by the fim column
 *
 * @method SubscribeTypePriceQuery groupById() Group by the inscricao_tipo_valor_id column
 * @method SubscribeTypePriceQuery groupBySubscribeTypeId() Group by the inscricao_tipo_id column
 * @method SubscribeTypePriceQuery groupByPrice() Group by the valor column
 * @method SubscribeTypePriceQuery groupByQuota() Group by the parcelas column
 * @method SubscribeTypePriceQuery groupByInstallmentValue() Group by the valor_parcelado column
 * @method SubscribeTypePriceQuery groupByStart() Group by the inicio column
 * @method SubscribeTypePriceQuery groupByEnd() Group by the fim column
 *
 * @method SubscribeTypePriceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SubscribeTypePriceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SubscribeTypePriceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SubscribeTypePriceQuery leftJoinSubscribeType($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubscribeType relation
 * @method SubscribeTypePriceQuery rightJoinSubscribeType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubscribeType relation
 * @method SubscribeTypePriceQuery innerJoinSubscribeType($relationAlias = null) Adds a INNER JOIN clause to the query using the SubscribeType relation
 *
 * @method SubscribeTypePrice findOne(PropelPDO $con = null) Return the first SubscribeTypePrice matching the query
 * @method SubscribeTypePrice findOneOrCreate(PropelPDO $con = null) Return the first SubscribeTypePrice matching the query, or a new SubscribeTypePrice object populated from the query conditions when no match is found
 *
 * @method SubscribeTypePrice findOneBySubscribeTypeId(int $inscricao_tipo_id) Return the first SubscribeTypePrice filtered by the inscricao_tipo_id column
 * @method SubscribeTypePrice findOneByPrice(string $valor) Return the first SubscribeTypePrice filtered by the valor column
 * @method SubscribeTypePrice findOneByQuota(int $parcelas) Return the first SubscribeTypePrice filtered by the parcelas column
 * @method SubscribeTypePrice findOneByInstallmentValue(string $valor_parcelado) Return the first SubscribeTypePrice filtered by the valor_parcelado column
 * @method SubscribeTypePrice findOneByStart(string $inicio) Return the first SubscribeTypePrice filtered by the inicio column
 * @method SubscribeTypePrice findOneByEnd(string $fim) Return the first SubscribeTypePrice filtered by the fim column
 *
 * @method array findById(int $inscricao_tipo_valor_id) Return SubscribeTypePrice objects filtered by the inscricao_tipo_valor_id column
 * @method array findBySubscribeTypeId(int $inscricao_tipo_id) Return SubscribeTypePrice objects filtered by the inscricao_tipo_id column
 * @method array findByPrice(string $valor) Return SubscribeTypePrice objects filtered by the valor column
 * @method array findByQuota(int $parcelas) Return SubscribeTypePrice objects filtered by the parcelas column
 * @method array findByInstallmentValue(string $valor_parcelado) Return SubscribeTypePrice objects filtered by the valor_parcelado column
 * @method array findByStart(string $inicio) Return SubscribeTypePrice objects filtered by the inicio column
 * @method array findByEnd(string $fim) Return SubscribeTypePrice objects filtered by the fim column
 */
abstract class BaseSubscribeTypePriceQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSubscribeTypePriceQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\SubscribeTypePrice';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SubscribeTypePriceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SubscribeTypePriceQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SubscribeTypePriceQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SubscribeTypePriceQuery) {
            return $criteria;
        }
        $query = new SubscribeTypePriceQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SubscribeTypePrice|SubscribeTypePrice[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SubscribeTypePricePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SubscribeTypePricePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SubscribeTypePrice A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SubscribeTypePrice A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `inscricao_tipo_valor_id`, `inscricao_tipo_id`, `valor`, `parcelas`, `valor_parcelado`, `inicio`, `fim` FROM `inscricao_tipo_valor` WHERE `inscricao_tipo_valor_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SubscribeTypePrice();
            $obj->hydrate($row);
            SubscribeTypePricePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SubscribeTypePrice|SubscribeTypePrice[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SubscribeTypePrice[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the inscricao_tipo_valor_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE inscricao_tipo_valor_id = 1234
     * $query->filterById(array(12, 34)); // WHERE inscricao_tipo_valor_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE inscricao_tipo_valor_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE inscricao_tipo_valor_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $id, $comparison);
    }

    /**
     * Filter the query on the inscricao_tipo_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySubscribeTypeId(1234); // WHERE inscricao_tipo_id = 1234
     * $query->filterBySubscribeTypeId(array(12, 34)); // WHERE inscricao_tipo_id IN (12, 34)
     * $query->filterBySubscribeTypeId(array('min' => 12)); // WHERE inscricao_tipo_id >= 12
     * $query->filterBySubscribeTypeId(array('max' => 12)); // WHERE inscricao_tipo_id <= 12
     * </code>
     *
     * @see       filterBySubscribeType()
     *
     * @param     mixed $subscribeTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterBySubscribeTypeId($subscribeTypeId = null, $comparison = null)
    {
        if (is_array($subscribeTypeId)) {
            $useMinMax = false;
            if (isset($subscribeTypeId['min'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, $subscribeTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subscribeTypeId['max'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, $subscribeTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, $subscribeTypeId, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE valor = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE valor >= 12
     * $query->filterByPrice(array('max' => 12)); // WHERE valor <= 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::VALOR, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::VALOR, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePricePeer::VALOR, $price, $comparison);
    }

    /**
     * Filter the query on the parcelas column
     *
     * Example usage:
     * <code>
     * $query->filterByQuota(1234); // WHERE parcelas = 1234
     * $query->filterByQuota(array(12, 34)); // WHERE parcelas IN (12, 34)
     * $query->filterByQuota(array('min' => 12)); // WHERE parcelas >= 12
     * $query->filterByQuota(array('max' => 12)); // WHERE parcelas <= 12
     * </code>
     *
     * @param     mixed $quota The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterByQuota($quota = null, $comparison = null)
    {
        if (is_array($quota)) {
            $useMinMax = false;
            if (isset($quota['min'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::PARCELAS, $quota['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quota['max'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::PARCELAS, $quota['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePricePeer::PARCELAS, $quota, $comparison);
    }

    /**
     * Filter the query on the valor_parcelado column
     *
     * Example usage:
     * <code>
     * $query->filterByInstallmentValue(1234); // WHERE valor_parcelado = 1234
     * $query->filterByInstallmentValue(array(12, 34)); // WHERE valor_parcelado IN (12, 34)
     * $query->filterByInstallmentValue(array('min' => 12)); // WHERE valor_parcelado >= 12
     * $query->filterByInstallmentValue(array('max' => 12)); // WHERE valor_parcelado <= 12
     * </code>
     *
     * @param     mixed $installmentValue The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterByInstallmentValue($installmentValue = null, $comparison = null)
    {
        if (is_array($installmentValue)) {
            $useMinMax = false;
            if (isset($installmentValue['min'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::VALOR_PARCELADO, $installmentValue['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($installmentValue['max'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::VALOR_PARCELADO, $installmentValue['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePricePeer::VALOR_PARCELADO, $installmentValue, $comparison);
    }

    /**
     * Filter the query on the inicio column
     *
     * Example usage:
     * <code>
     * $query->filterByStart('2011-03-14'); // WHERE inicio = '2011-03-14'
     * $query->filterByStart('now'); // WHERE inicio = '2011-03-14'
     * $query->filterByStart(array('max' => 'yesterday')); // WHERE inicio < '2011-03-13'
     * </code>
     *
     * @param     mixed $start The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterByStart($start = null, $comparison = null)
    {
        if (is_array($start)) {
            $useMinMax = false;
            if (isset($start['min'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::INICIO, $start['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($start['max'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::INICIO, $start['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePricePeer::INICIO, $start, $comparison);
    }

    /**
     * Filter the query on the fim column
     *
     * Example usage:
     * <code>
     * $query->filterByEnd('2011-03-14'); // WHERE fim = '2011-03-14'
     * $query->filterByEnd('now'); // WHERE fim = '2011-03-14'
     * $query->filterByEnd(array('max' => 'yesterday')); // WHERE fim < '2011-03-13'
     * </code>
     *
     * @param     mixed $end The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function filterByEnd($end = null, $comparison = null)
    {
        if (is_array($end)) {
            $useMinMax = false;
            if (isset($end['min'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::FIM, $end['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($end['max'])) {
                $this->addUsingAlias(SubscribeTypePricePeer::FIM, $end['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SubscribeTypePricePeer::FIM, $end, $comparison);
    }

    /**
     * Filter the query by a related SubscribeType object
     *
     * @param   SubscribeType|PropelObjectCollection $subscribeType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SubscribeTypePriceQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribeType($subscribeType, $comparison = null)
    {
        if ($subscribeType instanceof SubscribeType) {
            return $this
                ->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, $subscribeType->getId(), $comparison);
        } elseif ($subscribeType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_ID, $subscribeType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySubscribeType() only accepts arguments of type SubscribeType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubscribeType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function joinSubscribeType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubscribeType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubscribeType');
        }

        return $this;
    }

    /**
     * Use the SubscribeType relation SubscribeType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeTypeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribeType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubscribeType', '\AppBundle\Model\SubscribeTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SubscribeTypePrice $subscribeTypePrice Object to remove from the list of results
     *
     * @return SubscribeTypePriceQuery The current query, for fluid interface
     */
    public function prune($subscribeTypePrice = null)
    {
        if ($subscribeTypePrice) {
            $this->addUsingAlias(SubscribeTypePricePeer::INSCRICAO_TIPO_VALOR_ID, $subscribeTypePrice->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
