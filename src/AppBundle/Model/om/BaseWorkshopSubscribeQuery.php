<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Subscribe;
use AppBundle\Model\Workshop;
use AppBundle\Model\WorkshopSubscribe;
use AppBundle\Model\WorkshopSubscribePeer;
use AppBundle\Model\WorkshopSubscribeQuery;

/**
 * @method WorkshopSubscribeQuery orderByWorkshopId($order = Criteria::ASC) Order by the oficina_id column
 * @method WorkshopSubscribeQuery orderBySubscribeId($order = Criteria::ASC) Order by the inscricao_id column
 *
 * @method WorkshopSubscribeQuery groupByWorkshopId() Group by the oficina_id column
 * @method WorkshopSubscribeQuery groupBySubscribeId() Group by the inscricao_id column
 *
 * @method WorkshopSubscribeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method WorkshopSubscribeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method WorkshopSubscribeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method WorkshopSubscribeQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method WorkshopSubscribeQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method WorkshopSubscribeQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method WorkshopSubscribeQuery leftJoinWorkshop($relationAlias = null) Adds a LEFT JOIN clause to the query using the Workshop relation
 * @method WorkshopSubscribeQuery rightJoinWorkshop($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Workshop relation
 * @method WorkshopSubscribeQuery innerJoinWorkshop($relationAlias = null) Adds a INNER JOIN clause to the query using the Workshop relation
 *
 * @method WorkshopSubscribe findOne(PropelPDO $con = null) Return the first WorkshopSubscribe matching the query
 * @method WorkshopSubscribe findOneOrCreate(PropelPDO $con = null) Return the first WorkshopSubscribe matching the query, or a new WorkshopSubscribe object populated from the query conditions when no match is found
 *
 * @method WorkshopSubscribe findOneByWorkshopId(int $oficina_id) Return the first WorkshopSubscribe filtered by the oficina_id column
 * @method WorkshopSubscribe findOneBySubscribeId(int $inscricao_id) Return the first WorkshopSubscribe filtered by the inscricao_id column
 *
 * @method array findByWorkshopId(int $oficina_id) Return WorkshopSubscribe objects filtered by the oficina_id column
 * @method array findBySubscribeId(int $inscricao_id) Return WorkshopSubscribe objects filtered by the inscricao_id column
 */
abstract class BaseWorkshopSubscribeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseWorkshopSubscribeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\WorkshopSubscribe';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new WorkshopSubscribeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   WorkshopSubscribeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return WorkshopSubscribeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof WorkshopSubscribeQuery) {
            return $criteria;
        }
        $query = new WorkshopSubscribeQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$oficina_id, $inscricao_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   WorkshopSubscribe|WorkshopSubscribe[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = WorkshopSubscribePeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(WorkshopSubscribePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 WorkshopSubscribe A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `oficina_id`, `inscricao_id` FROM `oficina_inscricao` WHERE `oficina_id` = :p0 AND `inscricao_id` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new WorkshopSubscribe();
            $obj->hydrate($row);
            WorkshopSubscribePeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return WorkshopSubscribe|WorkshopSubscribe[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|WorkshopSubscribe[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return WorkshopSubscribeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(WorkshopSubscribePeer::OFICINA_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(WorkshopSubscribePeer::INSCRICAO_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return WorkshopSubscribeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(WorkshopSubscribePeer::OFICINA_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(WorkshopSubscribePeer::INSCRICAO_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the oficina_id column
     *
     * Example usage:
     * <code>
     * $query->filterByWorkshopId(1234); // WHERE oficina_id = 1234
     * $query->filterByWorkshopId(array(12, 34)); // WHERE oficina_id IN (12, 34)
     * $query->filterByWorkshopId(array('min' => 12)); // WHERE oficina_id >= 12
     * $query->filterByWorkshopId(array('max' => 12)); // WHERE oficina_id <= 12
     * </code>
     *
     * @see       filterByWorkshop()
     *
     * @param     mixed $workshopId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopSubscribeQuery The current query, for fluid interface
     */
    public function filterByWorkshopId($workshopId = null, $comparison = null)
    {
        if (is_array($workshopId)) {
            $useMinMax = false;
            if (isset($workshopId['min'])) {
                $this->addUsingAlias(WorkshopSubscribePeer::OFICINA_ID, $workshopId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($workshopId['max'])) {
                $this->addUsingAlias(WorkshopSubscribePeer::OFICINA_ID, $workshopId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WorkshopSubscribePeer::OFICINA_ID, $workshopId, $comparison);
    }

    /**
     * Filter the query on the inscricao_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySubscribeId(1234); // WHERE inscricao_id = 1234
     * $query->filterBySubscribeId(array(12, 34)); // WHERE inscricao_id IN (12, 34)
     * $query->filterBySubscribeId(array('min' => 12)); // WHERE inscricao_id >= 12
     * $query->filterBySubscribeId(array('max' => 12)); // WHERE inscricao_id <= 12
     * </code>
     *
     * @see       filterBySubscribe()
     *
     * @param     mixed $subscribeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WorkshopSubscribeQuery The current query, for fluid interface
     */
    public function filterBySubscribeId($subscribeId = null, $comparison = null)
    {
        if (is_array($subscribeId)) {
            $useMinMax = false;
            if (isset($subscribeId['min'])) {
                $this->addUsingAlias(WorkshopSubscribePeer::INSCRICAO_ID, $subscribeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subscribeId['max'])) {
                $this->addUsingAlias(WorkshopSubscribePeer::INSCRICAO_ID, $subscribeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WorkshopSubscribePeer::INSCRICAO_ID, $subscribeId, $comparison);
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 WorkshopSubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(WorkshopSubscribePeer::INSCRICAO_ID, $subscribe->getId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WorkshopSubscribePeer::INSCRICAO_ID, $subscribe->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return WorkshopSubscribeQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Filter the query by a related Workshop object
     *
     * @param   Workshop|PropelObjectCollection $workshop The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 WorkshopSubscribeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWorkshop($workshop, $comparison = null)
    {
        if ($workshop instanceof Workshop) {
            return $this
                ->addUsingAlias(WorkshopSubscribePeer::OFICINA_ID, $workshop->getId(), $comparison);
        } elseif ($workshop instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WorkshopSubscribePeer::OFICINA_ID, $workshop->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByWorkshop() only accepts arguments of type Workshop or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Workshop relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return WorkshopSubscribeQuery The current query, for fluid interface
     */
    public function joinWorkshop($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Workshop');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Workshop');
        }

        return $this;
    }

    /**
     * Use the Workshop relation Workshop object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\WorkshopQuery A secondary query class using the current class as primary query
     */
    public function useWorkshopQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWorkshop($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Workshop', '\AppBundle\Model\WorkshopQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   WorkshopSubscribe $workshopSubscribe Object to remove from the list of results
     *
     * @return WorkshopSubscribeQuery The current query, for fluid interface
     */
    public function prune($workshopSubscribe = null)
    {
        if ($workshopSubscribe) {
            $this->addCond('pruneCond0', $this->getAliasedColName(WorkshopSubscribePeer::OFICINA_ID), $workshopSubscribe->getWorkshopId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(WorkshopSubscribePeer::INSCRICAO_ID), $workshopSubscribe->getSubscribeId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
