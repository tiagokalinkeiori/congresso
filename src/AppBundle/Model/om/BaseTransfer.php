<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\Transfer;
use AppBundle\Model\TransferPeer;
use AppBundle\Model\TransferQuery;

abstract class BaseTransfer extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\TransferPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        TransferPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the inscricao_id field.
     * @var        int
     */
    protected $inscricao_id;

    /**
     * The value for the telefone field.
     * @var        string
     */
    protected $telefone;

    /**
     * The value for the chegada_por field.
     * @var        string
     */
    protected $chegada_por;

    /**
     * The value for the volta_por field.
     * @var        string
     */
    protected $volta_por;

    /**
     * The value for the data_chegada field.
     * @var        string
     */
    protected $data_chegada;

    /**
     * The value for the data_volta field.
     * @var        string
     */
    protected $data_volta;

    /**
     * The value for the dados_chegada field.
     * @var        string
     */
    protected $dados_chegada;

    /**
     * The value for the dados_volta field.
     * @var        string
     */
    protected $dados_volta;

    /**
     * @var        Subscribe
     */
    protected $aSubscribe;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [inscricao_id] column value.
     *
     * @return int
     */
    public function getSubscribeId()
    {

        return $this->inscricao_id;
    }

    /**
     * Get the [telefone] column value.
     *
     * @return string
     */
    public function getPhone()
    {

        return $this->telefone;
    }

    /**
     * Get the [chegada_por] column value.
     *
     * @return string
     */
    public function getArrival()
    {

        return $this->chegada_por;
    }

    /**
     * Get the [volta_por] column value.
     *
     * @return string
     */
    public function getComeBack()
    {

        return $this->volta_por;
    }

    /**
     * Get the [optionally formatted] temporal [data_chegada] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getArrivalDate($format = null)
    {
        if ($this->data_chegada === null) {
            return null;
        }

        if ($this->data_chegada === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_chegada);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_chegada, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [data_volta] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getComeBackDate($format = null)
    {
        if ($this->data_volta === null) {
            return null;
        }

        if ($this->data_volta === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->data_volta);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->data_volta, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [dados_chegada] column value.
     *
     * @return string
     */
    public function getArrivalInformation()
    {

        return $this->dados_chegada;
    }

    /**
     * Get the [dados_volta] column value.
     *
     * @return string
     */
    public function getComeBackInformation()
    {

        return $this->dados_volta;
    }

    /**
     * Set the value of [inscricao_id] column.
     *
     * @param  int $v new value
     * @return Transfer The current object (for fluent API support)
     */
    public function setSubscribeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->inscricao_id !== $v) {
            $this->inscricao_id = $v;
            $this->modifiedColumns[] = TransferPeer::INSCRICAO_ID;
        }

        if ($this->aSubscribe !== null && $this->aSubscribe->getId() !== $v) {
            $this->aSubscribe = null;
        }


        return $this;
    } // setSubscribeId()

    /**
     * Set the value of [telefone] column.
     *
     * @param  string $v new value
     * @return Transfer The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telefone !== $v) {
            $this->telefone = $v;
            $this->modifiedColumns[] = TransferPeer::TELEFONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [chegada_por] column.
     *
     * @param  string $v new value
     * @return Transfer The current object (for fluent API support)
     */
    public function setArrival($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->chegada_por !== $v) {
            $this->chegada_por = $v;
            $this->modifiedColumns[] = TransferPeer::CHEGADA_POR;
        }


        return $this;
    } // setArrival()

    /**
     * Set the value of [volta_por] column.
     *
     * @param  string $v new value
     * @return Transfer The current object (for fluent API support)
     */
    public function setComeBack($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->volta_por !== $v) {
            $this->volta_por = $v;
            $this->modifiedColumns[] = TransferPeer::VOLTA_POR;
        }


        return $this;
    } // setComeBack()

    /**
     * Sets the value of [data_chegada] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Transfer The current object (for fluent API support)
     */
    public function setArrivalDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_chegada !== null || $dt !== null) {
            $currentDateAsString = ($this->data_chegada !== null && $tmpDt = new DateTime($this->data_chegada)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_chegada = $newDateAsString;
                $this->modifiedColumns[] = TransferPeer::DATA_CHEGADA;
            }
        } // if either are not null


        return $this;
    } // setArrivalDate()

    /**
     * Sets the value of [data_volta] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Transfer The current object (for fluent API support)
     */
    public function setComeBackDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_volta !== null || $dt !== null) {
            $currentDateAsString = ($this->data_volta !== null && $tmpDt = new DateTime($this->data_volta)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->data_volta = $newDateAsString;
                $this->modifiedColumns[] = TransferPeer::DATA_VOLTA;
            }
        } // if either are not null


        return $this;
    } // setComeBackDate()

    /**
     * Set the value of [dados_chegada] column.
     *
     * @param  string $v new value
     * @return Transfer The current object (for fluent API support)
     */
    public function setArrivalInformation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dados_chegada !== $v) {
            $this->dados_chegada = $v;
            $this->modifiedColumns[] = TransferPeer::DADOS_CHEGADA;
        }


        return $this;
    } // setArrivalInformation()

    /**
     * Set the value of [dados_volta] column.
     *
     * @param  string $v new value
     * @return Transfer The current object (for fluent API support)
     */
    public function setComeBackInformation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dados_volta !== $v) {
            $this->dados_volta = $v;
            $this->modifiedColumns[] = TransferPeer::DADOS_VOLTA;
        }


        return $this;
    } // setComeBackInformation()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->inscricao_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->telefone = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->chegada_por = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->volta_por = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->data_chegada = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->data_volta = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->dados_chegada = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->dados_volta = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 8; // 8 = TransferPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Transfer object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSubscribe !== null && $this->inscricao_id !== $this->aSubscribe->getId()) {
            $this->aSubscribe = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TransferPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = TransferPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSubscribe = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TransferPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = TransferQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TransferPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TransferPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribe !== null) {
                if ($this->aSubscribe->isModified() || $this->aSubscribe->isNew()) {
                    $affectedRows += $this->aSubscribe->save($con);
                }
                $this->setSubscribe($this->aSubscribe);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(TransferPeer::INSCRICAO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscricao_id`';
        }
        if ($this->isColumnModified(TransferPeer::TELEFONE)) {
            $modifiedColumns[':p' . $index++]  = '`telefone`';
        }
        if ($this->isColumnModified(TransferPeer::CHEGADA_POR)) {
            $modifiedColumns[':p' . $index++]  = '`chegada_por`';
        }
        if ($this->isColumnModified(TransferPeer::VOLTA_POR)) {
            $modifiedColumns[':p' . $index++]  = '`volta_por`';
        }
        if ($this->isColumnModified(TransferPeer::DATA_CHEGADA)) {
            $modifiedColumns[':p' . $index++]  = '`data_chegada`';
        }
        if ($this->isColumnModified(TransferPeer::DATA_VOLTA)) {
            $modifiedColumns[':p' . $index++]  = '`data_volta`';
        }
        if ($this->isColumnModified(TransferPeer::DADOS_CHEGADA)) {
            $modifiedColumns[':p' . $index++]  = '`dados_chegada`';
        }
        if ($this->isColumnModified(TransferPeer::DADOS_VOLTA)) {
            $modifiedColumns[':p' . $index++]  = '`dados_volta`';
        }

        $sql = sprintf(
            'INSERT INTO `translado` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`inscricao_id`':
                        $stmt->bindValue($identifier, $this->inscricao_id, PDO::PARAM_INT);
                        break;
                    case '`telefone`':
                        $stmt->bindValue($identifier, $this->telefone, PDO::PARAM_STR);
                        break;
                    case '`chegada_por`':
                        $stmt->bindValue($identifier, $this->chegada_por, PDO::PARAM_STR);
                        break;
                    case '`volta_por`':
                        $stmt->bindValue($identifier, $this->volta_por, PDO::PARAM_STR);
                        break;
                    case '`data_chegada`':
                        $stmt->bindValue($identifier, $this->data_chegada, PDO::PARAM_STR);
                        break;
                    case '`data_volta`':
                        $stmt->bindValue($identifier, $this->data_volta, PDO::PARAM_STR);
                        break;
                    case '`dados_chegada`':
                        $stmt->bindValue($identifier, $this->dados_chegada, PDO::PARAM_STR);
                        break;
                    case '`dados_volta`':
                        $stmt->bindValue($identifier, $this->dados_volta, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSubscribe !== null) {
                if (!$this->aSubscribe->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSubscribe->getValidationFailures());
                }
            }


            if (($retval = TransferPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = TransferPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getSubscribeId();
                break;
            case 1:
                return $this->getPhone();
                break;
            case 2:
                return $this->getArrival();
                break;
            case 3:
                return $this->getComeBack();
                break;
            case 4:
                return $this->getArrivalDate();
                break;
            case 5:
                return $this->getComeBackDate();
                break;
            case 6:
                return $this->getArrivalInformation();
                break;
            case 7:
                return $this->getComeBackInformation();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Transfer'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Transfer'][$this->getPrimaryKey()] = true;
        $keys = TransferPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getSubscribeId(),
            $keys[1] => $this->getPhone(),
            $keys[2] => $this->getArrival(),
            $keys[3] => $this->getComeBack(),
            $keys[4] => $this->getArrivalDate(),
            $keys[5] => $this->getComeBackDate(),
            $keys[6] => $this->getArrivalInformation(),
            $keys[7] => $this->getComeBackInformation(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSubscribe) {
                $result['Subscribe'] = $this->aSubscribe->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = TransferPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setSubscribeId($value);
                break;
            case 1:
                $this->setPhone($value);
                break;
            case 2:
                $this->setArrival($value);
                break;
            case 3:
                $this->setComeBack($value);
                break;
            case 4:
                $this->setArrivalDate($value);
                break;
            case 5:
                $this->setComeBackDate($value);
                break;
            case 6:
                $this->setArrivalInformation($value);
                break;
            case 7:
                $this->setComeBackInformation($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = TransferPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setSubscribeId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPhone($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setArrival($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setComeBack($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setArrivalDate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setComeBackDate($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setArrivalInformation($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setComeBackInformation($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TransferPeer::DATABASE_NAME);

        if ($this->isColumnModified(TransferPeer::INSCRICAO_ID)) $criteria->add(TransferPeer::INSCRICAO_ID, $this->inscricao_id);
        if ($this->isColumnModified(TransferPeer::TELEFONE)) $criteria->add(TransferPeer::TELEFONE, $this->telefone);
        if ($this->isColumnModified(TransferPeer::CHEGADA_POR)) $criteria->add(TransferPeer::CHEGADA_POR, $this->chegada_por);
        if ($this->isColumnModified(TransferPeer::VOLTA_POR)) $criteria->add(TransferPeer::VOLTA_POR, $this->volta_por);
        if ($this->isColumnModified(TransferPeer::DATA_CHEGADA)) $criteria->add(TransferPeer::DATA_CHEGADA, $this->data_chegada);
        if ($this->isColumnModified(TransferPeer::DATA_VOLTA)) $criteria->add(TransferPeer::DATA_VOLTA, $this->data_volta);
        if ($this->isColumnModified(TransferPeer::DADOS_CHEGADA)) $criteria->add(TransferPeer::DADOS_CHEGADA, $this->dados_chegada);
        if ($this->isColumnModified(TransferPeer::DADOS_VOLTA)) $criteria->add(TransferPeer::DADOS_VOLTA, $this->dados_volta);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(TransferPeer::DATABASE_NAME);
        $criteria->add(TransferPeer::INSCRICAO_ID, $this->inscricao_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getSubscribeId();
    }

    /**
     * Generic method to set the primary key (inscricao_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setSubscribeId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getSubscribeId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Transfer (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPhone($this->getPhone());
        $copyObj->setArrival($this->getArrival());
        $copyObj->setComeBack($this->getComeBack());
        $copyObj->setArrivalDate($this->getArrivalDate());
        $copyObj->setComeBackDate($this->getComeBackDate());
        $copyObj->setArrivalInformation($this->getArrivalInformation());
        $copyObj->setComeBackInformation($this->getComeBackInformation());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            $relObj = $this->getSubscribe();
            if ($relObj) {
                $copyObj->setSubscribe($relObj->copy($deepCopy));
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setSubscribeId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Transfer Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return TransferPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new TransferPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Subscribe object.
     *
     * @param                  Subscribe $v
     * @return Transfer The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSubscribe(Subscribe $v = null)
    {
        if ($v === null) {
            $this->setSubscribeId(NULL);
        } else {
            $this->setSubscribeId($v->getId());
        }

        $this->aSubscribe = $v;

        // Add binding for other direction of this 1:1 relationship.
        if ($v !== null) {
            $v->setTransfer($this);
        }


        return $this;
    }


    /**
     * Get the associated Subscribe object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Subscribe The associated Subscribe object.
     * @throws PropelException
     */
    public function getSubscribe(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSubscribe === null && ($this->inscricao_id !== null) && $doQuery) {
            $this->aSubscribe = SubscribeQuery::create()->findPk($this->inscricao_id, $con);
            // Because this foreign key represents a one-to-one relationship, we will create a bi-directional association.
            $this->aSubscribe->setTransfer($this);
        }

        return $this->aSubscribe;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->inscricao_id = null;
        $this->telefone = null;
        $this->chegada_por = null;
        $this->volta_por = null;
        $this->data_chegada = null;
        $this->data_volta = null;
        $this->dados_chegada = null;
        $this->dados_volta = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSubscribe instanceof Persistent) {
              $this->aSubscribe->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSubscribe = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TransferPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
