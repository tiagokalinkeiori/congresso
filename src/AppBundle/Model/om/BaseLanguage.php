<?php

namespace AppBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Displacement;
use AppBundle\Model\DisplacementQuery;
use AppBundle\Model\Language;
use AppBundle\Model\LanguagePeer;
use AppBundle\Model\LanguageQuery;
use AppBundle\Model\Notice;
use AppBundle\Model\NoticeQuery;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\SubscribeTypeName;
use AppBundle\Model\SubscribeTypeNameQuery;
use AppBundle\Model\Text;
use AppBundle\Model\TextQuery;

abstract class BaseLanguage extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AppBundle\\Model\\LanguagePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        LanguagePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the lingua_id field.
     * @var        int
     */
    protected $lingua_id;

    /**
     * The value for the nome field.
     * @var        string
     */
    protected $nome;

    /**
     * The value for the codigo field.
     * @var        string
     */
    protected $codigo;

    /**
     * @var        PropelObjectCollection|Displacement[] Collection to store aggregation of Displacement objects.
     */
    protected $collDisplacements;
    protected $collDisplacementsPartial;

    /**
     * @var        PropelObjectCollection|Subscribe[] Collection to store aggregation of Subscribe objects.
     */
    protected $collSubscribes;
    protected $collSubscribesPartial;

    /**
     * @var        PropelObjectCollection|SubscribeTypeName[] Collection to store aggregation of SubscribeTypeName objects.
     */
    protected $collSubscribeTypeNames;
    protected $collSubscribeTypeNamesPartial;

    /**
     * @var        PropelObjectCollection|Notice[] Collection to store aggregation of Notice objects.
     */
    protected $collNotices;
    protected $collNoticesPartial;

    /**
     * @var        PropelObjectCollection|Text[] Collection to store aggregation of Text objects.
     */
    protected $collTexts;
    protected $collTextsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $displacementsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $subscribesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $subscribeTypeNamesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $noticesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $textsScheduledForDeletion = null;

    /**
     * Get the [lingua_id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->lingua_id;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->nome;
    }

    /**
     * Get the [codigo] column value.
     *
     * @return string
     */
    public function getCode()
    {

        return $this->codigo;
    }

    /**
     * Set the value of [lingua_id] column.
     *
     * @param  int $v new value
     * @return Language The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->lingua_id !== $v) {
            $this->lingua_id = $v;
            $this->modifiedColumns[] = LanguagePeer::LINGUA_ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nome] column.
     *
     * @param  string $v new value
     * @return Language The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[] = LanguagePeer::NOME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [codigo] column.
     *
     * @param  string $v new value
     * @return Language The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo !== $v) {
            $this->codigo = $v;
            $this->modifiedColumns[] = LanguagePeer::CODIGO;
        }


        return $this;
    } // setCode()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->lingua_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nome = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->codigo = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 3; // 3 = LanguagePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Language object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(LanguagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = LanguagePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collDisplacements = null;

            $this->collSubscribes = null;

            $this->collSubscribeTypeNames = null;

            $this->collNotices = null;

            $this->collTexts = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(LanguagePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = LanguageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(LanguagePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                LanguagePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->displacementsScheduledForDeletion !== null) {
                if (!$this->displacementsScheduledForDeletion->isEmpty()) {
                    DisplacementQuery::create()
                        ->filterByPrimaryKeys($this->displacementsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->displacementsScheduledForDeletion = null;
                }
            }

            if ($this->collDisplacements !== null) {
                foreach ($this->collDisplacements as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->subscribesScheduledForDeletion !== null) {
                if (!$this->subscribesScheduledForDeletion->isEmpty()) {
                    foreach ($this->subscribesScheduledForDeletion as $subscribe) {
                        // need to save related object because we set the relation to null
                        $subscribe->save($con);
                    }
                    $this->subscribesScheduledForDeletion = null;
                }
            }

            if ($this->collSubscribes !== null) {
                foreach ($this->collSubscribes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->subscribeTypeNamesScheduledForDeletion !== null) {
                if (!$this->subscribeTypeNamesScheduledForDeletion->isEmpty()) {
                    SubscribeTypeNameQuery::create()
                        ->filterByPrimaryKeys($this->subscribeTypeNamesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->subscribeTypeNamesScheduledForDeletion = null;
                }
            }

            if ($this->collSubscribeTypeNames !== null) {
                foreach ($this->collSubscribeTypeNames as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->noticesScheduledForDeletion !== null) {
                if (!$this->noticesScheduledForDeletion->isEmpty()) {
                    NoticeQuery::create()
                        ->filterByPrimaryKeys($this->noticesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->noticesScheduledForDeletion = null;
                }
            }

            if ($this->collNotices !== null) {
                foreach ($this->collNotices as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->textsScheduledForDeletion !== null) {
                if (!$this->textsScheduledForDeletion->isEmpty()) {
                    TextQuery::create()
                        ->filterByPrimaryKeys($this->textsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->textsScheduledForDeletion = null;
                }
            }

            if ($this->collTexts !== null) {
                foreach ($this->collTexts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = LanguagePeer::LINGUA_ID;
        if (null !== $this->lingua_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . LanguagePeer::LINGUA_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(LanguagePeer::LINGUA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`lingua_id`';
        }
        if ($this->isColumnModified(LanguagePeer::NOME)) {
            $modifiedColumns[':p' . $index++]  = '`nome`';
        }
        if ($this->isColumnModified(LanguagePeer::CODIGO)) {
            $modifiedColumns[':p' . $index++]  = '`codigo`';
        }

        $sql = sprintf(
            'INSERT INTO `lingua` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`lingua_id`':
                        $stmt->bindValue($identifier, $this->lingua_id, PDO::PARAM_INT);
                        break;
                    case '`nome`':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case '`codigo`':
                        $stmt->bindValue($identifier, $this->codigo, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = LanguagePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collDisplacements !== null) {
                    foreach ($this->collDisplacements as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSubscribes !== null) {
                    foreach ($this->collSubscribes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSubscribeTypeNames !== null) {
                    foreach ($this->collSubscribeTypeNames as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collNotices !== null) {
                    foreach ($this->collNotices as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTexts !== null) {
                    foreach ($this->collTexts as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = LanguagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getCode();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Language'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Language'][$this->getPrimaryKey()] = true;
        $keys = LanguagePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getCode(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collDisplacements) {
                $result['Displacements'] = $this->collDisplacements->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSubscribes) {
                $result['Subscribes'] = $this->collSubscribes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSubscribeTypeNames) {
                $result['SubscribeTypeNames'] = $this->collSubscribeTypeNames->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collNotices) {
                $result['Notices'] = $this->collNotices->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTexts) {
                $result['Texts'] = $this->collTexts->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = LanguagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setCode($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = LanguagePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCode($arr[$keys[2]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(LanguagePeer::DATABASE_NAME);

        if ($this->isColumnModified(LanguagePeer::LINGUA_ID)) $criteria->add(LanguagePeer::LINGUA_ID, $this->lingua_id);
        if ($this->isColumnModified(LanguagePeer::NOME)) $criteria->add(LanguagePeer::NOME, $this->nome);
        if ($this->isColumnModified(LanguagePeer::CODIGO)) $criteria->add(LanguagePeer::CODIGO, $this->codigo);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(LanguagePeer::DATABASE_NAME);
        $criteria->add(LanguagePeer::LINGUA_ID, $this->lingua_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (lingua_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Language (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setCode($this->getCode());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getDisplacements() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDisplacement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSubscribes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSubscribe($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSubscribeTypeNames() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSubscribeTypeName($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getNotices() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addNotice($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTexts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addText($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Language Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return LanguagePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new LanguagePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Displacement' == $relationName) {
            $this->initDisplacements();
        }
        if ('Subscribe' == $relationName) {
            $this->initSubscribes();
        }
        if ('SubscribeTypeName' == $relationName) {
            $this->initSubscribeTypeNames();
        }
        if ('Notice' == $relationName) {
            $this->initNotices();
        }
        if ('Text' == $relationName) {
            $this->initTexts();
        }
    }

    /**
     * Clears out the collDisplacements collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Language The current object (for fluent API support)
     * @see        addDisplacements()
     */
    public function clearDisplacements()
    {
        $this->collDisplacements = null; // important to set this to null since that means it is uninitialized
        $this->collDisplacementsPartial = null;

        return $this;
    }

    /**
     * reset is the collDisplacements collection loaded partially
     *
     * @return void
     */
    public function resetPartialDisplacements($v = true)
    {
        $this->collDisplacementsPartial = $v;
    }

    /**
     * Initializes the collDisplacements collection.
     *
     * By default this just sets the collDisplacements collection to an empty array (like clearcollDisplacements());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDisplacements($overrideExisting = true)
    {
        if (null !== $this->collDisplacements && !$overrideExisting) {
            return;
        }
        $this->collDisplacements = new PropelObjectCollection();
        $this->collDisplacements->setModel('Displacement');
    }

    /**
     * Gets an array of Displacement objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Language is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Displacement[] List of Displacement objects
     * @throws PropelException
     */
    public function getDisplacements($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDisplacementsPartial && !$this->isNew();
        if (null === $this->collDisplacements || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDisplacements) {
                // return empty collection
                $this->initDisplacements();
            } else {
                $collDisplacements = DisplacementQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDisplacementsPartial && count($collDisplacements)) {
                      $this->initDisplacements(false);

                      foreach ($collDisplacements as $obj) {
                        if (false == $this->collDisplacements->contains($obj)) {
                          $this->collDisplacements->append($obj);
                        }
                      }

                      $this->collDisplacementsPartial = true;
                    }

                    $collDisplacements->getInternalIterator()->rewind();

                    return $collDisplacements;
                }

                if ($partial && $this->collDisplacements) {
                    foreach ($this->collDisplacements as $obj) {
                        if ($obj->isNew()) {
                            $collDisplacements[] = $obj;
                        }
                    }
                }

                $this->collDisplacements = $collDisplacements;
                $this->collDisplacementsPartial = false;
            }
        }

        return $this->collDisplacements;
    }

    /**
     * Sets a collection of Displacement objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $displacements A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Language The current object (for fluent API support)
     */
    public function setDisplacements(PropelCollection $displacements, PropelPDO $con = null)
    {
        $displacementsToDelete = $this->getDisplacements(new Criteria(), $con)->diff($displacements);


        $this->displacementsScheduledForDeletion = $displacementsToDelete;

        foreach ($displacementsToDelete as $displacementRemoved) {
            $displacementRemoved->setLanguage(null);
        }

        $this->collDisplacements = null;
        foreach ($displacements as $displacement) {
            $this->addDisplacement($displacement);
        }

        $this->collDisplacements = $displacements;
        $this->collDisplacementsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Displacement objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Displacement objects.
     * @throws PropelException
     */
    public function countDisplacements(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDisplacementsPartial && !$this->isNew();
        if (null === $this->collDisplacements || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDisplacements) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDisplacements());
            }
            $query = DisplacementQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collDisplacements);
    }

    /**
     * Method called to associate a Displacement object to this object
     * through the Displacement foreign key attribute.
     *
     * @param    Displacement $l Displacement
     * @return Language The current object (for fluent API support)
     */
    public function addDisplacement(Displacement $l)
    {
        if ($this->collDisplacements === null) {
            $this->initDisplacements();
            $this->collDisplacementsPartial = true;
        }

        if (!in_array($l, $this->collDisplacements->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDisplacement($l);

            if ($this->displacementsScheduledForDeletion and $this->displacementsScheduledForDeletion->contains($l)) {
                $this->displacementsScheduledForDeletion->remove($this->displacementsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Displacement $displacement The displacement object to add.
     */
    protected function doAddDisplacement($displacement)
    {
        $this->collDisplacements[]= $displacement;
        $displacement->setLanguage($this);
    }

    /**
     * @param	Displacement $displacement The displacement object to remove.
     * @return Language The current object (for fluent API support)
     */
    public function removeDisplacement($displacement)
    {
        if ($this->getDisplacements()->contains($displacement)) {
            $this->collDisplacements->remove($this->collDisplacements->search($displacement));
            if (null === $this->displacementsScheduledForDeletion) {
                $this->displacementsScheduledForDeletion = clone $this->collDisplacements;
                $this->displacementsScheduledForDeletion->clear();
            }
            $this->displacementsScheduledForDeletion[]= clone $displacement;
            $displacement->setLanguage(null);
        }

        return $this;
    }

    /**
     * Clears out the collSubscribes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Language The current object (for fluent API support)
     * @see        addSubscribes()
     */
    public function clearSubscribes()
    {
        $this->collSubscribes = null; // important to set this to null since that means it is uninitialized
        $this->collSubscribesPartial = null;

        return $this;
    }

    /**
     * reset is the collSubscribes collection loaded partially
     *
     * @return void
     */
    public function resetPartialSubscribes($v = true)
    {
        $this->collSubscribesPartial = $v;
    }

    /**
     * Initializes the collSubscribes collection.
     *
     * By default this just sets the collSubscribes collection to an empty array (like clearcollSubscribes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSubscribes($overrideExisting = true)
    {
        if (null !== $this->collSubscribes && !$overrideExisting) {
            return;
        }
        $this->collSubscribes = new PropelObjectCollection();
        $this->collSubscribes->setModel('Subscribe');
    }

    /**
     * Gets an array of Subscribe objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Language is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     * @throws PropelException
     */
    public function getSubscribes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSubscribesPartial && !$this->isNew();
        if (null === $this->collSubscribes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSubscribes) {
                // return empty collection
                $this->initSubscribes();
            } else {
                $collSubscribes = SubscribeQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSubscribesPartial && count($collSubscribes)) {
                      $this->initSubscribes(false);

                      foreach ($collSubscribes as $obj) {
                        if (false == $this->collSubscribes->contains($obj)) {
                          $this->collSubscribes->append($obj);
                        }
                      }

                      $this->collSubscribesPartial = true;
                    }

                    $collSubscribes->getInternalIterator()->rewind();

                    return $collSubscribes;
                }

                if ($partial && $this->collSubscribes) {
                    foreach ($this->collSubscribes as $obj) {
                        if ($obj->isNew()) {
                            $collSubscribes[] = $obj;
                        }
                    }
                }

                $this->collSubscribes = $collSubscribes;
                $this->collSubscribesPartial = false;
            }
        }

        return $this->collSubscribes;
    }

    /**
     * Sets a collection of Subscribe objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $subscribes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Language The current object (for fluent API support)
     */
    public function setSubscribes(PropelCollection $subscribes, PropelPDO $con = null)
    {
        $subscribesToDelete = $this->getSubscribes(new Criteria(), $con)->diff($subscribes);


        $this->subscribesScheduledForDeletion = $subscribesToDelete;

        foreach ($subscribesToDelete as $subscribeRemoved) {
            $subscribeRemoved->setLanguage(null);
        }

        $this->collSubscribes = null;
        foreach ($subscribes as $subscribe) {
            $this->addSubscribe($subscribe);
        }

        $this->collSubscribes = $subscribes;
        $this->collSubscribesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Subscribe objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Subscribe objects.
     * @throws PropelException
     */
    public function countSubscribes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSubscribesPartial && !$this->isNew();
        if (null === $this->collSubscribes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSubscribes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSubscribes());
            }
            $query = SubscribeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collSubscribes);
    }

    /**
     * Method called to associate a Subscribe object to this object
     * through the Subscribe foreign key attribute.
     *
     * @param    Subscribe $l Subscribe
     * @return Language The current object (for fluent API support)
     */
    public function addSubscribe(Subscribe $l)
    {
        if ($this->collSubscribes === null) {
            $this->initSubscribes();
            $this->collSubscribesPartial = true;
        }

        if (!in_array($l, $this->collSubscribes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSubscribe($l);

            if ($this->subscribesScheduledForDeletion and $this->subscribesScheduledForDeletion->contains($l)) {
                $this->subscribesScheduledForDeletion->remove($this->subscribesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Subscribe $subscribe The subscribe object to add.
     */
    protected function doAddSubscribe($subscribe)
    {
        $this->collSubscribes[]= $subscribe;
        $subscribe->setLanguage($this);
    }

    /**
     * @param	Subscribe $subscribe The subscribe object to remove.
     * @return Language The current object (for fluent API support)
     */
    public function removeSubscribe($subscribe)
    {
        if ($this->getSubscribes()->contains($subscribe)) {
            $this->collSubscribes->remove($this->collSubscribes->search($subscribe));
            if (null === $this->subscribesScheduledForDeletion) {
                $this->subscribesScheduledForDeletion = clone $this->collSubscribes;
                $this->subscribesScheduledForDeletion->clear();
            }
            $this->subscribesScheduledForDeletion[]= $subscribe;
            $subscribe->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinDistrict($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('District', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinSubscribeType($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('SubscribeType', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinDisplacement($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('Displacement', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinState($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('State', $join_behavior);

        return $this->getSubscribes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Subscribes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Subscribe[] List of Subscribe objects
     */
    public function getSubscribesJoinCountry($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeQuery::create(null, $criteria);
        $query->joinWith('Country', $join_behavior);

        return $this->getSubscribes($query, $con);
    }

    /**
     * Clears out the collSubscribeTypeNames collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Language The current object (for fluent API support)
     * @see        addSubscribeTypeNames()
     */
    public function clearSubscribeTypeNames()
    {
        $this->collSubscribeTypeNames = null; // important to set this to null since that means it is uninitialized
        $this->collSubscribeTypeNamesPartial = null;

        return $this;
    }

    /**
     * reset is the collSubscribeTypeNames collection loaded partially
     *
     * @return void
     */
    public function resetPartialSubscribeTypeNames($v = true)
    {
        $this->collSubscribeTypeNamesPartial = $v;
    }

    /**
     * Initializes the collSubscribeTypeNames collection.
     *
     * By default this just sets the collSubscribeTypeNames collection to an empty array (like clearcollSubscribeTypeNames());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSubscribeTypeNames($overrideExisting = true)
    {
        if (null !== $this->collSubscribeTypeNames && !$overrideExisting) {
            return;
        }
        $this->collSubscribeTypeNames = new PropelObjectCollection();
        $this->collSubscribeTypeNames->setModel('SubscribeTypeName');
    }

    /**
     * Gets an array of SubscribeTypeName objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Language is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SubscribeTypeName[] List of SubscribeTypeName objects
     * @throws PropelException
     */
    public function getSubscribeTypeNames($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSubscribeTypeNamesPartial && !$this->isNew();
        if (null === $this->collSubscribeTypeNames || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSubscribeTypeNames) {
                // return empty collection
                $this->initSubscribeTypeNames();
            } else {
                $collSubscribeTypeNames = SubscribeTypeNameQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSubscribeTypeNamesPartial && count($collSubscribeTypeNames)) {
                      $this->initSubscribeTypeNames(false);

                      foreach ($collSubscribeTypeNames as $obj) {
                        if (false == $this->collSubscribeTypeNames->contains($obj)) {
                          $this->collSubscribeTypeNames->append($obj);
                        }
                      }

                      $this->collSubscribeTypeNamesPartial = true;
                    }

                    $collSubscribeTypeNames->getInternalIterator()->rewind();

                    return $collSubscribeTypeNames;
                }

                if ($partial && $this->collSubscribeTypeNames) {
                    foreach ($this->collSubscribeTypeNames as $obj) {
                        if ($obj->isNew()) {
                            $collSubscribeTypeNames[] = $obj;
                        }
                    }
                }

                $this->collSubscribeTypeNames = $collSubscribeTypeNames;
                $this->collSubscribeTypeNamesPartial = false;
            }
        }

        return $this->collSubscribeTypeNames;
    }

    /**
     * Sets a collection of SubscribeTypeName objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $subscribeTypeNames A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Language The current object (for fluent API support)
     */
    public function setSubscribeTypeNames(PropelCollection $subscribeTypeNames, PropelPDO $con = null)
    {
        $subscribeTypeNamesToDelete = $this->getSubscribeTypeNames(new Criteria(), $con)->diff($subscribeTypeNames);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->subscribeTypeNamesScheduledForDeletion = clone $subscribeTypeNamesToDelete;

        foreach ($subscribeTypeNamesToDelete as $subscribeTypeNameRemoved) {
            $subscribeTypeNameRemoved->setLanguage(null);
        }

        $this->collSubscribeTypeNames = null;
        foreach ($subscribeTypeNames as $subscribeTypeName) {
            $this->addSubscribeTypeName($subscribeTypeName);
        }

        $this->collSubscribeTypeNames = $subscribeTypeNames;
        $this->collSubscribeTypeNamesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SubscribeTypeName objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SubscribeTypeName objects.
     * @throws PropelException
     */
    public function countSubscribeTypeNames(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSubscribeTypeNamesPartial && !$this->isNew();
        if (null === $this->collSubscribeTypeNames || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSubscribeTypeNames) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSubscribeTypeNames());
            }
            $query = SubscribeTypeNameQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collSubscribeTypeNames);
    }

    /**
     * Method called to associate a SubscribeTypeName object to this object
     * through the SubscribeTypeName foreign key attribute.
     *
     * @param    SubscribeTypeName $l SubscribeTypeName
     * @return Language The current object (for fluent API support)
     */
    public function addSubscribeTypeName(SubscribeTypeName $l)
    {
        if ($this->collSubscribeTypeNames === null) {
            $this->initSubscribeTypeNames();
            $this->collSubscribeTypeNamesPartial = true;
        }

        if (!in_array($l, $this->collSubscribeTypeNames->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSubscribeTypeName($l);

            if ($this->subscribeTypeNamesScheduledForDeletion and $this->subscribeTypeNamesScheduledForDeletion->contains($l)) {
                $this->subscribeTypeNamesScheduledForDeletion->remove($this->subscribeTypeNamesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SubscribeTypeName $subscribeTypeName The subscribeTypeName object to add.
     */
    protected function doAddSubscribeTypeName($subscribeTypeName)
    {
        $this->collSubscribeTypeNames[]= $subscribeTypeName;
        $subscribeTypeName->setLanguage($this);
    }

    /**
     * @param	SubscribeTypeName $subscribeTypeName The subscribeTypeName object to remove.
     * @return Language The current object (for fluent API support)
     */
    public function removeSubscribeTypeName($subscribeTypeName)
    {
        if ($this->getSubscribeTypeNames()->contains($subscribeTypeName)) {
            $this->collSubscribeTypeNames->remove($this->collSubscribeTypeNames->search($subscribeTypeName));
            if (null === $this->subscribeTypeNamesScheduledForDeletion) {
                $this->subscribeTypeNamesScheduledForDeletion = clone $this->collSubscribeTypeNames;
                $this->subscribeTypeNamesScheduledForDeletion->clear();
            }
            $this->subscribeTypeNamesScheduledForDeletion[]= clone $subscribeTypeName;
            $subscribeTypeName->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SubscribeTypeNames from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SubscribeTypeName[] List of SubscribeTypeName objects
     */
    public function getSubscribeTypeNamesJoinSubscribeType($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SubscribeTypeNameQuery::create(null, $criteria);
        $query->joinWith('SubscribeType', $join_behavior);

        return $this->getSubscribeTypeNames($query, $con);
    }

    /**
     * Clears out the collNotices collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Language The current object (for fluent API support)
     * @see        addNotices()
     */
    public function clearNotices()
    {
        $this->collNotices = null; // important to set this to null since that means it is uninitialized
        $this->collNoticesPartial = null;

        return $this;
    }

    /**
     * reset is the collNotices collection loaded partially
     *
     * @return void
     */
    public function resetPartialNotices($v = true)
    {
        $this->collNoticesPartial = $v;
    }

    /**
     * Initializes the collNotices collection.
     *
     * By default this just sets the collNotices collection to an empty array (like clearcollNotices());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initNotices($overrideExisting = true)
    {
        if (null !== $this->collNotices && !$overrideExisting) {
            return;
        }
        $this->collNotices = new PropelObjectCollection();
        $this->collNotices->setModel('Notice');
    }

    /**
     * Gets an array of Notice objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Language is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Notice[] List of Notice objects
     * @throws PropelException
     */
    public function getNotices($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collNoticesPartial && !$this->isNew();
        if (null === $this->collNotices || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collNotices) {
                // return empty collection
                $this->initNotices();
            } else {
                $collNotices = NoticeQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collNoticesPartial && count($collNotices)) {
                      $this->initNotices(false);

                      foreach ($collNotices as $obj) {
                        if (false == $this->collNotices->contains($obj)) {
                          $this->collNotices->append($obj);
                        }
                      }

                      $this->collNoticesPartial = true;
                    }

                    $collNotices->getInternalIterator()->rewind();

                    return $collNotices;
                }

                if ($partial && $this->collNotices) {
                    foreach ($this->collNotices as $obj) {
                        if ($obj->isNew()) {
                            $collNotices[] = $obj;
                        }
                    }
                }

                $this->collNotices = $collNotices;
                $this->collNoticesPartial = false;
            }
        }

        return $this->collNotices;
    }

    /**
     * Sets a collection of Notice objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $notices A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Language The current object (for fluent API support)
     */
    public function setNotices(PropelCollection $notices, PropelPDO $con = null)
    {
        $noticesToDelete = $this->getNotices(new Criteria(), $con)->diff($notices);


        $this->noticesScheduledForDeletion = $noticesToDelete;

        foreach ($noticesToDelete as $noticeRemoved) {
            $noticeRemoved->setLanguage(null);
        }

        $this->collNotices = null;
        foreach ($notices as $notice) {
            $this->addNotice($notice);
        }

        $this->collNotices = $notices;
        $this->collNoticesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Notice objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Notice objects.
     * @throws PropelException
     */
    public function countNotices(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collNoticesPartial && !$this->isNew();
        if (null === $this->collNotices || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collNotices) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getNotices());
            }
            $query = NoticeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collNotices);
    }

    /**
     * Method called to associate a Notice object to this object
     * through the Notice foreign key attribute.
     *
     * @param    Notice $l Notice
     * @return Language The current object (for fluent API support)
     */
    public function addNotice(Notice $l)
    {
        if ($this->collNotices === null) {
            $this->initNotices();
            $this->collNoticesPartial = true;
        }

        if (!in_array($l, $this->collNotices->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddNotice($l);

            if ($this->noticesScheduledForDeletion and $this->noticesScheduledForDeletion->contains($l)) {
                $this->noticesScheduledForDeletion->remove($this->noticesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Notice $notice The notice object to add.
     */
    protected function doAddNotice($notice)
    {
        $this->collNotices[]= $notice;
        $notice->setLanguage($this);
    }

    /**
     * @param	Notice $notice The notice object to remove.
     * @return Language The current object (for fluent API support)
     */
    public function removeNotice($notice)
    {
        if ($this->getNotices()->contains($notice)) {
            $this->collNotices->remove($this->collNotices->search($notice));
            if (null === $this->noticesScheduledForDeletion) {
                $this->noticesScheduledForDeletion = clone $this->collNotices;
                $this->noticesScheduledForDeletion->clear();
            }
            $this->noticesScheduledForDeletion[]= clone $notice;
            $notice->setLanguage(null);
        }

        return $this;
    }

    /**
     * Clears out the collTexts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Language The current object (for fluent API support)
     * @see        addTexts()
     */
    public function clearTexts()
    {
        $this->collTexts = null; // important to set this to null since that means it is uninitialized
        $this->collTextsPartial = null;

        return $this;
    }

    /**
     * reset is the collTexts collection loaded partially
     *
     * @return void
     */
    public function resetPartialTexts($v = true)
    {
        $this->collTextsPartial = $v;
    }

    /**
     * Initializes the collTexts collection.
     *
     * By default this just sets the collTexts collection to an empty array (like clearcollTexts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTexts($overrideExisting = true)
    {
        if (null !== $this->collTexts && !$overrideExisting) {
            return;
        }
        $this->collTexts = new PropelObjectCollection();
        $this->collTexts->setModel('Text');
    }

    /**
     * Gets an array of Text objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Language is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Text[] List of Text objects
     * @throws PropelException
     */
    public function getTexts($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTextsPartial && !$this->isNew();
        if (null === $this->collTexts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTexts) {
                // return empty collection
                $this->initTexts();
            } else {
                $collTexts = TextQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTextsPartial && count($collTexts)) {
                      $this->initTexts(false);

                      foreach ($collTexts as $obj) {
                        if (false == $this->collTexts->contains($obj)) {
                          $this->collTexts->append($obj);
                        }
                      }

                      $this->collTextsPartial = true;
                    }

                    $collTexts->getInternalIterator()->rewind();

                    return $collTexts;
                }

                if ($partial && $this->collTexts) {
                    foreach ($this->collTexts as $obj) {
                        if ($obj->isNew()) {
                            $collTexts[] = $obj;
                        }
                    }
                }

                $this->collTexts = $collTexts;
                $this->collTextsPartial = false;
            }
        }

        return $this->collTexts;
    }

    /**
     * Sets a collection of Text objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $texts A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Language The current object (for fluent API support)
     */
    public function setTexts(PropelCollection $texts, PropelPDO $con = null)
    {
        $textsToDelete = $this->getTexts(new Criteria(), $con)->diff($texts);


        $this->textsScheduledForDeletion = $textsToDelete;

        foreach ($textsToDelete as $textRemoved) {
            $textRemoved->setLanguage(null);
        }

        $this->collTexts = null;
        foreach ($texts as $text) {
            $this->addText($text);
        }

        $this->collTexts = $texts;
        $this->collTextsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Text objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Text objects.
     * @throws PropelException
     */
    public function countTexts(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTextsPartial && !$this->isNew();
        if (null === $this->collTexts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTexts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTexts());
            }
            $query = TextQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collTexts);
    }

    /**
     * Method called to associate a Text object to this object
     * through the Text foreign key attribute.
     *
     * @param    Text $l Text
     * @return Language The current object (for fluent API support)
     */
    public function addText(Text $l)
    {
        if ($this->collTexts === null) {
            $this->initTexts();
            $this->collTextsPartial = true;
        }

        if (!in_array($l, $this->collTexts->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddText($l);

            if ($this->textsScheduledForDeletion and $this->textsScheduledForDeletion->contains($l)) {
                $this->textsScheduledForDeletion->remove($this->textsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Text $text The text object to add.
     */
    protected function doAddText($text)
    {
        $this->collTexts[]= $text;
        $text->setLanguage($this);
    }

    /**
     * @param	Text $text The text object to remove.
     * @return Language The current object (for fluent API support)
     */
    public function removeText($text)
    {
        if ($this->getTexts()->contains($text)) {
            $this->collTexts->remove($this->collTexts->search($text));
            if (null === $this->textsScheduledForDeletion) {
                $this->textsScheduledForDeletion = clone $this->collTexts;
                $this->textsScheduledForDeletion->clear();
            }
            $this->textsScheduledForDeletion[]= clone $text;
            $text->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Texts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Text[] List of Text objects
     */
    public function getTextsJoinPage($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TextQuery::create(null, $criteria);
        $query->joinWith('Page', $join_behavior);

        return $this->getTexts($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->lingua_id = null;
        $this->nome = null;
        $this->codigo = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collDisplacements) {
                foreach ($this->collDisplacements as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSubscribes) {
                foreach ($this->collSubscribes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSubscribeTypeNames) {
                foreach ($this->collSubscribeTypeNames as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collNotices) {
                foreach ($this->collNotices as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTexts) {
                foreach ($this->collTexts as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collDisplacements instanceof PropelCollection) {
            $this->collDisplacements->clearIterator();
        }
        $this->collDisplacements = null;
        if ($this->collSubscribes instanceof PropelCollection) {
            $this->collSubscribes->clearIterator();
        }
        $this->collSubscribes = null;
        if ($this->collSubscribeTypeNames instanceof PropelCollection) {
            $this->collSubscribeTypeNames->clearIterator();
        }
        $this->collSubscribeTypeNames = null;
        if ($this->collNotices instanceof PropelCollection) {
            $this->collNotices->clearIterator();
        }
        $this->collNotices = null;
        if ($this->collTexts instanceof PropelCollection) {
            $this->collTexts->clearIterator();
        }
        $this->collTexts = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(LanguagePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
