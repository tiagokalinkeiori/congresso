<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Displacement;
use AppBundle\Model\Language;
use AppBundle\Model\LanguagePeer;
use AppBundle\Model\LanguageQuery;
use AppBundle\Model\Notice;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribeTypeName;
use AppBundle\Model\Text;

/**
 * @method LanguageQuery orderById($order = Criteria::ASC) Order by the lingua_id column
 * @method LanguageQuery orderByName($order = Criteria::ASC) Order by the nome column
 * @method LanguageQuery orderByCode($order = Criteria::ASC) Order by the codigo column
 *
 * @method LanguageQuery groupById() Group by the lingua_id column
 * @method LanguageQuery groupByName() Group by the nome column
 * @method LanguageQuery groupByCode() Group by the codigo column
 *
 * @method LanguageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LanguageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LanguageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LanguageQuery leftJoinDisplacement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Displacement relation
 * @method LanguageQuery rightJoinDisplacement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Displacement relation
 * @method LanguageQuery innerJoinDisplacement($relationAlias = null) Adds a INNER JOIN clause to the query using the Displacement relation
 *
 * @method LanguageQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method LanguageQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method LanguageQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method LanguageQuery leftJoinSubscribeTypeName($relationAlias = null) Adds a LEFT JOIN clause to the query using the SubscribeTypeName relation
 * @method LanguageQuery rightJoinSubscribeTypeName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SubscribeTypeName relation
 * @method LanguageQuery innerJoinSubscribeTypeName($relationAlias = null) Adds a INNER JOIN clause to the query using the SubscribeTypeName relation
 *
 * @method LanguageQuery leftJoinNotice($relationAlias = null) Adds a LEFT JOIN clause to the query using the Notice relation
 * @method LanguageQuery rightJoinNotice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Notice relation
 * @method LanguageQuery innerJoinNotice($relationAlias = null) Adds a INNER JOIN clause to the query using the Notice relation
 *
 * @method LanguageQuery leftJoinText($relationAlias = null) Adds a LEFT JOIN clause to the query using the Text relation
 * @method LanguageQuery rightJoinText($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Text relation
 * @method LanguageQuery innerJoinText($relationAlias = null) Adds a INNER JOIN clause to the query using the Text relation
 *
 * @method Language findOne(PropelPDO $con = null) Return the first Language matching the query
 * @method Language findOneOrCreate(PropelPDO $con = null) Return the first Language matching the query, or a new Language object populated from the query conditions when no match is found
 *
 * @method Language findOneByName(string $nome) Return the first Language filtered by the nome column
 * @method Language findOneByCode(string $codigo) Return the first Language filtered by the codigo column
 *
 * @method array findById(int $lingua_id) Return Language objects filtered by the lingua_id column
 * @method array findByName(string $nome) Return Language objects filtered by the nome column
 * @method array findByCode(string $codigo) Return Language objects filtered by the codigo column
 */
abstract class BaseLanguageQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLanguageQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\Language';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LanguageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LanguageQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LanguageQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LanguageQuery) {
            return $criteria;
        }
        $query = new LanguageQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Language|Language[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LanguagePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LanguagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Language A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Language A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `lingua_id`, `nome`, `codigo` FROM `lingua` WHERE `lingua_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Language();
            $obj->hydrate($row);
            LanguagePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Language|Language[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Language[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LanguagePeer::LINGUA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LanguagePeer::LINGUA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the lingua_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE lingua_id = 1234
     * $query->filterById(array(12, 34)); // WHERE lingua_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE lingua_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE lingua_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LanguagePeer::LINGUA_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LanguagePeer::LINGUA_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LanguagePeer::LINGUA_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LanguagePeer::NOME, $name, $comparison);
    }

    /**
     * Filter the query on the codigo column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE codigo = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE codigo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LanguagePeer::CODIGO, $code, $comparison);
    }

    /**
     * Filter the query by a related Displacement object
     *
     * @param   Displacement|PropelObjectCollection $displacement  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LanguageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDisplacement($displacement, $comparison = null)
    {
        if ($displacement instanceof Displacement) {
            return $this
                ->addUsingAlias(LanguagePeer::LINGUA_ID, $displacement->getLanguageId(), $comparison);
        } elseif ($displacement instanceof PropelObjectCollection) {
            return $this
                ->useDisplacementQuery()
                ->filterByPrimaryKeys($displacement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDisplacement() only accepts arguments of type Displacement or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Displacement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function joinDisplacement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Displacement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Displacement');
        }

        return $this;
    }

    /**
     * Use the Displacement relation Displacement object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\DisplacementQuery A secondary query class using the current class as primary query
     */
    public function useDisplacementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDisplacement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Displacement', '\AppBundle\Model\DisplacementQuery');
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LanguageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(LanguagePeer::LINGUA_ID, $subscribe->getLanguageId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            return $this
                ->useSubscribeQuery()
                ->filterByPrimaryKeys($subscribe->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Filter the query by a related SubscribeTypeName object
     *
     * @param   SubscribeTypeName|PropelObjectCollection $subscribeTypeName  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LanguageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribeTypeName($subscribeTypeName, $comparison = null)
    {
        if ($subscribeTypeName instanceof SubscribeTypeName) {
            return $this
                ->addUsingAlias(LanguagePeer::LINGUA_ID, $subscribeTypeName->getLanguageId(), $comparison);
        } elseif ($subscribeTypeName instanceof PropelObjectCollection) {
            return $this
                ->useSubscribeTypeNameQuery()
                ->filterByPrimaryKeys($subscribeTypeName->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySubscribeTypeName() only accepts arguments of type SubscribeTypeName or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SubscribeTypeName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function joinSubscribeTypeName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SubscribeTypeName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SubscribeTypeName');
        }

        return $this;
    }

    /**
     * Use the SubscribeTypeName relation SubscribeTypeName object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeTypeNameQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeTypeNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribeTypeName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SubscribeTypeName', '\AppBundle\Model\SubscribeTypeNameQuery');
    }

    /**
     * Filter the query by a related Notice object
     *
     * @param   Notice|PropelObjectCollection $notice  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LanguageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByNotice($notice, $comparison = null)
    {
        if ($notice instanceof Notice) {
            return $this
                ->addUsingAlias(LanguagePeer::LINGUA_ID, $notice->getLanguageId(), $comparison);
        } elseif ($notice instanceof PropelObjectCollection) {
            return $this
                ->useNoticeQuery()
                ->filterByPrimaryKeys($notice->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByNotice() only accepts arguments of type Notice or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Notice relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function joinNotice($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Notice');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Notice');
        }

        return $this;
    }

    /**
     * Use the Notice relation Notice object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\NoticeQuery A secondary query class using the current class as primary query
     */
    public function useNoticeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinNotice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Notice', '\AppBundle\Model\NoticeQuery');
    }

    /**
     * Filter the query by a related Text object
     *
     * @param   Text|PropelObjectCollection $text  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LanguageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByText($text, $comparison = null)
    {
        if ($text instanceof Text) {
            return $this
                ->addUsingAlias(LanguagePeer::LINGUA_ID, $text->getLanguageId(), $comparison);
        } elseif ($text instanceof PropelObjectCollection) {
            return $this
                ->useTextQuery()
                ->filterByPrimaryKeys($text->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByText() only accepts arguments of type Text or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Text relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function joinText($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Text');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Text');
        }

        return $this;
    }

    /**
     * Use the Text relation Text object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\TextQuery A secondary query class using the current class as primary query
     */
    public function useTextQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinText($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Text', '\AppBundle\Model\TextQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Language $language Object to remove from the list of results
     *
     * @return LanguageQuery The current query, for fluid interface
     */
    public function prune($language = null)
    {
        if ($language) {
            $this->addUsingAlias(LanguagePeer::LINGUA_ID, $language->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
