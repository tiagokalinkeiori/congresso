<?php

namespace AppBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use AppBundle\Model\Subscribe;
use AppBundle\Model\Train;
use AppBundle\Model\TrainPeer;
use AppBundle\Model\TrainQuery;

/**
 * @method TrainQuery orderById($order = Criteria::ASC) Order by the caravana_id column
 * @method TrainQuery orderBySubscribeId($order = Criteria::ASC) Order by the inscricao_id column
 * @method TrainQuery orderByResponsibleName($order = Criteria::ASC) Order by the nome_responsavel column
 * @method TrainQuery orderByResponsibleEmail($order = Criteria::ASC) Order by the email_responsabel column
 * @method TrainQuery orderByResponsiblePhone($order = Criteria::ASC) Order by the telefone_responsavel column
 * @method TrainQuery orderBySpace($order = Criteria::ASC) Order by the vagas column
 * @method TrainQuery orderByAvailable($order = Criteria::ASC) Order by the disponiveis column
 * @method TrainQuery orderByDescription($order = Criteria::ASC) Order by the descricao column
 *
 * @method TrainQuery groupById() Group by the caravana_id column
 * @method TrainQuery groupBySubscribeId() Group by the inscricao_id column
 * @method TrainQuery groupByResponsibleName() Group by the nome_responsavel column
 * @method TrainQuery groupByResponsibleEmail() Group by the email_responsabel column
 * @method TrainQuery groupByResponsiblePhone() Group by the telefone_responsavel column
 * @method TrainQuery groupBySpace() Group by the vagas column
 * @method TrainQuery groupByAvailable() Group by the disponiveis column
 * @method TrainQuery groupByDescription() Group by the descricao column
 *
 * @method TrainQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method TrainQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method TrainQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method TrainQuery leftJoinSubscribe($relationAlias = null) Adds a LEFT JOIN clause to the query using the Subscribe relation
 * @method TrainQuery rightJoinSubscribe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Subscribe relation
 * @method TrainQuery innerJoinSubscribe($relationAlias = null) Adds a INNER JOIN clause to the query using the Subscribe relation
 *
 * @method Train findOne(PropelPDO $con = null) Return the first Train matching the query
 * @method Train findOneOrCreate(PropelPDO $con = null) Return the first Train matching the query, or a new Train object populated from the query conditions when no match is found
 *
 * @method Train findOneBySubscribeId(int $inscricao_id) Return the first Train filtered by the inscricao_id column
 * @method Train findOneByResponsibleName(string $nome_responsavel) Return the first Train filtered by the nome_responsavel column
 * @method Train findOneByResponsibleEmail(string $email_responsabel) Return the first Train filtered by the email_responsabel column
 * @method Train findOneByResponsiblePhone(string $telefone_responsavel) Return the first Train filtered by the telefone_responsavel column
 * @method Train findOneBySpace(int $vagas) Return the first Train filtered by the vagas column
 * @method Train findOneByAvailable(int $disponiveis) Return the first Train filtered by the disponiveis column
 * @method Train findOneByDescription(string $descricao) Return the first Train filtered by the descricao column
 *
 * @method array findById(int $caravana_id) Return Train objects filtered by the caravana_id column
 * @method array findBySubscribeId(int $inscricao_id) Return Train objects filtered by the inscricao_id column
 * @method array findByResponsibleName(string $nome_responsavel) Return Train objects filtered by the nome_responsavel column
 * @method array findByResponsibleEmail(string $email_responsabel) Return Train objects filtered by the email_responsabel column
 * @method array findByResponsiblePhone(string $telefone_responsavel) Return Train objects filtered by the telefone_responsavel column
 * @method array findBySpace(int $vagas) Return Train objects filtered by the vagas column
 * @method array findByAvailable(int $disponiveis) Return Train objects filtered by the disponiveis column
 * @method array findByDescription(string $descricao) Return Train objects filtered by the descricao column
 */
abstract class BaseTrainQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseTrainQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'AppBundle\\Model\\Train';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new TrainQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   TrainQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return TrainQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof TrainQuery) {
            return $criteria;
        }
        $query = new TrainQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Train|Train[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TrainPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(TrainPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Train A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Train A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `caravana_id`, `inscricao_id`, `nome_responsavel`, `email_responsabel`, `telefone_responsavel`, `vagas`, `disponiveis`, `descricao` FROM `caravana` WHERE `caravana_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Train();
            $obj->hydrate($row);
            TrainPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Train|Train[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Train[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TrainPeer::CARAVANA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TrainPeer::CARAVANA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the caravana_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE caravana_id = 1234
     * $query->filterById(array(12, 34)); // WHERE caravana_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE caravana_id >= 12
     * $query->filterById(array('max' => 12)); // WHERE caravana_id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TrainPeer::CARAVANA_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TrainPeer::CARAVANA_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TrainPeer::CARAVANA_ID, $id, $comparison);
    }

    /**
     * Filter the query on the inscricao_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySubscribeId(1234); // WHERE inscricao_id = 1234
     * $query->filterBySubscribeId(array(12, 34)); // WHERE inscricao_id IN (12, 34)
     * $query->filterBySubscribeId(array('min' => 12)); // WHERE inscricao_id >= 12
     * $query->filterBySubscribeId(array('max' => 12)); // WHERE inscricao_id <= 12
     * </code>
     *
     * @see       filterBySubscribe()
     *
     * @param     mixed $subscribeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterBySubscribeId($subscribeId = null, $comparison = null)
    {
        if (is_array($subscribeId)) {
            $useMinMax = false;
            if (isset($subscribeId['min'])) {
                $this->addUsingAlias(TrainPeer::INSCRICAO_ID, $subscribeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subscribeId['max'])) {
                $this->addUsingAlias(TrainPeer::INSCRICAO_ID, $subscribeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TrainPeer::INSCRICAO_ID, $subscribeId, $comparison);
    }

    /**
     * Filter the query on the nome_responsavel column
     *
     * Example usage:
     * <code>
     * $query->filterByResponsibleName('fooValue');   // WHERE nome_responsavel = 'fooValue'
     * $query->filterByResponsibleName('%fooValue%'); // WHERE nome_responsavel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $responsibleName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterByResponsibleName($responsibleName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($responsibleName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $responsibleName)) {
                $responsibleName = str_replace('*', '%', $responsibleName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TrainPeer::NOME_RESPONSAVEL, $responsibleName, $comparison);
    }

    /**
     * Filter the query on the email_responsabel column
     *
     * Example usage:
     * <code>
     * $query->filterByResponsibleEmail('fooValue');   // WHERE email_responsabel = 'fooValue'
     * $query->filterByResponsibleEmail('%fooValue%'); // WHERE email_responsabel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $responsibleEmail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterByResponsibleEmail($responsibleEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($responsibleEmail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $responsibleEmail)) {
                $responsibleEmail = str_replace('*', '%', $responsibleEmail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TrainPeer::EMAIL_RESPONSABEL, $responsibleEmail, $comparison);
    }

    /**
     * Filter the query on the telefone_responsavel column
     *
     * Example usage:
     * <code>
     * $query->filterByResponsiblePhone('fooValue');   // WHERE telefone_responsavel = 'fooValue'
     * $query->filterByResponsiblePhone('%fooValue%'); // WHERE telefone_responsavel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $responsiblePhone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterByResponsiblePhone($responsiblePhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($responsiblePhone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $responsiblePhone)) {
                $responsiblePhone = str_replace('*', '%', $responsiblePhone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TrainPeer::TELEFONE_RESPONSAVEL, $responsiblePhone, $comparison);
    }

    /**
     * Filter the query on the vagas column
     *
     * Example usage:
     * <code>
     * $query->filterBySpace(1234); // WHERE vagas = 1234
     * $query->filterBySpace(array(12, 34)); // WHERE vagas IN (12, 34)
     * $query->filterBySpace(array('min' => 12)); // WHERE vagas >= 12
     * $query->filterBySpace(array('max' => 12)); // WHERE vagas <= 12
     * </code>
     *
     * @param     mixed $space The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterBySpace($space = null, $comparison = null)
    {
        if (is_array($space)) {
            $useMinMax = false;
            if (isset($space['min'])) {
                $this->addUsingAlias(TrainPeer::VAGAS, $space['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($space['max'])) {
                $this->addUsingAlias(TrainPeer::VAGAS, $space['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TrainPeer::VAGAS, $space, $comparison);
    }

    /**
     * Filter the query on the disponiveis column
     *
     * Example usage:
     * <code>
     * $query->filterByAvailable(1234); // WHERE disponiveis = 1234
     * $query->filterByAvailable(array(12, 34)); // WHERE disponiveis IN (12, 34)
     * $query->filterByAvailable(array('min' => 12)); // WHERE disponiveis >= 12
     * $query->filterByAvailable(array('max' => 12)); // WHERE disponiveis <= 12
     * </code>
     *
     * @param     mixed $available The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterByAvailable($available = null, $comparison = null)
    {
        if (is_array($available)) {
            $useMinMax = false;
            if (isset($available['min'])) {
                $this->addUsingAlias(TrainPeer::DISPONIVEIS, $available['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($available['max'])) {
                $this->addUsingAlias(TrainPeer::DISPONIVEIS, $available['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TrainPeer::DISPONIVEIS, $available, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TrainPeer::DESCRICAO, $description, $comparison);
    }

    /**
     * Filter the query by a related Subscribe object
     *
     * @param   Subscribe|PropelObjectCollection $subscribe The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TrainQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySubscribe($subscribe, $comparison = null)
    {
        if ($subscribe instanceof Subscribe) {
            return $this
                ->addUsingAlias(TrainPeer::INSCRICAO_ID, $subscribe->getId(), $comparison);
        } elseif ($subscribe instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TrainPeer::INSCRICAO_ID, $subscribe->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySubscribe() only accepts arguments of type Subscribe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Subscribe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function joinSubscribe($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Subscribe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Subscribe');
        }

        return $this;
    }

    /**
     * Use the Subscribe relation Subscribe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \AppBundle\Model\SubscribeQuery A secondary query class using the current class as primary query
     */
    public function useSubscribeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSubscribe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Subscribe', '\AppBundle\Model\SubscribeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Train $train Object to remove from the list of results
     *
     * @return TrainQuery The current query, for fluid interface
     */
    public function prune($train = null)
    {
        if ($train) {
            $this->addUsingAlias(TrainPeer::CARAVANA_ID, $train->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
