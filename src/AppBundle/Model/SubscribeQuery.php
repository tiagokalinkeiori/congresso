<?php

namespace AppBundle\Model;

use AppBundle\Model\om\BaseSubscribeQuery;

class SubscribeQuery extends BaseSubscribeQuery
{
    
    public function listToAdmin(array $where = array())
    {

        $this->useSubscribePaymentQuery('sp', \Criteria::LEFT_JOIN)->endUse()
                ->addJoinCondition('sp', '(sp.Paid = 1)')
                ->useSubscribeTypeQuery('st')
                    ->useSubscribeTypeNameQuery('stn')
                        ->filterByLanguageId(1)
                    ->endUse()
                ->endUse()
                ->addJoinToSearch()
                ->withColumn('IF(SUM(sp.Price) IS NULL OR (SUM(sp.Price) IS NOT NULL AND SUM(sp.Price)) < s.Price, 1, 0)', 'PendentPayment')
                ->withColumn('SUM(sp.Price)', 'PaidPrice')
                ->withColumn('stn.Name', 'SubscribeTypeName')
                ->groupById()
                ->orderByName()
                ->orderByDate(\Criteria::ASC)
                ->addWhere($where);
        
        return $this;
    }
    
    public function addJoinToSearch()
    {
        
        $this->leftJoinCountry('ljc')
                ->leftJoinState('ljs')
                ->leftJoinDistrict('ljd');
        
        return $this;
    }
    
    public function addWhere(array $where = array())
    {
        
        $this->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null);

        if (isset($where['pais']) && !empty($where['pais'])) {
            switch ($where['pais']) {
                case 'brasileiro':
                    $this->filterByCountryId(30);
                    break;
                case 'estrangeiro':
                    $this->filterByCountryId(30, \Criteria::NOT_EQUAL);
                    break;
            }
        }

        if (isset($where['pagamento']) && !empty($where['pagamento'])) {
            switch ($where['pagamento']) {
                case 'pago':
                    $this->having('PendentPayment =  0');
                    break;
                case 'pendente':
                    $this->having('PendentPayment = 1');
                    break;
            }
        }
        
        if (isset($where['busca']) && !empty($where['busca'])) {
            $this->useSubscribePaymentQuery('ower')->endUse();
            $where['busca'] = str_replace(" ", "%", $where['busca']);
            
            $search = array();
            $search[] = "s.Name LIKE '%".$where['busca']."%'";
            $search[] = "s.Email LIKE '%".$where['busca']."%'";
            $search[] = "s.Register LIKE '%".$where['busca']."%'";
            $search[] = "s.Document LIKE '%".$where['busca']."%'";
            $search[] = "ljc.Name LIKE '%".$where['busca']."%'";
            $search[] = "ljs.Name LIKE '%".$where['busca']."%'";
            $search[] = "ljd.Name LIKE '%".$where['busca']."%'";
            $search[] = "s.City LIKE '%".$where['busca']."%'";
            if ((int) preg_replace("/[^0-9]/", "", $where['busca']) > 0) {
                $search[] = "ower.OwerNumber LIKE '%".preg_replace("/[^0-9]/", "", $where['busca'])."%'";
            }
            $this->where("(".implode(" OR ", $search).")");
        }

        return $this;
    }
}
