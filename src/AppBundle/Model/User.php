<?php

namespace AppBundle\Model;

use AppBundle\Model\om\BaseUser;
use Serializable;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User extends BaseUser implements UserInterface, AdvancedUserInterface, Serializable
{
    
    public function eraseCredentials() {}

    public function getRoles()
    {
        return array(
            $this->getGroup()->getRole(),
        );
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function isAccountNonExpired()
    {
        
        if ($this->getSubscribes()->count() > 0
                && $this->getSubscribes()->getFirst()->getCanceled()) {
            return false;
        }

        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return true;
    }

    public function serialize()
    {
        $id = $this->getId();
        return serialize(array(
            $id,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $id,
        ) = unserialize($serialized);
        $this->setId($id);
    }

}
