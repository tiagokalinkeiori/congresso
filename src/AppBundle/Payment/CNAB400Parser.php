<?php

namespace AppBundle\Payment;

class CNAB400Parser extends AbstractParser
{
    
    public function parse()
    {
        
        if (!$this->getParsed()) {
            $item = array();
            foreach ($this->getRows() as $row) {
                $segment = substr($row, 13, 1);

                if ($segment == 'T') {
                    $item[$segment] = array(
                        'cod_banco' => substr($row, 0, 3), //Código do banco na compensação
                        'lote' => substr($row, 3, 4), //Lote de serviço - Número seqüencial para identificar um lote de serviço.
                        'n_sequencial' => substr($row, 8, 5), //Nº Sequencial do registro no lote
                        'cod_seg' => substr($row, 15, 2), //Cód. Segmento do registro detalhe
                        'cod_conv_banco' => substr($row, 23, 6), //Código do convênio no banco - Código fornecido pela CAIXA, através da agência de relacionamento do cliente. Deve ser preenchido com o código do Cedente (6 posições).
                        'n_banco_sac' => substr($row, 32, 3), //Numero do banco de sacados
                        'mod_nosso_n' => substr($row, 39, 2), //Modalidade nosso número
                        'id_titulo_banco' => substr($row, 41, 15), //Identificação do titulo no banco - Número adotado pelo Banco Cedente para identificar o Título.
                        'cod_carteira' => substr($row, 57, 1), //Código da carteira - Código adotado pela FEBRABAN, para identificar a característica dos títulos. 1=Cobrança Simples, 3=Cobrança Caucionada, 4=Cobrança Descontada
                        'num_doc_cob' => substr($row, 58, 11), //Número do documento de cobrança - Número utilizado e controlado pelo Cliente, para identificar o título de cobrança.
                        'dt_vencimento' => substr($row, 73, 8), //Data de vencimento do titulo - Data de vencimento do título de cobrança.
                        'v_nominal' => substr($row, 81, 13), //Valor nominal do titulo - Valor original do Título. Quando o valor for expresso em moeda corrente, utilizar 2 casas decimais.
                        'cod_banco2' => substr($row, 96, 3), //Código do banco
                        'cod_ag_receb' => substr($row, 99, 5), //Codigo da agencia cobr/receb - Código adotado pelo Banco responsável pela cobrança, para identificar o estabelecimento bancário responsável pela cobrança do título.
                        'dv_ag_receb' => substr($row, 104, 1), //Dígito verificador da agencia cobr/receb
                        'id_titulo_empresa' => substr($row, 105, 25), //identificação do título na empresa - Campo destinado para uso da Empresa Cedente para identificação do Título. Informar o Número do Documento - Seu Número.
                        'cod_moeda' => substr($row, 130, 2), //Código da moeda
                        'tip_inscricao' => substr($row, 132, 1), //Tipo de inscrição - Código que identifica o tipo de inscrição da Empresa ou Pessoa Física perante uma Instituição governamental: 0=Não informado, 1=CPF, 2=CGC / CNPJ, 9=Outros.
                        'num_inscricao' => substr($row, 133, 15), //Número de inscrição - Número de inscrição da Empresa (CNPJ) ou Pessoa Física (CPF).
                        'nome' => substr($row, 148, 40), //Nome - Nome que identifica a entidade, pessoa física ou jurídica, Cedente original do título de cobrança.
                        'v_tarifa_custas' => substr($row, 198, 13), //Valor da tarifa/custas
                        'id_rejeicoes' => substr($row, 213, 10), //Identificação para rejeições, tarifas, custas, liquidação e baixas
                    );
                } elseif ($segment == 'U') {
                    $item[$segment] = array(
                        'cod_banco' => substr($row, 0, 3), //Código do banco na compensação
                        'lote' => substr($row, 3, 4), //Lote de serviço - Número seqüencial para identificar um lote de serviço.
                        'tipo_reg' => substr($row, 7, 1), //Tipo de Registro - Código adotado pela FEBRABAN para identificar o tipo de registro: 0=Header de Arquivo, 1=Header de Lote, 3=Detalhe, 5=Trailer de Lote, 9=Trailer de Arquivo.
                        'n_sequencial' => substr($row, 8, 5), //Nº Sequencial do registro no lote
                        'cod_seg' => substr($row, 15, 2), //Cód. Segmento do registro detalhe
                        'juros_multa' => substr($row, 17, 15), //Jurus / Multa / Encargos - Valor dos acréscimos efetuados no título de cobrança, expresso em moeda corrente.
                        'desconto' => substr($row, 32, 15), //Valor do desconto concedido - Valor dos descontos efetuados no título de cobrança, expresso em moeda corrente.
                        'abatimento' => substr($row, 47, 15), //Valor do abat. concedido/cancel. - Valor dos abatimentos efetuados ou cancelados no título de cobrança, expresso em moeda corrente.
                        'iof' => substr($row, 62, 15), //Valor do IOF recolhido - Valor do IOF - Imposto sobre Operações Financeiras - recolhido sobre o Título, expresso em moeda corrente.
                        'v_pago' => substr($row, 77, 15), //Valor pago pelo sacado - Valor do pagamento efetuado pelo Sacado referente ao título de cobrança, expresso em moeda corrente.
                        'v_liquido' => substr($row, 92, 15), //Valor liquido a ser creditado - Valor efetivo a ser creditado referente ao Título, expresso em moeda corrente.
                        'v_despesas' => substr($row, 107, 15), //Valor de outras despesas - Valor de despesas referente a Custas Cartorárias, se houver.
                        'v_creditos' => substr($row, 122, 15), //Valor de outros creditos - Valor efetivo de créditos referente ao título de cobrança, expresso em moeda corrente.
                        'dt_ocorencia' => substr(substr($row, 137, 8), 4, 4).'-'.substr(substr($row, 137, 8), 2, 2).'-'.substr(substr($row, 137, 8), 0, 2), //Data da ocorrência - Data do evento que afeta o estado do título de cobrança.
                        'dt_efetivacao' => substr($row, 145, 8), //Data da efetivação do credito - Data de efetivação do crédito referente ao pagamento do título de cobrança.
                        'dt_debito' => substr($row, 157, 8), //Data do débito da tarifa
                        'cod_sacado' => substr($row, 167, 15), //Código do sacado no banco
                        'cod_banco_comp' => substr($row, 210, 3), //Cód. Banco Correspondente compens - Código fornecido pelo Banco Central para identificação na Câmara de Compensação, do Banco ao qual será repassada a Cobrança do Título.
                        'nn_banco' => substr($row, 213, 20), //Nosso Nº banco correspondente - Código fornecido pelo Banco Correspondente para identificação do Título de Cobrança. Deixar branco (Somente para troca de arquivos entre Bancos).
                    );
                    
                    $item[$segment]['juros_multa'] = substr($item[$segment]['juros_multa'], 0, 13).'.'.substr($item[$segment]['juros_multa'], 13, 2);
                    $item[$segment]['desconto'] = substr($item[$segment]['desconto'], 0, 13).'.'.substr($item[$segment]['desconto'], 13, 2);
                    $item[$segment]['abatimento'] = substr($item[$segment]['abatimento'], 0, 13).'.'.substr($item[$segment]['abatimento'], 13, 2);
                    $item[$segment]['iof'] = substr($item[$segment]['iof'], 0, 13).'.'.substr($item[$segment]['iof'], 13, 2);
                    $item[$segment]['v_pago'] = substr($item[$segment]['v_pago'], 0, 13).'.'.substr($item[$segment]['v_pago'], 13, 2);
                    $item[$segment]['v_liquido'] = substr($item[$segment]['v_liquido'], 0, 13).'.'.substr($item[$segment]['v_liquido'], 13, 2);
                    $item[$segment]['v_despesas'] = substr($item[$segment]['v_despesas'], 0, 13).'.'.substr($item[$segment]['v_despesas'], 13, 2);
                    $item[$segment]['v_creditos'] = substr($item[$segment]['v_creditos'], 0, 13).'.'.substr($item[$segment]['v_creditos'], 13, 2);
                }

                if (isset($item['T']) && isset($item['U'])) {
                    $this->addParsedRow($item);
                    $item = array();
                }
            }

            $this->setParsed(true);
        }
        
        return $this->getParsedRows();
    }
}
