<?php

namespace AppBundle\Payment;

abstract class AbstractParser
{

    /**
     *
     * @var array
     */
    private $rows;
    
    /**
     *
     * @var array
     */
    private $parsedRows;
    
    /**
     *
     * @var boolean
     */
    private $parsed;

    /**
     * Constructor method
     */
    public function __construct()
    {
        $this->rows = array();
        $this->parsedRows = array();
        $this->parsed = false;
    }
    
    /**
     * 
     * @return array
     */
    protected function getRows()
    {
        
        return $this->rows;
    }
    
    /**
     * 
     * @param array $rows
     * @return \AppBundle\Payment\AbstractParser
     */
    protected function setRows(array $rows = array())
    {
        $this->rows = $rows;
        return $this;
    }
    
    /**
     * 
     * @return array
     */
    protected function getParsedRows()
    {
        
        return $this->parsedRows;
    }
    
    /**
     * 
     * @param array $parsedRows
     * @return \AppBundle\Payment\AbstractParser
     */
    protected function setParsedRows(array $parsedRows = array())
    {
        $this->parsedRows = $parsedRows;

        return $this;
    }
    
    /**
     * 
     * @param array $parsedRow
     * @return \AppBundle\Payment\AbstractParser
     */
    protected function addParsedRow(array $parsedRow = array())
    {
        $this->parsedRows[] = $parsedRow;

        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    protected function getParsed()
    {
        
        return $this->parsed;
    }
    
    /**
     * 
     * @param boolean $parsed
     * @return \AppBundle\Payment\AbstractParser
     */
    protected function setParsed($parsed)
    {
        $this->parsed = $parsed;
        return $this;
    }
}
