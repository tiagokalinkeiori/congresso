<?php

namespace AppBundle\EventListener;

use AppBundle\Model\Country;
use AppBundle\Model\StateQuery;
use AppBundle\Model\SubscribeTypeQuery;
use AppBundle\Model\SubscribeTypePriceQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationEventListener implements EventSubscriberInterface
{
    
    private $container;
    private $locale;
    private $isAdmin;
    
    public function __construct(ContainerInterface $container, $locale, $isAdmin = false)
    {
        $this->container = $container;
        $this->locale = $locale;
        $this->isAdmin = $isAdmin;
    }

    public static function getSubscribedEvents()
    {

        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND => 'preSetData',
        );
    }

    public function preSetData(FormEvent $event)
    {

        $translator = $this->container->get('translator');
        $data = $event->getData();
        $form = $event->getForm();

        if ($form->has('State')) {
            $form->remove('State');
            if (isset($data['Country']) && !empty($data['Country'])) {
                $states = StateQuery::create()
                        ->_if($data['Country'] instanceof Country)
                            ->filterByCountry($data['Country'])
                        ->_else()
                            ->filterByCountryId($data['Country'])
                        ->_endif()
                        ->find();
                $choiceStates = array();
                foreach ($states as $item) {
                    $choiceStates[$item->getId()] = $item->getName();
                }
                $form->add('State', 'choice', array(
                    'constraints' => array(),
                    'choices' => $choiceStates,
                    'empty_value' => $translator->trans("form.label.state", array(), null, $this->locale),
                    'required' => false,
                    'data' => ( isset($data['State']) && !empty($data['State']) ? $data['State'] : null )
                ));
            } else {
                $form->add('State', 'choice', array(
                    'constraints' => array(),
                    'choices' => array(),
                    'empty_value' => $translator->trans("form.empty.state", array(), null, $this->locale),
                    'required' => false,
                ));
            }
        }

        if ($form->has('SubscribeTypeId')) {
            $form->remove('SubscribeTypeId');
            $form->add('SubscribeTypeId', 'choice', array(
                'constraints' => array(
                    new NotBlank(),
                ),
                'choices' => $this->getSubscribeTypes($data['Genre']),
                'empty_value' => $translator->trans("form.label.subscribetype", array(), null, $this->locale),
                'required' => true,
                'data' => ( isset($data['SubscribeTypeId']) && !empty($data['SubscribeTypeId']) ? $data['SubscribeTypeId'] : null )
            ));

            $form->remove('Quota');
            if (!$this->isAdmin) {
                if (!empty($data['SubscribeTypeId'])) {
                    $price = SubscribeTypePriceQuery::create()
                            ->filterBySubscribeTypeId($data['SubscribeTypeId'])
                            ->filterByStart(time(), \Criteria::LESS_EQUAL)
                            ->filterByEnd(time(), \Criteria::GREATER_EQUAL)
                            ->findOne();
                    $choiceQuota = array();
                    if (null !== $price) {
                        $choiceQuota[1] = sprintf("1x R$ %s", number_format($price->getPrice(), 2, ",", "."));
                        if ($this->locale == "pt_BR" && $price->getQuota() > 1) {
                            if (null !== $price->getInstallmentValue()) {
                                $value = $price->getInstallmentValue();
                            } else {
                                $value = $price->getPrice();
                            }

                            for ($countQuota = 2; $countQuota <= $price->getQuota(); $countQuota++) {
                                $quotaPrice = $value / $countQuota;
                                $choiceQuota[$countQuota] = sprintf("%dx R$ %s", $countQuota, number_format($quotaPrice, 2, ",", "."));
                            }
                        }
                    }

                    $form->add('Quota', 'choice', array(
                        'constraints' => array(
                            new NotBlank(),
                        ),
                        'choices' => $choiceQuota,
                        'empty_value' => $translator->trans("form.label.quota", array(), null, $this->locale),
                        'required' => true,
                        'data' => ( isset($data['Quota']) && !empty($data['Quota']) ? $data['Quota'] : null )
                    ));
                } else {
                    $form->add('Quota', 'choice', array(
                        'constraints' => array(
                            new NotBlank(),
                        ),
                        'choices' => array(),
                        'empty_value' => $translator->trans("form.empty.quota", array(), null, $this->locale),
                        'required' => true,
                    ));
                }
            }
        }
    }
    
    protected function getSubscribeTypes($genre = "M")
    {
        
        $array = array();
        $types = SubscribeTypeQuery::create('st')
                ->leftJoinSubscribe('s')
                ->addJoinCondition('s', '((s.Canceled = 0 OR s.Canceled IS NULL) AND (s.Organization = 0 OR s.Organization IS NULL))')
                ->useSubscribeTypeNameQuery('stn')
                    ->useLanguageQuery('l')
                        ->filterByCode($this->locale)
                    ->endUse()
                ->endUse()
                ->withColumn('stn.Name', 'Name')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->select(array(
                    'Id',
                    'Name',
                    'SpaceAvailable',
                    'Total',
                ))
                ->filterByGenre($genre)
                ->_or()
                ->filterByGenre(null, \Criteria::ISNULL)
                ->groupById()
                ->find()
                ->toArray();
        foreach ($types as $item) {
            if ($this->isAdmin) {
                $array[$item['Id']] = $item['Name'];
            } else {
                if ((int) $item['SpaceAvailable'] == 0 || ((int) $item['SpaceAvailable'] > 0 && $item['SpaceAvailable'] > $item['Total'])) {
                    $array[$item['Id']] = $item['Name'];
                }
            }
        }
        
        return $array;
    }
}