<?php

namespace AppBundle\EventListener;

use AppBundle\Model\Country;
use AppBundle\Model\StateQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SubscribeEditEventListener implements EventSubscriberInterface
{
    
    private $container;
    private $locale;
    
    public function __construct(ContainerInterface $container, $locale, $isAdmin = false)
    {
        $this->container = $container;
        $this->locale = $locale;
    }

    public static function getSubscribedEvents()
    {

        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND => 'preSetData',
        );
    }

    public function preSetData(FormEvent $event)
    {

        $translator = $this->container->get('translator');
        $data = $event->getData();
        $form = $event->getForm();

        if ($form->has('State')) {
            $form->remove('State');
            if (isset($data['Country']) && !empty($data['Country'])) {
                $states = StateQuery::create()
                        ->_if($data['Country'] instanceof Country)
                            ->filterByCountry($data['Country'])
                        ->_else()
                            ->filterByCountryId($data['Country'])
                        ->_endif()
                        ->find();
                $choiceStates = array();
                foreach ($states as $item) {
                    $choiceStates[$item->getId()] = $item->getName();
                }
                $form->add('State', 'choice', array(
                    'constraints' => array(),
                    'choices' => $choiceStates,
                    'empty_value' => $translator->trans("form.label.state", array(), null, $this->locale),
                    'required' => false,
                    'data' => ( isset($data['State']) && !empty($data['State']) ? $data['State'] : null )
                ));
            } else {
                $form->add('State', 'choice', array(
                    'constraints' => array(),
                    'choices' => array(),
                    'empty_value' => $translator->trans("form.empty.state", array(), null, $this->locale),
                    'required' => false,
                ));
            }
        }
    }
}