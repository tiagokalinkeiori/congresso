<?php

namespace AppBundle\Controller;

use AppBundle\Form\ContactForm;
use AppBundle\Model\TrainQuery;
use AppBundle\Util\ConfigurationUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */
class DefaultController extends BaseController
{

    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        return $this->prepareView();
    }
    
    /**
     * @Route("/tema", name="theme")
     * @Template()
     */
    public function themeAction(Request $request)
    {

        return $this->prepareView();
    }
    
    /**
     * @Route("/informacoes", name="information")
     * @Template()
     */
    public function informationAction(Request $request)
    {

        return $this->prepareView();
    }
    
    /**
     * @Route("/atracoes", name="attraction")
     * @Template()
     */
    public function attractionAction(Request $request)
    {

        return $this->prepareView();
    }
    
    /**
     * @Route("/caravanas", name="train")
     * @Template()
     */
    public function trainAction(Request $request)
    {
        
        $page = $request->query->get('pagina', 1);
        $limit = 10;
        
        $trains = TrainQuery::create()
                ->paginate($page, $limit);

        return $this->prepareView(array(
            'trains' => $trains,
        ));
    }
    
    /**
     * @Route("/fale-conosco", name="contact")
     * @Template()
     */
    public function contactAction(Request $request)
    {
     
        $bind = array();
        if (null !== $this->getUser()) {
            $user = $this->getUser();
            $bind['Name'] = $user->getName();
            $bind['Email'] = $user->getEmail();
        }
        $form = $this->createForm(new ContactForm(), $bind, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $configuration = ConfigurationUtil::all();
                $formData = $form->getData();
                
                $body = $this->renderView('AppBundle:Includes:mail-contact.html.twig', $formData);
                $message = \Swift_Message::newInstance()
                    ->setSubject("Contato pelo site do Congressão")
                    ->setFrom($configuration['mail.sender'])
                    ->setTo($configuration['contact.mail.to'])
                    ->setReplyTo($formData['Email'])
                    ->setBody($body, 'text/html');
                $this->getMailer()->send($message);
                
                $request->getSession()->getFlashBag()->add('success', 'Contato enviado com sucesso.');
                
                return $this->redirect($this->generateUrl('contact'));
            }
        }

        return $this->prepareView(array(
            'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/alterar-idioma/{locale}", name="change_locale")
     * @param Request $request
     */
    public function localeAction(Request $request, $locale)
    {
        
        $request->getSession()->set('userLocale', $locale);
        
        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/texto/{routeName}", name="text_part")
     * @Template()
     * 
     * @param Request $request
     * @param string $routeName
     */
    public function textAction(Request $request, $routeName)
    {

        $routeName = str_replace("-", "_", $routeName);

        return $this->prepareView(array(
            'texts' => $this->getText($request, $routeName),
        ));
    }
}
