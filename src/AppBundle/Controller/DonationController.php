<?php

namespace AppBundle\Controller;

use AppBundle\Form\DonationForm;
use AppBundle\Model\DonationQuery;
use AppBundle\Model\SubscribeQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/doacao")
 */
class DonationController extends BaseController
{

    /**
     * @Route("/", name="donation")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        
        $donationLogin = $form = $donations = null;
        $session = $request->getSession();
        if ($session->has('donationLogin')) {
            $donationLogin = $session->get('donationLogin');
            $donations = DonationQuery::create()
                    ->filterByEmail($donationLogin)
                    ->find();
        } else {
            if ($request->request->has('login') && $request->request->get('login') != "") {
                $session->set('donationLogin', $request->request->get('login'));

                return $this->redirectToRoute('donation');
            }

            $form = $this->createForm(new DonationForm(), null, array(
                'csrf_protection' => true,
            ));
            if ($request->isMethod('post')) {
                $form->submit($request);
                if ($form->isValid()) {
                    $donation = $form->getData();

                    $subscribe = SubscribeQuery::create()
                            ->findOneByEmail($donation->getEmail());
                    if (null !== $subscribe) {
                        $donation->setSubscribe($subscribe);
                    }

                    $donation->setDate(time())
                            ->save();

                    $session->set('donationLogin', $donation->getEmail());

                    return $this->redirectToRoute('donation');
                }
            }

            $form = $form->createView();
        }

        return $this->prepareView(array(
            'donationLogin' => $donationLogin,
            'donations' => $donations,
            'form' => $form,
        ));
    }
    
    /**
     * @Route("/sair", name="donation_logout")
     * @Template()
     */
    public function logoutAction(Request $request)
    {

        $request->getSession()->remove('donationLogin');
        
        return $this->redirectToRoute('donation');
    }
}
