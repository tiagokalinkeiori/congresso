<?php

namespace AppBundle\Controller;

use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\UserQuery;
use AppBundle\Util\ConfigurationUtil;
use AppBundle\Util\StringUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * @Route("/")
 */
class SecurityController extends BaseController
{

    /**
     * @Route("/entrar", name="login")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        
        $this->get('security.context')->setToken(null);
        $session = $request->getSession();

        // Get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->prepareView(array(
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ));
    }
    
    /**
     * @Route("/lembrar-senha", name="remember_password")
     * @Template()
     */
    public function rememberAction(Request $request)
    {
        
        $hasErrorField = $email = false;
        if ($request->isMethod('post')) {
            $email = $request->request->get('_username');
            if (empty($email)) {
                $hasErrorField = true;
            } else {
                $user = UserQuery::create()
                        ->filterByEmail($email)
                        ->findOne();
                
                if (null !== $user) {
                    $newPassword = StringUtil::generatePassword();
                    
                    $factory = $this->get('security.encoder_factory');
                    $encoder = $factory->getEncoder($user);
                    $password = $encoder->encodePassword($newPassword, $user->getSalt());
                    $user->setPassword($password)
                            ->save();

                    try {
                        $configuration = ConfigurationUtil::all();

                        $emailBody = $this->renderView("AppBundle:Includes:remember-password.html.twig", array(
                            'newPassword' => $newPassword,
                            'locale' => $this->getLocale($request),
                        ));

                        if ($this->getLocale($request) == 'es') {
                            $subject = "Nueva contraseña para acceso a tu área de inscripto!";
                        } else {
                            $subject = "Nova senha para acesso à sua Área do Inscrito!";
                        }

                        $message = \Swift_Message::newInstance($subject)
                                ->setFrom($configuration['mail.sender'], 'Latino Americano 2017')
                                ->setTo($user->getEmail(), $user->getName())
                                ->setBody($emailBody, 'text/html', 'utf-8');
                        $this->getMailer()->send($message);

                    } catch(\Exception $em) {}
                    
                    $request->getSession()->getFlashBag()->add('success', 'Foi enviada uma nova senha para o seu e-mail!');
                        
                    return $this->redirectToRoute('remember_password');
                        
                } else {
                    $request->getSession()->getFlashBag()->add('danger', 'Nenhuma inscrição encontrada com esse e-mail!');
                }
            }
        }
        
        return $this->prepareView(array(
            'email' => $email,
            'hasErrorField' => $hasErrorField,
        ));
    }
    
    /**
     * @Route("/rest/auth/login", name="security_rest_auth_login")
     * 
     * @param Request $request
     * @return JsonResponse
     */
    public function restAuthAction(Request $request)
    {

        $request->setRequestFormat('json');

        if (!$request->isMethod("post")) {
            throw new MethodNotAllowedHttpException(array(
                "allow" => "POST",
            ));
        }
        
        $login = $request->get('usuario', null);
        $password = $request->get('senha', null);
        
        if (null === $login || null === $password) {
            throw new UnauthorizedHttpException("Rest");
        }
        
        $user = UserQuery::create('u')
                ->filterByEmail($login)
                ->useSubscribeQuery('s')
                    ->filterByCanceled(false)
                    ->_or()
                    ->filterByCanceled(null)
                ->endUse()
                ->findOne();
        
        if (null === $user) {
            throw new UnauthorizedHttpException("Rest");
        }
        
        $encoderService = $this->get('security.encoder_factory');
        $encoder = $encoderService->getEncoder($user);
        if (!$encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
            throw new UnauthorizedHttpException("Rest");
        }
        
        $subscribe = SubscribeQuery::create('s')
                ->filterByUser($user)
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->findOne();
        
        return new JsonResponse(array(
            "result" => true,
            "message" => "Usuário autorizado com sucesso.",
            "inscrito" => array(
                "id" => $subscribe->getId(),
                "usuario" => array(
                    "id" => $user->getId(),
                    "grupo" => array(
                        "id" => $user->getGroupId(),
                        "nome" => $user->getGroup()->getName(),
                    ),
                    "nome" => $user->getName(),
                    "email" => $user->getEmail(),
                    "senha" => $password,
                    "salt" => $user->getSalt(),
                ),
                "estado" => array(
                    "id" => $subscribe->getStateId(),
                    "nome" => $subscribe->getState()->getName(),
                    "pais" => array(
                        "id" => $subscribe->getState()->getCountryId(),
                        "nome" => $subscribe->getState()->getCountry()->getName(),
                        "codigo" => $subscribe->getState()->getCountry()->getCode(),
                    ),
                ),
                "distrito" => ( $subscribe->getDistrictId() > 0 ? array(
                    "id" => $subscribe->getDistrictId(),
                    "nome" => $subscribe->getDistrict()->getName(),
                ) : null ),
                "tipo" => array(
                    "id" => $subscribe->getSubscribeTypeId(),
                    "vagas" => $subscribe->getSubscribeType()->getSpaceAvailable(),
                    "sexo" => $subscribe->getSubscribeType()->getGenre(),
                ),
                "deslocamento" => ( $subscribe->getDisplacementId() > 0 ? array(
                    "id" => $subscribe->getDisplacementId(),
                    "nome" => $subscribe->getDisplacement()->getName(),
                ) : null ),
                "lingua" => array(
                    "id" => $subscribe->getLanguageId(),
                    "nome" => $subscribe->getLanguage()->getName(),
                ),
                "email" => $subscribe->getEmail(),
                "nome" => $subscribe->getName(),
                "data_nascimento" => $subscribe->getBirthday()->format("Y-m-d"),
                "registro" => $subscribe->getRegister(),
                "orgao_expedidor" => $subscribe->getDispatcher(),
                "orgao_expedidor_uf" => $subscribe->getDispatcherPlace(),
                "documento" => $subscribe->getDocument(),
                "sexo" => $subscribe->getGenre(),
                "telefone" => $subscribe->getPhone(),
                "endereco" => $subscribe->getAddress(),
                "numero" => $subscribe->getNumber(),
                "complemento" => $subscribe->getComplement(),
                "cidade" => $subscribe->getCity(),
                "bairro" => $subscribe->getRegion(),
                "cep" => $subscribe->getZipCode(),
                "membro_ielb" => $subscribe->getMember(),
                "confirmado" => $subscribe->getConfirmed(),
                "delegado_plenaria" => $subscribe->getDelegate(),
                "presidente_uj" => $subscribe->getUnityPresident(),
                "pastor" => $subscribe->getMinister(),
                "uj" => $subscribe->getUnityName(),
                "responsavel_caravana" => $subscribe->getTrainResponsible(),
                "parcelas" => $subscribe->getQuota(),
                "valor_total" => $subscribe->getPrice(),
                "necessidades_especiais" => $subscribe->getSpecialNeed(),
                "comentarios" => $subscribe->getComment(),
                "primeiro_congresso" => $subscribe->getFirstCongress(),
                "quantidade_congresso" => $subscribe->getAmountCongress(),
                "isento" => $subscribe->getFree(),
                "organizacao" => $subscribe->getOrganization(),
                "visitante" => $subscribe->getVisitor(),
                "cancelado" => $subscribe->getCanceled(),
                "data_inscricao" => $subscribe->getDate()->format("Y-m-d"),
            ),
        ));
    }
}