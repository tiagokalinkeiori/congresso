<?php

namespace AppBundle\Controller;

use AppBundle\Form\RegistrationForm;
use AppBundle\Model\CountryQuery;
use AppBundle\Model\DistrictQuery;
use AppBundle\Model\LanguageQuery;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\SubscribeTypePriceQuery;
use AppBundle\Model\User;
use AppBundle\Util\ConfigurationUtil;
use Propel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @Route("/")
 */
class SubscribeController extends BaseController
{

    /**
     * @Route("/inscritos", name="subscribers")
     * @Template()
     */
    public function subscribersAction(Request $request)
    {

        $districtId = $request->query->get('distrito');
        $page = $request->query->get('pagina', 1);
        $search = $request->query->get('busca');
        $limit = 20;
        
        $districts = DistrictQuery::create()
                ->leftJoinSubscribe('s')
                ->addJoinCondition('s', '(s.Canceled = 0 OR s.Canceled IS NULL)')
                ->withColumn('count(distinct s.Id)', 'Total')
                ->select(array(
                    'Id',
                    'Name',
                    'Total',
                ))
                ->orderById()
                ->groupById()
                ->filterById(null, \Criteria::ISNOTNULL)
                ->find()
                ->toArray();
        
        $countries = CountryQuery::create()
                ->leftJoinSubscribe('s')
                ->addJoinCondition('s', '(s.Canceled = 0 OR s.Canceled IS NULL)')
                ->withColumn('count(distinct s.Id)', 'Total')
                ->select(array(
                    'Id',
                    'Name',
                    'Code',
                    'Total',
                ))
                ->orderBy('Total', \Criteria::DESC)
                ->groupById()
                ->having('Total > 0')
                ->find()
                ->toArray();
        
        $subscribers = SubscribeQuery::create('ss')
                ->useStateQuery('s', \Criteria::LEFT_JOIN)->endUse()
                ->useDistrictQuery('d', \Criteria::LEFT_JOIN)->endUse()
                ->withColumn('s.Name', 'State')
                ->withColumn('d.Name', 'District')
                ->select(array(
                    'Id',
                    'Name',
                    'State',
                    'City',
                    'District',
                ))
                ->_if(!empty($districtId))
                    ->filterByDistrictId($districtId)
                ->_endif()
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->_if(!empty($search))
                    ->where('(s.Name LIKE "%'.$search.'%" OR d.Name LIKE "%'.$search.'%" OR ss.Name LIKE "%'.$search.'%")')
                ->_endif()
                ->orderByDate()
                ->paginate($page, $limit);

        return $this->prepareView(array(
            'districts' => $districts,
            'countries' => $countries,
            'districtId' => $districtId,
            'subscribers' => $subscribers,
            'search' => $search,
        ));
    }
    
    /**
     * @Route("/inscricao", name="registration")
     * @Template()
     */
    public function registrationAction(Request $request)
    {

        $configuration = ConfigurationUtil::all();
        if ($request->query->has('preview')
                || (empty($configuration['registration.date.start'])
                || (!empty($configuration['registration.date.start'])
                && date("Y-m-d") >= $configuration['registration.date.start']))
                && (empty($configuration['registration.date.end'])
                || (!empty($configuration['registration.date.end'])
                && date("Y-m-d") <= $configuration['registration.date.end']))) {
            $form = $this->createForm(new RegistrationForm($this->container, $this->getLocale($request)), array('Genre' => 'M'), array(
                'csrf_protection' => true,
            ));

            if ($request->isMethod('post')) {
                $form->submit($request);
                if ($form->isValid()) {
                    $data = $form->getData();

                    $exists = SubscribeQuery::create()
                            ->filterByEmail($data['Email'])
                            ->count();
                    if (!$exists) {
                        $conn = Propel::getConnection();

                        try {
                            $conn->beginTransaction();

                            $now = new \DateTime();
                            $age = $now->diff($data['Birthday'])->y;

                            $price = 0;
                            if ($age > (int) $configuration['subscribe.min.payment.age']) {
                                $subscribeTypePrice = SubscribeTypePriceQuery::create()
                                        ->filterBySubscribeTypeId($data['SubscribeTypeId'])
                                        ->filterByStart(time(), \Criteria::LESS_EQUAL)
                                        ->filterByEnd(time(), \Criteria::GREATER_EQUAL)
                                        ->findOne();
                                if ($data['Quota'] > 1 && $subscribeTypePrice->getInstallmentValue()) {
                                    $price = $subscribeTypePrice->getInstallmentValue();
                                } else {
                                    $price = $subscribeTypePrice->getPrice();
                                }
                            }
                            
                            $language = LanguageQuery::create()
                                    ->findOneByCode($this->getLocale($request));

                            $subscribe = new Subscribe();
                            $subscribe->setCountry($data['Country'])
                                    ->setDistrict($data['District'])
                                    ->setSubscribeTypeId($data['SubscribeTypeId'])
                                    ->setDisplacementId($data['DisplacementId'])
                                    ->setEmail($data['Email'])
                                    ->setName($data['Name'])
                                    ->setBirthday($data['Birthday'])
                                    ->setStateId($data['State'])
                                    ->setRegister($data['Register'])
                                    ->setDocument(preg_replace("/[^0-9]/", "", $data['Document']))
                                    ->setGenre($data['Genre'])
                                    ->setPhone($data['Phone'])
                                    ->setAddress($data['Address'])
                                    ->setNumber($data['Number'])
                                    ->setComplement($data['Complement'])
                                    ->setCity($data['City'])
                                    ->setRegion($data['Region'])
                                    ->setZipCode($data['ZipCode'])
                                    ->setMember($data['Member'])
                                    ->setConfirmed($data['Confirmed'])
                                    ->setMinister($data['Minister'])
                                    ->setUnityName($data['UnityName'])
                                    ->setSpecialNeed($data['SpecialNeed'])
                                    ->setComment($data['Comment'])
                                    ->setQuota($data['Quota'])
                                    ->setPrice($price)
                                    ->setVisitor((int) $data['Visitante'])
                                    ->setDate(time())
                                    ->setLanguage($language);
                            if (isset($data['TrainResponsible'])) {
                                $subscribe->setTrainResponsible($data['TrainResponsible']);
                            }

                            $user = new User();
                            $user->setGroupId(2)
                                    ->setName($data['Name'])
                                    ->setEmail($data['Email'])
                                    ->setSalt(md5(uniqid(null, true)));
                            $factory = $this->get('security.encoder_factory');
                            $encoder = $factory->getEncoder($user);
                            $password = $encoder->encodePassword($data['Password'], $user->getSalt());
                            $user->setPassword($password);

                            $subscribe->setUser($user);
                            
                            $dueDate = date("Y-m-10");
                            if ((int) date("d") >= 10) {
                                if ($data['Quota'] == 9
                                        || $data['Quota'] == 8
                                        || $data['Quota'] == 7) {
                                    $dueDate = date("Y-m-d", strtotime("+5 days"));
                                } else {
                                    $dueDate = date("Y-m-10", strtotime("+1 month"));
                                }
                            }

                            $quotaPrice = $price / $data['Quota'];
                            $quota = new SubscribePayment();
                            $quota->setCreateDate(time())
                                    ->setPrice($quotaPrice)
                                    ->setFirst(1)
                                    ->setDueDate($dueDate);
                            if ($price == 0) {
                                $quota->setPaid(0)
                                        ->setPaymentDate(time());
                                
                                $subscribe->setFree(1);
                            }
                            $subscribe->addSubscribePayment($quota);
                            $subscribe->save();

                            $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
                            $security = $this->container->get('security.context');
                            $security->setToken($token);
                            
                            try {

                                $emailBody = $this->renderView("AppBundle:Includes:registration.html.twig", array(
                                    'locale' => $this->getLocale($request),
                                ));
                                
                                if ($this->getLocale($request) == 'es') {
                                    $subject = "¡Su reserva esta Confirmada!";
                                } else {
                                    $subject = "Sua reserva está Confirmada!";
                                }

                                $message = \Swift_Message::newInstance($subject)
                                        ->setFrom($configuration['mail.sender'], 'Latino Americano 2017')
                                        ->setTo($subscribe->getEmail(), $subscribe->getName())
                                        ->setBody($emailBody, 'text/html', 'utf-8');
                                $this->getMailer()->send($message);

                            } catch(\Exception $em) {}

                            $conn->commit();

                            return $this->redirect($this->generateUrl('subscribe_area'));

                        } catch (Exception $ex) {
                            $conn->rollBack();
                        }
                    } else {
                        $form->get('Email')->addError(new FormError('form.exists.email'));
                    }
                }
            }

            return $this->prepareView(array(
                'form' => $form->createView(),
            ));
        } else {
            return $this->prepareView();
        }        
    }
}
