<?php

namespace AppBundle\Controller;

use AppBundle\Model\LanguageQuery;
use AppBundle\Model\TextQuery;
use AppBundle\Util\ConfigurationUtil;
use AppBundle\Util\StringUtil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends Controller
{
    
    /**
     * 
     * @param array $array
     * @return array
     */
    protected function prepareView(array $array = array())
    {

        $array['mobile'] = StringUtil::isMobile($this->getRequest()->headers->get('user-agent'));
        
        $array['locale'] = $this->getRequest()->getLocale();
        $array['base_dir'] = realpath($this->get('kernel')->getRootDir() . '/..');
        if (!isset($array['texts'])) {
            $array['texts'] = $this->getText($this->getRequest());
        }
        
        return $array;
    }
    
    /**
     * 
     * @param Request $request
     * @return array
     */
    protected function getText(Request $request, $routeName = null)
    {

        if ($routeName == null) {
            $routeName = $request->get('_route');
        }

        $texts = TextQuery::create()
                ->select('Content')
                ->usePageQuery()
                    ->filterByRoute($routeName)
                ->endUse()
                ->useLanguageQuery()
                    ->filterByCode($this->getLocale($request))
                ->endUse()
                ->find();
        
        return $texts;
    }
 
    /**
     * 
     * @param Request $request
     * @return string
     */
    protected function getLocale(Request $request)
    {
        
        if ($request->query->has('fb_locale')) {
            $queryLocale = $request->query->get('fb_locale');
            if ($queryLocale !== "pt_BR" && strpos($queryLocale, "_") !== false) {
                $queryLocale = substr($queryLocale, 0, strpos($queryLocale, "_"));
            }
            $locale = $request->getSession()->set('userLocale', $queryLocale);
        } elseif ($request->query->has('locale')) {
            $queryLocale = $request->query->get('locale');
            if ($queryLocale !== "pt_BR" && strpos($queryLocale, "_") !== false) {
                $queryLocale = substr($queryLocale, 0, strpos($queryLocale, "_"));
            }
            $locale = $request->getSession()->set('userLocale', $queryLocale);
        }
        
        if ($request->getSession()->has('userLocale')) {
            $locale = $request->getSession()->get('userLocale');
        } else {
            if (null !== $this->getUser()
                    && $this->getUser()->getSubscribes()->count() > 0
                    && null !== ($subscriber = $this->getUser()->getSubscribes()->getFirst())
                    && null !== $subscriber->getLanguage()) {
                $subscriber = $this->getUser()->getSubscribes()->getFirst();

                $locale = $subscriber->getLanguage()->getCode();
            } else {
                $defaultLocales = LanguageQuery::create()
                        ->select('Code')
                        ->find()
                        ->toArray();

                $locale = $request->getPreferredLanguage($defaultLocales);
                if ($locale !== "pt_BR") {
                    if (strpos($locale, "_")) {
                        $locale = trim(substr($locale, 0, strpos($locale, "_")));
                    }
                }

                if (!in_array($locale, $defaultLocales)) {
                    $locale = "pt_BR";
                }
            }
        }
        
        if ($locale == 'pt') {
            $locale = 'pt_BR';
        }

        $request->getSession()->set('_locale', $locale);
        $request->setLocale($request->getSession()->get('_locale', $locale));

        return $locale;
    }
    
    /**
     * 
     */
    protected function getMailer()
    {
     
        $configuration = ConfigurationUtil::all();

        if ((boolean) $configuration['mail.usesmtp']) {
            $transport = \Swift_SmtpTransport::newInstance($configuration['mail.smtp.server'], $configuration['mail.smtp.port']);
            if (!empty($configuration['mail.smtp.user'])) {
                $transport->setAuthMode('login')
                        ->setUsername($configuration['mail.smtp.user'])
                        ->setPassword($configuration['mail.smtp.password']);
            }
            if (!empty($configuration['mail.smtp.secure'])) {
                $transport->getEncryption($configuration['mail.smtp.secure']);
            }

            $mailer = \Swift_Mailer::newInstance($transport);
        } else {
            $mailer = $this->get('mailer');
        }
        
        return $mailer;
    }
}