<?php

namespace AppBundle\Controller;

use AppBundle\Form\SubscribeArea\SubscribeEditForm;
use AppBundle\Form\TrainForm;
use AppBundle\Form\TransferForm;
use AppBundle\Model\CountryQuery;
use AppBundle\Model\DistrictQuery;
use AppBundle\Model\PaymentTextQuery;
use AppBundle\Model\Subscribe;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePaymentQuery;
use AppBundle\Model\SubscribeTypeNameQuery;
use AppBundle\Model\SubscribeTypePriceQuery;
use AppBundle\Model\TrainQuery;
use AppBundle\Model\Transfer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/area-inscrito")
 */
class SubscribeAreaController extends BaseController
{
    
    /**
     * @Route("/", name="subscribe_area")
     * @Route("/editar-inscricao", name="subscribe_area_edit")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        
        $user = $this->getUser();
        $subscribe = $user->getSubscribes()->getFirst();
        $form = $this->getForm($subscribe);
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $data = $form->getData();

                $conn = \Propel::getConnection();

                try {
                    $conn->beginTransaction();

                    $subscribe->setCountry($data['Country'])
                            ->setDistrict($data['District'])
                            ->setDisplacementId($data['DisplacementId'])
                            ->setName($data['Name'])
                            ->setBirthday($data['Birthday'])
                            ->setStateId($data['State'])
                            ->setRegister($data['Register'])
                            ->setDocument($data['Document'])
                            ->setGenre($data['Genre'])
                            ->setPhone($data['Phone'])
                            ->setAddress($data['Address'])
                            ->setNumber($data['Number'])
                            ->setComplement($data['Complement'])
                            ->setCity($data['City'])
                            ->setRegion($data['Region'])
                            ->setZipCode($data['ZipCode'])
                            ->setMember($data['Member'])
                            ->setConfirmed($data['Confirmed'])
                            ->setMinister($data['Minister'])
                            ->setUnityName($data['UnityName'])
                            ->setSpecialNeed($data['SpecialNeed'])
                            ->setComment($data['Comment'])
                            ->save();
                    
                    if (!empty($data['Password'])) {
                        $user = $this->getUser();
                        $factory = $this->get('security.encoder_factory');
                        $encoder = $factory->getEncoder($user);
                        $password = $encoder->encodePassword($data['Password'], $user->getSalt());
                        $user->setPassword($password);
                    }

                    $user->setName($data['Name'])
                            ->save();

                    $conn->commit();

                    $request->getSession()->getFlashBag()->add('success', 'Seus dados foram salvos com sucesso!');
                
                    return $this->redirect($this->generateUrl('subscribe_area_edit'));

                } catch (Exception $ex) {
                    $conn->rollBack();
                }
            } else {
                echo "<pre>";
                print_r($form->getErrorsAsString());
                exit();
            }
        }
        
        $subscribeTypeName = SubscribeTypeNameQuery::create()
                ->select('Name')
                ->filterBySubscribeTypeId($subscribe->getSubscribeTypeId())
                ->useLanguageQuery()
                    ->filterByCode($this->getLocale($request))
                ->endUse()
                ->findOne();
        
        $subscribeNumber = $subscribe->getId();
        if (strlen($subscribeNumber) < 6) {
            $subscribeNumber = str_repeat("0", (6 - strlen($subscribeNumber))) . $subscribeNumber;
        }
        
        $openedPayment = (boolean) ($subscribe->getPaidPrice() < $subscribe->getPrice());

        $nextPayment = SubscribePaymentQuery::create()
                ->filterBySubscribeId($subscribe->getId())
                ->filterByCancel(0)
                ->_or()
                ->filterByCancel(null, \Criteria::ISNULL)
                ->_and()
                ->filterByPaid(0)
                ->_or()
                ->filterByPaid(null, \Criteria::ISNULL)
                ->findOne();
        
        $canceled = SubscribePaymentQuery::create()
                ->filterBySubscribeId($subscribe->getId())
                ->filterByFirst(1)
                ->orderById(\Criteria::DESC)
                ->findOne();
        
        $paymentText = null;
        if ($subscribe->getCountryId() > 0) {
            $text = PaymentTextQuery::create()
                    ->filterByCountryId($subscribe->getCountryId())
                    ->findOne();
            if (null !== $text) {
                $paymentText = $text->getText();
            } else {
                $text = PaymentTextQuery::create()
                        ->filterByCountryId(null)
                        ->findOne();
                if (null !== $text) {
                    $paymentText = $text->getText();
                }
            }
        }

        return $this->prepareView(array(
            'subscribeNumber' => $subscribeNumber,
            'subscribeTypeName' => $subscribeTypeName,
            'subscribe' => $subscribe,
            'openedPayment' => $openedPayment,
            'paidPrice' => $subscribe->getPaidPrice(),
            'nextPayment' => $nextPayment,
            'canceled' => $canceled,
            'paymentText' => $paymentText,
            'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/caravana", name="subscribe_area_train")
     * @Template()
     */
    public function trainAction(Request $request)
    {
        
        $subscribe = $this->getUser()->getSubscribes()->getFirst();
        $train = TrainQuery::create()
                ->filterBySubscribeId($subscribe->getId())
                ->findOneOrCreate();
        
        $form = $this->createForm(new TrainForm(), $train, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $train = $form->getData();
                $train->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Caravana salva com sucesso.');

                return $this->redirect($this->generateUrl('subscribe_area_train'));
            }
        }
        
        return $this->prepareView(array(
            'form' => $form->createView(),
            'subscribe' => $subscribe,
        ));
    }
    
    /**
     * @Route("/reativar-inscricao", name="subscribe_area_reactive")
     * 
     * @param Request $request
     */
    public function reactiveAction(Request $request)
    {
        
        $subscribe = $this->getUser()->getSubscribes()->getFirst();
        $newPrice = SubscribeTypePriceQuery::create()
                ->filterBySubscribeTypeId($subscribe->getSubscribeTypeId())
                ->filterByStart(time(), \Criteria::LESS_EQUAL)
                ->filterByEnd(time(), \Criteria::GREATER_EQUAL)
                ->findOne();
        $newQuotas = $subscribe->getQuota();
        if ($subscribe->getQuota() > $newPrice->getQuota()) {
            $newQuotas = $newPrice->getQuota();
        }
        $newPriceValue = $subscribe->getPrice();

        $subscribe->setQuota($newQuotas)
                ->save();
        
        $dueDate = date("Y-m-10");
        if ((int) date("d") >= 10) {
            if ($newQuotas == 9
                    || $newQuotas == 8
                    || $newQuotas == 7) {
                $dueDate = date("Y-m-d", strtotime("+5 days"));
            } else {
                $dueDate = date("Y-m-10", strtotime("+1 month"));
            }
        }

        $newPriceValue = $newPriceValue / $newQuotas;
        $quota = new SubscribePayment();
        $quota->setSubscribe($subscribe)
                ->setCreateDate(time())
                ->setDueDate($dueDate)
                ->setFirst(1)
                ->setPrice($newPriceValue)
                ->save();
        
        return $this->redirect($this->generateUrl('subscribe_area'));
    }
    
    /**
     * 
     */
    protected function getForm(Subscribe $subscribe)
    {
        
        $bind = SubscribeQuery::create()
                ->select(array(
                    'SubscribeTypeId',
                    'Quota',
                    'Email',
                    'Name',
                    'Birthday',
                    'Genre',
                    'Register',
                    'Document',
                    'Phone',
                    'CountryId',
                    'StateId',
                    'City',
                    'Region',
                    'Address',
                    'Number',
                    'Complement',
                    'ZipCode',
                    'Member',
                    'Confirmed',
                    'Minister',
                    'UnityName',
                    'DistrictId',
                    'DisplacementId',
                    'SpecialNeed',
                    'Comment',
                    'Free',
                    'Organization',
                    'Visitor',
                ))
                ->filterById($subscribe->getId())
                ->findOne();
        
        $bind['Birthday'] = new \DateTime($bind['Birthday']);
        $bind['Country'] = CountryQuery::create()
                ->findOneById($bind['CountryId']);
        $bind['State'] = $bind['StateId'];
        $bind['Member'] = (boolean) $bind['Member'];
        $bind['Free'] = (boolean) $bind['Free'];
        $bind['Organization'] = (boolean) $bind['Organization'];
        $bind['Confirmed'] = (boolean) $bind['Confirmed'];
        $bind['Visitor'] = (boolean) $bind['Visitor'];
        $bind['District'] = DistrictQuery::create()
                ->findOneById($bind['DistrictId']);
        
        $form = $this->createForm(new SubscribeEditForm($this->container, $this->getLocale($this->getRequest())), $bind, array(
            'csrf_protection' => true,
        ));
 
        return $form;
    }
    
    /**
     * @Route("/translado", name="subscribe_area_transfer")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function transferAction(Request $request)
    {
        
        $subscribe = $this->getUser()->getSubscribes()->getFirst();
        $transfer = $subscribe->getTransfer();
        if (null === $transfer) {
            $transfer = new Transfer();
            $transfer->setSubscribe($subscribe)
                    ->setPhone($subscribe->getPhone());
        }
        
        $form = $this->createForm(new TransferForm(), $transfer, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $transfer = $form->getData();
                $transfer->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Os dados do translado foram salvos com sucesso!');
                return $this->redirect($this->generateUrl('subscribe_area_transfer'));
            }
        }
        
        return $this->prepareView(array(
            'subscribe' => $subscribe,
            'form' => $form->createView(),
        ));
    }
}