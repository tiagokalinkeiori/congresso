<?php

namespace AppBundle\Controller;

use AppBundle\Model\PaymentTextQuery;
use AppBundle\Model\StateQuery;
use AppBundle\Model\SubscribeTypeQuery;
use AppBundle\Model\SubscribeTypePriceQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/ajax")
 */
class AjaxController extends BaseController
{

    /**
     * @Route("/states", name="ajax_states")
     */
    public function statesAction(Request $request)
    {
        
        $translator = $this->container->get('translator');
        $request->setRequestFormat('json');
        $countryId = (int) $request->query->get('id');
        
        $paymentText = null;
        
        if ($countryId > 0) {
            $text = PaymentTextQuery::create()
                    ->filterByCountryId($countryId)
                    ->findOne();
            if (null !== $text) {
                $paymentText = $text->getText();
            } else {
                $text = PaymentTextQuery::create()
                        ->filterByCountryId(null)
                        ->findOne();
                if (null !== $text) {
                    $paymentText = $text->getText();
                }
            }

            $choices = array(
                array("Id" => "", "Name" => $translator->trans("form.label.state", array(), null, $this->getLocale($request))),
            );
            $states = StateQuery::create()
                    ->filterByCountryId($countryId)
                    ->orderByName()
                    ->find();
            foreach ($states as $item) {
                $choices[] = array(
                    "Id" => $item->getId(),
                    "Name" => $item->getName(),
                );
            }
        } else {
            $choices = array(
                '' => $translator->trans("form.empty.state", array(), null, $this->getLocale($request)),
            );
        }
        
        return new JsonResponse(array(
            'result' => $choices,
            'text' => $paymentText,
        ));
    }
    
    /**
     * @Route("/tipos", name="ajax_types")
     */
    public function typesAction(Request $request)
    {
        
        $translator = $this->container->get('translator');
        $request->setRequestFormat('json');
        $genre = $request->query->get('id');
        
        $choices = array(
            array("Id" => "", "Name" => $translator->trans("form.label.subscribetype", array(), null, $this->getLocale($request))),
        );
        $types = SubscribeTypeQuery::create('st')
                ->leftJoinSubscribe('s')
                ->addJoinCondition('s', '((s.Canceled = 0 OR s.Canceled IS NULL) AND (s.Organization = 0 OR s.Organization IS NULL))')
                ->useSubscribeTypeNameQuery('stn')
                    ->useLanguageQuery('l')
                        ->filterByCode($this->getLocale($request))
                    ->endUse()
                ->endUse()
                ->withColumn('stn.Name', 'Name')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->select(array(
                    'Id',
                    'Name',
                    'SpaceAvailable',
                    'Total',
                ))
                ->filterByGenre($genre)
                ->_or()
                ->filterByGenre(null, \Criteria::ISNULL)
                ->groupById()
                ->find()
                ->toArray();
        foreach ($types as $item) {
            if ((int) $item['SpaceAvailable'] == 0 || ((int) $item['SpaceAvailable'] > 0 && $item['SpaceAvailable'] > $item['Total'])) {
                $choices[] = array(
                    "Id" => $item['Id'],
                    "Name" => $item['Name'],
                );
            }
        }
        
        return new JsonResponse(array(
            'result' => $choices,
        ));
    }
    
    /**
     * @Route("/quota", name="ajax_quota")
     */
    public function quotaAction(Request $request)
    {
        
        $translator = $this->container->get('translator');
        $request->setRequestFormat('json');
        $subscribeTypeId = (int) $request->query->get('id');
        if ($subscribeTypeId > 0) {
            $price = SubscribeTypePriceQuery::create()
                    ->filterBySubscribeTypeId($subscribeTypeId)
                    ->filterByStart(time(), \Criteria::LESS_EQUAL)
                    ->filterByEnd(time(), \Criteria::GREATER_EQUAL)
                    ->findOne();
            $choices = array();
            if (null !== $price) {
                $choices[] = array(
                    "Id" => 1,
                    "Name" => sprintf("1x R$ %s", number_format($price->getPrice(), 2, ",", ".")),
                );

                if ($this->getLocale($request) == "pt_BR" && $price->getQuota() > 1) {
                    if (null !== $price->getInstallmentValue()) {
                        $value = $price->getInstallmentValue();
                    } else {
                        $value = $price->getPrice();
                    }
                    
                    for ($countQuota = 2; $countQuota <= $price->getQuota(); $countQuota++) {
                        $quotaPrice = $value / $countQuota;
                        $choices[] = array(
                            "Id" => $countQuota,
                            "Name" => sprintf("%dx R$ %s", $countQuota, number_format($quotaPrice, 2, ",", ".")),
                        );
                    }
                }
            }
        } else {
            $choices = array(
                array("Id" => "", "Name" => $translator->trans("form.empty.quota", array(), null, $this->getLocale($request)))
            );
        }
        
        return new JsonResponse(array(
            'result' => $choices,
        ));
    }
}