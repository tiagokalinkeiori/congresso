<?php

namespace AppBundle\Command;

use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePaymentQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PaymentGenerateCommand extends ContainerAwareCommand
{

    /**
     * 
     * @see \Symfony\Component\Console\Command\ContainerAwareCommand::configure()
     */
    public function configure()
    {
        
        return $this->setName("payment:generate");
    }
    
    /**
     * 
     * @see \Symfony\Component\Console\Command\ContainerAwareCommand::execute(InputInterface $input, OutputInterface $output)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $output->writeln(sprintf("[START] Processo %s iniciado em %s", $this->getName(), date("Y-m-d H:i:s")));
        
        $date = date("Y-m-d", strtotime("-10 days"));
        $payments = SubscribePaymentQuery::create('sp')
                ->useSubscribeQuery('s')->endUse()
                ->filterByDueDate($date)
                ->filterByPaid(1)
                ->find();

        $total = 0;
        if ($payments->count() > 0) {
            foreach ($payments as $item) {
                $subscribe = $item->getSubscribe();

                if (ceil($subscribe->getPaidPrice()) < $subscribe->getPrice()) {
                    $generated = (boolean) SubscribePaymentQuery::create('sp')
                            ->filterByCreateDate(array(
                                'min' => date('Y-m-d 00:00:00'),
                                'max' => date('Y-m-d 23:59:59'),
                            ))
                            ->filterBySubscribe($subscribe)
                            ->count();

                    if (!$generated) {
                        $hasCancel = (boolean) SubscribePaymentQuery::create()
                                ->filterBySubscribe($subscribe)
                                ->filterByCancel(1)
                                ->count();

                        $price = $item->getPrice();
                        $dueDate = date("Y-m-10", strtotime("+1 month"));
                        if ($item->getFirst() && (int) $item->getDueDate()->format("d") != 10) {
                            $dueDate = date("Y-m-10");
                        }

                        $quota = new SubscribePayment();
                        $quota->setSubscribe($subscribe)
                                ->setCreateDate(time())
                                ->setDueDate($dueDate)
                                ->setPrice($price)
                                ->save();

                        $output->writeln(sprintf("Inscrito: %s", $subscribe->getName()));
                        $total++;
                    }
                }
            }
        }
        
        if ($total == 0) {
            $output->writeln("- Nenhuma parcela encontrada para ser gerada");
        }
    }
}
