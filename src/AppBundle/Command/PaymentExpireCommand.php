<?php

namespace AppBundle\Command;

use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePaymentQuery;
use AppBundle\Model\SubscribeTypePriceQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PaymentExpireCommand extends ContainerAwareCommand
{
    
    /**
     * 
     * @see \Symfony\Component\Console\Command\ContainerAwareCommand::configure()
     */
    public function configure()
    {
        
        return $this->setName("payment:expire");
    }
    
    /**
     * 
     * @see \Symfony\Component\Console\Command\ContainerAwareCommand::execute(InputInterface $input, OutputInterface $output)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $output->writeln(sprintf("[START] Processo %s iniciado em %s", $this->getName(), date("Y-m-d H:i:s")));

        $date = date("Y-m-d", strtotime("-10 days"));
        $payments = SubscribePaymentQuery::create('sp')
                ->useSubscribeQuery('s')
                    ->filterByCountryId(30)
                ->endUse()
                ->filterByDueDate($date, \Criteria::LESS_THAN)
                ->filterByCancel(0)
                ->_or()
                ->filterByCancel(null, \Criteria::ISNULL)
                ->_and()
                ->filterByPaid(0)
                ->_or()
                ->filterByPaid(null, \Criteria::ISNULL)
                ->find();
        
        $conn = \Propel::getConnection();
        if ($payments->count() > 0) {
            foreach ($payments as $item) {
                $subscribe = $item->getSubscribe();
                $paidPrice = $subscribe->getPaidPrice();

                try {
                    $conn->beginTransaction();
                    
                    $item->setCancel(true)
                        ->save();

                    if (!$item->getFirst()) {
                        $dueDate = date("Y-m-10", strtotime("+1 month"));

                        $newPrice = SubscribeTypePriceQuery::create()
                                ->filterBySubscribeTypeId($subscribe->getSubscribeTypeId())
                                ->filterByStart(time(), \Criteria::LESS_EQUAL)
                                ->filterByEnd(time(), \Criteria::GREATER_EQUAL)
                                ->findOne();

                        $newQuotas = $subscribe->getQuota();
                        if (null !== $newPrice && $subscribe->getQuota() > $newPrice->getQuota()) {
                            $newQuotas = $newPrice->getQuota();
                        }
                        $newPriceValue = $subscribe->getPrice();

                        $subscribe->setQuota($newQuotas)
                                ->save();

                        if ($newQuotas > 1) {
                            $newQuotaPriceValue = ($newPriceValue - $paidPrice) / ($newQuotas - 1);
                        } else {
                            $newQuotaPriceValue = $newPriceValue - $paidPrice;
                        }
                        $quota = new SubscribePayment();
                        $quota->setSubscribe($subscribe)
                                ->setCreateDate(time())
                                ->setDueDate($dueDate)
                                ->setPrice($newQuotaPriceValue)
                                ->save();
                    }
                    
                    $conn->commit();
                    
                } catch(\Exception $e) {
                    $conn->rollBack();
                }

                $output->writeln(sprintf("Inscrito: %s", $subscribe->getName()));
            }
        } else {
            $output->writeln("- Nenhum pagamento encontrado para ser expirado");
        }
    }
}
