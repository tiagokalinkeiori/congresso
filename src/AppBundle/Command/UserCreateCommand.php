<?php

namespace AppBundle\Command;

use AppBundle\Model\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command to create a new user.
 */
class UserCreateCommand extends ContainerAwareCommand
{

    /**
     * 
     * @see \Symfony\Component\Console\Command\ContainerAwareCommand::configure()
     */
    protected function configure()
    {

        $this->setName('user:create')
                ->setDescription('Command to create a new user.')
                ->addArgument('group', InputArgument::REQUIRED, 'User group ID' )
                ->addArgument('name', InputArgument::REQUIRED, 'User name')
                ->addArgument('email', InputArgument::REQUIRED, 'User email')
                ->addArgument('password', InputArgument::REQUIRED, 'User password');
    }

    /**
     * 
     * @see \Symfony\Component\Console\Command\ContainerAwareCommand::execute(InputInterface $input, OutputInterface $output)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('<comment>Creating the new user...</comment>');

        $group = $input->getArgument('group');
        $name = $input->getArgument('name');
        $email = $input->getArgument('email');
        $plainPassword = $input->getArgument('password');

        $user = new User();
        $user->setGroupId($group)
                ->setName($name)
                ->setEmail($email)
                ->setSalt(md5(uniqid(null, true)));

        $factory = $this->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($plainPassword, $user->getSalt());

        $user->setPassword($password);
        $user->save();

        $output->writeln('<info>Done!</info>');
    }

}