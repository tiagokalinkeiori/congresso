<?php

namespace AppBundle\Util;

class NumberUtil
{

    /**
     * 
     * @param string $str
     * @return float
     */
    public static function strToFloat($str)
    {
        $separators = array();
        $str = preg_replace('/[^0-9,\.]/', '', $str);
        if (preg_match_all('/[,\.]/', $str, $separators)) {
            $commaCount = substr_count($str, ',');
            $dotCount = substr_count($str, '.');

            // preg_match_all returns a multidimensional array
            $separators = array_shift($separators);
            $lastSeparator = end($separators);

            if (
                    // 1.234,99 or 1234,99
                    ($lastSeparator == ',' && $commaCount == 1)

                    // 1.234.567
                    || $dotCount > 1

                    // 1.234
                    // This case can match a decimal with precision 3, but what 
                    // can we do to avoid it? Only numbers above 999.999 with a 
                    // single dot, like 1234.000, will not match this condition, 
                    // since it's obviously a float.
                    || ('.' == substr($str, -4, 1) && strlen($str) <= 7)) {
                $str = str_replace(array('.', ','), array('', '.'), $str);
            } else { // 1,234,567 or 1,234.99
                $str = str_replace(',', '', $str);
            }
        }

        return (float) $str;
    }

    /**
     * 
     * @param string $str
     * @param boolean $getAllNumbers
     * @return int
     */
    public static function strToInt($str, $getAllNumbers = false)
    {
        if ($getAllNumbers) {
            $str = preg_replace('/[^0-9\.,]+/', '', $str);
        }

        return intval($str);
    }

}
