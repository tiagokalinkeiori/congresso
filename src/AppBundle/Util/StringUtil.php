<?php

namespace AppBundle\Util;

class StringUtil
{

    /**
     * Converts all accentued chars to basic lower case chars and removes all
     * other chars to <code>"-"</code>.
     *
     * @param string $str
     * @return string
     */
    public static function slugify($str)
    {

        $str = trim(strtolower(self::removeAccents($str)));
        if (trim(preg_replace('/[^0-9a-z]+/', '', $str)) !== "") {
            $str = preg_replace('/[^0-9a-z]+/', '-', $str);
            if (substr($str, -1) == "-") {
                $str = substr($str, 0, -1);
            }
            if (substr($str, 0, 1) == "-") {
                $str = substr($str, 1);
            }
            
            return $str;
        } else {
            return null;
        }
    }

    /**
     * Converts all accentued chars to basic lower case chars and removes all
     * other chars to <code>"-"</code>.
     *
     * @param string $str
     * @return string
     */
    public static function urlToQuery($str)
    {

        return str_replace('-', '%', $str);
    }

    /**
     * Converts all <code>&gt;br&lt;</code>, and variants, to line feeds.
     *
     * @param string $str
     * @return string
     */
    public static function br2nl($str)
    {

        return preg_replace('/<br[^>]*>/i', "\n", str_replace("\n", '', $str));
    }

    /**
     * Checks if a string is a base 64 encoded value.
     *
     * @param string $str
     * @return boolean
     */
    public static function isBase64($str)
    {

        return false !== base64_decode($str, true);
    }

    /**
     * Convert all accents to basic ASC-II characters.
     *
     * @param string $text
     * @return string
     */
    public static function convertAccents($text = '')
    {

        $accents = array(
            'À','Á','Ã','Â', 'à','á','ã','â',
            'Ê', 'É',
            'Í', 'í',
            'Ó','Õ','Ô', 'Ö', 'ó', 'õ', 'ô', 'ö',
            'Ú','Ü',
            'Ç', 'ç',
            'é','ê',
            'ú','ü',
            );
        $remove = array(
            'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a',
            'e', 'e',
            'i', 'i',
            'o', 'o','o', 'o', 'o', 'o','o', 'o',
            'u', 'u',
            'c', 'c',
            'e', 'e',
            'u', 'u',
            '´', '\'',
            'º', '',
            );

        return strtolower(str_replace($accents, $remove, urldecode($text)));
    }

    /**
     * Generate password
     * @return type
     */
    public static function generatePassword($limit = 8)
    {

        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789@#$%&*";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $limit; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass);
    }

    /**
     * Converts a number to your respective letter
     *
     * @param int $data
     * @return string
     */
    public static function toAlpha($data)
    {

        $alphabet =   array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
        if($data <= 25){
            return $alphabet[$data];
        }
        elseif($data > 25){
            $dividend = ($data + 1);
            $alpha = '';
            $modulo;
            while ($dividend > 0){
                $modulo = ($dividend - 1) % 26;
                $alpha = $alphabet[$modulo] . $alpha;
                $dividend = floor((($dividend - $modulo) / 26));
            }

            return strtoupper($alpha);
        }
    }
    
    /**
     * 
     * @param string $userAgent
     * @return boolean
     */
    public static function isMobile($userAgent)
    {

        if (preg_match('/(android|blackberry|iphone|phone|ipad|playbook)/i', $userAgent)) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param string $cpf
     * @return boolean
     */
    public static function isValidCpf($cpf = null)
    {

        $cpf = preg_replace("/[^0-9]/", "", $cpf);

        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }
 
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        elseif ($cpf == '00000000000'
                || $cpf == '11111111111'
                || $cpf == '22222222222'
                || $cpf == '33333333333'
                || $cpf == '44444444444'
                || $cpf == '55555555555'
                || $cpf == '66666666666'
                || $cpf == '77777777777'
                || $cpf == '88888888888'
                || $cpf == '99999999999') {
            return false;
        // Calcula os digitos verificadores para verificar se o
        // CPF é válido
        } else {   
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
 
            return true;
        }
    }
}
