<?php

namespace AppBundle\Util;

use AppBundle\Model\ConfigurationQuery;

class ConfigurationUtil
{
    
    public static $all;
    
    public static function all()
    {
        
        if (self :: $all == null) {
            self :: $all = array();

            $configuration = ConfigurationQuery::create()
                    ->find();
            
            foreach ($configuration as $item) {
                self :: $all[$item->getName()] = $item->getValue();
            }
        }
        
        return self :: $all;
    }
}