<?php

namespace AdminBundle\Form;

use AppBundle\Model\LanguageQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class NoticeForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('Language', 'model', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
            'class' => '\AppBundle\Model\Language',
            'property' => 'Name',
            'query' => LanguageQuery::create(),
            'empty_value' => 'Idioma',
        ));
        
        $builder->add('Title', 'text', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
        
        $builder->add('Content', 'textarea', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => '\AppBundle\Model\Notice',
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}