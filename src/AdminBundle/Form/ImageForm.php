<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;

class ImageForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('Image', 'file', array(
            'mapped' => false,
            'required' => false,
            'constraints' => array(
                new File(array(
                    'maxSize' => '5M',
                    'maxSizeMessage' => 'O tamanho do arquivo é maior que o permitido',
                )),
                new Image(array(
                    'mimeTypes'=> array(
                        'image/jpg',
                        'image/jpeg',
                        'image/png',
                        'image/gif',
                    ),
                    'mimeTypesMessage' => 'O tipo de arquivo não é permitido',
                )),
            ),
            'data_class' => null,
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}