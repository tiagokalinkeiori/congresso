<?php

namespace AdminBundle\Form;

use AdminBundle\Form\SubscribeTypePriceForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class SubscribeTypeForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        
        $builder->add("NamePt", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        /*$builder->add("NameEn", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));*/
        $builder->add("NameEs", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add("SpaceAvailable", "integer", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add('Prices', 'collection', array(
            'type' => new SubscribeTypePriceForm(),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference'  => false,
        ));
        $builder->add('Genre', 'choice', array(
            'required' => false,
            'choices' => array(
                '' => 'Todos',
                'M' => 'Masculino',
                'F' => 'Feminino',
            ),
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}
