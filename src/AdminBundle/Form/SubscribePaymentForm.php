<?php

namespace AdminBundle\Form;

use AppBundle\Form\DataTransformer\FloatTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubscribePaymentForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        
        $builder->add("SubscribeId", "hidden", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ));
        $builder->add($builder->create("Price", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ))->addModelTransformer(new FloatTransformer()));
        $builder->add('DueDate', 'date', array(
            'input' => 'string',
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'constraints' => array(
                new NotBlank(),
                new Date(),
            ),
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => '\AppBundle\Model\SubscribePayment',
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}
