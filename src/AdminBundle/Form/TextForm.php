<?php

namespace AdminBundle\Form;

use AppBundle\Model\LanguageQuery;
use AppBundle\Model\PageQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class TextForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('Page', 'model', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
            'class' => '\AppBundle\Model\Page',
            'property' => 'Name',
            'query' => PageQuery::create()->orderByName(),
            'empty_value' => 'Página',
        ));
        
        $builder->add('Language', 'model', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
            'class' => '\AppBundle\Model\Language',
            'property' => 'Name',
            'query' => LanguageQuery::create(),
            'empty_value' => 'Idioma',
        ));
        
        $builder->add('Content', 'textarea', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => '\AppBundle\Model\Text',
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}