<?php

namespace AdminBundle\Form;

use AppBundle\Model\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserForm extends AbstractType
{
    
    /**
     *
     * @var User
     */
    private $user;
    
    /**
     *
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);

        $builder->add('Name', 'text', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));

        $builder->add('Email', 'text', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
                new Email(),
            ),
        ));

        if (null !== $this->user) {
            $builder->add('Password', 'password', array(
                'required' => false,
                'constraints' => array(),
                'data' => '',
            ));
        } else {
            $builder->add('Password', 'password', array(
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                ),
                'data' => '',
            ));
        }
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}