<?php

namespace AdminBundle\Form;

use AppBundle\Form\DataTransformer\FloatTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class SubscribeTypePriceForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);
        
        $builder->add("Id", "hidden", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add("SubscribeTypeId", "hidden", array(
            "required" => false,
            "constraints" => array(),
        ));
        $builder->add($builder->create("Price", "text", array(
            "required" => true,
            "constraints" => array(
                new NotBlank(),
            ),
        ))->addModelTransformer(new FloatTransformer()));
        $builder->add("Quota", "integer", array(
            "required" => true,
            "constraints" => array(
                new Range(array(
                    'min' => 1,
                    'max' => 99,
                )),
            ),
        ));
        $builder->add($builder->create("InstallmentValue", "text", array(
            "required" => false,
            "constraints" => array(),
        ))->addModelTransformer(new FloatTransformer()));
        $builder->add('Start', 'date', array(
            'input' => 'string',
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'constraints' => array(
                new NotBlank(),
                new Date(),
            ),
        ));
        $builder->add('End', 'date', array(
            'input' => 'string',
            'required' => false,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'constraints' => array(
                new Date(),
            ),
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}
