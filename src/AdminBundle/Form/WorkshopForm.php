<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;

class WorkshopForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('Name', 'text', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
        $builder->add('Description', 'textarea', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
        $builder->add('Speaker', 'text', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
        $builder->add('Modality', 'text', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
        $builder->add('Start', 'datetime', array(
            'with_seconds' => false,
            'input' => 'datetime',
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy HH:mm',
            'constraints' => array(
                new NotBlank(),
                new DateTime(),
            ),
        ));
        $builder->add('End', 'datetime', array(
            'with_seconds' => false,
            'input' => 'datetime',
            'required' => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy HH:mm',
            'constraints' => array(
                new NotBlank(),
                new DateTime(),
            ),
        ));
        $builder->add('SpaceAvailable', 'integer', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
        $builder->add('Opened', 'checkbox', array(
            'required' => false,
            'constraints' => array(),
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => '\AppBundle\Model\Workshop',
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}