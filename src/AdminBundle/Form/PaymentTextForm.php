<?php

namespace AdminBundle\Form;

use AppBundle\Model\CountryQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PaymentTextForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);

        $builder->add('Country', 'model', array(
            'required' => false,
            'constraints' => array(),
            'class' => '\AppBundle\Model\Country',
            'property' => 'Name',
            'query' => CountryQuery::create(),
            'empty_value' => 'Todos',
        ));
        
        $builder->add('Text', 'textarea', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
            ),
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => '\AppBundle\Model\PaymentText',
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}