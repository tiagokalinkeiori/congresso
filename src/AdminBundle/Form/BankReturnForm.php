<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class BankReturnForm extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('File', 'file', array(
            'mapped' => false,
            'required' => false,
            'constraints' => array(
                new File(),
            ),
            'data_class' => null,
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {

        return 'form';
    }
}