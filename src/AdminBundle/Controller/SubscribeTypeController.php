<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\SubscribeTypeForm;
use AdminBundle\Form\SubscribeTypePriceForm;
use AppBundle\Model\SubscribeType;
use AppBundle\Model\SubscribeTypeNameQuery;
use AppBundle\Model\SubscribeTypePrice;
use AppBundle\Model\SubscribeTypePriceQuery;
use AppBundle\Model\SubscribeTypeQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/tipos-de-inscricao")
 */
class SubscribeTypeController extends Controller
{
    
    /**
     * @Route("/", name="admin_configuration_registrationtype")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $requestQuery = $request->query->all();
        $returnUrl = $this->generateUrl($request->get('_route'), $requestQuery);
        $request->getSession()->getFlashBag()->set('returnUrl', $returnUrl);

        $page = $request->query->get('pagina', 1);
        $limit = 10;
        
        $types = SubscribeTypeQuery::create()
                ->useSubscribeTypeNameQuery('stn')->endUse()
                ->addJoinCondition('stn', 'stn.LanguageId = 1')
                ->withColumn('stn.Name', 'Name')
                ->select(array(
                    'Id',
                    'Name',
                    'SpaceAvailable',
                    'Genre',
                ))
                ->groupById()
                ->paginate($page, $limit);

        return array(
            'types' => $types,
        );
    }
    
    /**
     * @Route("/criar", defaults={"id" = null}, name="admin_configuration_registrationtype_create")
     * @Route("/editar/{id}", defaults={"id" = "\d+"}, name="admin_configuration_registrationtype_edit")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function editAction(Request $request, $id = null)
    {
        
        $type = null;
        if (null !== $id) {
            $type = SubscribeTypeQuery::create()
                    ->leftJoinSubscribeTypeName('pt')
                    ->addJoinCondition('pt', 'pt.LanguageId = 1')
                    //->leftJoinSubscribeTypeName('en')
                    //->addJoinCondition('en', 'en.LanguageId = 2')
                    ->leftJoinSubscribeTypeName('es')
                    ->addJoinCondition('es', 'es.LanguageId = 3')
                    ->withColumn('pt.Name', 'NamePt')
                    //->withColumn('en.Name', 'NameEn')
                    ->withColumn('es.Name', 'NameEs')
                    ->select(array(
                        'Id',
                        'NamePt',
                        //'NameEn',
                        'NameEs',
                        'SpaceAvailable',
                        'Genre',
                    ))
                    ->filterById($id)
                    ->findOne();

            $type['Prices'] = SubscribeTypePriceQuery::create()
                    ->select(array(
                        'Id',
                        'SubscribeTypeId',
                        'Price',
                        'Quota',
                        'InstallmentValue',
                        'Start',
                        'End',
                    ))
                    ->filterBySubscribeTypeId($type['Id'])
                    ->orderByStart()
                    ->orderByEnd()
                    ->find()
                    ->toArray();
        }
        
        $form = $this->createForm(new SubscribeTypeForm(), $type, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $formData = $form->getData();
                
                if ($id > 0) {
                    $type = SubscribeTypeQuery::create()
                            ->findOneById($id);
                } else {
                    $type = new SubscribeType();
                }
                
                $type->setSpaceAvailable($formData['SpaceAvailable'])
                        ->setGenre($formData['Genre'])
                        ->save();

                $languages = array(
                    1 => 'NamePt',
                    //2 => 'NameEn',
                    3 => 'NameEs',
                );
                foreach ($languages as $languageId => $fieldName) {
                    $name = SubscribeTypeNameQuery::create()
                            ->filterByLanguageId($languageId)
                            ->filterBySubscribeTypeId($type->getId())
                            ->findOneOrCreate();
                    $name->setName($formData[$fieldName])
                            ->save();
                }
                
                $savedPrices = array();
                foreach ($formData['Prices'] as $item) {
                    if ($item['Id'] > 0) {
                        $price = SubscribeTypePriceQuery::create()
                                ->findOneById($item['Id']);
                    } else {
                        $price = new SubscribeTypePrice();
                        $price->setSubscribeTypeId($type->getId());
                    }
                    
                    $price->setStart($item['Start'])
                            ->setEnd($item['End'])
                            ->setPrice($item['Price'])
                            ->setQuota($item['Quota'])
                            ->setInstallmentValue($item['InstallmentValue'])
                            ->save();
                    
                    $savedPrices[] = $price->getId();
                }
                
                SubscribeTypePriceQuery::create()
                        ->filterBySubscribeTypeId($type->getId())
                        ->filterById($savedPrices, \Criteria::NOT_IN)
                        ->delete();

                $request->getSession()->getFlashBag()->add('success', 'Tipo de inscrição salvo com sucesso!');

                return $this->redirect($this->generateUrl('admin_configuration_registrationtype_edit', array(
                    'id' => $type->getId(),
                )));
            }
        }
        
        return array(
            'id' => $id,
            'type' => $type,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/formulario-preco", name="admin_configuration_registrationtype_ajax_price")
     * @Template("AdminBundle:SubscribeType:price.html.twig")
     */
    public function priceFormAction()
    {

        $id = (int) $this->getRequest()->query->get('id');
        $count = (int) $this->getRequest()->query->get('posicao', 0);
        $bind = array(
            'Quota' => 1,
        );
        if ($id > 0) {
            $bind['SubscribeTypeId'] = $id;
        }

        $form = $this->createForm(new SubscribeTypePriceForm(), $bind, array(
            'csrf_protection' => false,
        ));
        $formView = $form->createView();
        foreach ($formView->children as $key => $item) {
            $formView->children[$key]->vars['full_name'] = 'form[Prices]['. $count .']['.$key.']';
            $formView->children[$key]->vars['id'] = 'form_Prices_'. $count .'_'.$key;
        }

        return array(
            'item' => $formView
        );
    }
}
