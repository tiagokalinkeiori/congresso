<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ImageForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/imagens")
 */
class ImageController extends Controller
{
    
    /**
     * @Route("/", name="admin_content_image")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {

        $dir = $this->get('kernel')->getRootDir() . "/../web/assets/images/uploaded/";
        $form = $this->createForm(new ImageForm(), null, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                try {
                    $file = $form->get('Image')->getData();
                    $fileName = md5(time()) . "." . $file->getClientOriginalExtension();
                    $file->move($dir, $fileName);

                    $request->getSession()->getFlashBag()->add('success', 'Imagem enviada com sucesso!');
                } catch(\Exception $e) {
                    $request->getSession()->getFlashBag()->add('error', 'Ocorreu um erro ao enviar a imagem. Tente novamente!');
                }

                return $this->redirect($this->generateUrl('admin_content_image'));
            }
        }
        
        try {
            $finder = new Finder();
            $files = $finder->sortByName()->files()->in($dir);
        } catch (\Exception $e) {
            $files = array();
        }
        
        return array(
            'form' => $form->createView(),
            'files' => $files,
        );
    }
    
    /**
     * @Route("/remove", name="admin_content_image_remove")
     * 
     * @param Request $request
     */
    public function removeAction(Request $request)
    {
        
        $fileName = $request->query->get('arquivo');
        $dir = $this->get('kernel')->getRootDir() . "/../web/assets/images/uploaded/";
        unlink($dir . $fileName);
        
        return $this->redirect($this->generateUrl('admin_content_image'));
    }
}