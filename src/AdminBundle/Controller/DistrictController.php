<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\DistrictForm;
use AppBundle\Model\DistrictQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/distritos")
 */
class DistrictController extends Controller
{
    
    /**
     * @Route("/", name="admin_content_district")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $requestQuery = $request->query->all();
        $returnUrl = $this->generateUrl($request->get('_route'), $requestQuery);
        $request->getSession()->getFlashBag()->set('returnUrl', $returnUrl);

        $page = $request->query->get('pagina', 1);
        $limit = 10;
        
        $districts = DistrictQuery::create()
                ->paginate($page, $limit);
        
        return array(
            'districts' => $districts,
        );
    }
    
    /**
     * @Route("/criar", defaults={"id" = null}, name="admin_content_district_create")
     * @Route("/editar/{id}", defaults={"id" = "\d+"}, name="admin_content_district_edit")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function editAction(Request $request, $id = null)
    {
        
        $district = null;
        if (null !== $id) {
            $district = DistrictQuery::create()->findOneById($id);
        }
        
        $form = $this->createForm(new DistrictForm(), $district, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $district = $form->getData();
                $district->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Distrito salvo com sucesso!');
                
                return $this->redirect($this->generateUrl('admin_content_district_edit', array(
                    'id' => $district->getId(),
                )));
            }
        }
        
        return array(
            'id' => $id,
            'district' => $district,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/remove/{id}", defaults={"id" = "\d+"}, name="admin_content_district_remove")
     * 
     * @param Request $request
     * @param int $id
     */
    public function removeAction(Request $request, $id)
    {
        
        $notice = NoticeQuery::create()->findOneById($id);
        if (null !== $notice) {
            $notice->delete();
        }
        
        $flash = $request->getSession()->getFlashBag()->get('returnUrl');
        if (is_array($flash) && !empty($flash)) {
            $returnUrl = $flash[0];

            return $this->redirect($returnUrl);
        } else {        
            return $this->redirect($this->generateUrl('admin_content_district'));
        }
    }
}