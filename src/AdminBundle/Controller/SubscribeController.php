<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\RegistrationForm;
use AdminBundle\Form\SubscribePaymentForm;
use AppBundle\Controller\BaseController;
use AppBundle\Model\CountryQuery;
use AppBundle\Model\DistrictQuery;
use AppBundle\Model\SubscribePayment;
use AppBundle\Model\SubscribePaymentQuery;
use AppBundle\Model\SubscribeTypePriceQuery;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Util\ConfigurationUtil;
use AppBundle\Util\NumberUtil;
use AppBundle\Util\StringUtil;
use Propel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @Route("/admin/inscritos")
 */
class SubscribeController extends BaseController
{
    
    /**
     * @Route("/", name="admin_subscribe_subscribe")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        
        $problems = SubscribeQuery::create('s')
                ->listToAdmin(array())
                ->useSubscribePaymentQuery('spproblem', \Criteria::LEFT_JOIN)->endUse()
                ->leftJoinSubscribePayment('spproblem')
                ->addJoinCondition('spproblem', 'spproblem.Paid = 1 AND spproblem.Cancel = 1')
                ->useSubscribePaymentQuery('spcancel', \Criteria::LEFT_JOIN)->endUse()
                ->leftJoinSubscribePayment('spcancel')
                ->addJoinCondition('spcancel', 'spcancel.Cancel = 1')
                ->useSubscribePaymentQuery('splast', \Criteria::LEFT_JOIN)->endUse()
                ->leftJoinSubscribePayment('splast')
                ->addJoinCondition('splast', 'splast.Id = (SELECT MAX(inscricao_pagamento_id) FROM inscricao_pagamento WHERE inscricao_id = s.Id)')
                ->withColumn('CAST(((s.Price - IF(sp.Id IS NOT NULL, sp.Price, 0.0)) / splast.Price) AS SIGNED) + COUNT(DISTINCT sp.Id)', 'RealQuota')
                ->withColumn('s.Quota', 'Quota')
                ->withColumn('spproblem.Id', 'ProblemId')
                ->withColumn('IF(spcancel.Id IS NOT NULL, 1, 0)', 'HasCancel')
                ->groupById()
                ->having('(RealQuota <> Quota AND HasCancel) OR ProblemId IS NOT NULL')
                ->find();

        $requestQuery = $request->query->all();
        $returnUrl = $this->generateUrl($request->get('_route'), $requestQuery);
        $request->getSession()->getFlashBag()->set('returnUrl', $returnUrl);
        $where = $request->query->all();

        $page = $request->query->get('pagina', 1);
        $limit = 10;

        $subscribers = SubscribeQuery::create('s')
                ->listToAdmin($where)
                ->paginate($page, $limit);
        
        return array(
            'subscribers' => $subscribers,
            'where' => $where,
            'problems' => $problems,
        );
    }
    
    /**
     * @Route("/visualizar/{id}", defaults={"id" = "\d+"}, name="admin_subscribe_subscribe_view")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function viewAction(Request $request, $id = null)
    {

        $subscriber = SubscribeQuery::create()
                ->findOneById($id);
        
        $bind = SubscribeQuery::create()
                ->select(array(
                    'SubscribeTypeId',
                    'Quota',
                    'Email',
                    'Name',
                    'Birthday',
                    'Genre',
                    'Register',
                    'Document',
                    'Phone',
                    'CountryId',
                    'StateId',
                    'City',
                    'Region',
                    'Address',
                    'Number',
                    'Complement',
                    'ZipCode',
                    'Member',
                    'Confirmed',
                    'Minister',
                    'UnityName',
                    'DistrictId',
                    'DisplacementId',
                    'SpecialNeed',
                    'Comment',
                    'Free',
                    'Organization',
                ))
                ->filterById($id)
                ->findOne();
        
        $bind['Birthday'] = new \DateTime($bind['Birthday']);
        $bind['Country'] = CountryQuery::create()
                ->findOneById($bind['CountryId']);
        $bind['State'] = $bind['StateId'];
        $bind['Member'] = (boolean) $bind['Member'];
        $bind['Free'] = (boolean) $bind['Free'];
        $bind['Organization'] = (boolean) $bind['Organization'];
        $bind['Confirmed'] = (boolean) $bind['Confirmed'];
        $bind['District'] = DistrictQuery::create()
                ->findOneById($bind['DistrictId']);
        
        $paid = (boolean) SubscribePaymentQuery::create()
                ->filterBySubscribeId($subscriber->getId())
                ->filterByPaid(1)
                ->count();
        
        if ($paid) {
            unset($bind['SubscribeTypeId'], $bind['Quota']);
        }

        $form = $this->createForm(new RegistrationForm($this->container, $paid), $bind, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $configuration = ConfigurationUtil::all();
                $data = $form->getData();

                $conn = Propel::getConnection();

                try {
                    $conn->beginTransaction();
                    
                    $isNewPassword = false;
                    if ($data['Email'] !== $subscriber->getEmail()) {
                        $isNewPassword = true;
                    }

                    $subscriber->setEmail($data['Email'])
                            ->setCountry($data['Country'])
                            ->setDistrict($data['District'])
                            ->setDisplacementId($data['DisplacementId'])
                            ->setEmail($data['Email'])
                            ->setName($data['Name'])
                            ->setBirthday($data['Birthday'])
                            ->setStateId($data['State'])
                            ->setRegister($data['Register'])
                            ->setDocument($data['Document'])
                            ->setGenre($data['Genre'])
                            ->setPhone($data['Phone'])
                            ->setAddress($data['Address'])
                            ->setNumber($data['Number'])
                            ->setComplement($data['Complement'])
                            ->setCity($data['City'])
                            ->setRegion($data['Region'])
                            ->setZipCode($data['ZipCode'])
                            ->setMember($data['Member'])
                            ->setConfirmed($data['Confirmed'])
                            ->setMinister($data['Minister'])
                            ->setUnityName($data['UnityName'])
                            ->setSpecialNeed($data['SpecialNeed'])
                            ->setComment($data['Comment'])
                            ->setFree($data['Free'])
                            ->setOrganization($data['Organization'])
                            ->save();

                    $user = $subscriber->getUser();
                    $user->setEmail($data['Email'])
                            ->save();
                    
                    if ($isNewPassword) {
                        $newPassword = StringUtil::generatePassword();

                        $factory = $this->get('security.encoder_factory');
                        $encoder = $factory->getEncoder($user);
                        $password = $encoder->encodePassword($newPassword, $user->getSalt());
                        $user->setPassword($password)
                                ->save();

                        $configuration = ConfigurationUtil::all();

                        $emailBody = $this->renderView("AppBundle:Includes:remember-password.html.twig", array(
                            'newPassword' => $newPassword,
                            'locale' => 'pt_BR',
                        ));

                        $subject = "Nova senha para acesso à sua Área do Inscrito!";
                        $message = \Swift_Message::newInstance($subject)
                                ->setFrom($configuration['mail.sender'], 'Latino Americano 2017')
                                ->setTo($user->getEmail(), $user->getName())
                                ->setBody($emailBody, 'text/html', 'utf-8');
                        $this->getMailer()->send($message);
                    }

                    if (isset($data['SubscribeTypeId']) && $data['SubscribeTypeId'] != $subscriber->getSubscribeTypeId()) {
                        $now = new \DateTime();
                        $age = $now->diff($data['Birthday'])->y;
                        
                        $price = 0;
                        if ($age > (int) $configuration['subscribe.min.payment.age']) {
                            $subscribeTypePrice = SubscribeTypePriceQuery::create()
                                    ->filterBySubscribeTypeId($data['SubscribeTypeId'])
                                    ->filterByStart(time(), \Criteria::LESS_EQUAL)
                                    ->filterByEnd(time(), \Criteria::GREATER_EQUAL)
                                    ->findOne();
                            $price = $subscribeTypePrice->getPrice();
                        }
                        
                        SubscribePaymentQuery::create()
                                ->filterByPaid(false)
                                ->_or()
                                ->filterByPaid(null)
                                ->filterByCancel(false)
                                ->_or()
                                ->filterByCancel(null)
                                ->filterBySubscribe($subscriber)
                                ->update(array(
                                    'Cancel' => true,
                                ));

                        $subscriber->setSubscribeTypeId($data['SubscribeTypeId'])
                                ->setQuota(1)
                                ->setPrice($price);
                        $totalPaid = $subscriber->getPaidPrice();
                        $nextQuotaPrice = $price - $totalPaid;

                        $dueDate = date("Y-m-10");
                        if ((int) date("d") >= 10) {
                            $dueDate = strtotime("Y-m-10", strtotime("+1 month"));
                        }
                        $quota = new SubscribePayment();
                        $quota->setCreateDate(time())
                                ->setPrice($nextQuotaPrice)
                                ->setFirst(false)
                                ->setDueDate($dueDate);
                        if ($price == 0) {
                            $quota->setPaid(0)
                                    ->setPaymentDate(time());
                        }
                        $subscriber->addSubscribePayment($quota);
                        $subscriber->save();
                    }
                    
                    if ((int) $data['Free'] == 1) {
                        SubscribePaymentQuery::create()
                                ->filterBySubscribe($subscriber)
                                ->filterByCancel(0)
                                ->_or()
                                ->filterByCancel(null, \Criteria::ISNULL)
                                ->update(array(
                                    'Paid' => 1,
                                    'PaymentDate' => time(),
                                ));
                    }
                    
                    $conn->commit();
                    
                    $request->getSession()->getFlashBag()->add('success', 'Inscrito salvo com sucesso!');
                
                    return $this->redirect($this->generateUrl('admin_subscribe_subscribe_view', array(
                        'id' => $subscriber->getId(),
                    )));

                } catch (Exception $ex) {
                    throw $ex;
                    $conn->rollBack();
                }
            }
        }
        
        return array(
            'id' => $id,
            'subscriber' => $subscriber,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/remove/{id}", defaults={"id" = "\d+"}, name="admin_subscribe_subscribe_remove")
     * 
     * @param Request $request
     * @param int $id
     */
    public function removeAction(Request $request, $id)
    {
        
        $subscriber = SubscribeQuery::create()
                ->findOneById($id);
        
        if (null !== $subscriber) {
            $subscriber->setCanceled(true)
                    ->save();
            
            SubscribePaymentQuery::create()
                    ->filterBySubscribe($subscriber)
                    ->update(array(
                        'Cancel' => true,
                    ));
        }

        $flash = $request->getSession()->getFlashBag()->get('returnUrl');
        if (is_array($flash) && !empty($flash)) {
            $returnUrl = $flash[0];

            return $this->redirect($returnUrl);
        } else {        
            return $this->redirect($this->generateUrl('admin_configuration_user'));
        }
    }
    
    /**
     * @Route("/visualizar/{id}/pagamento", defaults={"id" = "\d+"}, name="admin_subscribe_subscribe_view_payment")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function paymentAction(Request $request, $id)
    {

        $subscriber = SubscribeQuery::create()
                ->findOneById($id);
        
        $payment = new SubscribePayment();
        $payment->setSubscribeId($id);

        $form = $this->createForm(new SubscribePaymentForm(), $payment, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $payment = $form->getData();
                $payment->setCreateDate(time())
                        ->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Parcela gerada com sucesso!');
                
                return $this->redirectToRoute('admin_subscribe_subscribe_view_payment', array(
                    'id' => $id,
                ));
            }
        }

        return array(
            'subscriber' => $subscriber,
            'id' => $id,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/visualizar/{id}/pagamento/{paymentId}/marcar-pago", defaults={"id" = "\d+", "paymentId" = "\d+"}, name="admin_subscribe_subscribe_view_payment_paid")
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function paidAction(Request $request, $id, $paymentId)
    {
        
        SubscribePaymentQuery::create()
                ->filterBySubscribeId($id)
                ->filterById($paymentId)
                ->update(array(
                    'Paid' => 1,
                    'PaymentDate' => time(),
                    'Cancel' => false,
                ));
        
        if ($request->query->has('return')) {
            return $this->redirectToRoute($request->query->get('return'), array(
                'id' => $id,
            ));
        } else {
            return $this->redirectToRoute('admin_subscribe_subscribe_view_payment', array(
                'id' => $id,
            ));
        }
    }
    
    /**
     * @Route("/visualizar/{id}/pagamento/{paymentId}/alterar-vencimento", defaults={"id" = "\d+", "paymentId" = "\d+"}, name="admin_subscribe_subscribe_view_payment_duedate")
     */
    public function changeDueDateAction(Request $request, $id, $paymentId)
    {
        
        $dueDate = implode("-", array_reverse(explode("/", $request->query->get('dueDate'))));
        
        SubscribePaymentQuery::create()
                ->filterBySubscribeId($id)
                ->filterById($paymentId)
                ->update(array(
                    'DueDate' => $dueDate,
                    'Cancel' => false,
                ));
        
        return new JsonResponse(array(
            "Result" => "OK",
        ));
    }
    
    /**
     * @Route("/visualizar/{id}/pagamento/{paymentId}/alterar-valor", defaults={"id" = "\d+", "paymentId" = "\d+"}, name="admin_subscribe_subscribe_view_payment_price")
     */
    public function changePriceAction(Request $request, $id, $paymentId)
    {
        
        $price = NumberUtil::strToFloat($request->query->get('price'));
        
        SubscribePaymentQuery::create()
                ->filterBySubscribeId($id)
                ->filterById($paymentId)
                ->update(array(
                    'Price' => $price,
                    'Cancel' => false,
                ));
        
        return new JsonResponse(array(
            "Result" => "OK",
        ));
    }
    
    /**
     * @Route("/problemas-pagamento", name="admin_subscribe_problem")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function paymentProblemAction(Request $request)
    {
        
        $where = $request->query->all();
        $page = $request->query->get('pagina', 1);
        $limit = 20;

        $subscribers = SubscribeQuery::create('s')
                ->listToAdmin($where)
                ->useSubscribePaymentQuery('spproblem', \Criteria::LEFT_JOIN)->endUse()
                ->leftJoinSubscribePayment('spproblem')
                ->addJoinCondition('spproblem', 'spproblem.Paid = 1 AND spproblem.Cancel = 1')
                ->useSubscribePaymentQuery('spcancel', \Criteria::LEFT_JOIN)->endUse()
                ->leftJoinSubscribePayment('spcancel')
                ->addJoinCondition('spcancel', 'spcancel.Cancel = 1')
                ->useSubscribePaymentQuery('splast', \Criteria::LEFT_JOIN)->endUse()
                ->leftJoinSubscribePayment('splast')
                ->addJoinCondition('splast', 'splast.Id = (SELECT MAX(inscricao_pagamento_id) FROM inscricao_pagamento WHERE inscricao_id = s.Id)')
                ->withColumn('CAST(((s.Price - IF(sp.Id IS NOT NULL, sp.Price, 0.0)) / splast.Price) AS SIGNED) + COUNT(DISTINCT sp.Id)', 'RealQuota')
                ->withColumn('IF(ROUND((((s.Quota - COUNT(DISTINCT sp.Id)) * splast.Price) + SUM(sp.Price)), 1) <> s.Price, 1, 0)', 'HasPriceError')
                ->withColumn('s.Quota', 'Quota')
                ->withColumn('spproblem.Id', 'ProblemId')
                ->withColumn('IF(spcancel.Id IS NOT NULL, 1, 0)', 'HasCancel')
                ->groupById()
                ->having('(HasPriceError AND HasCancel) OR ProblemId IS NOT NULL')
                ->paginate($page, $limit);

        return array(
            'subscribers' => $subscribers,
            'where' => $where,
        );
    }
    
    /**
     * @Route("/problemas-pagamento/{id}", name="admin_subscribe_problem_item")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function paymentProblemItemAction(Request $request, $id)
    {

        $problemItem = SubscribeQuery::create('s')
                ->filterById($id)
                ->listToAdmin(array())
                ->useSubscribePaymentQuery('spcancel', \Criteria::LEFT_JOIN)->endUse()
                ->leftJoinSubscribePayment('spcancel')
                ->addJoinCondition('spcancel', 'spcancel.Cancel = 1')
                ->useSubscribePaymentQuery('splast', \Criteria::LEFT_JOIN)->endUse()
                ->leftJoinSubscribePayment('splast')
                ->addJoinCondition('splast', 'splast.Id = (SELECT MAX(inscricao_pagamento_id) FROM inscricao_pagamento WHERE inscricao_id = s.Id)')
                ->withColumn('CAST(((s.Price - IF(sp.Id IS NOT NULL, sp.Price, 0.0)) / splast.Price) AS SIGNED) + COUNT(DISTINCT sp.Id)', 'RealQuota')
                ->withColumn('IF(ROUND((((s.Quota - COUNT(DISTINCT sp.Id)) * splast.Price) + SUM(sp.Price)), 1) <> s.Price, 1, 0)', 'HasPriceError')
                ->withColumn('s.Quota', 'Quota')
                ->withColumn('IF(spcancel.Id IS NOT NULL, 1, 0)', 'HasCancel')
                ->groupById()
                ->having('HasPriceError AND HasCancel')
                ->findOne();

        $subscriber = SubscribeQuery::create('s')
                ->findOneById($id);

        return array(
            'id' => $id,
            'problemItem' => $problemItem,
            'subscriber' => $subscriber,
        );
    }
    
    /**
     * @Route("/problemas-pagamento/{id}/corrigir-parcela/{paymentId}", name="admin_subscribe_problem_item_correct")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function paymentProblemItemCorrectAction(Request $request, $id, $paymentId)
    {

        $payment = SubscribePaymentQuery::create('sp')
                ->useSubscribeQuery('s')
                    ->filterById($id)
                ->endUse()
                ->findOneById($paymentId);
        $payment->setCancel(false)
                ->save();
        
        $request->getSession()->getFlashBag()->add('success', 'Parcela corrigida com sucesso!');

        return $this->redirectToRoute('admin_subscribe_problem_item', array(
            'id' => $id,
        ));
    }
    
    /**
     * @Route("/problemas-pagamento/{id}/corrigir-inscrito/parcelas-{quotas}", name="admin_subscribe_problem_quotas_correct")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function paymentProblemQuotasCorrectAction(Request $request, $id, $quotas)
    {

        $subscriber = SubscribeQuery::create('s')
                ->findOneById($id);
        
        $subscriber->setQuota($quotas)
                ->save();
        
        $payment = SubscribePaymentQuery::create('sp')
                ->filterBySubscribe($subscriber)
                ->filterByPaid(false)
                ->_or()
                ->filterByPaid(null)
                ->orderById(\Criteria::DESC)
                ->findOne();

        if (null !== $payment) {
            $payment->setPrice(($subscriber->getPrice() / $subscriber->getQuota()))
                    ->setCancel(false)
                    ->save();
        }

        $request->getSession()->getFlashBag()->add('success', 'O número de parcelas do inscrito foram corrigidas com sucesso!');

        return $this->redirectToRoute('admin_subscribe_problem_item', array(
            'id' => $id,
        ));
    }
    
    /**
     * @Route("/visualizar/{id}/pagamento/{paymentId}/excluir", defaults={"id" = "\d+"}, name="admin_subscribe_subscribe_view_payment_delete")
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function paymentDeleteAction(Request $request, $id, $paymentId)
    {
        
        SubscribePaymentQuery::create()
                ->filterBySubscribeId($id)
                ->filterById($paymentId)
                ->delete();
        
        if ($request->query->has('return')) {
            return $this->redirectToRoute($request->query->get('return'), array(
                'id' => $id,
            ));
        } else {
            return $this->redirectToRoute('admin_subscribe_subscribe_view_payment', array(
                'id' => $id,
            ));
        }
    }
    
    /**
     * @Route("/problemas-pagamento/{id}/alterar-inscrito/parcelas", name="admin_subscribe_problem_quotas_change")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function paymentProblemQuotasChangeAction(Request $request, $id)
    {

        $quotas = (int) $request->query->get('quotas');
        $subscriber = SubscribeQuery::create('s')
                ->findOneById($id);
        
        $subscriber->setQuota($quotas)
                ->save();
        
        $request->getSession()->getFlashBag()->add('success', 'O número de parcelas do inscrito foram corrigidas com sucesso!');

        return new JsonResponse(array(
            "Result" => "OK",
        ));
    }
    
    /**
     * @Route("/area-inscrito/{id}/acessar", name="admin_subscribe_subscribe_areaaccess")
     * @Template()
     */
    public function subscribeAreaAccessAction(Request $request, $id)
    {

        $subscribe = SubscribeQuery::create('s')
                ->filterById($id)
                ->findOne();
        $user = $subscribe->getUser();

        $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
        $security = $this->container->get('security.context');
        $security->setToken($token);
        
        return $this->redirectToRoute('subscribe_area');
    }
    
    /**
     * @Route("/exportar", name="admin_subscribe_subscribe_export")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function exportSubscribeAction(Request $request)
    {

        $where = $request->query->all();
        $subscribers = SubscribeQuery::create('s')
                ->listToAdmin($where)
                ->find();

        $phpExcel = new \PHPExcel();
        $phpExcel->getProperties()->setCreator("JELB");
        $phpExcel->getProperties()->setLastModifiedBy("JELB");
        $phpExcel->getProperties()->setTitle("Relatório de inscritos em ".date("d/m/Y"));
        $phpExcel->getProperties()->setSubject("Relatório de inscritos em ".date("d/m/Y"));

        $phpExcel->setActiveSheetIndex(0);

        $titleStyle = array(
            'font'  => array(
            'bold'  => true,
            'color' => array('rgb' => '000000'),
            'size'  => 11,
            'name'  => 'Verdana',
        ));

        $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(false);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(4);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('F')->setAutoSize(false);
        $phpExcel->getActiveSheet()->getColumnDimension('H')->setWidth(4);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('H')->setAutoSize(false);
        $phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(4);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('J')->setAutoSize(false);
        
        $phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(false);
        
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(70);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(false);
        
        $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('G')->setAutoSize(false);
        $phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(16);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('I')->setAutoSize(false);
        $phpExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('K')->setAutoSize(false);
        
        $phpExcel->getActiveSheet()->setCellValue('A1', 'RELATÓRIO DE INSCRIÇÕES - '.date('d/m/Y'));
        $phpExcel->getActiveSheet()->setCellValue('A2', 'CONGRESSO JELB 2017');
        $phpExcel->getActiveSheet()->getStyle("A1")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("A1")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->getStyle("A2")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("A2")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->mergeCells('A1:K1');
        $phpExcel->getActiveSheet()->mergeCells('A2:K2');
        
        $phpExcel->getActiveSheet()->setCellValue('K4', date("d/m/Y"));
        $phpExcel->getActiveSheet()->getStyle("K4")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("K4")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $phpExcel->getActiveSheet()->setCellValue('A6', "NOME");
        $phpExcel->getActiveSheet()->getStyle("A6")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("A6")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->mergeCells('A6:B6');
        
        $phpExcel->getActiveSheet()->setCellValue('D6', "ENDEREÇO");
        $phpExcel->getActiveSheet()->getStyle("D6")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("D6")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->mergeCells('D6:E6');
        
        $phpExcel->getActiveSheet()->setCellValue('G6', "GÊNERO");
        $phpExcel->getActiveSheet()->getStyle("G6")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("G6")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $phpExcel->getActiveSheet()->setCellValue('I6', "IDADE");
        $phpExcel->getActiveSheet()->getStyle("I6")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("I6")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $phpExcel->getActiveSheet()->setCellValue('K6', "MODALIDADE");
        $phpExcel->getActiveSheet()->getStyle("K6")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("K6")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $i = 7;
        foreach ($subscribers AS $subscriber) {
            $phpExcel->getActiveSheet()->setCellValue('A'.$i, $subscriber->getName());
            $phpExcel->getActiveSheet()->mergeCells('A'.$i.':B'.$i);

            
            $address = sprintf(
                    "%s, %s - %s, CEP: %s - %s - %s",
                    $subscriber->getAddress(),
                    $subscriber->getNumber(),
                    $subscriber->getRegion(),
                    $subscriber->getZipCode(),
                    $subscriber->getCity(),
                    $subscriber->getCountry()->getName()
            );
            $phpExcel->getActiveSheet()->setCellValue('D'.$i, $address);
            $phpExcel->getActiveSheet()->mergeCells('D'.$i.':E'.$i);
            
            $genre = $subscriber->getGenre();
            switch ($genre) {
                case 'M':
                    $genre = 'Masculino';
                    break;
                case 'F':
                    $genre = 'Feminino';
                    break;
            }
            $phpExcel->getActiveSheet()->setCellValue('G'.$i, $subscriber->getGenre());
            $phpExcel->getActiveSheet()->getStyle("G".$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $age = $subscriber->getBirthday()
                    ->diff(new \DateTime(date("Y-m-d H:i:s")))
                    ->y;
            $phpExcel->getActiveSheet()->setCellValue('I'.$i, $age);
            $phpExcel->getActiveSheet()->getStyle("I".$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $criteria = new \Criteria();
            $subscribeTypeName = $subscriber->getSubscribeType()
                    ->getSubscribeTypeNames($criteria->add('lingua_id', 1))
                    ->getFirst()
                    ->getName();
            $phpExcel->getActiveSheet()->setCellValue('K'.$i, $subscribeTypeName);
            $phpExcel->getActiveSheet()->getStyle("K".$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $i++;
        }
        
        $objWriter = new \PHPExcel_Writer_Excel2007($phpExcel);
        ob_start();
        $objWriter->save("php://output");
        $excelContent = ob_get_clean();

        $response = new Response($excelContent);
        $response->headers->add(array(
            "Content-Type" => "application/vnd.ms-excel; charset=utf-8",
            "Content-Disposition" => "attachment; filename=\"relatorio-inscricoes-".date("Y-m-d").".xls\"",
            "Expires" => "0",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Cache-Control" => "private",
        ));

        return $response;
    }
}
