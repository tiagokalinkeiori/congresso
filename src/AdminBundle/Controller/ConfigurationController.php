<?php

namespace AdminBundle\Controller;

use AppBundle\Model\ConfigurationQuery;
use AppBundle\Util\ConfigurationUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/configuracoes")
 */
class ConfigurationController extends Controller
{
    
    /**
     * @Route("/", name="admin_configuration_configuration")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $configuration = ConfigurationUtil::all();
        $defaults = array(
            "subscribe.min.payment.age" => array(
                "type" => "integer",
                "label" => "Idade mínima para pagamento",
                "class" => "form-control",
                "format" => "",
            ),
            "contact.mail.to" => array(
                "type" => "email",
                "label" => "E-mail de destino dos contatos",
                "class" => "form-control",
                "format" => "",
            ),
            "mail.sender" => array(
                "type" => "email",
                "label" => "E-mail de saída",
                "class" => "form-control",
                "format" => "",
            ),
            "mail.usesmtp" => array(
                "type" => "checkbox",
                "label" => "Usar SMTP?",
                "class" => "",
                "format" => "",
            ),
            "mail.smtp.server" => array(
                "type" => "text",
                "label" => "Servidor SMTP",
                "class" => "form-control",
                "format" => "",
            ),
            "mail.smtp.user" => array(
                "type" => "text",
                "label" => "Usuário SMTP",
                "class" => "form-control",
                "format" => "",
            ),
            "mail.smtp.password" => array(
                "type" => "password",
                "label" => "Senha SMTP",
                "class" => "form-control",
                "format" => "",
            ),
            "mail.smtp.port" => array(
                "type" => "text",
                "label" => "Porta SMTP",
                "class" => "form-control",
                "format" => "",
            ),
            "mail.smtp.secure" => array(
                "type" => "select",
                "label" => "Segurança SMTP",
                "class" => "form-control",
                "format" => "",
                "choices" => array(
                    "" => "Nenhuma",
                    "ssl" => "SSL",
                    "tls" => "TLS",
                    "starttls" => "STARTTLS",
                ),
            ),
            "registration.date.start" => array(
                "type" => "text",
                "label" => "Data de inicio das inscrições",
                "class" => "form-control trigger-mask-date",
                "format" => "date",
            ),
            "registration.date.end" => array(
                "type" => "text",
                "label" => "Data final das inscrições",
                "class" => "form-control trigger-mask-date",
                "format" => "date",
            ),
        );
        
        foreach ($configuration as $key => $item) {
            if ($item != "") {
                if ($defaults[$key]['format'] == 'date') {
                    $item = date("d/m/Y", strtotime($item));
                }
            }
            $configuration[$key] = array_merge(array(
                'value' => $item,
            ), $defaults[$key]);
        }
        
        if ($request->isMethod('post')) {
            $data = $request->request->all();
            foreach ($data as $key => $value) {
                $key = str_replace("_", ".", $key);
                
                if ($value == "__/__/____") {
                    $value = "";
                }

                if ($value != "") {
                    if ($configuration[$key]["format"] == "date") {
                        $value = implode("-", array_reverse(explode("/", $value)));
                    }
                }
                
                ConfigurationQuery::create()
                        ->filterByName($key)
                        ->update(array(
                            'Value' => $value,
                        ));
            }
            
            $request->getSession()->getFlashBag()->add('success', 'Configurações salvas com sucesso!');
                
            return $this->redirect($this->generateUrl('admin_configuration_configuration'));
        }
        
        return array(
            'configuration' => $configuration,
        );
    }
}