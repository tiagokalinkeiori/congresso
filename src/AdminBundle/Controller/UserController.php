<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\UserForm;
use AppBundle\Model\User;
use AppBundle\Model\UserQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/usuarios")
 */
class UserController extends Controller
{
    
    /**
     * @Route("/", name="admin_configuration_user")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $requestQuery = $request->query->all();
        $returnUrl = $this->generateUrl($request->get('_route'), $requestQuery);
        $request->getSession()->getFlashBag()->set('returnUrl', $returnUrl);

        $page = $request->query->get('pagina', 1);
        $limit = 10;
        
        $users = UserQuery::create()
                ->filterByGroupId(1)
                ->paginate($page, $limit);
        
        return array(
            'users' => $users,
        );
    }
    
    /**
     * @Route("/criar", defaults={"id" = null}, name="admin_configuration_user_create")
     * @Route("/editar/{id}", defaults={"id" = "\d+"}, name="admin_configuration_user_edit")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function editAction(Request $request, $id = null)
    {
        
        $user = null;
        if (null !== $id) {
            $user = UserQuery::create()
                    ->findOneById($id);
        }
        
        $form = $this->createForm(new UserForm($user), ( null !== $user ? $user->toArray() : null ), array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $formData = $form->getData();
                
                if (null === $user) {
                    $user = new User();
                    $user->setGroupId(1)
                            ->setSalt(md5(uniqid(null, true)));
                }
                
                $user->setName($formData['Name'])
                        ->setEmail($formData['Email']);
                if (null !== $user && !empty($formData['Password'])) {
                    $factory = $this->container->get('security.encoder_factory');
                    $encoder = $factory->getEncoder($user);
                    $password = $encoder->encodePassword($formData['Password'], $user->getSalt());

                    $user->setPassword($password);
                }
                $user->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Usuário salvo com sucesso!');
                
                return $this->redirect($this->generateUrl('admin_configuration_user_edit', array(
                    'id' => $user->getId(),
                )));
            }
        }
        
        return array(
            'id' => $id,
            'user' => $user,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/remove/{id}", defaults={"id" = "\d+"}, name="admin_configuration_user_remove")
     * 
     * @param Request $request
     * @param int $id
     */
    public function removeAction(Request $request, $id)
    {
     
        if ($id != $this->getUser()->getId()) {
            $notice = NoticeQuery::create()->findOneById($id);
            if (null !== $notice) {
                $notice->delete();
            }
        }
        
        $flash = $request->getSession()->getFlashBag()->get('returnUrl');
        if (is_array($flash) && !empty($flash)) {
            $returnUrl = $flash[0];

            return $this->redirect($returnUrl);
        } else {        
            return $this->redirect($this->generateUrl('admin_configuration_user'));
        }
    }
}