<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\PaymentTextForm;
use AppBundle\Model\PaymentTextQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/textos-pagamento")
 */
class PaymentTextController extends Controller
{
    
    /**
     * @Route("/", name="admin_content_paymenttext")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $requestQuery = $request->query->all();
        $returnUrl = $this->generateUrl($request->get('_route'), $requestQuery);
        $request->getSession()->getFlashBag()->set('returnUrl', $returnUrl);

        $page = $request->query->get('pagina', 1);
        $limit = 10;
        
        $texts = PaymentTextQuery::create()
                ->paginate($page, $limit);
        
        return array(
            'texts' => $texts,
        );
    }
    
    /**
     * @Route("/criar", defaults={"id" = null}, name="admin_content_paymenttext_create")
     * @Route("/editar/{id}", defaults={"id" = "\d+"}, name="admin_content_paymenttext_edit")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function editAction(Request $request, $id = null)
    {
        
        $text = null;
        if (null !== $id) {
            $text = PaymentTextQuery::create()->findOneById($id);
        }
        
        $form = $this->createForm(new PaymentTextForm(), $text, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $text = $form->getData();
                $text->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Texto salvo com sucesso!');
                
                return $this->redirect($this->generateUrl('admin_content_paymenttext_edit', array(
                    'id' => $text->getId(),
                )));
            }
        }
        
        return array(
            'id' => $id,
            'text' => $text,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/remove/{id}", defaults={"id" = "\d+"}, name="admin_content_paymenttext_remove")
     * 
     * @param Request $request
     * @param int $id
     */
    public function removeAction(Request $request, $id)
    {
        
        $text = PaymentTextQuery::create()->findOneById($id);
        if (null !== $text) {
            $text->delete();
        }
        
        $flash = $request->getSession()->getFlashBag()->get('returnUrl');
        if (is_array($flash) && !empty($flash)) {
            $returnUrl = $flash[0];

            return $this->redirect($returnUrl);
        } else {        
            return $this->redirect($this->generateUrl('admin_content_paymenttext'));
        }
    }
}