<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\WorkshopForm;
use AppBundle\Model\WorkshopQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/oficinas")
 */
class WorkshopController extends Controller
{
    
    /**
     * @Route("/", name="admin_configuration_workshop")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $requestQuery = $request->query->all();
        $returnUrl = $this->generateUrl($request->get('_route'), $requestQuery);
        $request->getSession()->getFlashBag()->set('returnUrl', $returnUrl);

        $page = $request->query->get('pagina', 1);
        $limit = 10;
        
        $workshops = WorkshopQuery::create()
                ->orderByStart()
                ->orderByEnd()
                ->paginate($page, $limit);
        
        return array(
            'workshops' => $workshops,
        );
    }
    
    /**
     * @Route("/criar", defaults={"id" = null}, name="admin_configuration_workshop_create")
     * @Route("/editar/{id}", defaults={"id" = "\d+"}, name="admin_configuration_workshop_edit")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function editAction(Request $request, $id = null)
    {
        
        $workshop = null;
        if (null !== $id) {
            $workshop = WorkshopQuery::create()->findOneById($id);
        }
        
        $form = $this->createForm(new WorkshopForm(), $workshop, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $workshop = $form->getData();
                $workshop->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Oficina salva com sucesso!');
                
                return $this->redirect($this->generateUrl('admin_configuration_workshop_edit', array(
                    'id' => $workshop->getId(),
                )));
            }
        }
        
        return array(
            'id' => $id,
            'workshop' => $workshop,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/remove/{id}", defaults={"id" = "\d+"}, name="admin_configuration_workshop_remove")
     * 
     * @param Request $request
     * @param int $id
     */
    public function removeAction(Request $request, $id)
    {
        
        $workshop = WorkshopQuery::create()->findOneById($id);
        if (null !== $workshop) {
            $workshop->delete();
        }
        
        $flash = $request->getSession()->getFlashBag()->get('returnUrl');
        if (is_array($flash) && !empty($flash)) {
            $returnUrl = $flash[0];

            return $this->redirect($returnUrl);
        } else {        
            return $this->redirect($this->generateUrl('admin_configuration_workshop'));
        }
    }
}