<?php

namespace AdminBundle\Controller;

use AppBundle\Model\CountryQuery;
use AppBundle\Model\DistrictQuery;
use AppBundle\Model\DonationQuery;
use AppBundle\Model\StateQuery;
use AppBundle\Model\SubscribeQuery;
use AppBundle\Model\SubscribeTypeQuery;
use AppBundle\Model\SubscribePaymentQuery;
use AppBundle\Util\ConfigurationUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/relatorios")
 */
class ReportController extends Controller
{
    
    /**
     * @Route("/", name="admin_report_general")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {

        $countries = CountryQuery::create('c')
                ->leftJoinSubscribe('s')
                ->addJoinCondition('s', '((s.Canceled = 0 OR s.Canceled IS NULL) AND (s.Organization = 0 OR s.Organization IS NULL))')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->select(array(
                    'Id',
                    'Name',
                    'Total',
                ))
                ->groupById()
                ->having('Total > 0')
                ->orderBy('Total', \Criteria::DESC)
                ->find()
                ->toArray();
        
        $states = StateQuery::create('st')
                ->leftJoinSubscribe('s')
                ->addJoinCondition('s', '((s.Canceled = 0 OR s.Canceled IS NULL) AND (s.Organization = 0 OR s.Organization IS NULL))')
                ->useCountryQuery('c')->endUse()
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->withColumn('CONCAT(st.Name, " - ", c.Code)', 'Name')
                ->select(array(
                    'Id',
                    'Name',
                    'Total',
                ))
                ->groupById()
                ->having('Total > 0')
                ->orderBy('Total', \Criteria::DESC)
                ->find()
                ->toArray();
        
        $districts = DistrictQuery::create('d')
                ->leftJoinSubscribe('s')
                ->addJoinCondition('s', '((s.Canceled = 0 OR s.Canceled IS NULL) AND (s.Organization = 0 OR s.Organization IS NULL))')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->select(array(
                    'Id',
                    'Name',
                    'Total',
                ))
                ->groupById()
                ->having('Total > 0')
                ->orderBy('Total', \Criteria::DESC)
                ->find()
                ->toArray();
        
        $genre = SubscribeQuery::create('s')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->withColumn('IF(s.Genre = "F", "Feminino", "Masculino")', 'Genre')
                ->select(array(
                    'Genre',
                    'Total',
                ))
                ->groupByGenre()
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->_and()
                ->filterByOrganization(false)
                ->_or()
                ->filterByOrganization(null)
                ->having('Total > 0')
                ->orderBy('Total', \Criteria::DESC)
                ->find()
                ->toArray();
        
        $quotas = SubscribeQuery::create('s')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->select(array(
                    'Quota',
                    'Total',
                ))
                ->groupByQuota()
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->_and()
                ->filterByOrganization(false)
                ->_or()
                ->filterByOrganization(null)
                ->having('Total > 0')
                ->orderBy('Total', \Criteria::DESC)
                ->find()
                ->toArray();
        
        $types = SubscribeTypeQuery::create('st')
                ->useSubscribeTypeNameQuery('stn')->endUse()
                ->innerJoinSubscribeTypeName('stn')
                ->addJoinCondition('stn', 'stn.LanguageId = 1')
                ->leftJoinSubscribe('s')
                ->addJoinCondition('s', '((s.Canceled = 0 OR s.Canceled IS NULL) AND (s.Organization = 0 OR s.Organization IS NULL))')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->withColumn('stn.Name', 'Name')
                ->select(array(
                    'Id',
                    'Name',
                    'Total',
                ))
                ->groupById()
                ->having('Total > 0')
                ->orderBy('Total', \Criteria::DESC)
                ->find()
                ->toArray();
        
        $configuration = ConfigurationUtil::all();
        $diffDays = 'COUNT(DISTINCT DATE_FORMAT(s.Date, "%Y-%m-%d"))';
        if (!empty($configuration['registration.date.start'])) {
            $startDate = new \DateTime($configuration['registration.date.start']);
            $diffDays = $startDate->diff(new \DateTime())->d;
            if ($diffDays <= 0) {
                $diffDays = 1;
            }
        }
        $dayAvg['Avg'] = SubscribeQuery::create('s')
                ->withColumn('COUNT(DISTINCT s.Id) / '.$diffDays, 'Avg')
                ->select('Avg')
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->_and()
                ->filterByOrganization(false)
                ->_or()
                ->filterByOrganization(null)
                ->findOne();
        
        $diffMonths = 'COUNT(DISTINCT DATE_FORMAT(s.Date, "%Y-%m"))';
        if (!empty($configuration['registration.date.start'])) {
            $startDate = new \DateTime($configuration['registration.date.start']);
            $diffMonths = $startDate->diff(new \DateTime())->m;
            if ($diffMonths <= 0) {
                $diffMonths = 1;
            }
        }
        $monthAvg['Avg'] = SubscribeQuery::create('s')
                ->withColumn('COUNT(DISTINCT s.Id) / '.$diffMonths, 'Avg')
                ->select('Avg')
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->_and()
                ->filterByOrganization(false)
                ->_or()
                ->filterByOrganization(null)
                ->findOne();
        
        $mostUsedMonthDay = SubscribeQuery::create('s')
                ->withColumn('DATE_FORMAT(s.Date, "%d")', 'Day')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->select(array(
                    'Day',
                    'Total',
                ))
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->_and()
                ->filterByOrganization(false)
                ->_or()
                ->filterByOrganization(null)
                ->groupBy('Day')
                ->orderBy('Total', \Criteria::DESC)
                ->limit(7)
                ->find()
                ->toArray();
        
        $mostUsedWeekDay = SubscribeQuery::create('s')
                ->withColumn('DATE_FORMAT(s.Date, "%W")', 'Day')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->select(array(
                    'Day',
                    'Total',
                ))
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->_and()
                ->filterByOrganization(false)
                ->_or()
                ->filterByOrganization(null)
                ->groupBy('Day')
                ->orderBy('Total', \Criteria::DESC)
                ->find()
                ->toArray();
        
        $ages = SubscribeQuery::create('s')
                ->withColumn('FLOOR(DATEDIFF(NOW(), s.Birthday) / 365)', 'Age')
                ->withColumn('COUNT(DISTINCT s.Id)', 'Total')
                ->filterByCanceled(false)
                ->_or()
                ->filterByCanceled(null)
                ->_and()
                ->filterByOrganization(false)
                ->_or()
                ->filterByOrganization(null)
                ->groupBy('Age')
                ->orderBy('Total', \Criteria::DESC)
                ->find()
                ->toArray();

        return array(
            'countries' => $countries,
            'states' => $states,
            'districts' => $districts,
            'genre' => $genre,
            'quotas' => $quotas,
            'types' => $types,
            'dayAvg' => $dayAvg,
            'monthAvg' => $monthAvg,
            'mostUsedMonthDay' => $mostUsedMonthDay,
            'mostUsedWeekDay' => $mostUsedWeekDay,
            'ages' => $ages,
        );
    }
    
    /**
     * @Route("/financeiro", name="admin_report_financial")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function financialAction(Request $request)
    {
        
        $totalPaid = SubscribePaymentQuery::create('sp')
                ->useSubscribeQuery('s')
                    ->filterByFree(0)
                    ->_or()
                    ->filterByFree(null, \Criteria::ISNULL)
                ->endUse()
                ->withColumn('SUM(sp.Price)', 'TotalPaid')
                ->select('TotalPaid')
                ->filterByPaid(1)
                ->findOne();

        $totalPaidToday = SubscribePaymentQuery::create('sp')
                ->useSubscribeQuery('s')
                    ->filterByFree(0)
                    ->_or()
                    ->filterByFree(null, \Criteria::ISNULL)
                ->endUse()
                ->withColumn('SUM(sp.Price)', 'TotalPaid')
                ->select('TotalPaid')
                ->filterByPaymentDate(array(
                    'min' => date("Y-m-d 00:00:00"),
                    'max' => date("Y-m-d 23:59:59"),
                ))
                ->filterByPaid(1)
                ->findOne();
        
        $firstPendent = SubscribeQuery::create('s')
                ->innerJoinSubscribePayment('sp0')
                ->addJoinCondition('sp0', 'sp0.First = 1 AND sp0.Cancel = 1')
                ->leftJoinSubscribePayment('sp1')
                ->addJoinCondition('sp1', 'sp1.First = 1 AND (sp1.Cancel = 0 OR sp1.Cancel IS NULL)')
                ->where('sp1.Id IS NULL')
                ->groupById()
                ->count();
        
        $reactives = SubscribeQuery::create('s')
                ->innerJoinSubscribePayment('sp0')
                ->addJoinCondition('sp0', 'sp0.First = 1 AND sp0.Cancel = 1')
                ->innerJoinSubscribePayment('sp1')
                ->addJoinCondition('sp1', 'sp1.First = 1 AND (sp1.Cancel = 0 OR sp1.Cancel IS NULL)')
                ->groupById()
                ->count();
        
        $reactivesPaid = SubscribeQuery::create('s')
                ->innerJoinSubscribePayment('sp0')
                ->addJoinCondition('sp0', 'sp0.First = 1 AND sp0.Cancel = 1')
                ->innerJoinSubscribePayment('sp1')
                ->addJoinCondition('sp1', 'sp1.First = 1 AND (sp1.Cancel = 0 OR sp1.Cancel IS NULL) AND sp1.Paid = 1')
                ->groupById()
                ->count();
        
        $donations = DonationQuery::create()
                ->find();
        
        $totalDonation = DonationQuery::create('d')
                ->withColumn('SUM(d.Price)', 'Total')
                ->select('Total')
                ->filterByPaid(1)
                ->findOne();
        
        return array(
            'totalPaid' => $totalPaid,
            'totalPaidToday' => $totalPaidToday,
            'firstPendent' => $firstPendent,
            'reactives' => $reactives,
            'reactivesPaid' => $reactivesPaid,
            'donations' => $donations,
            'totalDonation' => $totalDonation,
        );
    }
    
    /**
     * @Route("/doacao/marcar-pago/{id}", name="admin_report_financial_donation_paid")
     * @param type $id
     */
    public function donationPaidAction($id)
    {
        
        DonationQuery::create()
                ->filterById($id)
                ->update(array(
                    'PaymentDate' => time(),
                    'Paid' => true,
                ));
     
        return $this->redirectToRoute('admin_report_financial');
    }
    
    /**
     * @Route("/financeiro/exportar", name="admin_report_financial_export")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function exportFinancialAction(Request $request)
    {
        
        $payments = SubscribePaymentQuery::create('sp')
                ->useSubscribeQuery('s')
                    ->filterByFree(false)
                    ->_or()
                    ->filterByFree(null)
                    ->_and()
                    ->filterByOrganization(false)
                    ->_or()
                    ->filterByOrganization(null)
                ->endUse()
                ->filterByPaid(true)
                ->filterByPaymentDate(array(
                    'min' => date("Y-m-d 00:00:00"),
                    'max' => date("Y-m-d 23:59:59"),
                ))
                ->groupById()
                ->find();
        
        $phpExcel = new \PHPExcel();
        $phpExcel->getProperties()->setCreator("JELB");
        $phpExcel->getProperties()->setLastModifiedBy("JELB");
        $phpExcel->getProperties()->setTitle("Movimentação financeira em ".date("d/m/Y"));
        $phpExcel->getProperties()->setSubject("Movimentação financeira em ".date("d/m/Y"));

        $phpExcel->setActiveSheetIndex(0);
        
        $titleStyle = array(
            'font'  => array(
            'bold'  => true,
            'color' => array('rgb' => '000000'),
            'size'  => 11,
            'name'  => 'Verdana',
        ));
        
        $phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(4);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('I')->setAutoSize(false);
        $phpExcel->getActiveSheet()->getColumnDimension('K')->setWidth(4);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('K')->setAutoSize(false);
        
        $phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('J')->setAutoSize(false);
        
        $phpExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $phpExcel->getActiveSheet()->getColumnDimensionByColumn('L')->setAutoSize(false);

        $phpExcel->getActiveSheet()->setCellValue('A1', 'RELATÓRIO DE MOVIMENTO FINANCEIRO DE RECEBIMENTO DE INSCRIÇÕES - '.date('d/m/Y'));
        $phpExcel->getActiveSheet()->setCellValue('A2', 'CONGRESSO JELB 2017');
        $phpExcel->getActiveSheet()->getStyle("A1")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("A1")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->getStyle("A2")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("A2")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->mergeCells('A1:L1');
        $phpExcel->getActiveSheet()->mergeCells('A2:L2');
        
        $phpExcel->getActiveSheet()->setCellValue('J4', date("d/m/Y"));
        $phpExcel->getActiveSheet()->getStyle("J4")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("J4")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->mergeCells('J4:L4');
        
        $phpExcel->getActiveSheet()->setCellValue('A6', "NOME");
        $phpExcel->getActiveSheet()->getStyle("A6")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("A6")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcel->getActiveSheet()->mergeCells('A6:H6');
        
        $phpExcel->getActiveSheet()->setCellValue('J6', "PARCELA");
        $phpExcel->getActiveSheet()->getStyle("J6")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("J6")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $phpExcel->getActiveSheet()->setCellValue('L6', "VALOR (R$)");
        $phpExcel->getActiveSheet()->getStyle("L6")->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle("L6")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $i = 7;
        $total = 0;
        foreach ($payments AS $payment) {
            $total += $payment->getPrice();

            $paids = SubscribePaymentQuery::create('sp')
                    ->filterBySubscribe($payment->getSubscribe())
                    ->filterByPaid(true)
                    ->count();
            
            $phpExcel->getActiveSheet()->setCellValue('A'.$i, $payment->getSubscribe()->getName());
            $phpExcel->getActiveSheet()->mergeCells('A'.$i.':H'.$i);
            
            $phpExcel->getActiveSheet()->setCellValue('J'.$i, $paids."/".$payment->getSubscribe()->getQuota());
            $phpExcel->getActiveSheet()->getStyle('J'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $phpExcel->getActiveSheet()->setCellValue('L'.$i, number_format($payment->getPrice(), 2, ",", "."));
            $phpExcel->getActiveSheet()->getStyle('L'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            
            $i++;
        }
        
        $i++;
        $phpExcel->getActiveSheet()->setCellValue('H'.$i, 'TOTAL RECEBIDO');
        $phpExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $phpExcel->getActiveSheet()->mergeCells('H'.$i.':J'.$i);

        $phpExcel->getActiveSheet()->setCellValue('L'.$i, number_format($total, 2, ",", "."));
        $phpExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($titleStyle);
        $phpExcel->getActiveSheet()->getStyle('L'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objWriter = new \PHPExcel_Writer_Excel2007($phpExcel);
        ob_start();
        $objWriter->save("php://output");
        $excelContent = ob_get_clean();

        $response = new Response($excelContent);
        $response->headers->add(array(
            "Content-Type" => "application/vnd.ms-excel; charset=utf-8",
            "Content-Disposition" => "attachment; filename=\"movimentacao-financeira-".date("Y-m-d").".xls\"",
            "Expires" => "0",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Cache-Control" => "private",
        ));

        return $response;
    }
}