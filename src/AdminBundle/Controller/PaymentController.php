<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\BankReturnForm;
use AppBundle\Model\SubscribePaymentQuery;
use AppBundle\Util\StringUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/retorno-bancario")
 */
class PaymentController extends Controller
{
    
    /**
     * @Route("/", name="admin_subscribe_payment")
     * @Template()
     * 
     * @param \AdminBundle\Controller\Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {

        $form = $this->createForm(new BankReturnForm(), null, array(
            'csrf_protection' => true,
        ));
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $dir = $this->get('kernel')->getRootDir() . "/logs/";

                try {
                    $file = $form->get('File')->getData();
                    $fileName = md5(time()) . "." . $file->getClientOriginalExtension();
                    $file->move($dir, $fileName);
                    
                    $rows = $this->readFile($dir . $fileName);
                    $paids = array();
                    
                    foreach ($rows as $item) {
                        $payment = SubscribePaymentQuery::create()
                                ->filterByOwerNumber($item['nosso_numero'])
                                ->filterByPaid(0)
                                ->_or()
                                ->filterByPaid(null)
                                ->findOne();
                        
                        if (null !== $payment) {
                            $payment->setPaid(true)
                                    ->setCancel(false)
                                    ->setPaymentDate(time())
                                    ->save();

                            $paids[] = $payment;
                        }
                    }
                    
                    $form = $this->createForm(new BankReturnForm(), null, array(
                        'csrf_protection' => true,
                    ));
                    
                    return array(
                        'form' => $form->createView(),
                        'paids' => $paids,
                    );

                } catch(\Exception $e) {
                    $request->getSession()->getFlashBag()->add('error', 'Ocorreu um erro ao enviar o arquivo. Tente novamente!');
                }

                return $this->redirect($this->generateUrl('admin_subscribe_payment'));
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/remessa", name="admin_subscribe_payment_send")
     * 
     * @param Request $request
     * @return array
     */
    public function sendFileAction(Request $request)
    {
        
        mb_internal_encoding('WINDOWS-1252');
        
        $stateCode = array(
            512 => 'AC',
            513 => 'AL',
            514 => 'AP',
            515 => 'AM',
            516 => 'BA',
            517 => 'CE',
            518 => 'DF',
            519 => 'ES',
            520 => 'SP',
            521 => 'GO',
            522 => 'MA',
            523 => 'MT',
            524 => 'MS',
            525 => 'MG',
            526 => 'PA',
            527 => 'PB',
            528 => 'PR',
            529 => 'PE',
            530 => 'PI',
            533 => 'RJ',
            531 => 'RN',
            532 => 'RS',
            534 => 'RO',
            535 => 'RM',
            536 => 'SC',
            537 => 'SP',
            538 => 'SE',
            539 => 'TO',
        );

        $fileNumber = 1;
        $count = 1;
        $header = array(
            0,
            1,
            "REMESSA",
            "01",
            "COBRANÇA",
            "",
            3271,
            9,
            42548,
            6,
            "",
            "JUVENTUDE EVANGELICA LUTERANA DO BRASIL - JELB",
            "756BANCOOBCED",
            date("dmy"),
            $fileNumber,
            "",
            $count
        );
        $count++;

        $payments = SubscribePaymentQuery::create('sp')
                ->filterByPaid(null)
                ->_or()
                ->filterByPaid(false)
                ->filterByOwerNumber(null, \Criteria::ISNOTNULL)
                ->filterBySent(false)
                //->filterById(array(1042,1253,1263,1268,1269,1276,1278,1281,1311,1342,1355,1425,1427,1468,1473,1477,1481,1482,1501,1506,1525,1549,1550,1597,1618,1628,1689,1691,1739,1760,1800,1806,1807,1813,1829,1832,1834))
                ->find();

        $itens = array();
        foreach ($payments as $item) {
            $subscribe = $item->getSubscribe();
            
            if (!StringUtil::isValidCpf($subscribe->getDocument())) {
                $subscribe->setDocument(null);
            }

            $itens[] = array(
                "1" => 1,
                "2" => "02",
                "3" => "10429854000141",
                "4" => 3271,
                "5" => 9,
                "6" => 41994,
                "7" => 0,
                "8" => 0,
                "9" => "",
                "10" => $item->getOwerNumber(),
                "11" => "01",
                "12" => "00",
                "13" => "",
                "14" => "",
                "15" => "",
                "16" => "000",
                "17" => 0,
                "18" => "00000",
                "19" => "0",
                "20" => "",
                "21" => "",
                "22" => 2,
                "23" => "01",
                "24" => "01",
                "25" => $item->getId(),
                "26" => $item->getDueDate()->format("dmy"),
                "27" => $this->tiraMoeda($item->getPrice()),
                "28" => 756,
                "29" => 3271,
                "30" => 9,
                "31" => "01",
                "32" => "0",
                "33" => $item->getCreateDate()->format("dmy"),
                "34" => "00",
                "35" => "00",
                "36" => 0,
                "37" => 0,
                "38" => 2,
                "39" => 0,
                "40" => 0,
                "41" => "9000000000000",
                "42" => 0,
                "43" => "01",
                "44" => preg_replace("/[^0-9]/", "", $subscribe->getDocument()),
                "45" => preg_replace('/[^A-Za-z0-9\-\ ]/', '', StringUtil::convertAccents($subscribe->getName())),
                "46" => preg_replace('/[^A-Za-z0-9\-\ \,]/', '', (StringUtil::convertAccents($subscribe->getAddress()).", ".$subscribe->getNumber())),
                "47" => preg_replace('/[^A-Za-z0-9\-\ ]/', '', StringUtil::convertAccents($subscribe->getRegion())),
                "48" => preg_replace("/[^0-9]/", "", $subscribe->getZipCode()),
                "49" => preg_replace('/[^A-Za-z0-9\- \ ]/', '', StringUtil::convertAccents($subscribe->getCity())),
                "50" => (null !== $subscribe->getState() ? $stateCode[$subscribe->getStateId()] : ""),
                "51" => "",
                "52" => "00",
                "53" => "",
                "54" => $count,
            );
            
            $item->setSent(true)
                    ->save();
            
            $count++;
        }

        $footer = array(
            9,
            "",
            "",
            "",
            "",
            "",
            "",
            $count,
        );

        $content = $this->montaLinha($header, $this->getConfigHeaderFile(), $this->getConfigTypeHeaderFile());
        foreach ($itens as $item) {
            $content .= $this->montaLinha($item, $this->getConfigItemFile(), $this->getConfigTypeItemFile());
        }
        $content .= $this->montaLinha($footer, $this->getConfigFooterFile(), $this->getConfigTypeFooterFile());

        $content = iconv("UTF-8", "WINDOWS-1252//TRANSLIT", $content);
        $path = $this->get("kernel")->getRootDir()."/logs/";
        $fileName = "remessa-".date("dmyHis").".rem";
        $filePath = $path . $fileName;
        $fp = fopen($filePath, "a+");
        fwrite($fp, $content);
        fclose($fp);

        $response = new Response($content);
        $response->headers->add(array(
            "Content-Type" => "application/x-msdownload; encoding=WINDOWS-1252",
            "Content-Disposition" => "attachment; filename=\"".$fileName."\"",
            "Expires" => "0",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Cache-Control" => "private",
        ));

        return $response;
    }
    
    /**
     * 
     * @param array $linha
     * @param array $config
     * @param array $tipo
     */
    protected function montaLinha($linha, $config, $tipo)
    {

        $novaLinha = '';
        $dadosHeader = array_values($linha);
        $dadosConfig = array_values($config);
        $dadosTipo = array_values($tipo);
        
        for ($i = 0; $i < sizeof($linha); $i++) {
            if ($dadosTipo[$i] == "num") {
                if (mb_strlen($dadosHeader[$i]) > $dadosConfig[$i]) {
                    $dadosHeader[$i] = trim(mb_substr($dadosHeader[$i], 0, $dadosConfig[$i]));
                }

                while(mb_strlen($dadosHeader[$i]) < $dadosConfig[$i]){
                    $dadosHeader[$i] = '0' . $dadosHeader[$i];
                }
            } else {
                if ($dadosHeader[$i] != "COBRANÇA") {
                    $dadosHeader[$i] = strtoupper(StringUtil::convertAccents($dadosHeader[$i]));

                    if (mb_strlen($dadosHeader[$i]) > $dadosConfig[$i]) {
                        $dadosHeader[$i] = trim(mb_substr($dadosHeader[$i], 0, $dadosConfig[$i]));
                    }

                    while(mb_strlen($dadosHeader[$i]) < $dadosConfig[$i]){
                        $dadosHeader[$i] = $dadosHeader[$i] . " ";
                    }
                }
            }

            $novaLinha = $novaLinha . $dadosHeader[$i];
        }
        
        return $novaLinha . "\r\n";
    }
    
    /**
     * 
     * @param decimal $valor
     * @return string
     */
    protected function tiraMoeda($valor)
    {

        $pontos = array(",", ".");
        $result = str_replace($pontos, "", $valor);

        return $result;
    }
    
    /**
     * 
     * @return array
     */
    protected function getConfigTypeItemFile()
    {
        
        return array (
            "1" => "num",
            "2" => "num",
            "3" => "num",
            "4" => "num",
            "5" => "num",
            "6" => "num",
            "7" => "str",
            "8" => "num",
            "9" => "str",
            "10" => "num",
            "11" => "num",
            "12" => "num",
            "13" => "str",
            "14" => "str",
            "15" => "str",
            "16" => "num",
            "17" => "num",
            "18" => "num",
            "19" => "str",
            "20" => "str",
            "21" => "str",
            "22" => "num",
            "23" => "num",
            "24" => "num",
            "25" => "str",
            "26" => "str",
            "27" => "num",
            "28" => "num",
            "29" => "num",
            "30" => "str",
            "31" => "num",
            "32" => "str",
            "33" => "num",
            "34" => "num",
            "35" => "num",
            "36" => "num",
            "37" => "num",
            "38" => "num",
            "39" => "num",
            "40" => "num",
            "41" => "num",
            "42" => "num",
            "43" => "num",
            "44" => "num",
            "45" => "str",
            "46" => "str",
            "47" => "str",
            "48" => "num",
            "49" => "str",
            "50" => "str",
            "51" => "str",
            "52" => "str",
            "53" => "str",
            "54" => "num",
        );
    }
    
    /**
     * 
     * @return array
     */
    protected function getConfigItemFile()
    {
        
        return array (
            "1" => 1,
            "2" => 2,
            "3" => 14,
            "4" => 4,
            "5" => 1,
            "6" => 8,
            "7" => 1,
            "8" => 6,
            "9" => 25,
            "10" => 12,
            "11" => 2,
            "12" => 2,
            "13" => 3,
            "14" => 1,
            "15" => 3,
            "16" => 3,
            "17" => 1,
            "18" => 5,
            "19" => 1,
            "20" => 6,
            "21" => 4,
            "22" => 1,
            "23" => 2,
            "24" => 2,
            "25" => 10,
            "26" => 6,
            "27" => 13,
            "28" => 3,
            "29" => 4,
            "30" => 1,
            "31" => 2,
            "32" => 1,
            "33" => 6,
            "34" => 2,
            "35" => 2,
            "36" => 6,
            "37" => 6,
            "38" => 1,
            "39" => 6,
            "40" => 13,
            "41" => 13,
            "42" => 13,
            "43" => 2,
            "44" => 14,
            "45" => 40,
            "46" => 37,
            "47" => 15,
            "48" => 8,
            "49" => 15,
            "50" => 2,
            "51" => 40,
            "52" => 2,
            "53" => 1,
            "54" => 6,
        );
    }
    
    /**
     * 
     * @return array
     */
    protected function getConfigTypeHeaderFile()
    {
        
        return array (
            "1" => "num",
            "2" => "num",
            "3" => "str",
            "4" => "num",
            "5" => "str",
            "6" => "str",
            "7" => "num",
            "8" => "str",
            "9" => "num",
            "10" => "str",
            "11" => "num",
            "12" => "str",
            "13" => "str",
            "14" => "num",
            "15" => "num",
            "16" => "str",
            "17" => "num",
        );
    }
    
    /**
     * 
     * @return array
     */
    protected function getConfigHeaderFile()
    {
        
        return array (
            "1" => 1,
            "2" => 1,
            "3" => 7,
            "4" => 2,
            "5" => 8,
            "6" => 7,
            "7" => 4,
            "8" => 1,
            "9" => 8,
            "10" => 1,
            "11" => 6,
            "12" => 30,
            "13" => 18,
            "14" => 6,
            "15" => 7,
            "16" => 287,
            "17" => 6,
        );
    }
    
    /**
     * 
     * @return array
     */
    protected function getConfigTypeFooterFile()
    {
        
        return array (
            "1" => "num",
            "2" => "str",
            "3" => "str",
            "4" => "str",
            "5" => "str",
            "6" => "str",
            "7" => "str",
            "8" => "num",
        );
    }
    
    /**
     * 
     * @return array
     */
    protected function getConfigFooterFile()
    {
        
        return array (
            "1" => 1,
            "2" => 193,
            "3" => 40,
            "4" => 40,
            "5" => 40,
            "6" => 40,
            "7" => 40,
            "8" => 6,
        );
    }
    
    /** 
     * Formata uma string, contendo um valor real (float) sem o separador de decimais,
     * para a sua correta representação real.
     * 
     * @param string $valor String contendo o valor na representação
     * usada nos arquivos de retorno do banco, sem o separador de decimais.
     * @param int $numCasasDecimais Total de casas decimais do número
     * representado em $valor.
     * @return float Retorna o número representado em $valor, no seu formato float,
     * contendo o separador de decimais. 
     */
    protected function formataNumero($valor, $numCasasDecimais = 2)
    {

        if ($valor == "") {
            return 0;
        }
        
        $casas = $numCasasDecimais;
        if ($casas > 0) {
            $valor = substr($valor, 0, strlen($valor) - $casas) . "." . substr($valor, strlen($valor) - $casas, $casas);
            return (float)$valor;
        }
        
        return (int) $valor;
    }
    /** 
     * Formata uma string, contendo uma data sem o separador, no formato DDMMAA,
     * para o formato DD/MM/AAAA.
     * 
     * @param string $data String contendo a data no formato DDMMAA.
     * @return string Retorna a data non formato DD/MM/AAAA. */
    protected function formataData($data)
    {

        if ($data == "") {
            return "";
        }
        
        //formata a data par ao padrão americano MM/DD/AA
        $data = substr($data, 2, 2) . "/" . substr($data, 0, 2) . "/" . substr($data, 4, 4);
        //formata a data, a partir do padrão americano, para o padrão DD/MM/AAAA
        return date("d/m/Y", strtotime($data));
    }
    
    /**
     * 
     * @param string $fileName
     * @return array
     */
    protected function readFile($fileName)
    {
        $rows = explode("\r", trim(file_get_contents($fileName)));
        $return = array();
        for ($i = 0; $i < count($rows); $i++) {
            $linha = trim($rows[$i]);
            if ($linha != "") {
                $linha = "-" . $linha;
                if ($i > 0 && $i < (count($rows) - 1)) {
                    $itemType = substr($linha, 14, 1);
                    if ($itemType == 'T') {
	                    $vetor = array();
	                    //X = ALFANUMÉRICO 9 = NUMÉRICO V = VÍRGULA DECIMAL ASSUMIDA
	                    $vetor["nosso_numero"] = trim(substr($linha, 40, 8)); //9  Nosso-Número
	                    $vetor["data_pagamento"] = date("d/m/Y");

	                    $return[] = $vetor;
	            }
                }
            }
        }

        return $return;
    }
}