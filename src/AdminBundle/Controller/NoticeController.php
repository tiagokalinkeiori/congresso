<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\NoticeForm;
use AppBundle\Model\LanguageQuery;
use AppBundle\Model\NoticeQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/noticias")
 */
class NoticeController extends Controller
{
    
    /**
     * @Route("/", name="admin_content_notice")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $requestQuery = $request->query->all();
        $returnUrl = $this->generateUrl($request->get('_route'), $requestQuery);
        $request->getSession()->getFlashBag()->set('returnUrl', $returnUrl);

        $pageId = $request->query->get('local');
        $languageId = $request->query->get('idioma');
        
        $page = $request->query->get('pagina', 1);
        $limit = 10;
        
        $languages = LanguageQuery::create()
                ->find();
        
        $notices = NoticeQuery::create()
                ->_if(!empty($languageId))
                    ->filterByLanguageId($languageId)
                ->_endif()
                ->paginate($page, $limit);
        
        return array(
            'languages' => $languages,
            'pageId' => $pageId,
            'languageId' => $languageId,
            'notices' => $notices,
        );
    }
    
    /**
     * @Route("/criar", defaults={"id" = null}, name="admin_content_notice_create")
     * @Route("/editar/{id}", defaults={"id" = "\d+"}, name="admin_content_notice_edit")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function editAction(Request $request, $id = null)
    {
        
        $notice = null;
        if (null !== $id) {
            $notice = NoticeQuery::create()->findOneById($id);
        }
        
        $form = $this->createForm(new NoticeForm(), $notice, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $notice = $form->getData();
                if (null === $id) {
                    $notice->setDate(time());
                }
                $notice->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Notícia salva com sucesso!');
                
                return $this->redirect($this->generateUrl('admin_content_notice_edit', array(
                    'id' => $notice->getId(),
                )));
            }
        }
        
        return array(
            'id' => $id,
            'notice' => $notice,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/remove/{id}", defaults={"id" = "\d+"}, name="admin_content_notice_remove")
     * 
     * @param Request $request
     * @param int $id
     */
    public function removeAction(Request $request, $id)
    {
        
        $notice = NoticeQuery::create()->findOneById($id);
        if (null !== $notice) {
            $notice->delete();
        }
        
        $flash = $request->getSession()->getFlashBag()->get('returnUrl');
        if (is_array($flash) && !empty($flash)) {
            $returnUrl = $flash[0];

            return $this->redirect($returnUrl);
        } else {        
            return $this->redirect($this->generateUrl('admin_content_notice'));
        }
    }
}