<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\TextForm;
use AppBundle\Model\LanguageQuery;
use AppBundle\Model\PageQuery;
use AppBundle\Model\TextQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/textos")
 */
class TextController extends Controller
{
    
    /**
     * @Route("/", name="admin_content_text")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $requestQuery = $request->query->all();
        $returnUrl = $this->generateUrl($request->get('_route'), $requestQuery);
        $request->getSession()->getFlashBag()->set('returnUrl', $returnUrl);

        $pageId = $request->query->get('local');
        $languageId = $request->query->get('idioma');
        
        $page = $request->query->get('pagina', 1);
        $limit = 10;
        
        $pages = PageQuery::create()
                ->orderByName()
                ->find();
        
        $languages = LanguageQuery::create()
                ->find();
        
        $texts = TextQuery::create()
                ->_if(!empty($pageId))
                    ->filterByPageId($pageId)
                ->_endif()
                ->_if(!empty($languageId))
                    ->filterByLanguageId($languageId)
                ->_endif()
                ->paginate($page, $limit);
        
        return array(
            'pages' => $pages,
            'languages' => $languages,
            'pageId' => $pageId,
            'languageId' => $languageId,
            'texts' => $texts,
        );
    }
    
    /**
     * @Route("/criar", defaults={"id" = null}, name="admin_content_text_create")
     * @Route("/editar/{id}", defaults={"id" = "\d+"}, name="admin_content_text_edit")
     * @Template()
     * 
     * @param Request $request
     * @param int|null $id
     * @return array
     */
    public function editAction(Request $request, $id = null)
    {
        
        $text = null;
        if (null !== $id) {
            $text = TextQuery::create()->findOneById($id);
        }
        
        $form = $this->createForm(new TextForm(), $text, array(
            'csrf_protection' => true,
        ));
        
        if ($request->isMethod('post')) {
            $form->submit($request);
            if ($form->isValid()) {
                $text = $form->getData();
                $text->save();
                
                $request->getSession()->getFlashBag()->add('success', 'Texto salvo com sucesso!');
                
                return $this->redirect($this->generateUrl('admin_content_text_edit', array(
                    'id' => $text->getId(),
                )));
            }
        }
        
        $dir = $this->get('kernel')->getRootDir() . "/../web/assets/images/uploaded/";
        try {
            $finder = new Finder();
            $files = $finder->sortByName()->files()->in($dir);
        } catch (\Exception $e) {
            $files = array();
        }
        
        return array(
            'id' => $id,
            'text' => $text,
            'form' => $form->createView(),
            'files' => $files,
        );
    }
    
    /**
     * @Route("/remove/{id}", defaults={"id" = "\d+"}, name="admin_content_text_remove")
     * 
     * @param Request $request
     * @param int $id
     */
    public function removeAction(Request $request, $id)
    {
        
        $text = TextQuery::create()->findOneById($id);
        if (null !== $text) {
            $text->delete();
        }
        
        $flash = $request->getSession()->getFlashBag()->get('returnUrl');
        if (is_array($flash) && !empty($flash)) {
            $returnUrl = $flash[0];

            return $this->redirect($returnUrl);
        } else {        
            return $this->redirect($this->generateUrl('admin_content_text'));
        }
    }
}