<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/log")
 */
class LogController extends Controller
{
    
    /**
     * @Route("/", name="admin_configuration_log")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        
        $generate = $expire = array();
        $generateFile = $this->get('kernel')->getRootDir().'/logs/cron_payment_generate.log';
        $expireFile = $this->get('kernel')->getRootDir().'/logs/cron_payment_expire.log';
        if (is_file($generateFile)) {
            $generateFileContent = preg_split("/\\r\\n|\\r|\\n/", file_get_contents($generateFile));
            $i = 0;
            foreach ($generateFileContent as $line) {
                $line = trim($line);
                if (!empty($line)
                        && strpos($line, "X-Powered-By") === false
                        && strpos($line, "Content-type") === false) {
                    if (strpos($line, "[START]") !== false) {
                        $i++;
                        
                        if(preg_match("/(\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}\:\d{2})/", $line, $matches)) {
                            $generate[$i] = array(
                                'lines' => array(),
                                'date' => $matches[0],
                            );
                        }
                        continue;
                    }
                    $generate[$i]['lines'][] = $line;
                }
            }
            
            $generate = array_reverse($generate);
        }
        
        if (is_file($expireFile)) {
            $expireFileContent = preg_split("/\\r\\n|\\r|\\n/", file_get_contents($expireFile));
            $i = 0;
            foreach ($expireFileContent as $line) {
                $line = trim($line);
                if (!empty($line)
                        && strpos($line, "X-Powered-By") === false
                        && strpos($line, "Content-type") === false) {
                    if (strpos($line, "[START]") !== false) {
                        $i++;
                        
                        if(preg_match("/(\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}\:\d{2})/", $line, $matches)) {
                            $expire[$i] = array(
                                'lines' => array(),
                                'date' => $date,
                            );
                        }
                        continue;
                    }

                    $expire[$i]['lines'][] = $line;
                }
            }
            
            $expire = array_reverse($expire);
        }

        return array(
            'generate' => $generate,
            'expire' => $expire,
        );
    }
}